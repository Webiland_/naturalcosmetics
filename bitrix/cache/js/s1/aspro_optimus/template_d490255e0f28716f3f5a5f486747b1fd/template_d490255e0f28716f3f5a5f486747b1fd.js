
; /* Start:"a:4:{s:4:"full";s:70:"/bitrix/templates/aspro_optimus/js/jquery.actual.min.js?14990989751251";s:6:"source";s:55:"/bitrix/templates/aspro_optimus/js/jquery.actual.min.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
/*! Copyright 2012, Ben Lin (http://dreamerslab.com/)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version: 1.0.18
 *
 * Requires: jQuery >= 1.2.3
 */
(function(a){if(typeof define==="function"&&define.amd){define(["jquery"],a);
}else{a(jQuery);}}(function(a){a.fn.addBack=a.fn.addBack||a.fn.andSelf;a.fn.extend({actual:function(b,l){if(!this[b]){throw'$.actual => The jQuery method "'+b+'" you called does not exist';
}var f={absolute:false,clone:false,includeMargin:false,display:"block"};var i=a.extend(f,l);var e=this.eq(0);var h,j;if(i.clone===true){h=function(){var m="position: absolute !important; top: -1000 !important; ";
e=e.clone().attr("style",m).appendTo("body");};j=function(){e.remove();};}else{var g=[];var d="";var c;h=function(){c=e.parents().addBack().filter(":hidden");
d+="visibility: hidden !important; display: "+i.display+" !important; ";if(i.absolute===true){d+="position: absolute !important; ";}c.each(function(){var m=a(this);
var n=m.attr("style");g.push(n);m.attr("style",n?n+";"+d:d);});};j=function(){c.each(function(m){var o=a(this);var n=g[m];if(n===undefined){o.removeAttr("style");
}else{o.attr("style",n);}});};}h();var k=/(outer)/.test(b)?e[b](i.includeMargin):e[b]();j();return k;}});}));
/* End */
;
; /* Start:"a:4:{s:4:"full";s:64:"/bitrix/templates/aspro_optimus/js/jqModal.min.js?14990989753355";s:6:"source";s:45:"/bitrix/templates/aspro_optimus/js/jqModal.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
/*
 * jqModal - Minimalist Modaling with jQuery
 *   (http://dev.iceburg.net/jquery/jqModal/)
 *
 * Copyright (c) 2007,2008 Brice Burgess <bhb@iceburg.net>
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 * 
 * $Version: 03/01/2009 +r14
 */
(function($) {
$.fn.jqm=function(o){
var p={
overlay: 50,
overlayClass: 'jqmOverlay',
closeClass: 'jqmClose',
trigger: '.jqModal',
ajax: F,
ajaxText: '',
target: F,
modal: F,
toTop: F,
onShow: F,
onHide: F,
onLoad: F
};
return this.each(function(){if(this._jqm)return H[this._jqm].c=$.extend({},H[this._jqm].c,o);s++;this._jqm=s;
H[s]={c:$.extend(p,$.jqm.params,o),a:F,w:$(this).addClass('jqmID'+s),s:s};
if(p.trigger)$(this).jqmAddTrigger(p.trigger);
});};

$.fn.jqmAddClose=function(e){return hs(this,e,'jqmHide');};
$.fn.jqmAddTrigger=function(e){return hs(this,e,'jqmShow');};
$.fn.jqmShow=function(t){return this.each(function(){t=t||window.event;$.jqm.open(this._jqm,t);});};
$.fn.jqmHide=function(t){return this.each(function(){t=t||window.event;$.jqm.close(this._jqm,t)});};

$.jqm = {
hash:{},
open:function(s,t){var h=H[s],c=h.c,cc='.'+c.closeClass,z=(parseInt(h.w.css('z-index'))),z=(z>0)?z:3000,o=$('<div></div>').css({height:'100%',width:'100%',position:'fixed',left:0,top:0,'z-index':z-1,opacity:c.overlay/100});if(h.a)return F;h.t=t;h.a=true;h.w.css('z-index',z);
 if(c.modal) {if(!A[0])L('bind');A.push(s);}
 else if(c.overlay > 0)h.w.jqmAddClose(o);
 else o=F;

 h.o=(o)?o.addClass(c.overlayClass).prependTo('body'):F;
 if(ie6){$('html,body').css({height:'100%',width:'100%'});if(o){o=o.css({position:'absolute'})[0];for(var y in {Top:1,Left:1})o.style.setExpression(y.toLowerCase(),"(_=(document.documentElement.scroll"+y+" || document.body.scroll"+y+"))+'px'");}}

 if(c.ajax) {var r=c.target||h.w,u=c.ajax,r=(typeof r == 'string')?$(r,h.w):$(r),u=(u.substr(0,1) == '@')?$(t).attr(u.substring(1)):u;
  r.html(c.ajaxText).load(u,function(){if(c.onLoad)c.onLoad.call(this,h);if(cc)h.w.jqmAddClose($(cc,h.w));e(h);});}
 else if(cc)h.w.jqmAddClose($(cc,h.w));

 if(c.toTop&&h.o)h.w.before('<span id="jqmP'+h.w[0]._jqm+'"></span>').insertAfter(h.o);	
 (c.onShow)?c.onShow(h):h.w.show();e(h);return F;
},
close:function(s){var h=H[s];if(!h.a)return F;h.a=F;
 if(A[0]){A.pop();if(!A[0])L('unbind');}
 if(h.c.toTop&&h.o)$('#jqmP'+h.w[0]._jqm).after(h.w).remove();
 if(h.c.onHide)h.c.onHide(h);else{h.w.hide();if(h.o)h.o.remove();} return F;
},
params:{}};
var s=0,H=$.jqm.hash,A=[],ie6=$.browser.msie&&($.browser.version == "6.0"),F=false,
i=$('<iframe src="javascript:false;document.write(\'\');" class="jqm"></iframe>').css({opacity:0}),
e=function(h){if(ie6)if(h.o)h.o.html('<p style="width:100%;height:100%"/>').prepend(i);else if(!$('iframe.jqm',h.w)[0])h.w.prepend(i); f(h);},
f=function(h){try{$(':input:visible',h.w)[0].focus();}catch(_){}},
L=function(t){$()[t]("keypress",m)[t]("keydown",m)[t]("mousedown",m);},
m=function(e){var h=H[A[A.length-1]],r=(!$(e.target).parents('.jqmID'+h.s)[0]);if(r)f(h);return !r;},
hs=function(w,t,c){return w.each(function(){var s=this._jqm;$(t).each(function() {
 if(!this[c]){this[c]=[];$(this).click(function(){for(var i in {jqmShow:1,jqmHide:1})for(var s in this[i])if(H[this[i][s]])H[this[i][s]].w[i](this);return F;});}this[c].push(s);});});};
})(jQuery);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:73:"/bitrix/templates/aspro_optimus/js/jquery.fancybox.min.js?149909897521528";s:6:"source";s:53:"/bitrix/templates/aspro_optimus/js/jquery.fancybox.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
!function(e,t,n,i){"use strict";var o=n(e),a=n(t),r=n.fancybox=function(){r.open.apply(this,arguments)},s=null,l=t.createTouch!==i,c=function(e){return e&&e.hasOwnProperty&&e instanceof n},p=function(e){return e&&"string"===n.type(e)},d=function(e){return p(e)&&e.indexOf("%")>0},h=function(e){return e&&!(e.style.overflow&&"hidden"===e.style.overflow)&&(e.clientWidth&&e.scrollWidth>e.clientWidth||e.clientHeight&&e.scrollHeight>e.clientHeight)},f=function(e,t){var n=parseInt(e,10);return t&&d(e)&&(n=r.getViewport()[t]/100*n),Math.ceil(n)},u=function(e,t){return f(e,t)+"px"};n.extend(r,{version:"2.1.0",defaults:{padding:15,margin:20,width:800,height:600,minWidth:100,minHeight:100,maxWidth:9999,maxHeight:9999,autoSize:!0,autoHeight:!1,autoWidth:!1,autoResize:!l,autoCenter:!l,fitToView:!0,aspectRatio:!1,topRatio:.5,leftRatio:.5,scrolling:"auto",wrapCSS:"",arrows:!0,closeBtn:!0,closeClick:!1,nextClick:!1,mouseWheel:!0,autoPlay:!1,playSpeed:3e3,preload:3,modal:!1,loop:!0,ajax:{dataType:"html",headers:{"X-fancyBox":!0}},iframe:{scrolling:"auto",preload:!0},swf:{wmode:"transparent",allowfullscreen:"true",allowscriptaccess:"always"},keys:{next:{13:"left",34:"up",39:"left",40:"up"},prev:{8:"right",33:"down",37:"right",38:"down"},close:[27],play:[32],toggle:[70]},direction:{next:"left",prev:"right"},scrollOutside:!0,index:0,type:null,href:null,content:null,title:null,tpl:{wrap:'<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',image:'<img class="fancybox-image" src="{href}" alt="" />',iframe:'<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0"'+(n.browser.msie?' allowtransparency="true"':"")+"></iframe>",error:'<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',closeBtn:'<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',next:'<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',prev:'<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'},openEffect:"fade",openSpeed:250,openEasing:"swing",openOpacity:!0,openMethod:"zoomIn",closeEffect:"fade",closeSpeed:250,closeEasing:"swing",closeOpacity:!0,closeMethod:"zoomOut",nextEffect:"elastic",nextSpeed:250,nextEasing:"swing",nextMethod:"changeIn",prevEffect:"elastic",prevSpeed:250,prevEasing:"swing",prevMethod:"changeOut",helpers:{overlay:{closeClick:!0,speedOut:200,showEarly:!0,css:{}},title:{type:"float"}},onCancel:n.noop,beforeLoad:n.noop,afterLoad:n.noop,beforeShow:n.noop,afterShow:n.noop,beforeChange:n.noop,beforeClose:n.noop,afterClose:n.noop},group:{},opts:{},previous:null,coming:null,current:null,isActive:!1,isOpen:!1,isOpened:!1,wrap:null,skin:null,outer:null,inner:null,player:{timer:null,isActive:!1},ajaxLoad:null,imgPreload:null,transitions:{},helpers:{},open:function(e,t){return e&&(n.isPlainObject(t)||(t={}),!1!==r.close(!0))?(n.isArray(e)||(e=c(e)?n(e).get():[e]),n.each(e,function(o,a){var s,l,d,h,f,u,g,m={};"object"===n.type(a)&&(a.nodeType&&(a=n(a)),c(a)?(m={href:a.attr("href"),title:a.attr("title"),isDom:!0,element:a},n.metadata&&n.extend(!0,m,a.metadata())):m=a),s=t.href||m.href||(p(a)?a:null),l=t.title!==i?t.title:m.title||"",d=t.content||m.content,h=d?"html":t.type||m.type,!h&&m.isDom&&(h=a.data("fancybox-type"),h||(f=a.prop("class").match(/fancybox\.(\w+)/),h=f?f[1]:null)),p(s)&&(h||(r.isImage(s)?h="image":r.isSWF(s)?h="swf":"#"===s.charAt(0)?h="inline":p(a)&&(h="html",d=a)),"ajax"===h&&(u=s.split(/\s+/,2),s=u.shift(),g=u.shift())),d||("inline"===h?s?d=n(p(s)?s.replace(/.*(?=#[^\s]+$)/,""):s):m.isDom&&(d=a):"html"===h?d=s:h||s||!m.isDom||(h="inline",d=a)),n.extend(m,{href:s,type:h,content:d,title:l,selector:g}),e[o]=m}),r.opts=n.extend(!0,{},r.defaults,t),t.keys!==i&&(r.opts.keys=t.keys?n.extend({},r.defaults.keys,t.keys):!1),r.group=e,r._start(r.opts.index)):void 0},cancel:function(){var e=r.coming;e&&!1!==r.trigger("onCancel")&&(r.hideLoading(),r.ajaxLoad&&r.ajaxLoad.abort(),r.ajaxLoad=null,r.imgPreload&&(r.imgPreload.onload=r.imgPreload.onerror=null),e.wrap&&e.wrap.stop(!0).trigger("onReset").remove(),r.current||r.trigger("afterClose"),r.coming=null)},close:function(e){r.cancel(),!1!==r.trigger("beforeClose")&&(r.unbindEvents(),r.isOpen&&e!==!0?(r.isOpen=r.isOpened=!1,r.isClosing=!0,n(".fancybox-item, .fancybox-nav").remove(),r.wrap.stop(!0,!0).removeClass("fancybox-opened"),"fixed"===r.wrap.css("position")&&r.wrap.css(r._getPosition(!0)),r.transitions[r.current.closeMethod]()):(n(".fancybox-wrap").stop(!0).trigger("onReset").remove(),r._afterZoomOut()))},play:function(e){var t=function(){clearTimeout(r.player.timer)},i=function(){t(),r.current&&r.player.isActive&&(r.player.timer=setTimeout(r.next,r.current.playSpeed))},o=function(){t(),n("body").unbind(".player"),r.player.isActive=!1,r.trigger("onPlayEnd")},a=function(){r.current&&(r.current.loop||r.current.index<r.group.length-1)&&(r.player.isActive=!0,n("body").bind({"afterShow.player onUpdate.player":i,"onCancel.player beforeClose.player":o,"beforeLoad.player":t}),i(),r.trigger("onPlayStart"))};e===!0||!r.player.isActive&&e!==!1?a():o()},next:function(e){var t=r.current;t&&(p(e)||(e=t.direction.next),r.jumpto(t.index+1,e,"next"))},prev:function(e){var t=r.current;t&&(p(e)||(e=t.direction.prev),r.jumpto(t.index-1,e,"prev"))},jumpto:function(e,t,n){var o=r.current;o&&(e=f(e),r.direction=t||o.direction[e>=o.index?"next":"prev"],r.router=n||"jumpto",o.loop&&(0>e&&(e=o.group.length+e%o.group.length),e%=o.group.length),o.group[e]!==i&&(r.cancel(),r._start(e)))},reposition:function(e,t){var n;r.isOpen&&(n=r._getPosition(t),e&&"scroll"===e.type?(delete n.position,r.wrap.stop(!0,!0).animate(n,200)):r.wrap.css(n))},update:function(e){var t=e&&e.type,n=!t||"orientationchange"===t;n&&(clearTimeout(s),s=null),r.isOpen&&!s&&((n||l)&&(r.wrap.removeAttr("style").addClass("fancybox-tmp"),r.trigger("onUpdate")),s=setTimeout(function(){var n=r.current;n&&(r.wrap.removeClass("fancybox-tmp"),"scroll"!==t&&r._setDimension(),"scroll"===t&&n.canShrink||r.reposition(e),r.trigger("onUpdate"),s=null)},l?500:n?20:300))},toggle:function(e){r.isOpen&&(r.current.fitToView="boolean"===n.type(e)?e:!r.current.fitToView,r.update())},hideLoading:function(){a.unbind("keypress.fb"),n("#fancybox-loading").remove()},showLoading:function(){var e,t;r.hideLoading(),a.bind("keypress.fb",function(e){27===(e.which||e.keyCode)&&(e.preventDefault(),r.cancel())}),e=n('<div id="fancybox-loading"><div></div></div>').click(r.cancel).appendTo("body"),r.defaults.fixed||(t=r.getViewport(),e.css({position:"absolute",top:.5*t.h+t.y,left:.5*t.w+t.x}))},getViewport:function(){var t=r.current?r.current.locked:!1,n={x:o.scrollLeft(),y:o.scrollTop()};return t?(n.w=t[0].clientWidth,n.h=t[0].clientHeight):(n.w=l&&e.innerWidth?e.innerWidth:o.width(),n.h=l&&e.innerHeight?e.innerHeight:o.height()),n},unbindEvents:function(){r.wrap&&c(r.wrap)&&r.wrap.unbind(".fb"),a.unbind(".fb"),o.unbind(".fb")},bindEvents:function(){var e,t=r.current;t&&(o.bind("orientationchange.fb"+(l?"":" resize.fb")+(t.autoCenter&&!t.locked?" scroll.fb":""),r.update),e=t.keys,e&&a.bind("keydown.fb",function(o){var a=o.which||o.keyCode,s=o.target||o.srcElement;o.ctrlKey||o.altKey||o.shiftKey||o.metaKey||s&&(s.type||n(s).is("[contenteditable]"))||n.each(e,function(e,s){return t.group.length>1&&s[a]!==i?(r[e](s[a]),o.preventDefault(),!1):n.inArray(a,s)>-1?(r[e](),o.preventDefault(),!1):void 0})}),n.fn.mousewheel&&t.mouseWheel&&r.wrap.bind("mousewheel.fb",function(e,i,o,a){for(var s=e.target||null,l=n(s),c=!1;l.length&&!(c||l.is(".fancybox-skin")||l.is(".fancybox-wrap"));)c=h(l[0]),l=n(l).parent();0===i||c||r.group.length>1&&!t.canShrink&&(a>0||o>0?r.prev(a>0?"down":"left"):(0>a||0>o)&&r.next(0>a?"up":"right"),e.preventDefault())}))},trigger:function(e,t){var i,o=t||r.coming||r.current;if(o){if(n.isFunction(o[e])&&(i=o[e].apply(o,Array.prototype.slice.call(arguments,1))),i===!1)return!1;"onCancel"!==e||r.isOpened||(r.isActive=!1),o.helpers&&n.each(o.helpers,function(t,i){i&&r.helpers[t]&&n.isFunction(r.helpers[t][e])&&r.helpers[t][e](i,o)}),n.event.trigger(e+".fb")}},isImage:function(e){return p(e)&&e.match(/\.(jp(e|g|eg)|gif|png|bmp|webp)((\?|#).*)?$/i)},isSWF:function(e){return p(e)&&e.match(/\.(swf)((\?|#).*)?$/i)},_start:function(e){var t,i,o,a,s,c={};if(e=f(e),t=r.group[e]||null,!t)return!1;if(c=n.extend(!0,{},r.opts,t),a=c.margin,s=c.padding,"number"===n.type(a)&&(c.margin=[a,a,a,a]),"number"===n.type(s)&&(c.padding=[s,s,s,s]),c.modal&&n.extend(!0,c,{closeBtn:!1,closeClick:!1,nextClick:!1,arrows:!1,mouseWheel:!1,keys:null,helpers:{overlay:{closeClick:!1}}}),c.autoSize&&(c.autoWidth=c.autoHeight=!0),"auto"===c.width&&(c.autoWidth=!0),"auto"===c.height&&(c.autoHeight=!0),c.group=r.group,c.index=e,r.coming=c,!1===r.trigger("beforeLoad"))return void(r.coming=null);if(o=c.type,i=c.href,!o)return r.coming=null,r.current&&r.router&&"jumpto"!==r.router?(r.current.index=e,r[r.router](r.direction)):!1;if(r.isActive=!0,("image"===o||"swf"===o)&&(c.autoHeight=c.autoWidth=!1,c.scrolling="visible"),"image"===o&&(c.aspectRatio=!0),"iframe"===o&&l&&(c.scrolling="scroll"),c.wrap=n(c.tpl.wrap).addClass("fancybox-"+(l?"mobile":"desktop")+" fancybox-type-"+o+" fancybox-tmp "+c.wrapCSS).appendTo(c.parent),n.extend(c,{skin:n(".fancybox-skin",c.wrap),outer:n(".fancybox-outer",c.wrap),inner:n(".fancybox-inner",c.wrap)}),n.each(["Top","Right","Bottom","Left"],function(e,t){c.skin.css("padding"+t,u(c.padding[e]))}),r.trigger("onReady"),"inline"===o||"html"===o){if(!c.content||!c.content.length)return r._error("content")}else if(!i)return r._error("href");"image"===o?r._loadImage():"ajax"===o?r._loadAjax():"iframe"===o?r._loadIframe():r._afterLoad()},_error:function(e){n.extend(r.coming,{type:"html",autoWidth:!0,autoHeight:!0,minWidth:0,minHeight:0,scrolling:"no",hasError:e,content:r.coming.tpl.error}),r._afterLoad()},_loadImage:function(){var e=r.imgPreload=new Image;e.onload=function(){this.onload=this.onerror=null,r.coming.width=this.width,r.coming.height=this.height,r._afterLoad()},e.onerror=function(){this.onload=this.onerror=null,r._error("image")},e.src=r.coming.href,e.complete!==i&&e.complete||r.showLoading()},_loadAjax:function(){var e=r.coming;r.showLoading(),r.ajaxLoad=n.ajax(n.extend({},e.ajax,{url:e.href,error:function(e,t){r.coming&&"abort"!==t?r._error("ajax",e):r.hideLoading()},success:function(t,n){"success"===n&&(e.content=t,r._afterLoad())}}))},_loadIframe:function(){var e=r.coming,t=n(e.tpl.iframe.replace(/\{rnd\}/g,(new Date).getTime())).attr("scrolling",l?"auto":e.iframe.scrolling).attr("src",e.href);n(e.wrap).bind("onReset",function(){try{n(this).find("iframe").hide().attr("src","//about:blank").end().empty()}catch(e){}}),e.iframe.preload&&(r.showLoading(),t.one("load",function(){n(this).data("ready",1),l||n(this).bind("load.fb",r.update),n(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show(),r._afterLoad()})),e.content=t.appendTo(e.inner),e.iframe.preload||r._afterLoad()},_preloadImages:function(){var e,t,n=r.group,i=r.current,o=n.length,a=i.preload?Math.min(i.preload,o-1):0;for(t=1;a>=t;t+=1)e=n[(i.index+t)%o],"image"===e.type&&e.href&&((new Image).src=e.href)},_afterLoad:function(){var e,t,i,o,a,s,l=r.coming,p=r.current,d="fancybox-placeholder";if(r.hideLoading(),l&&r.isActive!==!1){if(!1===r.trigger("afterLoad",l,p))return l.wrap.stop(!0).trigger("onReset").remove(),void(r.coming=null);switch(p&&(r.trigger("beforeChange",p),p.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove(),"fixed"===p.wrap.css("position")&&p.wrap.css(r._getPosition(!0))),r.unbindEvents(),e=l,t=l.content,i=l.type,o=l.scrolling,n.extend(r,{wrap:e.wrap,skin:e.skin,outer:e.outer,inner:e.inner,current:e,previous:p}),a=e.href,i){case"inline":case"ajax":case"html":e.selector?t=n("<div>").html(t).find(e.selector):c(t)&&(t.data(d)||t.data(d,n('<div class="'+d+'"></div>').insertAfter(t).hide()),t=t.show().detach(),e.wrap.bind("onReset",function(){n(this).find(t).length&&t.hide().replaceAll(t.data(d)).data(d,!1)}));break;case"image":t=e.tpl.image.replace("{href}",a);break;case"swf":t='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="'+a+'"></param>',s="",n.each(e.swf,function(e,n){t+='<param name="'+e+'" value="'+n+'"></param>',s+=" "+e+'="'+n+'"'}),t+='<embed src="'+a+'" type="application/x-shockwave-flash" width="100%" height="100%"'+s+"></embed></object>"}c(t)&&t.parent().is(e.inner)||e.inner.append(t),r.trigger("beforeShow"),e.inner.css("overflow","yes"===o?"scroll":"no"===o?"hidden":o),r._setDimension(),e.wrap.removeClass("fancybox-tmp"),e.pos=n.extend({},e.dim,r._getPosition(!0)),r.isOpen=!1,r.coming=null,r.bindEvents(),r.isOpened?p.prevMethod&&r.transitions[p.prevMethod]():n(".fancybox-wrap").not(e.wrap).stop(!0).trigger("onReset").remove(),r.transitions[r.isOpened?e.nextMethod:e.openMethod](),r._preloadImages()}},_setDimension:function(){var e,t,i,o,a,s,l,c,p,h,g,m,y,w,v,b=r.getViewport(),x=0,k=!1,C=!1,O=r.wrap,W=r.skin,_=r.inner,S=r.current,P=S.width,E=S.height,T=S.minWidth,j=S.minHeight,L=S.maxWidth,M=S.maxHeight,H=S.scrolling,R=S.scrollOutside?S.scrollbarWidth:0,A=S.margin,I=A[1]+A[3],D=A[0]+A[2];if(O.add(W).add(_).width("auto").height("auto"),e=W.outerWidth(!0)-W.width(),t=W.outerHeight(!0)-W.height(),i=I+e,o=D+t,a=d(P)?(b.w-i)*f(P)/100:P,s=d(E)?(b.h-o)*f(E)/100:E,"iframe"===S.type){if(w=S.content,S.autoHeight&&1===w.data("ready"))try{w[0].contentWindow.document.location&&(_.width(a).height(9999),v=w.contents().find("body"),R&&v.css("overflow-x","hidden"),s=v.height())}catch(V){}}else(S.autoWidth||S.autoHeight)&&(_.addClass("fancybox-tmp"),S.autoWidth||_.width(a),S.autoHeight||_.height(s),S.autoWidth&&(a=_.width()),S.autoHeight&&(s=_.height()),_.removeClass("fancybox-tmp"));if(P=f(a),E=f(s),p=a/s,T=f(d(T)?f(T,"w")-i:T),L=f(d(L)?f(L,"w")-i:L),j=f(d(j)?f(j,"h")-o:j),M=f(d(M)?f(M,"h")-o:M),l=L,c=M,m=b.w-I,y=b.h-D,S.aspectRatio?(P>L&&(P=L,E=P/p),E>M&&(E=M,P=E*p),T>P&&(P=T,E=P/p),j>E&&(E=j,P=E*p)):(P=Math.max(T,Math.min(P,L)),E=Math.max(j,Math.min(E,M))),S.fitToView)if(L=Math.min(b.w-i,L),M=Math.min(b.h-o,M),_.width(f(P)).height(f(E)),O.width(f(P+e)),h=O.width(),g=O.height(),S.aspectRatio)for(;(h>m||g>y)&&P>T&&E>j&&!(x++>19);)E=Math.max(j,Math.min(M,E-10)),P=E*p,T>P&&(P=T,E=P/p),P>L&&(P=L,E=P/p),_.width(f(P)).height(f(E)),O.width(f(P+e)),h=O.width(),g=O.height();else P=Math.max(T,Math.min(P,P-(h-m))),E=Math.max(j,Math.min(E,E-(g-y)));R&&"auto"===H&&s>E&&m>P+e+R&&(P+=R),_.width(f(P)).height(f(E)),O.width(f(P+e)),h=O.width(),g=O.height(),k=(h>m||g>y)&&P>T&&E>j,C=S.aspectRatio?l>P&&c>E&&a>P&&s>E:(l>P||c>E)&&(a>P||s>E),n.extend(S,{dim:{width:u(h),height:u(g)},origWidth:a,origHeight:s,canShrink:k,canExpand:C,wPadding:e,hPadding:t,wrapSpace:g-W.outerHeight(!0),skinSpace:W.height()-E}),!w&&S.autoHeight&&E>j&&M>E&&!C&&_.height("auto")},_getPosition:function(e){var t=r.current,n=r.getViewport(),i=t.margin,o=r.wrap.width()+i[1]+i[3],a=r.wrap.height()+i[0]+i[2],s={position:"absolute",top:i[0],left:i[3]};return t.autoCenter&&t.fixed&&!e&&a<=n.h&&o<=n.w?s.position="fixed":t.locked||(s.top+=n.y,s.left+=n.x),s.top=u(Math.max(s.top,s.top+(n.h-a)*t.topRatio)),s.left=u(Math.max(s.left,s.left+(n.w-o)*t.leftRatio)),s},_afterZoomIn:function(){var e=r.current;e&&(r.isOpen=r.isOpened=!0,r.wrap.addClass("fancybox-opened").css("overflow","visible"),r.reposition(),(e.closeClick||e.nextClick)&&r.inner.css("cursor","pointer").bind("click.fb",function(t){n(t.target).is("a")||n(t.target).parent().is("a")||r[e.closeClick?"close":"next"]()}),e.closeBtn&&n(e.tpl.closeBtn).appendTo(r.skin).bind("click.fb",r.close),e.arrows&&r.group.length>1&&((e.loop||e.index>0)&&n(e.tpl.prev).appendTo(r.outer).bind("click.fb",r.prev),(e.loop||e.index<r.group.length-1)&&n(e.tpl.next).appendTo(r.outer).bind("click.fb",r.next)),r.trigger("afterShow"),e.loop||e.index!==e.group.length-1?r.opts.autoPlay&&!r.player.isActive&&(r.opts.autoPlay=!1,r.play()):r.play(!1))},_afterZoomOut:function(){var e=r.current;n(".fancybox-wrap").stop(!0).trigger("onReset").remove(),n.extend(r,{group:{},opts:{},router:!1,current:null,isActive:!1,isOpened:!1,isOpen:!1,isClosing:!1,wrap:null,skin:null,outer:null,inner:null}),r.trigger("afterClose",e)}}),r.transitions={getOrigPosition:function(){var e=r.current,t=e.element,n=e.orig,i={},o=50,a=50,s=e.hPadding,l=e.wPadding,p=r.getViewport();return!n&&e.isDom&&t.is(":visible")&&(n=t.find("img:first"),n.length||(n=t)),c(n)?(i=n.offset(),n.is("img")&&(o=n.outerWidth(),a=n.outerHeight())):(i.top=p.y+(p.h-a)*e.topRatio,i.left=p.x+(p.w-o)*e.leftRatio),e.locked&&(i.top-=p.y,i.left-=p.x),i={top:u(i.top-s*e.topRatio),left:u(i.left-l*e.leftRatio),width:u(o+l),height:u(a+s)}},step:function(e,t){var n,i,o,a=t.prop,s=r.current,l=s.wrapSpace,c=s.skinSpace;("width"===a||"height"===a)&&(n=t.end===t.start?1:(e-t.start)/(t.end-t.start),r.isClosing&&(n=1-n),i="width"===a?s.wPadding:s.hPadding,o=e-i,r.skin[a](f("width"===a?o:o-l*n)),r.inner[a](f("width"===a?o:o-l*n-c*n)))},zoomIn:function(){var e=r.current,t=e.pos,i=e.openEffect,o="elastic"===i,a=n.extend({opacity:1},t);delete a.position,o?(t=this.getOrigPosition(),e.openOpacity&&(t.opacity=.1)):"fade"===i&&(t.opacity=.1),r.wrap.css(t).animate(a,{duration:"none"===i?0:e.openSpeed,easing:e.openEasing,step:o?this.step:null,complete:r._afterZoomIn})},zoomOut:function(){var e=r.current,t=e.closeEffect,n="elastic"===t,i={opacity:.1};n&&(i=this.getOrigPosition(),e.closeOpacity&&(i.opacity=.1)),r.wrap.animate(i,{duration:"none"===t?0:e.closeSpeed,easing:e.closeEasing,step:n?this.step:null,complete:r._afterZoomOut})},changeIn:function(){var e,t=r.current,n=t.nextEffect,i=t.pos,o={opacity:1},a=r.direction,s=200;i.opacity=.1,"elastic"===n&&(e="down"===a||"up"===a?"top":"left","down"===a||"right"===a?(i[e]=u(f(i[e])-s),o[e]="+="+s+"px"):(i[e]=u(f(i[e])+s),o[e]="-="+s+"px")),"none"===n?r._afterZoomIn():r.wrap.css(i).animate(o,{duration:t.nextSpeed,easing:t.nextEasing,complete:r._afterZoomIn})},changeOut:function(){var e=r.previous,t=e.prevEffect,i={opacity:.1},o=r.direction,a=200;"elastic"===t&&(i["down"===o||"up"===o?"top":"left"]=("up"===o||"left"===o?"-":"+")+"="+a+"px"),e.wrap.animate(i,{duration:"none"===t?0:e.prevSpeed,easing:e.prevEasing,complete:function(){n(this).trigger("onReset").remove()}})}},r.helpers.overlay={overlay:null,update:function(){var e,i="100%";this.overlay.width(i).height("100%"),n.browser.msie?(e=Math.max(t.documentElement.offsetWidth,t.body.offsetWidth),a.width()>e&&(i=a.width())):a.width()>o.width()&&(i=a.width()),this.overlay.width(i).height(a.height())},onReady:function(e,i){n(".fancybox-overlay").stop(!0,!0),this.overlay||n.extend(this,{overlay:n('<div class="fancybox-overlay"></div>').appendTo(i.parent),margin:a.height()>o.height()||"scroll"===n("body").css("overflow-y")?n("body").css("margin-right"):!1,el:n(t.all&&!t.querySelector?"html":"body")}),i.fixed&&!l&&(this.overlay.addClass("fancybox-overlay-fixed"),i.autoCenter&&(this.overlay.append(i.wrap),i.locked=this.overlay)),e.showEarly===!0&&this.beforeShow.apply(this,arguments)},beforeShow:function(e,t){var i=this.overlay.unbind(".fb").width("auto").height("auto").css(e.css);e.closeClick&&i.bind("click.fb",function(e){n(e.target).hasClass("fancybox-overlay")&&r.close()}),t.fixed&&!l?t.locked&&(this.el.addClass("fancybox-lock"),this.margin!==!1&&n("body").css("margin-right",f(this.margin)+t.scrollbarWidth)):this.update(),i.show()},onUpdate:function(e,t){(!t.fixed||l)&&this.update()},afterClose:function(e){var t=this,i=e.speedOut||0;t.overlay&&!r.isActive&&t.overlay.fadeOut(i||0,function(){n("body").css("margin-right",t.margin),t.el.removeClass("fancybox-lock"),t.overlay.remove(),t.overlay=null})}},r.helpers.title={beforeShow:function(e){var t,i,o=r.current.title,a=e.type;if(p(o)&&""!==n.trim(o)){switch(t=n('<div class="fancybox-title fancybox-title-'+a+'-wrap">'+o+"</div>"),a){case"inside":i=r.skin;break;case"outside":i=r.wrap;break;case"over":i=r.inner;break;default:i=r.skin,t.appendTo("body").width(t.width()).wrapInner('<span class="child"></span>'),r.current.margin[2]+=Math.abs(f(t.css("margin-bottom")))}"top"===e.position?t.prependTo(i):t.appendTo(i)}}},n.fn.fancybox=function(e){var t,i=n(this),o=this.selector||"",s=function(a){var s,l,c=n(this).blur(),p=t;a.ctrlKey||a.altKey||a.shiftKey||a.metaKey||c.is(".fancybox-wrap")||(s=e.groupAttr||"data-fancybox-group",l=c.attr(s),l||(s="rel",l=c.get(0)[s]),l&&""!==l&&"nofollow"!==l&&(c=o.length?n(o):i,c=c.filter("["+s+'="'+l+'"]'),p=c.index(this)),e.index=p,r.open(c,e)!==!1&&a.preventDefault())};return e=e||{},t=e.index||0,o&&e.live!==!1?a.undelegate(o,"click.fb-start").delegate(o+":not('.fancybox-item, .fancybox-nav')","click.fb-start",s):i.unbind("click.fb-start").bind("click.fb-start",s),this},a.ready(function(){n.scrollbarWidth===i&&(n.scrollbarWidth=function(){var e=n('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"),t=e.children(),i=t.innerWidth()-t.height(99).innerWidth();return e.remove(),i}),n.support.fixedPosition===i&&(n.support.fixedPosition=function(){var e=n('<div style="position:fixed;top:20px;"></div>').appendTo("body"),t=20===e[0].offsetTop||15===e[0].offsetTop;return e.remove(),t}()),n.extend(r.defaults,{scrollbarWidth:n.scrollbarWidth(),fixed:n.support.fixedPosition,parent:n("body")})})}(window,document,jQuery);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:72:"/bitrix/templates/aspro_optimus/js/jquery.history.min.js?149909897521571";s:6:"source";s:52:"/bitrix/templates/aspro_optimus/js/jquery.history.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
window.JSON||(window.JSON={}),function(){function f(a){return a<10?"0"+a:a}function quote(a){return escapable.lastIndex=0,escapable.test(a)?'"'+a.replace(escapable,function(a){var b=meta[a];return typeof b=="string"?b:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+a+'"'}function str(a,b){var c,d,e,f,g=gap,h,i=b[a];i&&typeof i=="object"&&typeof i.toJSON=="function"&&(i=i.toJSON(a)),typeof rep=="function"&&(i=rep.call(b,a,i));switch(typeof i){case"string":return quote(i);case"number":return isFinite(i)?String(i):"null";case"boolean":case"null":return String(i);case"object":if(!i)return"null";gap+=indent,h=[];if(Object.prototype.toString.apply(i)==="[object Array]"){f=i.length;for(c=0;c<f;c+=1)h[c]=str(c,i)||"null";return e=h.length===0?"[]":gap?"[\n"+gap+h.join(",\n"+gap)+"\n"+g+"]":"["+h.join(",")+"]",gap=g,e}if(rep&&typeof rep=="object"){f=rep.length;for(c=0;c<f;c+=1)d=rep[c],typeof d=="string"&&(e=str(d,i),e&&h.push(quote(d)+(gap?": ":":")+e))}else for(d in i)Object.hasOwnProperty.call(i,d)&&(e=str(d,i),e&&h.push(quote(d)+(gap?": ":":")+e));return e=h.length===0?"{}":gap?"{\n"+gap+h.join(",\n"+gap)+"\n"+g+"}":"{"+h.join(",")+"}",gap=g,e}}"use strict",typeof Date.prototype.toJSON!="function"&&(Date.prototype.toJSON=function(a){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(a){return this.valueOf()});var JSON=window.JSON,cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},rep;typeof JSON.stringify!="function"&&(JSON.stringify=function(a,b,c){var d;gap="",indent="";if(typeof c=="number")for(d=0;d<c;d+=1)indent+=" ";else typeof c=="string"&&(indent=c);rep=b;if(!b||typeof b=="function"||typeof b=="object"&&typeof b.length=="number")return str("",{"":a});throw new Error("JSON.stringify")}),typeof JSON.parse!="function"&&(JSON.parse=function(text,reviver){function walk(a,b){var c,d,e=a[b];if(e&&typeof e=="object")for(c in e)Object.hasOwnProperty.call(e,c)&&(d=walk(e,c),d!==undefined?e[c]=d:delete e[c]);return reviver.call(a,b,e)}var j;text=String(text),cx.lastIndex=0,cx.test(text)&&(text=text.replace(cx,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)}));if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return j=eval("("+text+")"),typeof reviver=="function"?walk({"":j},""):j;throw new SyntaxError("JSON.parse")})}(),function(a,b){"use strict";var c=a.History=a.History||{},d=a.jQuery;if(typeof c.Adapter!="undefined")throw new Error("History.js Adapter has already been loaded...");c.Adapter={bind:function(a,b,c){d(a).bind(b,c)},trigger:function(a,b,c){d(a).trigger(b,c)},extractEventData:function(a,c,d){var e=c&&c.originalEvent&&c.originalEvent[a]||d&&d[a]||b;return e},onDomLoad:function(a){d(a)}},typeof c.init!="undefined"&&c.init()}(window),function(a,b){"use strict";var c=a.document,d=a.setTimeout||d,e=a.clearTimeout||e,f=a.setInterval||f,g=a.History=a.History||{};if(typeof g.initHtml4!="undefined")throw new Error("History.js HTML4 Support has already been loaded...");g.initHtml4=function(){if(typeof g.initHtml4.initialized!="undefined")return!1;g.initHtml4.initialized=!0,g.enabled=!0,g.savedHashes=[],g.isLastHash=function(a){var b=g.getHashByIndex(),c;return c=a===b,c},g.saveHash=function(a){return g.isLastHash(a)?!1:(g.savedHashes.push(a),!0)},g.getHashByIndex=function(a){var b=null;return typeof a=="undefined"?b=g.savedHashes[g.savedHashes.length-1]:a<0?b=g.savedHashes[g.savedHashes.length+a]:b=g.savedHashes[a],b},g.discardedHashes={},g.discardedStates={},g.discardState=function(a,b,c){var d=g.getHashByState(a),e;return e={discardedState:a,backState:c,forwardState:b},g.discardedStates[d]=e,!0},g.discardHash=function(a,b,c){var d={discardedHash:a,backState:c,forwardState:b};return g.discardedHashes[a]=d,!0},g.discardedState=function(a){var b=g.getHashByState(a),c;return c=g.discardedStates[b]||!1,c},g.discardedHash=function(a){var b=g.discardedHashes[a]||!1;return b},g.recycleState=function(a){var b=g.getHashByState(a);return g.discardedState(a)&&delete g.discardedStates[b],!0},g.emulated.hashChange&&(g.hashChangeInit=function(){g.checkerFunction=null;var b="",d,e,h,i;return g.isInternetExplorer()?(d="historyjs-iframe",e=c.createElement("iframe"),e.setAttribute("id",d),e.style.display="none",c.body.appendChild(e),e.contentWindow.document.open(),e.contentWindow.document.close(),h="",i=!1,g.checkerFunction=function(){if(i)return!1;i=!0;var c=g.getHash()||"",d=g.unescapeHash(e.contentWindow.document.location.hash)||"";return c!==b?(b=c,d!==c&&(h=d=c,e.contentWindow.document.open(),e.contentWindow.document.close(),e.contentWindow.document.location.hash=g.escapeHash(c)),g.Adapter.trigger(a,"hashchange")):d!==h&&(h=d,g.setHash(d,!1)),i=!1,!0}):g.checkerFunction=function(){var c=g.getHash();return c!==b&&(b=c,g.Adapter.trigger(a,"hashchange")),!0},g.intervalList.push(f(g.checkerFunction,g.options.hashChangeInterval)),!0},g.Adapter.onDomLoad(g.hashChangeInit)),g.emulated.pushState&&(g.onHashChange=function(b){var d=b&&b.newURL||c.location.href,e=g.getHashByUrl(d),f=null,h=null,i=null,j;return g.isLastHash(e)?(g.busy(!1),!1):(g.doubleCheckComplete(),g.saveHash(e),e&&g.isTraditionalAnchor(e)?(g.Adapter.trigger(a,"anchorchange"),g.busy(!1),!1):(f=g.extractState(g.getFullUrl(e||c.location.href,!1),!0),g.isLastSavedState(f)?(g.busy(!1),!1):(h=g.getHashByState(f),j=g.discardedState(f),j?(g.getHashByIndex(-2)===g.getHashByState(j.forwardState)?g.back(!1):g.forward(!1),!1):(g.pushState(f.data,f.title,f.url,!1),!0))))},g.Adapter.bind(a,"hashchange",g.onHashChange),g.pushState=function(b,d,e,f){if(g.getHashByUrl(e))throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(f!==!1&&g.busy())return g.pushQueue({scope:g,callback:g.pushState,args:arguments,queue:f}),!1;g.busy(!0);var h=g.createStateObject(b,d,e),i=g.getHashByState(h),j=g.getState(!1),k=g.getHashByState(j),l=g.getHash();return g.storeState(h),g.expectedStateId=h.id,g.recycleState(h),g.setTitle(h),i===k?(g.busy(!1),!1):i!==l&&i!==g.getShortUrl(c.location.href)?(g.setHash(i,!1),!1):(g.saveState(h),g.Adapter.trigger(a,"statechange"),g.busy(!1),!0)},g.replaceState=function(a,b,c,d){if(g.getHashByUrl(c))throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(d!==!1&&g.busy())return g.pushQueue({scope:g,callback:g.replaceState,args:arguments,queue:d}),!1;g.busy(!0);var e=g.createStateObject(a,b,c),f=g.getState(!1),h=g.getStateByIndex(-2);return g.discardState(f,e,h),g.pushState(e.data,e.title,e.url,!1),!0}),g.emulated.pushState&&g.getHash()&&!g.emulated.hashChange&&g.Adapter.onDomLoad(function(){g.Adapter.trigger(a,"hashchange")})},typeof g.init!="undefined"&&g.init()}(window),function(a,b){"use strict";var c=a.console||b,d=a.document,e=a.navigator,f=a.sessionStorage||!1,g=a.setTimeout,h=a.clearTimeout,i=a.setInterval,j=a.clearInterval,k=a.JSON,l=a.alert,m=a.History=a.History||{},n=a.history;k.stringify=k.stringify||k.encode,k.parse=k.parse||k.decode;if(typeof m.init!="undefined")throw new Error("History.js Core has already been loaded...");m.init=function(){return typeof m.Adapter=="undefined"?!1:(typeof m.initCore!="undefined"&&m.initCore(),typeof m.initHtml4!="undefined"&&m.initHtml4(),!0)},m.initCore=function(){if(typeof m.initCore.initialized!="undefined")return!1;m.initCore.initialized=!0,m.options=m.options||{},m.options.hashChangeInterval=m.options.hashChangeInterval||100,m.options.safariPollInterval=m.options.safariPollInterval||500,m.options.doubleCheckInterval=m.options.doubleCheckInterval||500,m.options.storeInterval=m.options.storeInterval||1e3,m.options.busyDelay=m.options.busyDelay||250,m.options.debug=m.options.debug||!1,m.options.initialTitle=m.options.initialTitle||d.title,m.intervalList=[],m.clearAllIntervals=function(){var a,b=m.intervalList;if(typeof b!="undefined"&&b!==null){for(a=0;a<b.length;a++)j(b[a]);m.intervalList=null}},m.debug=function(){(m.options.debug||!1)&&m.log.apply(m,arguments)},m.log=function(){var a=typeof c!="undefined"&&typeof c.log!="undefined"&&typeof c.log.apply!="undefined",b=d.getElementById("log"),e,f,g,h,i;a?(h=Array.prototype.slice.call(arguments),e=h.shift(),typeof c.debug!="undefined"?c.debug.apply(c,[e,h]):c.log.apply(c,[e,h])):e="\n"+arguments[0]+"\n";for(f=1,g=arguments.length;f<g;++f){i=arguments[f];if(typeof i=="object"&&typeof k!="undefined")try{i=k.stringify(i)}catch(j){}e+="\n"+i+"\n"}return b?(b.value+=e+"\n-----\n",b.scrollTop=b.scrollHeight-b.clientHeight):a||l(e),!0},m.getInternetExplorerMajorVersion=function(){var a=m.getInternetExplorerMajorVersion.cached=typeof m.getInternetExplorerMajorVersion.cached!="undefined"?m.getInternetExplorerMajorVersion.cached:function(){var a=3,b=d.createElement("div"),c=b.getElementsByTagName("i");while((b.innerHTML="<!--[if gt IE "+ ++a+"]><i></i><![endif]-->")&&c[0]);return a>4?a:!1}();return a},m.isInternetExplorer=function(){var a=m.isInternetExplorer.cached=typeof m.isInternetExplorer.cached!="undefined"?m.isInternetExplorer.cached:Boolean(m.getInternetExplorerMajorVersion());return a},m.emulated={pushState:!Boolean(a.history&&a.history.pushState&&a.history.replaceState&&!/ Mobile\/([1-7][a-z]|(8([abcde]|f(1[0-8]))))/i.test(e.userAgent)&&!/AppleWebKit\/5([0-2]|3[0-2])/i.test(e.userAgent)),hashChange:Boolean(!("onhashchange"in a||"onhashchange"in d)||m.isInternetExplorer()&&m.getInternetExplorerMajorVersion()<8)},m.enabled=!m.emulated.pushState,m.bugs={setHash:Boolean(!m.emulated.pushState&&e.vendor==="Apple Computer, Inc."&&/AppleWebKit\/5([0-2]|3[0-3])/.test(e.userAgent)),safariPoll:Boolean(!m.emulated.pushState&&e.vendor==="Apple Computer, Inc."&&/AppleWebKit\/5([0-2]|3[0-3])/.test(e.userAgent)),ieDoubleCheck:Boolean(m.isInternetExplorer()&&m.getInternetExplorerMajorVersion()<8),hashEscape:Boolean(m.isInternetExplorer()&&m.getInternetExplorerMajorVersion()<7)},m.isEmptyObject=function(a){for(var b in a)return!1;return!0},m.cloneObject=function(a){var b,c;return a?(b=k.stringify(a),c=k.parse(b)):c={},c},m.getRootUrl=function(){var a=d.location.protocol+"//"+(d.location.hostname||d.location.host);if(d.location.port||!1)a+=":"+d.location.port;return a+="/",a},m.getBaseHref=function(){var a=d.getElementsByTagName("base"),b=null,c="";return a.length===1&&(b=a[0],c=b.href.replace(/[^\/]+$/,"")),c=c.replace(/\/+$/,""),c&&(c+="/"),c},m.getBaseUrl=function(){var a=m.getBaseHref()||m.getBasePageUrl()||m.getRootUrl();return a},m.getPageUrl=function(){var a=m.getState(!1,!1),b=(a||{}).url||d.location.href,c;return c=b.replace(/\/+$/,"").replace(/[^\/]+$/,function(a,b,c){return/\./.test(a)?a:a+"/"}),c},m.getBasePageUrl=function(){var a=d.location.href.replace(/[#\?].*/,"").replace(/[^\/]+$/,function(a,b,c){return/[^\/]$/.test(a)?"":a}).replace(/\/+$/,"")+"/";return a},m.getFullUrl=function(a,b){var c=a,d=a.substring(0,1);return b=typeof b=="undefined"?!0:b,/[a-z]+\:\/\//.test(a)||(d==="/"?c=m.getRootUrl()+a.replace(/^\/+/,""):d==="#"?c=m.getPageUrl().replace(/#.*/,"")+a:d==="?"?c=m.getPageUrl().replace(/[\?#].*/,"")+a:b?c=m.getBaseUrl()+a.replace(/^(\.\/)+/,""):c=m.getBasePageUrl()+a.replace(/^(\.\/)+/,"")),c.replace(/\#$/,"")},m.getShortUrl=function(a){var b=a,c=m.getBaseUrl(),d=m.getRootUrl();return m.emulated.pushState&&(b=b.replace(c,"")),b=b.replace(d,"/"),m.isTraditionalAnchor(b)&&(b="./"+b),b=b.replace(/^(\.\/)+/g,"./").replace(/\#$/,""),b},m.store={},m.idToState=m.idToState||{},m.stateToId=m.stateToId||{},m.urlToId=m.urlToId||{},m.storedStates=m.storedStates||[],m.savedStates=m.savedStates||[],m.normalizeStore=function(){m.store.idToState=m.store.idToState||{},m.store.urlToId=m.store.urlToId||{},m.store.stateToId=m.store.stateToId||{}},m.getState=function(a,b){typeof a=="undefined"&&(a=!0),typeof b=="undefined"&&(b=!0);var c=m.getLastSavedState();return!c&&b&&(c=m.createStateObject()),a&&(c=m.cloneObject(c),c.url=c.cleanUrl||c.url),c},m.getIdByState=function(a){var b=m.extractId(a.url),c;if(!b){c=m.getStateString(a);if(typeof m.stateToId[c]!="undefined")b=m.stateToId[c];else if(typeof m.store.stateToId[c]!="undefined")b=m.store.stateToId[c];else{for(;;){b=(new Date).getTime()+String(Math.random()).replace(/\D/g,"");if(typeof m.idToState[b]=="undefined"&&typeof m.store.idToState[b]=="undefined")break}m.stateToId[c]=b,m.idToState[b]=a}}return b},m.normalizeState=function(a){var b,c;if(!a||typeof a!="object")a={};if(typeof a.normalized!="undefined")return a;if(!a.data||typeof a.data!="object")a.data={};b={},b.normalized=!0,b.title=a.title||"",b.url=m.getFullUrl(m.unescapeString(a.url||d.location.href)),b.hash=m.getShortUrl(b.url),b.data=m.cloneObject(a.data),b.id=m.getIdByState(b),b.cleanUrl=b.url.replace(/\??\&_suid.*/,""),b.url=b.cleanUrl,c=!m.isEmptyObject(b.data);if(b.title||c)b.hash=m.getShortUrl(b.url).replace(/\??\&_suid.*/,""),/\?/.test(b.hash)||(b.hash+="?"),b.hash+="&_suid="+b.id;return b.hashedUrl=m.getFullUrl(b.hash),(m.emulated.pushState||m.bugs.safariPoll)&&m.hasUrlDuplicate(b)&&(b.url=b.hashedUrl),b},m.createStateObject=function(a,b,c){var d={data:a,title:b,url:c};return d=m.normalizeState(d),d},m.getStateById=function(a){a=String(a);var c=m.idToState[a]||m.store.idToState[a]||b;return c},m.getStateString=function(a){var b,c,d;return b=m.normalizeState(a),c={data:b.data,title:a.title,url:a.url},d=k.stringify(c),d},m.getStateId=function(a){var b,c;return b=m.normalizeState(a),c=b.id,c},m.getHashByState=function(a){var b,c;return b=m.normalizeState(a),c=b.hash,c},m.extractId=function(a){var b,c,d;return c=/(.*)\&_suid=([0-9]+)$/.exec(a),d=c?c[1]||a:a,b=c?String(c[2]||""):"",b||!1},m.isTraditionalAnchor=function(a){var b=!/[\/\?\.]/.test(a);return b},m.extractState=function(a,b){var c=null,d,e;return b=b||!1,d=m.extractId(a),d&&(c=m.getStateById(d)),c||(e=m.getFullUrl(a),d=m.getIdByUrl(e)||!1,d&&(c=m.getStateById(d)),!c&&b&&!m.isTraditionalAnchor(a)&&(c=m.createStateObject(null,null,e))),c},m.getIdByUrl=function(a){var c=m.urlToId[a]||m.store.urlToId[a]||b;return c},m.getLastSavedState=function(){return m.savedStates[m.savedStates.length-1]||b},m.getLastStoredState=function(){return m.storedStates[m.storedStates.length-1]||b},m.hasUrlDuplicate=function(a){var b=!1,c;return c=m.extractState(a.url),b=c&&c.id!==a.id,b},m.storeState=function(a){return m.urlToId[a.url]=a.id,m.storedStates.push(m.cloneObject(a)),a},m.isLastSavedState=function(a){var b=!1,c,d,e;return m.savedStates.length&&(c=a.id,d=m.getLastSavedState(),e=d.id,b=c===e),b},m.saveState=function(a){return m.isLastSavedState(a)?!1:(m.savedStates.push(m.cloneObject(a)),!0)},m.getStateByIndex=function(a){var b=null;return typeof a=="undefined"?b=m.savedStates[m.savedStates.length-1]:a<0?b=m.savedStates[m.savedStates.length+a]:b=m.savedStates[a],b},m.getHash=function(){var a=m.unescapeHash(d.location.hash);return a},m.unescapeString=function(b){var c=b,d;for(;;){d=a.unescape(c);if(d===c)break;c=d}return c},m.unescapeHash=function(a){var b=m.normalizeHash(a);return b=m.unescapeString(b),b},m.normalizeHash=function(a){var b=a.replace(/[^#]*#/,"").replace(/#.*/,"");return b},m.setHash=function(a,b){var c,e,f;return b!==!1&&m.busy()?(m.pushQueue({scope:m,callback:m.setHash,args:arguments,queue:b}),!1):(c=m.escapeHash(a),m.busy(!0),e=m.extractState(a,!0),e&&!m.emulated.pushState?m.pushState(e.data,e.title,e.url,!1):d.location.hash!==c&&(m.bugs.setHash?(f=m.getPageUrl(),m.pushState(null,null,f+"#"+c,!1)):d.location.hash=c),m)},m.escapeHash=function(b){var c=m.normalizeHash(b);return c=a.escape(c),m.bugs.hashEscape||(c=c.replace(/\%21/g,"!").replace(/\%26/g,"&").replace(/\%3D/g,"=").replace(/\%3F/g,"?")),c},m.getHashByUrl=function(a){var b=String(a).replace(/([^#]*)#?([^#]*)#?(.*)/,"$2");return b=m.unescapeHash(b),b},m.setTitle=function(a){var b=a.title,c;b||(c=m.getStateByIndex(0),c&&c.url===a.url&&(b=c.title||m.options.initialTitle));try{d.getElementsByTagName("title")[0].innerHTML=b.replace("<","&lt;").replace(">","&gt;").replace(" & "," &amp; ")}catch(e){}return d.title=b,m},m.queues=[],m.busy=function(a){typeof a!="undefined"?m.busy.flag=a:typeof m.busy.flag=="undefined"&&(m.busy.flag=!1);if(!m.busy.flag){h(m.busy.timeout);var b=function(){var a,c,d;if(m.busy.flag)return;for(a=m.queues.length-1;a>=0;--a){c=m.queues[a];if(c.length===0)continue;d=c.shift(),m.fireQueueItem(d),m.busy.timeout=g(b,m.options.busyDelay)}};m.busy.timeout=g(b,m.options.busyDelay)}return m.busy.flag},m.busy.flag=!1,m.fireQueueItem=function(a){return a.callback.apply(a.scope||m,a.args||[])},m.pushQueue=function(a){return m.queues[a.queue||0]=m.queues[a.queue||0]||[],m.queues[a.queue||0].push(a),m},m.queue=function(a,b){return typeof a=="function"&&(a={callback:a}),typeof b!="undefined"&&(a.queue=b),m.busy()?m.pushQueue(a):m.fireQueueItem(a),m},m.clearQueue=function(){return m.busy.flag=!1,m.queues=[],m},m.stateChanged=!1,m.doubleChecker=!1,m.doubleCheckComplete=function(){return m.stateChanged=!0,m.doubleCheckClear(),m},m.doubleCheckClear=function(){return m.doubleChecker&&(h(m.doubleChecker),m.doubleChecker=!1),m},m.doubleCheck=function(a){return m.stateChanged=!1,m.doubleCheckClear(),m.bugs.ieDoubleCheck&&(m.doubleChecker=g(function(){return m.doubleCheckClear(),m.stateChanged||a(),!0},m.options.doubleCheckInterval)),m},m.safariStatePoll=function(){var b=m.extractState(d.location.href),c;if(!m.isLastSavedState(b))c=b;else return;return c||(c=m.createStateObject()),m.Adapter.trigger(a,"popstate"),m},m.back=function(a){return a!==!1&&m.busy()?(m.pushQueue({scope:m,callback:m.back,args:arguments,queue:a}),!1):(m.busy(!0),m.doubleCheck(function(){m.back(!1)}),n.go(-1),!0)},m.forward=function(a){return a!==!1&&m.busy()?(m.pushQueue({scope:m,callback:m.forward,args:arguments,queue:a}),!1):(m.busy(!0),m.doubleCheck(function(){m.forward(!1)}),n.go(1),!0)},m.go=function(a,b){var c;if(a>0)for(c=1;c<=a;++c)m.forward(b);else{if(!(a<0))throw new Error("History.go: History.go requires a positive or negative integer passed.");for(c=-1;c>=a;--c)m.back(b)}return m};if(m.emulated.pushState){var o=function(){};m.pushState=m.pushState||o,m.replaceState=m.replaceState||o}else m.onPopState=function(b,c){var e=!1,f=!1,g,h;return m.doubleCheckComplete(),g=m.getHash(),g?(h=m.extractState(g||d.location.href,!0),h?m.replaceState(h.data,h.title,h.url,!1):(m.Adapter.trigger(a,"anchorchange"),m.busy(!1)),m.expectedStateId=!1,!1):(e=m.Adapter.extractEventData("state",b,c)||!1,e?f=m.getStateById(e):m.expectedStateId?f=m.getStateById(m.expectedStateId):f=m.extractState(d.location.href),f||(f=m.createStateObject(null,null,d.location.href)),m.expectedStateId=!1,m.isLastSavedState(f)?(m.busy(!1),!1):(m.storeState(f),m.saveState(f),m.setTitle(f),m.Adapter.trigger(a,"statechange"),m.busy(!1),!0))},m.Adapter.bind(a,"popstate",m.onPopState),m.pushState=function(b,c,d,e){if(m.getHashByUrl(d)&&m.emulated.pushState)throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(e!==!1&&m.busy())return m.pushQueue({scope:m,callback:m.pushState,args:arguments,queue:e}),!1;m.busy(!0);var f=m.createStateObject(b,c,d);return m.isLastSavedState(f)?m.busy(!1):(m.storeState(f),m.expectedStateId=f.id,n.pushState(f.id,f.title,f.url),m.Adapter.trigger(a,"popstate")),!0},m.replaceState=function(b,c,d,e){if(m.getHashByUrl(d)&&m.emulated.pushState)throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");if(e!==!1&&m.busy())return m.pushQueue({scope:m,callback:m.replaceState,args:arguments,queue:e}),!1;m.busy(!0);var f=m.createStateObject(b,c,d);return m.isLastSavedState(f)?m.busy(!1):(m.storeState(f),m.expectedStateId=f.id,n.replaceState(f.id,f.title,f.url),m.Adapter.trigger(a,"popstate")),!0};if(f){try{m.store=k.parse(f.getItem("History.store"))||{}}catch(p){m.store={}}m.normalizeStore()}else m.store={},m.normalizeStore();m.Adapter.bind(a,"beforeunload",m.clearAllIntervals),m.Adapter.bind(a,"unload",m.clearAllIntervals),m.saveState(m.storeState(m.extractState(d.location.href,!0))),f&&(m.onUnload=function(){var a,b;try{a=k.parse(f.getItem("History.store"))||{}}catch(c){a={}}a.idToState=a.idToState||{},a.urlToId=a.urlToId||{},a.stateToId=a.stateToId||{};for(b in m.idToState){if(!m.idToState.hasOwnProperty(b))continue;a.idToState[b]=m.idToState[b]}for(b in m.urlToId){if(!m.urlToId.hasOwnProperty(b))continue;a.urlToId[b]=m.urlToId[b]}for(b in m.stateToId){if(!m.stateToId.hasOwnProperty(b))continue;a.stateToId[b]=m.stateToId[b]}m.store=a,m.normalizeStore(),f.setItem("History.store",k.stringify(a))},m.intervalList.push(i(m.onUnload,m.options.storeInterval)),m.Adapter.bind(a,"beforeunload",m.onUnload),m.Adapter.bind(a,"unload",m.onUnload));if(!m.emulated.pushState){m.bugs.safariPoll&&m.intervalList.push(i(m.safariStatePoll,m.options.safariPollInterval));if(e.vendor==="Apple Computer, Inc."||(e.appCodeName||"")==="Mozilla")m.Adapter.bind(a,"hashchange",function(){m.Adapter.trigger(a,"popstate")}),m.getHash()&&m.Adapter.onDomLoad(function(){m.Adapter.trigger(a,"hashchange")})}},m.init()}(window)
/* End */
;
; /* Start:"a:4:{s:4:"full";s:75:"/bitrix/templates/aspro_optimus/js/jquery.flexslider.min.js?149909897522345";s:6:"source";s:55:"/bitrix/templates/aspro_optimus/js/jquery.flexslider.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
!function(e){e.flexslider=function(t,a){var n=e(t);n.vars=e.extend({},e.flexslider.defaults,a);var i,s=n.vars.namespace,r=window.navigator&&window.navigator.msPointerEnabled&&window.MSGesture,o=("ontouchstart"in window||r||window.DocumentTouch&&document instanceof DocumentTouch)&&n.vars.touch,l="click touchend MSPointerUp keyup",c="",d="vertical"===n.vars.direction,u=n.vars.reverse,v=n.vars.itemWidth>0,p="fade"===n.vars.animation,m=""!==n.vars.asNavFor,f={},g=!0;e.data(t,"flexslider",n),f={init:function(){n.animating=!1,n.currentSlide=parseInt(n.vars.startAt?n.vars.startAt:0,10),isNaN(n.currentSlide)&&(n.currentSlide=0),n.animatingTo=n.currentSlide,n.atEnd=0===n.currentSlide||n.currentSlide===n.last,n.containerSelector=n.vars.selector.substr(0,n.vars.selector.search(" ")),n.slides=e(n.vars.selector,n),n.container=e(n.containerSelector,n),n.count=n.slides.length,n.syncExists=e(n.vars.sync).length>0,"slide"===n.vars.animation&&(n.vars.animation="swing"),n.prop=d?"top":"marginLeft",n.args={},n.manualPause=!1,n.stopped=!1,n.started=!1,n.startTimeout=null,n.transitions=!n.vars.video&&!p&&n.vars.useCSS&&function(){var e=document.createElement("div"),t=["perspectiveProperty","WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var a in t)if(void 0!==e.style[t[a]])return n.pfx=t[a].replace("Perspective","").toLowerCase(),n.prop="-"+n.pfx+"-transform",!0;return!1}(),n.ensureAnimationEnd="",""!==n.vars.controlsContainer&&(n.controlsContainer=e(n.vars.controlsContainer).length>0&&e(n.vars.controlsContainer)),""!==n.vars.manualControls&&(n.manualControls=e(n.vars.manualControls).length>0&&e(n.vars.manualControls)),""!==n.vars.customDirectionNav&&(n.customDirectionNav=2===e(n.vars.customDirectionNav).length&&e(n.vars.customDirectionNav)),n.vars.randomize&&(n.slides.sort(function(){return Math.round(Math.random())-.5}),n.container.empty().append(n.slides)),n.doMath(),n.setup("init"),n.vars.controlNav&&f.controlNav.setup(),n.vars.directionNav&&f.directionNav.setup(),n.vars.keyboard&&(1===e(n.containerSelector).length||n.vars.multipleKeyboard)&&e(document).bind("keyup",function(e){var t=e.keyCode;if(!n.animating&&(39===t||37===t)){var a=39===t?n.getTarget("next"):37===t?n.getTarget("prev"):!1;n.flexAnimate(a,n.vars.pauseOnAction)}}),n.vars.mousewheel&&n.bind("mousewheel",function(e,t,a,i){e.preventDefault();var s=0>t?n.getTarget("next"):n.getTarget("prev");n.flexAnimate(s,n.vars.pauseOnAction)}),n.vars.pausePlay&&f.pausePlay.setup(),n.vars.slideshow&&n.vars.pauseInvisible&&f.pauseInvisible.init(),n.vars.slideshow&&(n.vars.pauseOnHover&&n.hover(function(){n.manualPlay||n.manualPause||n.pause()},function(){n.manualPause||n.manualPlay||n.stopped||n.play()}),n.vars.pauseInvisible&&f.pauseInvisible.isHidden()||(n.vars.initDelay>0?n.startTimeout=setTimeout(n.play,n.vars.initDelay):n.play())),m&&f.asNav.setup(),o&&n.vars.touch&&f.touch(),(!p||p&&n.vars.smoothHeight)&&e(window).bind("resize orientationchange focus",f.resize),n.find("img").attr("draggable","false"),setTimeout(function(){n.vars.start(n)},200)},asNav:{setup:function(){n.asNav=!0,n.animatingTo=Math.floor(n.currentSlide/n.move),n.currentItem=n.currentSlide,n.slides.removeClass(s+"active-slide").eq(n.currentItem).addClass(s+"active-slide"),r?(t._slider=n,n.slides.each(function(){var t=this;t._gesture=new MSGesture,t._gesture.target=t,t.addEventListener("MSPointerDown",function(e){e.preventDefault(),e.currentTarget._gesture&&e.currentTarget._gesture.addPointer(e.pointerId)},!1),t.addEventListener("MSGestureTap",function(t){t.preventDefault();var a=e(this),i=a.index();e(n.vars.asNavFor).data("flexslider").animating||a.hasClass("active")||(n.direction=n.currentItem<i?"next":"prev",n.flexAnimate(i,n.vars.pauseOnAction,!1,!0,!0))})})):n.slides.on(l,function(t){t.preventDefault();var a=e(this),i=a.index(),r=a.offset().left-e(n).scrollLeft();0>=r&&a.hasClass(s+"active-slide")?n.flexAnimate(n.getTarget("prev"),!0):e(n.vars.asNavFor).data("flexslider").animating||a.hasClass(s+"active-slide")||(n.direction=n.currentItem<i?"next":"prev",n.flexAnimate(i,n.vars.pauseOnAction,!1,!0,!0))})}},controlNav:{setup:function(){n.manualControls?f.controlNav.setupManual():f.controlNav.setupPaging()},setupPaging:function(){var t,a,i="thumbnails"===n.vars.controlNav?"control-thumbs":"control-paging",r=1;if(n.controlNavScaffold=e('<ol class="'+s+"control-nav "+s+i+'"></ol>'),n.pagingCount>1)for(var o=0;o<n.pagingCount;o++){if(a=n.slides.eq(o),t="thumbnails"===n.vars.controlNav?'<img src="'+a.attr("data-thumb")+'"/>':"<a>"+r+"</a>","thumbnails"===n.vars.controlNav&&!0===n.vars.thumbCaptions){var d=a.attr("data-thumbcaption");""!==d&&void 0!==d&&(t+='<span class="'+s+'caption">'+d+"</span>")}n.controlNavScaffold.append("<li>"+t+"</li>"),r++}n.controlsContainer?e(n.controlsContainer).append(n.controlNavScaffold):n.append(n.controlNavScaffold),f.controlNav.set(),f.controlNav.active(),n.controlNavScaffold.delegate("a, img",l,function(t){if(t.preventDefault(),""===c||c===t.type){var a=e(this),i=n.controlNav.index(a);a.hasClass(s+"active")||(n.direction=i>n.currentSlide?"next":"prev",n.flexAnimate(i,n.vars.pauseOnAction))}""===c&&(c=t.type),f.setToClearWatchedEvent()})},setupManual:function(){n.controlNav=n.manualControls,f.controlNav.active(),n.controlNav.bind(l,function(t){if(t.preventDefault(),""===c||c===t.type){var a=e(this),i=n.controlNav.index(a);a.hasClass(s+"active")||(i>n.currentSlide?n.direction="next":n.direction="prev",n.flexAnimate(i,n.vars.pauseOnAction))}""===c&&(c=t.type),f.setToClearWatchedEvent()})},set:function(){var t="thumbnails"===n.vars.controlNav?"img":"a";n.controlNav=e("."+s+"control-nav li "+t,n.controlsContainer?n.controlsContainer:n)},active:function(){n.controlNav.removeClass(s+"active").eq(n.animatingTo).addClass(s+"active")},update:function(t,a){n.pagingCount>1&&"add"===t?n.controlNavScaffold.append(e("<li><a>"+n.count+"</a></li>")):1===n.pagingCount?n.controlNavScaffold.find("li").remove():n.controlNav.eq(a).closest("li").remove(),f.controlNav.set(),n.pagingCount>1&&n.pagingCount!==n.controlNav.length?n.update(a,t):f.controlNav.active()}},directionNav:{setup:function(){var t=e('<ul class="'+s+'direction-nav"><li class="'+s+'nav-prev"><a class="'+s+'prev" href="#">'+n.vars.prevText+'</a></li><li class="'+s+'nav-next"><a class="'+s+'next" href="#">'+n.vars.nextText+"</a></li></ul>");n.customDirectionNav?n.directionNav=n.customDirectionNav:n.controlsContainer?(e(n.controlsContainer).append(t),n.directionNav=e("."+s+"direction-nav li a",n.controlsContainer)):(n.append(t),n.directionNav=e("."+s+"direction-nav li a",n)),f.directionNav.update(),n.directionNav.bind(l,function(t){t.preventDefault();var a;(""===c||c===t.type)&&(a=e(this).hasClass(s+"next")?n.getTarget("next"):n.getTarget("prev"),n.flexAnimate(a,n.vars.pauseOnAction)),""===c&&(c=t.type),f.setToClearWatchedEvent()})},update:function(){var e=s+"disabled";1===n.pagingCount?(n.directionNav.addClass(e).attr("tabindex","-1"),n.directionNav.parent().addClass(e)):n.vars.animationLoop?(n.directionNav.removeClass(e).removeAttr("tabindex"),n.directionNav.parent().removeClass(e)):0===n.animatingTo?(n.directionNav.removeClass(e).filter("."+s+"prev").addClass(e).attr("tabindex","-1"),n.directionNav.parent().removeClass(e),n.directionNav.filter("."+s+"prev").parent().addClass(e)):n.animatingTo===n.last?(n.directionNav.removeClass(e).filter("."+s+"next").addClass(e).attr("tabindex","-1"),n.directionNav.parent().removeClass(e),n.directionNav.filter("."+s+"next").parent().addClass(e)):(n.directionNav.removeClass(e).removeAttr("tabindex"),n.directionNav.parent().removeClass(e))}},pausePlay:{setup:function(){var t=e('<div class="'+s+'pauseplay"><a></a></div>');n.controlsContainer?(n.controlsContainer.append(t),n.pausePlay=e("."+s+"pauseplay a",n.controlsContainer)):(n.append(t),n.pausePlay=e("."+s+"pauseplay a",n)),f.pausePlay.update(n.vars.slideshow?s+"pause":s+"play"),n.pausePlay.bind(l,function(t){t.preventDefault(),(""===c||c===t.type)&&(e(this).hasClass(s+"pause")?(n.manualPause=!0,n.manualPlay=!1,n.pause()):(n.manualPause=!1,n.manualPlay=!0,n.play())),""===c&&(c=t.type),f.setToClearWatchedEvent()})},update:function(e){"play"===e?n.pausePlay.removeClass(s+"pause").addClass(s+"play").html(n.vars.playText):n.pausePlay.removeClass(s+"play").addClass(s+"pause").html(n.vars.pauseText)}},touch:function(){function e(e){e.stopPropagation(),n.animating?e.preventDefault():(n.pause(),t._gesture.addPointer(e.pointerId),C=0,c=d?n.h:n.w,f=Number(new Date),l=v&&u&&n.animatingTo===n.last?0:v&&u?n.limit-(n.itemW+n.vars.itemMargin)*n.move*n.animatingTo:v&&n.currentSlide===n.last?n.limit:v?(n.itemW+n.vars.itemMargin)*n.move*n.currentSlide:u?(n.last-n.currentSlide+n.cloneOffset)*c:(n.currentSlide+n.cloneOffset)*c)}function a(e){e.stopPropagation();var a=e.target._slider;if(a){var n=-e.translationX,i=-e.translationY;return C+=d?i:n,m=C,y=d?Math.abs(C)<Math.abs(-n):Math.abs(C)<Math.abs(-i),e.detail===e.MSGESTURE_FLAG_INERTIA?void setImmediate(function(){t._gesture.stop()}):void((!y||Number(new Date)-f>500)&&(e.preventDefault(),!p&&a.transitions&&(a.vars.animationLoop||(m=C/(0===a.currentSlide&&0>C||a.currentSlide===a.last&&C>0?Math.abs(C)/c+2:1)),a.setProps(l+m,"setTouch"))))}}function i(e){e.stopPropagation();var t=e.target._slider;if(t){if(t.animatingTo===t.currentSlide&&!y&&null!==m){var a=u?-m:m,n=a>0?t.getTarget("next"):t.getTarget("prev");t.canAdvance(n)&&(Number(new Date)-f<550&&Math.abs(a)>50||Math.abs(a)>c/2)?t.flexAnimate(n,t.vars.pauseOnAction):p||t.flexAnimate(t.currentSlide,t.vars.pauseOnAction,!0)}s=null,o=null,m=null,l=null,C=0}}var s,o,l,c,m,f,g,h,S,y=!1,x=0,b=0,C=0;r?(t.style.msTouchAction="none",t._gesture=new MSGesture,t._gesture.target=t,t.addEventListener("MSPointerDown",e,!1),t._slider=n,t.addEventListener("MSGestureChange",a,!1),t.addEventListener("MSGestureEnd",i,!1)):(g=function(e){n.animating?e.preventDefault():(window.navigator.msPointerEnabled||1===e.touches.length)&&(n.pause(),c=d?n.h:n.w,f=Number(new Date),x=e.touches[0].pageX,b=e.touches[0].pageY,l=v&&u&&n.animatingTo===n.last?0:v&&u?n.limit-(n.itemW+n.vars.itemMargin)*n.move*n.animatingTo:v&&n.currentSlide===n.last?n.limit:v?(n.itemW+n.vars.itemMargin)*n.move*n.currentSlide:u?(n.last-n.currentSlide+n.cloneOffset)*c:(n.currentSlide+n.cloneOffset)*c,s=d?b:x,o=d?x:b,t.addEventListener("touchmove",h,!1),t.addEventListener("touchend",S,!1))},h=function(e){x=e.touches[0].pageX,b=e.touches[0].pageY,m=d?s-b:s-x,y=d?Math.abs(m)<Math.abs(x-o):Math.abs(m)<Math.abs(b-o);var t=500;(!y||Number(new Date)-f>t)&&(e.preventDefault(),!p&&n.transitions&&(n.vars.animationLoop||(m/=0===n.currentSlide&&0>m||n.currentSlide===n.last&&m>0?Math.abs(m)/c+2:1),n.setProps(l+m,"setTouch")))},S=function(e){if(t.removeEventListener("touchmove",h,!1),n.animatingTo===n.currentSlide&&!y&&null!==m){var a=u?-m:m,i=a>0?n.getTarget("next"):n.getTarget("prev");n.canAdvance(i)&&(Number(new Date)-f<550&&Math.abs(a)>50||Math.abs(a)>c/2)?n.flexAnimate(i,n.vars.pauseOnAction):p||n.flexAnimate(n.currentSlide,n.vars.pauseOnAction,!0)}t.removeEventListener("touchend",S,!1),s=null,o=null,m=null,l=null},t.addEventListener("touchstart",g,!1))},resize:function(){!n.animating&&n.is(":visible")&&(v||n.doMath(),p?f.smoothHeight():v?(n.slides.width(n.computedW),n.update(n.pagingCount),n.setProps()):d?(n.viewport.height(n.h),n.setProps(n.h,"setTotal")):(n.vars.smoothHeight&&f.smoothHeight(),n.newSlides.width(n.computedW),n.setProps(n.computedW,"setTotal")))},smoothHeight:function(e){if(!d||p){var t=p?n:n.viewport;e?t.animate({height:n.slides.eq(n.animatingTo).height()},e):t.height(n.slides.eq(n.animatingTo).height())}},sync:function(t){var a=e(n.vars.sync).data("flexslider"),i=n.animatingTo;switch(t){case"animate":a.flexAnimate(i,n.vars.pauseOnAction,!1,!0);break;case"play":a.playing||a.asNav||a.play();break;case"pause":a.pause()}},uniqueID:function(t){return t.filter("[id]").add(t.find("[id]")).each(function(){var t=e(this);t.attr("id",t.attr("id")+"_clone")}),t},pauseInvisible:{visProp:null,init:function(){var e=f.pauseInvisible.getHiddenProp();if(e){var t=e.replace(/[H|h]idden/,"")+"visibilitychange";document.addEventListener(t,function(){f.pauseInvisible.isHidden()?n.startTimeout?clearTimeout(n.startTimeout):n.pause():n.started?n.play():n.vars.initDelay>0?setTimeout(n.play,n.vars.initDelay):n.play()})}},isHidden:function(){var e=f.pauseInvisible.getHiddenProp();return e?document[e]:!1},getHiddenProp:function(){var e=["webkit","moz","ms","o"];if("hidden"in document)return"hidden";for(var t=0;t<e.length;t++)if(e[t]+"Hidden"in document)return e[t]+"Hidden";return null}},setToClearWatchedEvent:function(){clearTimeout(i),i=setTimeout(function(){c=""},3e3)}},n.flexAnimate=function(t,a,i,r,l){if(n.vars.animationLoop||t===n.currentSlide||(n.direction=t>n.currentSlide?"next":"prev"),m&&1===n.pagingCount&&(n.direction=n.currentItem<t?"next":"prev"),!n.animating&&(n.canAdvance(t,l)||i)&&n.is(":visible")){if(m&&r){var c=e(n.vars.asNavFor).data("flexslider");if(n.atEnd=0===t||t===n.count-1,c.flexAnimate(t,!0,!1,!0,l),n.direction=n.currentItem<t?"next":"prev",c.direction=n.direction,Math.ceil((t+1)/n.visible)-1===n.currentSlide||0===t)return n.currentItem=t,n.slides.removeClass(s+"active-slide").eq(t).addClass(s+"active-slide"),!1;n.currentItem=t,n.slides.removeClass(s+"active-slide").eq(t).addClass(s+"active-slide"),t=Math.floor(t/n.visible)}if(n.animating=!0,n.animatingTo=t,a&&n.pause(),n.vars.before(n),n.syncExists&&!l&&f.sync("animate"),n.vars.controlNav&&f.controlNav.active(),v||n.slides.removeClass(s+"active-slide").eq(t).addClass(s+"active-slide"),n.atEnd=0===t||t===n.last,n.vars.directionNav&&f.directionNav.update(),t===n.last&&(n.vars.end(n),n.vars.animationLoop||n.pause()),p)o?(n.slides.eq(n.currentSlide).css({opacity:0,zIndex:1}),n.slides.eq(t).css({opacity:1,zIndex:2}),n.wrapup(y)):(n.slides.eq(n.currentSlide).css({zIndex:1}).animate({opacity:0},n.vars.animationSpeed,n.vars.easing),n.slides.eq(t).css({zIndex:2}).animate({opacity:1},n.vars.animationSpeed,n.vars.easing,n.wrapup));else{var g,h,S,y=d?n.slides.filter(":first").height():n.computedW;v?(g=n.vars.itemMargin,S=(n.itemW+g)*n.move*n.animatingTo,h=S>n.limit&&1!==n.visible?n.limit:S):h=0===n.currentSlide&&t===n.count-1&&n.vars.animationLoop&&"next"!==n.direction?u?(n.count+n.cloneOffset)*y:0:n.currentSlide===n.last&&0===t&&n.vars.animationLoop&&"prev"!==n.direction?u?0:(n.count+1)*y:u?(n.count-1-t+n.cloneOffset)*y:(t+n.cloneOffset)*y,n.setProps(h,"",n.vars.animationSpeed),n.transitions?(n.vars.animationLoop&&n.atEnd||(n.animating=!1,n.currentSlide=n.animatingTo),n.container.unbind("webkitTransitionEnd transitionend"),n.container.bind("webkitTransitionEnd transitionend",function(){clearTimeout(n.ensureAnimationEnd),n.wrapup(y)}),clearTimeout(n.ensureAnimationEnd),n.ensureAnimationEnd=setTimeout(function(){n.wrapup(y)},n.vars.animationSpeed+100)):n.container.animate(n.args,n.vars.animationSpeed,n.vars.easing,function(){n.wrapup(y)})}n.vars.smoothHeight&&f.smoothHeight(n.vars.animationSpeed)}},n.wrapup=function(e){p||v||(0===n.currentSlide&&n.animatingTo===n.last&&n.vars.animationLoop?n.setProps(e,"jumpEnd"):n.currentSlide===n.last&&0===n.animatingTo&&n.vars.animationLoop&&n.setProps(e,"jumpStart")),n.animating=!1,n.currentSlide=n.animatingTo,n.vars.after(n)},n.animateSlides=function(){!n.animating&&g&&n.flexAnimate(n.getTarget("next"))},n.pause=function(){clearInterval(n.animatedSlides),n.animatedSlides=null,n.playing=!1,n.vars.pausePlay&&f.pausePlay.update("play"),n.syncExists&&f.sync("pause")},n.play=function(){n.playing&&clearInterval(n.animatedSlides),n.animatedSlides=n.animatedSlides||setInterval(n.animateSlides,n.vars.slideshowSpeed),n.started=n.playing=!0,n.vars.pausePlay&&f.pausePlay.update("pause"),n.syncExists&&f.sync("play")},n.stop=function(){n.pause(),n.stopped=!0},n.canAdvance=function(e,t){var a=m?n.pagingCount-1:n.last;return t?!0:m&&n.currentItem===n.count-1&&0===e&&"prev"===n.direction?!0:m&&0===n.currentItem&&e===n.pagingCount-1&&"next"!==n.direction?!1:e!==n.currentSlide||m?n.vars.animationLoop?!0:n.atEnd&&0===n.currentSlide&&e===a&&"next"!==n.direction?!1:n.atEnd&&n.currentSlide===a&&0===e&&"next"===n.direction?!1:!0:!1},n.getTarget=function(e){return n.direction=e,"next"===e?n.currentSlide===n.last?0:n.currentSlide+1:0===n.currentSlide?n.last:n.currentSlide-1},n.setProps=function(e,t,a){var i=function(){var a=e?e:(n.itemW+n.vars.itemMargin)*n.move*n.animatingTo,i=function(){if(v)return"setTouch"===t?e:u&&n.animatingTo===n.last?0:u?n.limit-(n.itemW+n.vars.itemMargin)*n.move*n.animatingTo:n.animatingTo===n.last?n.limit:a;switch(t){case"setTotal":return u?(n.count-1-n.currentSlide+n.cloneOffset)*e:(n.currentSlide+n.cloneOffset)*e;case"setTouch":return u?e:e;case"jumpEnd":return u?e:n.count*e;case"jumpStart":return u?n.count*e:e;default:return e}}();return-1*i+"px"}();n.transitions&&(i=d?"translate3d(0,"+i+",0)":"translate3d("+i+",0,0)",a=void 0!==a?a/1e3+"s":"0s",n.container.css("-"+n.pfx+"-transition-duration",a),n.container.css("transition-duration",a)),n.args[n.prop]=i,(n.transitions||void 0===a)&&n.container.css(n.args),n.container.css("transform",i)},n.setup=function(t){if(p)n.slides.css({width:"100%","float":"left",marginRight:"-100%",position:"relative"}),"init"===t&&(o?n.slides.css({opacity:0,display:"block",webkitTransition:"opacity "+n.vars.animationSpeed/1e3+"s ease",zIndex:1}).eq(n.currentSlide).css({opacity:1,zIndex:2}):0==n.vars.fadeFirstSlide?n.slides.css({opacity:0,display:"block",zIndex:1}).eq(n.currentSlide).css({zIndex:2}).css({opacity:1}):n.slides.css({opacity:0,display:"block",zIndex:1}).eq(n.currentSlide).css({zIndex:2}).animate({opacity:1},n.vars.animationSpeed,n.vars.easing)),n.vars.smoothHeight&&f.smoothHeight();else{var a,i;"init"===t&&(n.viewport=e('<div class="'+s+'viewport"></div>').css({overflow:"hidden",position:"relative"}).appendTo(n).append(n.container),n.cloneCount=0,n.cloneOffset=0,u&&(i=e.makeArray(n.slides).reverse(),n.slides=e(i),n.container.empty().append(n.slides))),n.vars.animationLoop&&!v&&(n.cloneCount=2,n.cloneOffset=1,"init"!==t&&n.container.find(".clone").remove(),n.container.append(f.uniqueID(n.slides.first().clone().addClass("clone")).attr("aria-hidden","true")).prepend(f.uniqueID(n.slides.last().clone().addClass("clone")).attr("aria-hidden","true"))),n.newSlides=e(n.vars.selector,n),a=u?n.count-1-n.currentSlide+n.cloneOffset:n.currentSlide+n.cloneOffset,d&&!v?(n.container.height(200*(n.count+n.cloneCount)+"%").css("position","absolute").width("100%"),setTimeout(function(){n.newSlides.css({display:"block"}),n.doMath(),n.viewport.height(n.h),n.setProps(a*n.h,"init")},"init"===t?100:0)):(n.container.width(200*(n.count+n.cloneCount)+"%"),n.setProps(a*n.computedW,"init"),setTimeout(function(){n.doMath(),n.newSlides.css({width:n.computedW,"float":"left",display:"block"}),n.newSlides.css({opacity:1}),n.vars.smoothHeight&&f.smoothHeight()},"init"===t?100:0))}v||n.slides.removeClass(s+"active-slide").eq(n.currentSlide).addClass(s+"active-slide"),n.vars.init(n)},n.doMath=function(){var e=n.slides.first(),t=n.vars.itemMargin,a=n.vars.minItems,i=n.vars.maxItems;n.w=void 0===n.viewport?n.width():n.viewport.width(),n.h=e.height(),n.boxPadding=e.outerWidth()-e.width(),v?(n.itemT=n.vars.itemWidth+t,n.minW=a?a*n.itemT:n.w,n.maxW=i?i*n.itemT-t:n.w,n.itemW=n.minW>n.w?(n.w-t*(a-1))/a:n.maxW<n.w?(n.w-t*(i-1))/i:n.vars.itemWidth>n.w?n.w:n.vars.itemWidth,n.visible=Math.floor(n.w/n.itemW),n.move=n.vars.move>0&&n.vars.move<n.visible?n.vars.move:n.visible,n.pagingCount=Math.ceil((n.count-n.visible)/n.move+1),n.last=n.pagingCount-1,n.limit=1===n.pagingCount?0:n.vars.itemWidth>n.w?n.itemW*(n.count-1)+t*(n.count-1):(n.itemW+t)*n.count-n.w-t):(n.itemW=n.w,n.pagingCount=n.count,n.last=n.count-1),n.computedW=n.itemW-n.boxPadding},n.update=function(e,t){n.doMath(),v||(e<n.currentSlide?n.currentSlide+=1:e<=n.currentSlide&&0!==e&&(n.currentSlide-=1),n.animatingTo=n.currentSlide),n.vars.controlNav&&!n.manualControls&&("add"===t&&!v||n.pagingCount>n.controlNav.length?f.controlNav.update("add"):("remove"===t&&!v||n.pagingCount<n.controlNav.length)&&(v&&n.currentSlide>n.last&&(n.currentSlide-=1,n.animatingTo-=1),f.controlNav.update("remove",n.last))),n.vars.directionNav&&f.directionNav.update()},n.addSlide=function(t,a){var i=e(t);n.count+=1,n.last=n.count-1,d&&u?void 0!==a?n.slides.eq(n.count-a).after(i):n.container.prepend(i):void 0!==a?n.slides.eq(a).before(i):n.container.append(i),n.update(a,"add"),n.slides=e(n.vars.selector+":not(.clone)",n),n.setup(),n.vars.added(n)},n.removeSlide=function(t){var a=isNaN(t)?n.slides.index(e(t)):t;n.count-=1,n.last=n.count-1,isNaN(t)?e(t,n.slides).remove():d&&u?n.slides.eq(n.last).remove():n.slides.eq(t).remove(),n.doMath(),n.update(a,"remove"),n.slides=e(n.vars.selector+":not(.clone)",n),n.setup(),n.vars.removed(n)},f.init()},e(window).blur(function(e){focused=!1}).focus(function(e){focused=!0}),e.flexslider.defaults={namespace:"flex-",selector:".slides > li",animation:"fade",easing:"swing",direction:"horizontal",reverse:!1,animationLoop:!0,smoothHeight:!1,startAt:0,slideshow:!0,slideshowSpeed:7e3,animationSpeed:600,initDelay:0,randomize:!1,fadeFirstSlide:!0,thumbCaptions:!1,pauseOnAction:!0,pauseOnHover:!1,pauseInvisible:!0,useCSS:!0,touch:!0,video:!1,controlNav:!0,directionNav:!0,prevText:"Previous",nextText:"Next",keyboard:!0,multipleKeyboard:!1,mousewheel:!1,pausePlay:!1,pauseText:"Pause",playText:"Play",controlsContainer:"",manualControls:"",customDirectionNav:"",sync:"",asNavFor:"",itemWidth:0,itemMargin:0,minItems:1,maxItems:0,move:0,allowOneSlide:!0,start:function(){},before:function(){},after:function(){},end:function(){},added:function(){},removed:function(){},init:function(){}},e.fn.flexslider=function(t){if(void 0===t&&(t={}),"object"==typeof t)return this.each(function(){var a=e(this),n=t.selector?t.selector:".slides > li",i=a.find(n);1===i.length&&t.allowOneSlide===!0||0===i.length?(i.fadeIn(400),t.start&&t.start(a)):void 0===a.data("flexslider")&&new e.flexslider(this,t)});var a=e(this).data("flexslider");switch(t){case"play":a.play();break;case"pause":a.pause();break;case"stop":a.stop();break;case"next":a.flexAnimate(a.getTarget("next"),!0);break;case"prev":case"previous":a.flexAnimate(a.getTarget("prev"),!0);break;default:"number"==typeof t&&a.flexAnimate(t,!0)}}}(jQuery);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:73:"/bitrix/templates/aspro_optimus/js/jquery.validate.min.js?149909897522257";s:6:"source";s:57:"/bitrix/templates/aspro_optimus/js/jquery.validate.min.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
/*! jQuery Validation Plugin - v1.13.1 - 10/14/2014
 * http://jqueryvalidation.org/
 * Copyright (c) 2014 Jörn Zaefferer; Licensed MIT */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){a.extend(a.fn,{validate:function(b){if(!this.length)return void(b&&b.debug&&window.console&&console.warn("Nothing selected, can't validate, returning nothing."));var c=a.data(this[0],"validator");return c?c:(this.attr("novalidate","novalidate"),c=new a.validator(b,this[0]),a.data(this[0],"validator",c),c.settings.onsubmit&&(this.validateDelegate(":submit","click",function(b){c.settings.submitHandler&&(c.submitButton=b.target),a(b.target).hasClass("cancel")&&(c.cancelSubmit=!0),void 0!==a(b.target).attr("formnovalidate")&&(c.cancelSubmit=!0)}),this.submit(function(b){function d(){var d,e;return c.settings.submitHandler?(c.submitButton&&(d=a("<input type='hidden'/>").attr("name",c.submitButton.name).val(a(c.submitButton).val()).appendTo(c.currentForm)),e=c.settings.submitHandler.call(c,c.currentForm,b),c.submitButton&&d.remove(),void 0!==e?e:!1):!0}return c.settings.debug&&b.preventDefault(),c.cancelSubmit?(c.cancelSubmit=!1,d()):c.form()?c.pendingRequest?(c.formSubmitted=!0,!1):d():(c.focusInvalid(),!1)})),c)},valid:function(){var b,c;return a(this[0]).is("form")?b=this.validate().form():(b=!0,c=a(this[0].form).validate(),this.each(function(){b=c.element(this)&&b})),b},removeAttrs:function(b){var c={},d=this;return a.each(b.split(/\s/),function(a,b){c[b]=d.attr(b),d.removeAttr(b)}),c},rules:function(b,c){var d,e,f,g,h,i,j=this[0];if(b)switch(d=a.data(j.form,"validator").settings,e=d.rules,f=a.validator.staticRules(j),b){case"add":a.extend(f,a.validator.normalizeRule(c)),delete f.messages,e[j.name]=f,c.messages&&(d.messages[j.name]=a.extend(d.messages[j.name],c.messages));break;case"remove":return c?(i={},a.each(c.split(/\s/),function(b,c){i[c]=f[c],delete f[c],"required"===c&&a(j).removeAttr("aria-required")}),i):(delete e[j.name],f)}return g=a.validator.normalizeRules(a.extend({},a.validator.classRules(j),a.validator.attributeRules(j),a.validator.dataRules(j),a.validator.staticRules(j)),j),g.required&&(h=g.required,delete g.required,g=a.extend({required:h},g),a(j).attr("aria-required","true")),g.remote&&(h=g.remote,delete g.remote,g=a.extend(g,{remote:h})),g}}),a.extend(a.expr[":"],{blank:function(b){return!a.trim(""+a(b).val())},filled:function(b){return!!a.trim(""+a(b).val())},unchecked:function(b){return!a(b).prop("checked")}}),a.validator=function(b,c){this.settings=a.extend(!0,{},a.validator.defaults,b),this.currentForm=c,this.init()},a.validator.format=function(b,c){return 1===arguments.length?function(){var c=a.makeArray(arguments);return c.unshift(b),a.validator.format.apply(this,c)}:(arguments.length>2&&c.constructor!==Array&&(c=a.makeArray(arguments).slice(1)),c.constructor!==Array&&(c=[c]),a.each(c,function(a,c){b=b.replace(new RegExp("\\{"+a+"\\}","g"),function(){return c})}),b)},a.extend(a.validator,{defaults:{messages:{},groups:{},rules:{},errorClass:"error",validClass:"valid",errorElement:"label",focusCleanup:!1,focusInvalid:!0,errorContainer:a([]),errorLabelContainer:a([]),onsubmit:!0,ignore:":hidden",ignoreTitle:!1,onfocusin:function(a){this.lastActive=a,this.settings.focusCleanup&&(this.settings.unhighlight&&this.settings.unhighlight.call(this,a,this.settings.errorClass,this.settings.validClass),this.hideThese(this.errorsFor(a)))},onfocusout:function(a){this.checkable(a)||!(a.name in this.submitted)&&this.optional(a)||this.element(a)},onkeyup:function(a,b){(9!==b.which||""!==this.elementValue(a))&&(a.name in this.submitted||a===this.lastElement)&&this.element(a)},onclick:function(a){a.name in this.submitted?this.element(a):a.parentNode.name in this.submitted&&this.element(a.parentNode)},highlight:function(b,c,d){"radio"===b.type?this.findByName(b.name).addClass(c).removeClass(d):a(b).addClass(c).removeClass(d)},unhighlight:function(b,c,d){"radio"===b.type?this.findByName(b.name).removeClass(c).addClass(d):a(b).removeClass(c).addClass(d)}},setDefaults:function(b){a.extend(a.validator.defaults,b)},messages:{required:"This field is required.",remote:"Please fix this field.",email:"Please enter a valid email address.",url:"Please enter a valid URL.",date:"Please enter a valid date.",dateISO:"Please enter a valid date ( ISO ).",number:"Please enter a valid number.",digits:"Please enter only digits.",creditcard:"Please enter a valid credit card number.",equalTo:"Please enter the same value again.",maxlength:a.validator.format("Please enter no more than {0} characters."),minlength:a.validator.format("Please enter at least {0} characters."),rangelength:a.validator.format("Please enter a value between {0} and {1} characters long."),range:a.validator.format("Please enter a value between {0} and {1}."),max:a.validator.format("Please enter a value less than or equal to {0}."),min:a.validator.format("Please enter a value greater than or equal to {0}.")},autoCreateRanges:!1,prototype:{init:function(){function b(b){var c=a.data(this[0].form,"validator"),d="on"+b.type.replace(/^validate/,""),e=c.settings;e[d]&&!this.is(e.ignore)&&e[d].call(c,this[0],b)}this.labelContainer=a(this.settings.errorLabelContainer),this.errorContext=this.labelContainer.length&&this.labelContainer||a(this.currentForm),this.containers=a(this.settings.errorContainer).add(this.settings.errorLabelContainer),this.submitted={},this.valueCache={},this.pendingRequest=0,this.pending={},this.invalid={},this.reset();var c,d=this.groups={};a.each(this.settings.groups,function(b,c){"string"==typeof c&&(c=c.split(/\s/)),a.each(c,function(a,c){d[c]=b})}),c=this.settings.rules,a.each(c,function(b,d){c[b]=a.validator.normalizeRule(d)}),a(this.currentForm).validateDelegate(":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'] ,[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox']","focusin focusout keyup",b).validateDelegate("select, option, [type='radio'], [type='checkbox']","click",b),this.settings.invalidHandler&&a(this.currentForm).bind("invalid-form.validate",this.settings.invalidHandler),a(this.currentForm).find("[required], [data-rule-required], .required").attr("aria-required","true")},form:function(){return this.checkForm(),a.extend(this.submitted,this.errorMap),this.invalid=a.extend({},this.errorMap),this.valid()||a(this.currentForm).triggerHandler("invalid-form",[this]),this.showErrors(),this.valid()},checkForm:function(){this.prepareForm();for(var a=0,b=this.currentElements=this.elements();b[a];a++)this.check(b[a]);return this.valid()},element:function(b){var c=this.clean(b),d=this.validationTargetFor(c),e=!0;return this.lastElement=d,void 0===d?delete this.invalid[c.name]:(this.prepareElement(d),this.currentElements=a(d),e=this.check(d)!==!1,e?delete this.invalid[d.name]:this.invalid[d.name]=!0),a(b).attr("aria-invalid",!e),this.numberOfInvalids()||(this.toHide=this.toHide.add(this.containers)),this.showErrors(),e},showErrors:function(b){if(b){a.extend(this.errorMap,b),this.errorList=[];for(var c in b)this.errorList.push({message:b[c],element:this.findByName(c)[0]});this.successList=a.grep(this.successList,function(a){return!(a.name in b)})}this.settings.showErrors?this.settings.showErrors.call(this,this.errorMap,this.errorList):this.defaultShowErrors()},resetForm:function(){a.fn.resetForm&&a(this.currentForm).resetForm(),this.submitted={},this.lastElement=null,this.prepareForm(),this.hideErrors(),this.elements().removeClass(this.settings.errorClass).removeData("previousValue").removeAttr("aria-invalid")},numberOfInvalids:function(){return this.objectLength(this.invalid)},objectLength:function(a){var b,c=0;for(b in a)c++;return c},hideErrors:function(){this.hideThese(this.toHide)},hideThese:function(a){a.not(this.containers).text(""),this.addWrapper(a).hide()},valid:function(){return 0===this.size()},size:function(){return this.errorList.length},focusInvalid:function(){if(this.settings.focusInvalid)try{a(this.findLastActive()||this.errorList.length&&this.errorList[0].element||[]).filter(":visible").focus().trigger("focusin")}catch(b){}},findLastActive:function(){var b=this.lastActive;return b&&1===a.grep(this.errorList,function(a){return a.element.name===b.name}).length&&b},elements:function(){var b=this,c={};return a(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, [disabled], [readonly]").not(this.settings.ignore).filter(function(){return!this.name&&b.settings.debug&&window.console&&console.error("%o has no name assigned",this),this.name in c||!b.objectLength(a(this).rules())?!1:(c[this.name]=!0,!0)})},clean:function(b){return a(b)[0]},errors:function(){var b=this.settings.errorClass.split(" ").join(".");return a(this.settings.errorElement+"."+b,this.errorContext)},reset:function(){this.successList=[],this.errorList=[],this.errorMap={},this.toShow=a([]),this.toHide=a([]),this.currentElements=a([])},prepareForm:function(){this.reset(),this.toHide=this.errors().add(this.containers)},prepareElement:function(a){this.reset(),this.toHide=this.errorsFor(a)},elementValue:function(b){var c,d=a(b),e=b.type;return"radio"===e||"checkbox"===e?a("input[name='"+b.name+"']:checked").val():"number"===e&&"undefined"!=typeof b.validity?b.validity.badInput?!1:d.val():(c=d.val(),"string"==typeof c?c.replace(/\r/g,""):c)},check:function(b){b=this.validationTargetFor(this.clean(b));var c,d,e,f=a(b).rules(),g=a.map(f,function(a,b){return b}).length,h=!1,i=this.elementValue(b);for(d in f){e={method:d,parameters:f[d]};try{if(c=a.validator.methods[d].call(this,i,b,e.parameters),"dependency-mismatch"===c&&1===g){h=!0;continue}if(h=!1,"pending"===c)return void(this.toHide=this.toHide.not(this.errorsFor(b)));if(!c)return this.formatAndAdd(b,e),!1}catch(j){throw this.settings.debug&&window.console&&console.log("Exception occurred when checking element "+b.id+", check the '"+e.method+"' method.",j),j}}if(!h)return this.objectLength(f)&&this.successList.push(b),!0},customDataMessage:function(b,c){return a(b).data("msg"+c.charAt(0).toUpperCase()+c.substring(1).toLowerCase())||a(b).data("msg")},customMessage:function(a,b){var c=this.settings.messages[a];return c&&(c.constructor===String?c:c[b])},findDefined:function(){for(var a=0;a<arguments.length;a++)if(void 0!==arguments[a])return arguments[a];return void 0},defaultMessage:function(b,c){return this.findDefined(this.customMessage(b.name,c),this.customDataMessage(b,c),!this.settings.ignoreTitle&&b.title||void 0,a.validator.messages[c],"<strong>Warning: No message defined for "+b.name+"</strong>")},formatAndAdd:function(b,c){var d=this.defaultMessage(b,c.method),e=/\$?\{(\d+)\}/g;"function"==typeof d?d=d.call(this,c.parameters,b):e.test(d)&&(d=a.validator.format(d.replace(e,"{$1}"),c.parameters)),this.errorList.push({message:d,element:b,method:c.method}),this.errorMap[b.name]=d,this.submitted[b.name]=d},addWrapper:function(a){return this.settings.wrapper&&(a=a.add(a.parent(this.settings.wrapper))),a},defaultShowErrors:function(){var a,b,c;for(a=0;this.errorList[a];a++)c=this.errorList[a],this.settings.highlight&&this.settings.highlight.call(this,c.element,this.settings.errorClass,this.settings.validClass),this.showLabel(c.element,c.message);if(this.errorList.length&&(this.toShow=this.toShow.add(this.containers)),this.settings.success)for(a=0;this.successList[a];a++)this.showLabel(this.successList[a]);if(this.settings.unhighlight)for(a=0,b=this.validElements();b[a];a++)this.settings.unhighlight.call(this,b[a],this.settings.errorClass,this.settings.validClass);this.toHide=this.toHide.not(this.toShow),this.hideErrors(),this.addWrapper(this.toShow).show()},validElements:function(){return this.currentElements.not(this.invalidElements())},invalidElements:function(){return a(this.errorList).map(function(){return this.element})},showLabel:function(b,c){var d,e,f,g=this.errorsFor(b),h=this.idOrName(b),i=a(b).attr("aria-describedby");g.length?(g.removeClass(this.settings.validClass).addClass(this.settings.errorClass),g.html(c)):(g=a("<"+this.settings.errorElement+">").attr("id",h+"-error").addClass(this.settings.errorClass).html(c||""),d=g,this.settings.wrapper&&(d=g.hide().show().wrap("<"+this.settings.wrapper+"/>").parent()),this.labelContainer.length?this.labelContainer.append(d):this.settings.errorPlacement?this.settings.errorPlacement(d,a(b)):d.insertAfter(b),g.is("label")?g.attr("for",h):0===g.parents("label[for='"+h+"']").length&&(f=g.attr("id").replace(/(:|\.|\[|\])/g,"\\$1"),i?i.match(new RegExp("\\b"+f+"\\b"))||(i+=" "+f):i=f,a(b).attr("aria-describedby",i),e=this.groups[b.name],e&&a.each(this.groups,function(b,c){c===e&&a("[name='"+b+"']",this.currentForm).attr("aria-describedby",g.attr("id"))}))),!c&&this.settings.success&&(g.text(""),"string"==typeof this.settings.success?g.addClass(this.settings.success):this.settings.success(g,b)),this.toShow=this.toShow.add(g)},errorsFor:function(b){var c=this.idOrName(b),d=a(b).attr("aria-describedby"),e="label[for='"+c+"'], label[for='"+c+"'] *";return d&&(e=e+", #"+d.replace(/\s+/g,", #")),this.errors().filter(e)},idOrName:function(a){return this.groups[a.name]||(this.checkable(a)?a.name:a.id||a.name)},validationTargetFor:function(b){return this.checkable(b)&&(b=this.findByName(b.name)),a(b).not(this.settings.ignore)[0]},checkable:function(a){return/radio|checkbox/i.test(a.type)},findByName:function(b){return a(this.currentForm).find("[name='"+b+"']")},getLength:function(b,c){switch(c.nodeName.toLowerCase()){case"select":return a("option:selected",c).length;case"input":if(this.checkable(c))return this.findByName(c.name).filter(":checked").length}return b.length},depend:function(a,b){return this.dependTypes[typeof a]?this.dependTypes[typeof a](a,b):!0},dependTypes:{"boolean":function(a){return a},string:function(b,c){return!!a(b,c.form).length},"function":function(a,b){return a(b)}},optional:function(b){var c=this.elementValue(b);return!a.validator.methods.required.call(this,c,b)&&"dependency-mismatch"},startRequest:function(a){this.pending[a.name]||(this.pendingRequest++,this.pending[a.name]=!0)},stopRequest:function(b,c){this.pendingRequest--,this.pendingRequest<0&&(this.pendingRequest=0),delete this.pending[b.name],c&&0===this.pendingRequest&&this.formSubmitted&&this.form()?(a(this.currentForm).submit(),this.formSubmitted=!1):!c&&0===this.pendingRequest&&this.formSubmitted&&(a(this.currentForm).triggerHandler("invalid-form",[this]),this.formSubmitted=!1)},previousValue:function(b){return a.data(b,"previousValue")||a.data(b,"previousValue",{old:null,valid:!0,message:this.defaultMessage(b,"remote")})}},classRuleSettings:{required:{required:!0},email:{email:!0},url:{url:!0},date:{date:!0},dateISO:{dateISO:!0},number:{number:!0},digits:{digits:!0},creditcard:{creditcard:!0}},addClassRules:function(b,c){b.constructor===String?this.classRuleSettings[b]=c:a.extend(this.classRuleSettings,b)},classRules:function(b){var c={},d=a(b).attr("class");return d&&a.each(d.split(" "),function(){this in a.validator.classRuleSettings&&a.extend(c,a.validator.classRuleSettings[this])}),c},attributeRules:function(b){var c,d,e={},f=a(b),g=b.getAttribute("type");for(c in a.validator.methods)"required"===c?(d=b.getAttribute(c),""===d&&(d=!0),d=!!d):d=f.attr(c),/min|max/.test(c)&&(null===g||/number|range|text/.test(g))&&(d=Number(d)),d||0===d?e[c]=d:g===c&&"range"!==g&&(e[c]=!0);return e.maxlength&&/-1|2147483647|524288/.test(e.maxlength)&&delete e.maxlength,e},dataRules:function(b){var c,d,e={},f=a(b);for(c in a.validator.methods)d=f.data("rule"+c.charAt(0).toUpperCase()+c.substring(1).toLowerCase()),void 0!==d&&(e[c]=d);return e},staticRules:function(b){var c={},d=a.data(b.form,"validator");return d.settings.rules&&(c=a.validator.normalizeRule(d.settings.rules[b.name])||{}),c},normalizeRules:function(b,c){return a.each(b,function(d,e){if(e===!1)return void delete b[d];if(e.param||e.depends){var f=!0;switch(typeof e.depends){case"string":f=!!a(e.depends,c.form).length;break;case"function":f=e.depends.call(c,c)}f?b[d]=void 0!==e.param?e.param:!0:delete b[d]}}),a.each(b,function(d,e){b[d]=a.isFunction(e)?e(c):e}),a.each(["minlength","maxlength"],function(){b[this]&&(b[this]=Number(b[this]))}),a.each(["rangelength","range"],function(){var c;b[this]&&(a.isArray(b[this])?b[this]=[Number(b[this][0]),Number(b[this][1])]:"string"==typeof b[this]&&(c=b[this].replace(/[\[\]]/g,"").split(/[\s,]+/),b[this]=[Number(c[0]),Number(c[1])]))}),a.validator.autoCreateRanges&&(null!=b.min&&null!=b.max&&(b.range=[b.min,b.max],delete b.min,delete b.max),null!=b.minlength&&null!=b.maxlength&&(b.rangelength=[b.minlength,b.maxlength],delete b.minlength,delete b.maxlength)),b},normalizeRule:function(b){if("string"==typeof b){var c={};a.each(b.split(/\s/),function(){c[this]=!0}),b=c}return b},addMethod:function(b,c,d){a.validator.methods[b]=c,a.validator.messages[b]=void 0!==d?d:a.validator.messages[b],c.length<3&&a.validator.addClassRules(b,a.validator.normalizeRule(b))},methods:{required:function(b,c,d){if(!this.depend(d,c))return"dependency-mismatch";if("select"===c.nodeName.toLowerCase()){var e=a(c).val();return e&&e.length>0}return this.checkable(c)?this.getLength(b,c)>0:a.trim(b).length>0},email:function(a,b){return this.optional(b)||/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a)},url:function(a,b){return this.optional(b)||/^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(a)},date:function(a,b){/*return this.optional(b)||!/Invalid|NaN/.test(new Date(a).toString())*/ /*customized!!!*/ var check = false,re=new RegExp((typeof(VALIDATE_DATE_MASK)!=='undefined'?VALIDATE_DATE_MASK:'^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}$')),adata,gg,mm,aaaa,xdata;if(re.test(a)){adata=a.split('.');if(adata.length===1){adata=a.split('-');if(adata.length===1){adata=a.split(' ');if(adata.length===1){adata=a.split('/');if(adata.length===1){adata=a.split(':');}}}}gg=parseInt(adata[0],10);mm=parseInt(adata[1],10);aaaa=parseInt(adata[2],10);xdata=new Date(aaaa,mm-1,gg,12,0,0,0);if((xdata.getUTCFullYear()===aaaa)&&(xdata.getUTCMonth()===mm-1)&&(xdata.getUTCDate()===gg)){check = true;}else{check = false;}}else{check = false;}return this.optional(b)||check;},dateISO:function(a,b){return this.optional(b)||/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(a)},number:function(a,b){return this.optional(b)||/^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(a)},digits:function(a,b){return this.optional(b)||/^\d+$/.test(a)},creditcard:function(a,b){if(this.optional(b))return"dependency-mismatch";if(/[^0-9 \-]+/.test(a))return!1;var c,d,e=0,f=0,g=!1;if(a=a.replace(/\D/g,""),a.length<13||a.length>19)return!1;for(c=a.length-1;c>=0;c--)d=a.charAt(c),f=parseInt(d,10),g&&(f*=2)>9&&(f-=9),e+=f,g=!g;return e%10===0},minlength:function(b,c,d){var e=a.isArray(b)?b.length:this.getLength(b,c);return this.optional(c)||e>=d},maxlength:function(b,c,d){var e=a.isArray(b)?b.length:this.getLength(b,c);return this.optional(c)||d>=e},rangelength:function(b,c,d){var e=a.isArray(b)?b.length:this.getLength(b,c);return this.optional(c)||e>=d[0]&&e<=d[1]},min:function(a,b,c){return this.optional(b)||a>=c},max:function(a,b,c){return this.optional(b)||c>=a},range:function(a,b,c){return this.optional(b)||a>=c[0]&&a<=c[1]},equalTo:function(b,c,d){var e=a(d);return this.settings.onfocusout&&e.unbind(".validate-equalTo").bind("blur.validate-equalTo",function(){a(c).valid()}),b===e.val()},remote:function(b,c,d){if(this.optional(c))return"dependency-mismatch";var e,f,g=this.previousValue(c);return this.settings.messages[c.name]||(this.settings.messages[c.name]={}),g.originalMessage=this.settings.messages[c.name].remote,this.settings.messages[c.name].remote=g.message,d="string"==typeof d&&{url:d}||d,g.old===b?g.valid:(g.old=b,e=this,this.startRequest(c),f={},f[c.name]=b,a.ajax(a.extend(!0,{url:d,mode:"abort",port:"validate"+c.name,dataType:"json",data:f,context:e.currentForm,success:function(d){var f,h,i,j=d===!0||"true"===d;e.settings.messages[c.name].remote=g.originalMessage,j?(i=e.formSubmitted,e.prepareElement(c),e.formSubmitted=i,e.successList.push(c),delete e.invalid[c.name],e.showErrors()):(f={},h=d||e.defaultMessage(c,"remote"),f[c.name]=g.message=a.isFunction(h)?h(b):h,e.invalid[c.name]=!0,e.showErrors(f)),g.valid=j,e.stopRequest(c,j)}},d)),"pending")}}}),a.format=function(){throw"$.format has been deprecated. Please use $.validator.format instead."};var b,c={};a.ajaxPrefilter?a.ajaxPrefilter(function(a,b,d){var e=a.port;"abort"===a.mode&&(c[e]&&c[e].abort(),c[e]=d)}):(b=a.ajax,a.ajax=function(d){var e=("mode"in d?d:a.ajaxSettings).mode,f=("port"in d?d:a.ajaxSettings).port;return"abort"===e?(c[f]&&c[f].abort(),c[f]=b.apply(this,arguments),c[f]):b.apply(this,arguments)}),a.extend(a.fn,{validateDelegate:function(b,c,d){return this.bind(c,function(c){var e=a(c.target);return e.is(b)?d.apply(e,arguments):void 0})}})});
/* End */
;
; /* Start:"a:4:{s:4:"full";s:81:"/bitrix/templates/aspro_optimus/js/jquery.inputmask.bundle.min.js?149909897563845";s:6:"source";s:65:"/bitrix/templates/aspro_optimus/js/jquery.inputmask.bundle.min.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
/*!
* jquery.inputmask.bundle
* http://github.com/RobinHerbots/jquery.inputmask
* Copyright (c) 2010 - 2015 Robin Herbots
* Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
* Version: 3.1.64-73
*/
!function(a){function b(b){this.el=void 0,this.opts=a.extend(!0,{},this.defaults,b),this.noMasksCache=b&&void 0!==b.definitions,this.userOptions=b||{},e(this.opts.alias,b,this.opts)}function c(a){var b=document.createElement("input"),c="on"+a,d=c in b;return d||(b.setAttribute(c,"return;"),d="function"==typeof b[c]),b=null,d}function d(a){var b="text"==a||"tel"==a||"password"==a;if(!b){var c=document.createElement("input");c.setAttribute("type",a),b="text"===c.type,c=null}return b}function e(b,c,d){var f=d.aliases[b];return f?(f.alias&&e(f.alias,void 0,d),a.extend(!0,d,f),a.extend(!0,d,c),!0):(void 0==d.mask&&(d.mask=b),!1)}function f(b,c,d){var f=a(b),g=f.data("inputmask");if(g&&""!=g)try{g=g.replace(new RegExp("'","g"),'"');var h=a.parseJSON("{"+g+"}");a.extend(!0,d,h)}catch(i){}for(var j in c){var k=f.data("inputmask-"+j.toLowerCase());void 0!=k&&(k="boolean"==typeof k?k:k.toString(),"mask"==j&&0==k.indexOf("[")?(d[j]=k.replace(/[\s[\]]/g,"").split("','"),d[j][0]=d[j][0].replace("'",""),d[j][d[j].length-1]=d[j][d[j].length-1].replace("'","")):d[j]=k)}return d.alias?e(d.alias,d,c):a.extend(!0,c,d),c}function g(c,d){function e(b){function d(a,b,c,d){this.matches=[],this.isGroup=a||!1,this.isOptional=b||!1,this.isQuantifier=c||!1,this.isAlternator=d||!1,this.quantifier={min:1,max:1}}function e(b,d,e){var f=c.definitions[d],g=0==b.matches.length;if(e=void 0!=e?e:b.matches.length,f&&!m){f.placeholder=a.isFunction(f.placeholder)?f.placeholder.call(this,c):f.placeholder;for(var h=f.prevalidator,i=h?h.length:0,j=1;j<f.cardinality;j++){var k=i>=j?h[j-1]:[],l=k.validator,n=k.cardinality;b.matches.splice(e++,0,{fn:l?"string"==typeof l?new RegExp(l):new function(){this.test=l}:new RegExp("."),cardinality:n?n:1,optionality:b.isOptional,newBlockMarker:g,casing:f.casing,def:f.definitionSymbol||d,placeholder:f.placeholder,mask:d})}b.matches.splice(e++,0,{fn:f.validator?"string"==typeof f.validator?new RegExp(f.validator):new function(){this.test=f.validator}:new RegExp("."),cardinality:f.cardinality,optionality:b.isOptional,newBlockMarker:g,casing:f.casing,def:f.definitionSymbol||d,placeholder:f.placeholder,mask:d})}else b.matches.splice(e++,0,{fn:null,cardinality:0,optionality:b.isOptional,newBlockMarker:g,casing:null,def:d,placeholder:void 0,mask:d}),m=!1}for(var f,g,h,i,j,k,l=/(?:[?*+]|\{[0-9\+\*]+(?:,[0-9\+\*]*)?\})\??|[^.?*+^${[]()|\\]+|./g,m=!1,n=new d,o=[],p=[];f=l.exec(b);)switch(g=f[0],g.charAt(0)){case c.optionalmarker.end:case c.groupmarker.end:if(h=o.pop(),o.length>0){if(i=o[o.length-1],i.matches.push(h),i.isAlternator){j=o.pop();for(var q=0;q<j.matches.length;q++)j.matches[q].isGroup=!1;o.length>0?(i=o[o.length-1],i.matches.push(j)):n.matches.push(j)}}else n.matches.push(h);break;case c.optionalmarker.start:o.push(new d(!1,!0));break;case c.groupmarker.start:o.push(new d(!0));break;case c.quantifiermarker.start:var r=new d(!1,!1,!0);g=g.replace(/[{}]/g,"");var s=g.split(","),t=isNaN(s[0])?s[0]:parseInt(s[0]),u=1==s.length?t:isNaN(s[1])?s[1]:parseInt(s[1]);if(("*"==u||"+"==u)&&(t="*"==u?0:1),r.quantifier={min:t,max:u},o.length>0){var v=o[o.length-1].matches;if(f=v.pop(),!f.isGroup){var w=new d(!0);w.matches.push(f),f=w}v.push(f),v.push(r)}else{if(f=n.matches.pop(),!f.isGroup){var w=new d(!0);w.matches.push(f),f=w}n.matches.push(f),n.matches.push(r)}break;case c.escapeChar:m=!0;break;case c.alternatormarker:o.length>0?(i=o[o.length-1],k=i.matches.pop()):k=n.matches.pop(),k.isAlternator?o.push(k):(j=new d(!1,!1,!1,!0),j.matches.push(k),o.push(j));break;default:if(o.length>0){if(i=o[o.length-1],i.matches.length>0&&!i.isAlternator&&(k=i.matches[i.matches.length-1],k.isGroup&&(k.isGroup=!1,e(k,c.groupmarker.start,0),e(k,c.groupmarker.end))),e(i,g),i.isAlternator){j=o.pop();for(var q=0;q<j.matches.length;q++)j.matches[q].isGroup=!1;o.length>0?(i=o[o.length-1],i.matches.push(j)):n.matches.push(j)}}else n.matches.length>0&&(k=n.matches[n.matches.length-1],k.isGroup&&(k.isGroup=!1,e(k,c.groupmarker.start,0),e(k,c.groupmarker.end))),e(n,g)}return n.matches.length>0&&(k=n.matches[n.matches.length-1],k.isGroup&&(k.isGroup=!1,e(k,c.groupmarker.start,0),e(k,c.groupmarker.end)),p.push(n)),p}function f(f,g){if(void 0==f||""==f)return void 0;if(1==f.length&&0==c.greedy&&0!=c.repeat&&(c.placeholder=""),c.repeat>0||"*"==c.repeat||"+"==c.repeat){var h="*"==c.repeat?0:"+"==c.repeat?1:c.repeat;f=c.groupmarker.start+f+c.groupmarker.end+c.quantifiermarker.start+h+","+c.repeat+c.quantifiermarker.end}var i;return void 0==b.prototype.masksCache[f]||d===!0?(i={mask:f,maskToken:e(f),validPositions:{},_buffer:void 0,buffer:void 0,tests:{},metadata:g},d!==!0&&(b.prototype.masksCache[f]=i)):i=a.extend(!0,{},b.prototype.masksCache[f]),i}function g(a){if(a=a.toString(),c.numericInput){a=a.split("").reverse();for(var b=0;b<a.length;b++)a[b]==c.optionalmarker.start?a[b]=c.optionalmarker.end:a[b]==c.optionalmarker.end?a[b]=c.optionalmarker.start:a[b]==c.groupmarker.start?a[b]=c.groupmarker.end:a[b]==c.groupmarker.end&&(a[b]=c.groupmarker.start);a=a.join("")}return a}var h=void 0;if(a.isFunction(c.mask)&&(c.mask=c.mask.call(this,c)),a.isArray(c.mask)){if(c.mask.length>1){c.keepStatic=void 0==c.keepStatic?!0:c.keepStatic;var i="(";return a.each(c.mask,function(b,c){i.length>1&&(i+=")|("),i+=g(void 0==c.mask||a.isFunction(c.mask)?c:c.mask)}),i+=")",f(i,c.mask)}c.mask=c.mask.pop()}return c.mask&&(h=void 0==c.mask.mask||a.isFunction(c.mask.mask)?f(g(c.mask),c.mask):f(g(c.mask.mask),c.mask)),h}function h(e,f,g){function h(a,b,c){b=b||0;var d,e,f,g=[],h=0;do{if(a===!0&&i().validPositions[h]){var j=i().validPositions[h];e=j.match,d=j.locator.slice(),g.push(c===!0?j.input:H(h,e))}else f=r(h,d,h-1),e=f.match,d=f.locator.slice(),g.push(H(h,e));h++}while((void 0==fa||fa>h-1)&&null!=e.fn||null==e.fn&&""!=e.def||b>=h);return g.pop(),g}function i(){return f}function n(a){var b=i();b.buffer=void 0,b.tests={},a!==!0&&(b._buffer=void 0,b.validPositions={},b.p=0)}function o(a,b){var c=i(),d=-1,e=c.validPositions;void 0==a&&(a=-1);var f=d,g=d;for(var h in e){var j=parseInt(h);e[j]&&(b||null!=e[j].match.fn)&&(a>=j&&(f=j),j>=a&&(g=j))}return d=-1!=f&&a-f>1||a>g?f:g}function p(b,c,d){if(g.insertMode&&void 0!=i().validPositions[b]&&void 0==d){var e,f=a.extend(!0,{},i().validPositions),h=o();for(e=b;h>=e;e++)delete i().validPositions[e];i().validPositions[b]=c;var j,k=!0,l=i().validPositions;for(e=j=b;h>=e;e++){var m=f[e];if(void 0!=m)for(var n=j,p=-1;n<C()&&(null==m.match.fn&&l[e]&&(l[e].match.optionalQuantifier===!0||l[e].match.optionality===!0)||null!=m.match.fn);){if(null==m.match.fn||!g.keepStatic&&l[e]&&(void 0!=l[e+1]&&u(e+1,l[e].locator.slice(),e).length>1||void 0!=l[e].alternation)?n++:n=D(j),t(n,m.match.def)){k=A(n,m.input,!0,!0)!==!1,j=n;break}if(k=null==m.match.fn,p==n)break;p=n}if(!k)break}if(!k)return i().validPositions=a.extend(!0,{},f),!1}else i().validPositions[b]=c;return!0}function q(a,b,c,d){var e,f=a;i().p=a;for(e=f;b>e;e++)void 0!=i().validPositions[e]&&(c===!0||0!=g.canClearPosition(i(),e,o(),d,g))&&delete i().validPositions[e];for(n(!0),e=f+1;e<=o();){for(;void 0!=i().validPositions[f];)f++;var h=i().validPositions[f];f>e&&(e=f+1);var j=i().validPositions[e];void 0!=j&&void 0==h?(t(f,j.match.def)&&A(f,j.input,!0)!==!1&&(delete i().validPositions[e],e++),f++):e++}var k=o(),l=C();for(c!==!0&&void 0!=i().validPositions[k]&&i().validPositions[k].input==g.radixPoint&&delete i().validPositions[k],e=k+1;l>=e;e++)i().validPositions[e]&&delete i().validPositions[e];n(!0)}function r(a,b,c){var d=i().validPositions[a];if(void 0==d)for(var e=u(a,b,c),f=o(),h=i().validPositions[f]||u(0)[0],j=void 0!=h.alternation?h.locator[h.alternation].toString().split(","):[],k=0;k<e.length&&(d=e[k],!(d.match&&(g.greedy&&d.match.optionalQuantifier!==!0||(d.match.optionality===!1||d.match.newBlockMarker===!1)&&d.match.optionalQuantifier!==!0)&&(void 0==h.alternation||h.alternation!=d.alternation||void 0!=d.locator[h.alternation]&&z(d.locator[h.alternation].toString().split(","),j))));k++);return d}function s(a){return i().validPositions[a]?i().validPositions[a].match:u(a)[0].match}function t(a,b){for(var c=!1,d=u(a),e=0;e<d.length;e++)if(d[e].match&&d[e].match.def==b){c=!0;break}return c}function u(b,c,d,e){function f(c,d,e,g){function j(e,g,n){if(h>1e4)return alert("jquery.inputmask: There is probably an error in your mask definition or in the code. Create an issue on github with an example of the mask you are using. "+i().mask),!0;if(h==b&&void 0==e.matches)return k.push({match:e,locator:g.reverse()}),!0;if(void 0!=e.matches){if(e.isGroup&&n!==!0){if(e=j(c.matches[m+1],g))return!0}else if(e.isOptional){var o=e;if(e=f(e,d,g,n)){var p=k[k.length-1].match,q=0==a.inArray(p,o.matches);if(!q)return!0;l=!0,h=b}}else if(e.isAlternator){var r,s=e,t=[],u=k.slice(),v=g.length,w=d.length>0?d.shift():-1;if(-1==w||"string"==typeof w){var x=h,y=d.slice(),z=[];"string"==typeof w&&(z=w.split(","));for(var A=0;A<s.matches.length;A++){if(k=[],e=j(s.matches[A],[A].concat(g),n)||e,e!==!0&&void 0!=e&&z[z.length-1]<s.matches.length){var B=c.matches.indexOf(e)+1;c.matches.length>B&&(e=j(c.matches[B],[B].concat(g.slice(1,g.length)),n),e&&(z.push(B.toString()),a.each(k,function(a,b){b.alternation=g.length-1})))}r=k.slice(),h=x,k=[];for(var C=0;C<y.length;C++)d[C]=y[C];for(var D=0;D<r.length;D++){var E=r[D];E.alternation=E.alternation||v;for(var F=0;F<t.length;F++){var G=t[F];if(E.match.mask==G.match.mask&&("string"!=typeof w||-1!=a.inArray(E.locator[E.alternation].toString(),z))){r.splice(D,1),D--,G.locator[E.alternation]=G.locator[E.alternation]+","+E.locator[E.alternation],G.alternation=E.alternation;break}}}t=t.concat(r)}"string"==typeof w&&(t=a.map(t,function(b,c){if(isFinite(c)){var d,e=b.alternation,f=b.locator[e].toString().split(",");b.locator[e]=void 0,b.alternation=void 0;for(var g=0;g<f.length;g++)d=-1!=a.inArray(f[g],z),d&&(void 0!=b.locator[e]?(b.locator[e]+=",",b.locator[e]+=f[g]):b.locator[e]=parseInt(f[g]),b.alternation=e);if(void 0!=b.locator[e])return b}})),k=u.concat(t),h=b,l=k.length>0}else e=s.matches[w]?j(s.matches[w],[w].concat(g),n):!1;if(e)return!0}else if(e.isQuantifier&&n!==!0)for(var H=e,I=d.length>0&&n!==!0?d.shift():0;I<(isNaN(H.quantifier.max)?I+1:H.quantifier.max)&&b>=h;I++){var J=c.matches[a.inArray(H,c.matches)-1];if(e=j(J,[I].concat(g),!0)){var p=k[k.length-1].match;p.optionalQuantifier=I>H.quantifier.min-1;var q=0==a.inArray(p,J.matches);if(q){if(I>H.quantifier.min-1){l=!0,h=b;break}return!0}return!0}}else if(e=f(e,d,g,n))return!0}else h++}for(var m=d.length>0?d.shift():0;m<c.matches.length;m++)if(c.matches[m].isQuantifier!==!0){var n=j(c.matches[m],[m].concat(e),g);if(n&&h==b)return n;if(h>b)break}}var g=i().maskToken,h=c?d:0,j=c||[0],k=[],l=!1;if(e===!0&&i().tests[b])return i().tests[b];if(void 0==c){for(var m,n=b-1;void 0==(m=i().validPositions[n])&&n>-1&&(!i().tests[n]||void 0==(m=i().tests[n][0]));)n--;void 0!=m&&n>-1&&(h=n,j=m.locator.slice())}for(var o=j.shift();o<g.length;o++){var p=f(g[o],j,[o]);if(p&&h==b||h>b)break}return(0==k.length||l)&&k.push({match:{fn:null,cardinality:0,optionality:!0,casing:null,def:""},locator:[]}),i().tests[b]=a.extend(!0,[],k),i().tests[b]}function v(){return void 0==i()._buffer&&(i()._buffer=h(!1,1)),i()._buffer}function w(){return void 0==i().buffer&&(i().buffer=h(!0,o(),!0)),i().buffer}function x(a,b,c){if(c=c||w().slice(),a===!0)n(),a=0,b=c.length;else for(var d=a;b>d;d++)delete i().validPositions[d],delete i().tests[d];for(var d=a;b>d;d++)c[d]!=g.skipOptionalPartCharacter&&A(d,c[d],!0,!0)}function y(a,b){switch(b.casing){case"upper":a=a.toUpperCase();break;case"lower":a=a.toLowerCase()}return a}function z(b,c){for(var d=g.greedy?c:c.slice(0,1),e=!1,f=0;f<b.length;f++)if(-1!=a.inArray(b[f],d)){e=!0;break}return e}function A(b,c,d,e){function f(b,c,d,e){var f=!1;return a.each(u(b),function(h,j){for(var k=j.match,l=c?1:0,m="",r=(w(),k.cardinality);r>l;r--)m+=F(b-(r-1));if(c&&(m+=c),f=null!=k.fn?k.fn.test(m,i(),b,d,g):c!=k.def&&c!=g.skipOptionalPartCharacter||""==k.def?!1:{c:k.def,pos:b},f!==!1){var s=void 0!=f.c?f.c:c;s=s==g.skipOptionalPartCharacter&&null===k.fn?k.def:s;var t=b,u=w();if(void 0!=f.remove&&(a.isArray(f.remove)||(f.remove=[f.remove]),a.each(f.remove.sort(function(a,b){return b-a}),function(a,b){q(b,b+1,!0)})),void 0!=f.insert&&(a.isArray(f.insert)||(f.insert=[f.insert]),a.each(f.insert.sort(function(a,b){return a-b}),function(a,b){A(b.pos,b.c,!0)})),f.refreshFromBuffer){var v=f.refreshFromBuffer;if(d=!0,x(v===!0?v:v.start,v.end,u),void 0==f.pos&&void 0==f.c)return f.pos=o(),!1;if(t=void 0!=f.pos?f.pos:b,t!=b)return f=a.extend(f,A(t,s,!0)),!1}else if(f!==!0&&void 0!=f.pos&&f.pos!=b&&(t=f.pos,x(b,t),t!=b))return f=a.extend(f,A(t,s,!0)),!1;return 1!=f&&void 0==f.pos&&void 0==f.c?!1:(h>0&&n(!0),p(t,a.extend({},j,{input:y(s,k)}),e)||(f=!1),!1)}}),f}function h(b,c,d,e){for(var f,h,j,k,l=a.extend(!0,{},i().validPositions),m=o();m>=0&&(k=i().validPositions[m],!k||void 0==k.alternation||(f=m,h=i().validPositions[f].alternation,r(f).locator[k.alternation]==k.locator[k.alternation]));m--);if(void 0!=h){f=parseInt(f);for(var p in i().validPositions)if(p=parseInt(p),k=i().validPositions[p],p>=f&&void 0!=k.alternation){var q=i().validPositions[f].locator[h].toString().split(","),s=k.locator[h]||q[0];s.length>0&&(s=s.split(",")[0]);for(var t=0;t<q.length;t++)if(s<q[t]){for(var u,v,w=p;w>=0;w--)if(u=i().validPositions[w],void 0!=u){v=u.locator[h],u.locator[h]=parseInt(q[t]);break}if(s!=u.locator[h]){for(var x=[],y=0,z=p+1;z<o()+1;z++){var B=i().validPositions[z];B&&(null!=B.match.fn?x.push(B.input):b>z&&y++),delete i().validPositions[z],delete i().tests[z]}for(n(!0),g.keepStatic=!g.keepStatic,j=!0;x.length>0;){var C=x.shift();if(C!=g.skipOptionalPartCharacter&&!(j=A(o()+1,C,!1,!0)))break}if(u.alternation=h,u.locator[h]=v,j){for(var D=o(b)+1,E=0,z=p+1;z<o()+1;z++){var B=i().validPositions[z];B&&null==B.match.fn&&b>z&&E++}b+=E-y,j=A(b>D?D:b,c,d,e)}if(g.keepStatic=!g.keepStatic,j)return j;n(),i().validPositions=a.extend(!0,{},l)}}break}}return!1}function j(b,c){for(var d=i().validPositions[c],e=d.locator,f=e.length,g=b;c>g;g++)if(!B(g)){var h=u(g),j=h[0],k=-1;a.each(h,function(a,b){for(var c=0;f>c;c++)b.locator[c]&&z(b.locator[c].toString().split(","),e[c].toString().split(","))&&c>k&&(k=c,j=b)}),p(g,a.extend({},j,{input:j.match.def}),!0)}}d=d===!0;for(var k=w(),l=b-1;l>-1&&!i().validPositions[l];l--);for(l++;b>l;l++)void 0==i().validPositions[l]&&((!B(l)||k[l]!=H(l))&&u(l).length>1||k[l]==g.radixPoint||"0"==k[l]&&a.inArray(g.radixPoint,k)<l)&&f(l,k[l],!0);var m=b,s=!1,t=a.extend(!0,{},i().validPositions);if(m<C()&&(s=f(m,c,d,e),(!d||e)&&s===!1)){var v=i().validPositions[m];if(!v||null!=v.match.fn||v.match.def!=c&&c!=g.skipOptionalPartCharacter){if((g.insertMode||void 0==i().validPositions[D(m)])&&!B(m))for(var E=m+1,G=D(m);G>=E;E++)if(s=f(E,c,d,e),s!==!1){j(m,E),m=E;break}}else s={caret:D(m)}}if(s===!1&&g.keepStatic&&P(k)&&(s=h(b,c,d,e)),s===!0&&(s={pos:m}),a.isFunction(g.postValidation)&&0!=s&&!d){n(!0);var I=g.postValidation(w(),g);if(!I)return n(!0),i().validPositions=a.extend(!0,{},t),!1}return s}function B(a){var b=s(a);if(null!=b.fn)return b.fn;if(!g.keepStatic&&void 0==i().validPositions[a]){for(var c=u(a),d=!0,e=0;e<c.length;e++)if(""!=c[e].match.def&&(void 0==c[e].alternation||c[e].locator[c[e].alternation].length>1)){d=!1;break}return d}return!1}function C(){var a;fa=ea.prop("maxLength"),-1==fa&&(fa=void 0);var b,c=o(),d=i().validPositions[c],e=void 0!=d?d.locator.slice():void 0;for(b=c+1;void 0==d||null!=d.match.fn||null==d.match.fn&&""!=d.match.def;b++)d=r(b,e,b-1),e=d.locator.slice();var f=s(b-1);return a=""!=f.def?b:b-1,void 0==fa||fa>a?a:fa}function D(a){var b=C();if(a>=b)return b;for(var c=a;++c<b&&!B(c)&&(g.nojumps!==!0||g.nojumpsThreshold>c););return c}function E(a){var b=a;if(0>=b)return 0;for(;--b>0&&!B(b););return b}function F(a){return void 0==i().validPositions[a]?H(a):i().validPositions[a].input}function G(b,c,d,e,f){if(e&&a.isFunction(g.onBeforeWrite)){var h=g.onBeforeWrite.call(b,e,c,d,g);if(h){if(h.refreshFromBuffer){var i=h.refreshFromBuffer;x(i===!0?i:i.start,i.end,h.buffer),n(!0),c=w()}d=h.caret||d}}b._valueSet(c.join("")),void 0!=d&&M(b,d),f===!0&&(ia=!0,a(b).trigger("input"))}function H(a,b){if(b=b||s(a),void 0!=b.placeholder)return b.placeholder;if(null==b.fn){if(!g.keepStatic&&void 0==i().validPositions[a]){for(var c,d=u(a),e=!1,f=0;f<d.length;f++){if(c&&""!=d[f].match.def&&d[f].match.def!=c.match.def&&(void 0==d[f].alternation||d[f].alternation==c.alternation)){e=!0;break}1!=d[f].match.optionality&&1!=d[f].match.optionalQuantifier&&(c=d[f])}if(e)return g.placeholder.charAt(a%g.placeholder.length)}return b.def}return g.placeholder.charAt(a%g.placeholder.length)}function I(b,c,d,e){function f(){var a=!1,b=v().slice(j,D(j)).join("").indexOf(h);if(-1!=b&&!B(j)){a=!0;for(var c=v().slice(j,j+b),d=0;d<c.length;d++)if(" "!=c[d]){a=!1;break}}return a}var g=void 0!=e?e.slice():b._valueGet().split(""),h="",j=0;if(n(),i().p=D(-1),c&&b._valueSet(""),!d){var k=v().slice(0,D(-1)).join(""),l=g.join("").match(new RegExp("^"+J(k),"g"));l&&l.length>0&&(g.splice(0,l.length*k.length),j=D(j))}a.each(g,function(c,e){var g=a.Event("keypress");g.which=e.charCodeAt(0),h+=e;var k=o(void 0,!0),l=i().validPositions[k],m=r(k+1,l?l.locator.slice():void 0,k);if(!f()||d){var n=d?c:null==m.match.fn&&m.match.optionality&&k+1<i().p?k+1:i().p;V.call(b,g,!0,!1,d,n),j=n+1,h=""}else V.call(b,g,!0,!1,!0,k+1)}),c&&G(b,w(),a(b).is(":focus")?D(o(0)):void 0,a.Event("checkval"))}function J(a){return b.escapeRegex(a)}function K(b){if(b[0].inputmask&&!b.hasClass("hasDatepicker")){var c=[],d=i().validPositions;for(var e in d)d[e].match&&null!=d[e].match.fn&&c.push(d[e].input);var f=(ga?c.reverse():c).join(""),h=(ga?w().slice().reverse():w()).join("");return a.isFunction(g.onUnMask)&&(f=g.onUnMask.call(b,h,f,g)||f),f}return b[0]._valueGet()}function L(a){if(ga&&"number"==typeof a&&(!g.greedy||""!=g.placeholder)){var b=w().length;a=b-a}return a}function M(b,c,d){var e,f=b.jquery&&b.length>0?b[0]:b;if("number"!=typeof c)return f.setSelectionRange?(c=f.selectionStart,d=f.selectionEnd):window.getSelection?(e=window.getSelection().getRangeAt(0),(e.commonAncestorContainer.parentNode==f||e.commonAncestorContainer==f)&&(c=e.startOffset,d=e.endOffset)):document.selection&&document.selection.createRange&&(e=document.selection.createRange(),c=0-e.duplicate().moveStart("character",-1e5),d=c+e.text.length),{begin:L(c),end:L(d)};if(c=L(c),d=L(d),d="number"==typeof d?d:c,a(f).is(":visible")){var h=a(f).css("font-size").replace("px","")*d;if(f.scrollLeft=h>f.scrollWidth?h:0,k||0!=g.insertMode||c!=d||d++,f.setSelectionRange)f.selectionStart=c,f.selectionEnd=d;else if(window.getSelection){if(e=document.createRange(),void 0==f.firstChild){var i=document.createTextNode("");f.appendChild(i)}e.setStart(f.firstChild,c<f._valueGet().length?c:f._valueGet().length),e.setEnd(f.firstChild,d<f._valueGet().length?d:f._valueGet().length),e.collapse(!0);var j=window.getSelection();j.removeAllRanges(),j.addRange(e)}else f.createTextRange&&(e=f.createTextRange(),e.collapse(!0),e.moveEnd("character",d),e.moveStart("character",c),e.select())}}function N(b){var c,d,e=w(),f=e.length,g=o(),h={},j=i().validPositions[g],k=void 0!=j?j.locator.slice():void 0;for(c=g+1;c<e.length;c++)d=r(c,k,c-1),k=d.locator.slice(),h[c]=a.extend(!0,{},d);var l=j&&void 0!=j.alternation?j.locator[j.alternation]:void 0;for(c=f-1;c>g&&(d=h[c],(d.match.optionality||d.match.optionalQuantifier||l&&(l!=h[c].locator[j.alternation]&&null!=d.match.fn||null==d.match.fn&&d.locator[j.alternation]&&z(d.locator[j.alternation].toString().split(","),l.split(","))&&""!=u(c)[0].def))&&e[c]==H(c,d.match));c--)f--;return b?{l:f,def:h[f]?h[f].match:void 0}:f}function O(a){for(var b=N(),c=a.length-1;c>b&&!B(c);c--);return a.splice(b,c+1-b),a}function P(b){if(a.isFunction(g.isComplete))return g.isComplete.call(ea,b,g);if("*"==g.repeat)return void 0;{var c=!1,d=N(!0),e=E(d.l);o()}if(void 0==d.def||d.def.newBlockMarker||d.def.optionality||d.def.optionalQuantifier){c=!0;for(var f=0;e>=f;f++){var h=r(f).match;if(null!=h.fn&&void 0==i().validPositions[f]&&h.optionality!==!0&&h.optionalQuantifier!==!0||null==h.fn&&b[f]!=H(f,h)){c=!1;break}}}return c}function Q(a,b){return ga?a-b>1||a-b==1&&g.insertMode:b-a>1||b-a==1&&g.insertMode}function R(c){var d=a._data(c).events,e=!1;a.each(d,function(c,d){a.each(d,function(a,c){if("inputmask"==c.namespace&&"setvalue"!=c.type){var d=c.handler;c.handler=function(a){if(!(this.disabled||this.readOnly&&!("keydown"==a.type&&a.ctrlKey&&67==a.keyCode||a.keyCode==b.keyCode.TAB))){switch(a.type){case"input":if(ia===!0||e===!0)return ia=!1,a.preventDefault();break;case"keydown":ha=!1,e=!1;break;case"keypress":if(ha===!0)return a.preventDefault();ha=!0;break;case"compositionstart":e=!0;break;case"compositionupdate":ia=!0;break;case"compositionend":e=!1}return d.apply(this,arguments)}a.preventDefault()}}})})}function S(b){function c(b){if(void 0==a.valHooks[b]||1!=a.valHooks[b].inputmaskpatch){var c=a.valHooks[b]&&a.valHooks[b].get?a.valHooks[b].get:function(a){return a.value},d=a.valHooks[b]&&a.valHooks[b].set?a.valHooks[b].set:function(a,b){return a.value=b,a};a.valHooks[b]={get:function(b){a(b);if(b.inputmask){if(b.inputmask.opts.autoUnmask)return b.inputmask.unmaskedvalue();var d=c(b),e=b.inputmask.maskset,f=e._buffer;return f=f?f.join(""):"",d!=f?d:""}return c(b)},set:function(b,c){var e,f=a(b);return e=d(b,c),b.inputmask&&f.triggerHandler("setvalue.inputmask"),e},inputmaskpatch:!0}}}function d(){a(this);return this.inputmask?this.inputmask.opts.autoUnmask?this.inputmask.unmaskedvalue():g.call(this)!=v().join("")?g.call(this):"":g.call(this)}function e(b){h.call(this,b),this.inputmask&&a(this).triggerHandler("setvalue.inputmask")}function f(b){a(b).bind("mouseenter.inputmask",function(b){var c=a(this),d=this,e=d._valueGet();""!=e&&e!=w().join("")&&c.triggerHandler("setvalue.inputmask")});
//!! the bound handlers are executed in the order they where bound
var c=a._data(b).events,d=c.mouseover;if(d){for(var e=d[d.length-1],f=d.length-1;f>0;f--)d[f]=d[f-1];d[0]=e}}var g,h;if(!b._valueGet){var i;Object.getOwnPropertyDescriptor&&void 0==b.value?(g=function(){return this.textContent},h=function(a){this.textContent=a},Object.defineProperty(b,"value",{get:d,set:e})):((i=Object.getOwnPropertyDescriptor&&Object.getOwnPropertyDescriptor(b,"value"))&&i.configurable,document.__lookupGetter__&&b.__lookupGetter__("value")?(g=b.__lookupGetter__("value"),h=b.__lookupSetter__("value"),b.__defineGetter__("value",d),b.__defineSetter__("value",e)):(g=function(){return b.value},h=function(a){b.value=a},c(b.type),f(b))),b._valueGet=function(a){return ga&&a!==!0?g.call(this).split("").reverse().join(""):g.call(this)},b._valueSet=function(a){h.call(this,ga?a.split("").reverse().join(""):a)}}}function T(c,d,e,f){function h(){if(g.keepStatic){n(!0);var b,d=[],e=a.extend(!0,{},i().validPositions);for(b=o();b>=0;b--){var f=i().validPositions[b];if(f&&(null!=f.match.fn&&d.push(f.input),delete i().validPositions[b],void 0!=f.alternation&&f.locator[f.alternation]==r(b).locator[f.alternation]))break}if(b>-1)for(;d.length>0;){i().p=D(o());var h=a.Event("keypress");h.which=d.pop().charCodeAt(0),V.call(c,h,!0,!1,!1,i().p)}else i().validPositions=a.extend(!0,{},e)}}if((g.numericInput||ga)&&(d==b.keyCode.BACKSPACE?d=b.keyCode.DELETE:d==b.keyCode.DELETE&&(d=b.keyCode.BACKSPACE),ga)){var j=e.end;e.end=e.begin,e.begin=j}if(d==b.keyCode.BACKSPACE&&(e.end-e.begin<1||0==g.insertMode)?(e.begin=E(e.begin),void 0==i().validPositions[e.begin]||i().validPositions[e.begin].input!=g.groupSeparator&&i().validPositions[e.begin].input!=g.radixPoint||e.begin--):d==b.keyCode.DELETE&&e.begin==e.end&&(e.end=B(e.end)?e.end+1:D(e.end)+1,void 0==i().validPositions[e.begin]||i().validPositions[e.begin].input!=g.groupSeparator&&i().validPositions[e.begin].input!=g.radixPoint||e.end++),q(e.begin,e.end,!1,f),f!==!0){h();var k=o(e.begin);k<e.begin?(-1==k&&n(),i().p=D(k)):i().p=e.begin}}function U(d){var e=this,f=a(e),h=d.keyCode,k=M(e);h==b.keyCode.BACKSPACE||h==b.keyCode.DELETE||j&&127==h||d.ctrlKey&&88==h&&!c("cut")?(d.preventDefault(),88==h&&(aa=w().join("")),T(e,h,k),G(e,w(),i().p,d,aa!=w().join("")),e._valueGet()==v().join("")?f.trigger("cleared"):P(w())===!0&&f.trigger("complete"),g.showTooltip&&f.prop("title",i().mask)):h==b.keyCode.END||h==b.keyCode.PAGE_DOWN?setTimeout(function(){var a=D(o());g.insertMode||a!=C()||d.shiftKey||a--,M(e,d.shiftKey?k.begin:a,a)},0):h==b.keyCode.HOME&&!d.shiftKey||h==b.keyCode.PAGE_UP?M(e,0,d.shiftKey?k.begin:0):(g.undoOnEscape&&h==b.keyCode.ESCAPE||90==h&&d.ctrlKey)&&d.altKey!==!0?(I(e,!0,!1,aa.split("")),f.click()):h!=b.keyCode.INSERT||d.shiftKey||d.ctrlKey?0!=g.insertMode||d.shiftKey||(h==b.keyCode.RIGHT?setTimeout(function(){var a=M(e);M(e,a.begin)},0):h==b.keyCode.LEFT&&setTimeout(function(){var a=M(e);M(e,ga?a.begin+1:a.begin-1)},0)):(g.insertMode=!g.insertMode,M(e,g.insertMode||k.begin!=C()?k.begin:k.begin-1)),g.onKeyDown.call(this,d,w(),M(e).begin,g),ja=-1!=a.inArray(h,g.ignorables)}function V(c,d,e,f,h){var j=this,k=a(j),l=c.which||c.charCode||c.keyCode;if(!(d===!0||c.ctrlKey&&c.altKey)&&(c.ctrlKey||c.metaKey||ja))return!0;if(l){46==l&&0==c.shiftKey&&","==g.radixPoint&&(l=44);var m,o=d?{begin:h,end:h}:M(j),q=String.fromCharCode(l),r=Q(o.begin,o.end);r&&(i().undoPositions=a.extend(!0,{},i().validPositions),T(j,b.keyCode.DELETE,o,!0),o.begin=i().p,g.insertMode||(g.insertMode=!g.insertMode,p(o.begin,f),g.insertMode=!g.insertMode),r=!g.multi),i().writeOutBuffer=!0;var s=ga&&!r?o.end:o.begin,t=A(s,q,f);if(t!==!1){if(t!==!0&&(s=void 0!=t.pos?t.pos:s,q=void 0!=t.c?t.c:q),n(!0),void 0!=t.caret)m=t.caret;else{var v=i().validPositions;m=!g.keepStatic&&(void 0!=v[s+1]&&u(s+1,v[s].locator.slice(),s).length>1||void 0!=v[s].alternation)?s+1:D(s)}i().p=m}if(e!==!1){var y=this;if(setTimeout(function(){g.onKeyValidation.call(y,t,g)},0),i().writeOutBuffer&&t!==!1){var z=w();G(j,z,d?void 0:g.numericInput?E(m):m,c,d!==!0),d!==!0&&setTimeout(function(){P(z)===!0&&k.trigger("complete")},0)}else r&&(i().buffer=void 0,i().validPositions=i().undoPositions)}else r&&(i().buffer=void 0,i().validPositions=i().undoPositions);if(g.showTooltip&&k.prop("title",i().mask),d&&a.isFunction(g.onBeforeWrite)){var B=g.onBeforeWrite.call(this,c,w(),m,g);if(B&&B.refreshFromBuffer){var C=B.refreshFromBuffer;x(C===!0?C:C.start,C.end,B.buffer),n(!0),B.caret&&(i().p=B.caret)}}c.preventDefault()}}function W(b){var c=this,d=a(c),e=c._valueGet(!0),f=M(c);if("propertychange"==b.type&&c._valueGet().length<=C())return!0;if("paste"==b.type){var h=e.substr(0,f.begin),i=e.substr(f.end,e.length);h==v().slice(0,f.begin).join("")&&(h=""),i==v().slice(f.end).join("")&&(i=""),window.clipboardData&&window.clipboardData.getData?e=h+window.clipboardData.getData("Text")+i:b.originalEvent&&b.originalEvent.clipboardData&&b.originalEvent.clipboardData.getData&&(e=h+b.originalEvent.clipboardData.getData("text/plain")+i)}var j=e;if(a.isFunction(g.onBeforePaste)){if(j=g.onBeforePaste.call(c,e,g),j===!1)return b.preventDefault(),!1;j||(j=e)}return I(c,!1,!1,ga?j.split("").reverse():j.split("")),G(c,w(),void 0,b,!0),d.click(),P(w())===!0&&d.trigger("complete"),!1}function X(b){var c=this;I(c,!0,!1),P(w())===!0&&a(c).trigger("complete"),b.preventDefault()}function Y(a){var b=this;aa=w().join(""),(""==ca||0!=a.originalEvent.data.indexOf(ca))&&(ba=M(b))}function Z(b){var c=this,d=M(c);0==b.originalEvent.data.indexOf(ca)&&(n(),d=ba);var e=b.originalEvent.data;M(c,d.begin,d.end);for(var f=0;f<e.length;f++){var h=a.Event("keypress");h.which=e.charCodeAt(f),ha=!1,ja=!1,V.call(c,h)}setTimeout(function(){var a=i().p;G(c,w(),g.numericInput?E(a):a)},0),ca=b.originalEvent.data}function $(a){}function _(c){ea=a(c),g.showTooltip&&ea.prop("title",i().mask),("rtl"==c.dir||g.rightAlign)&&ea.css("text-align","right"),("rtl"==c.dir||g.numericInput)&&(c.dir="ltr",ea.removeAttr("dir"),c.inputmask.isRTL=!0,ga=!0),ea.unbind(".inputmask"),(ea.is(":input")&&d(ea.attr("type"))||c.isContentEditable)&&(ea.closest("form").bind("submit",function(a){aa!=w().join("")&&ea.change(),g.clearMaskOnLostFocus&&ea[0]._valueGet&&ea[0]._valueGet()==v().join("")&&ea[0]._valueSet(""),g.removeMaskOnSubmit&&ea.inputmask("remove")}).bind("reset",function(){setTimeout(function(){ea.triggerHandler("setvalue.inputmask")},0)}),ea.bind("mouseenter.inputmask",function(){var b=a(this),c=this;la=!0,!b.is(":focus")&&g.showMaskOnHover&&c._valueGet()!=w().join("")&&G(c,w())}).bind("blur.inputmask",function(b){var c=a(this),d=this;if(d.inputmask){var e=d._valueGet(),f=w().slice();ka=!0,aa!=f.join("")&&setTimeout(function(){c.change(),aa=f.join("")},0),""!=e&&(g.clearMaskOnLostFocus&&(e==v().join("")?f=[]:O(f)),P(f)===!1&&(c.trigger("incomplete"),g.clearIncomplete&&(n(),f=g.clearMaskOnLostFocus?[]:v().slice())),G(d,f,void 0,b))}}).bind("focus.inputmask",function(b){var c=(a(this),this),d=c._valueGet();g.showMaskOnFocus&&(!g.showMaskOnHover||g.showMaskOnHover&&""==d)?c._valueGet()!=w().join("")&&G(c,w(),D(o())):la===!1&&M(c,D(o())),aa=w().join("")}).bind("mouseleave.inputmask",function(){var b=a(this),c=this;if(la=!1,g.clearMaskOnLostFocus){var d=w().slice(),e=c._valueGet();b.is(":focus")||e==b.attr("placeholder")||""==e||(e==v().join("")?d=[]:O(d),G(c,d))}}).bind("click.inputmask",function(){var b=a(this),c=this;if(b.is(":focus")){var d=M(c);if(d.begin==d.end)if(g.radixFocus&&""!=g.radixPoint&&-1!=a.inArray(g.radixPoint,w())&&(ka||w().join("")==v().join("")))M(c,a.inArray(g.radixPoint,w())),ka=!1;else{var e=L(d.begin),f=D(o(e));f>e?M(c,B(e)?e:D(e)):M(c,f)}}}).bind("dblclick.inputmask",function(){var a=this;setTimeout(function(){M(a,0,D(o()))},0)}).bind(m+".inputmask dragdrop.inputmask drop.inputmask",W).bind("cut.inputmask",function(c){ia=!0;var d=this,e=a(d),f=M(d);T(d,b.keyCode.DELETE,f),G(d,w(),i().p,c,aa!=w().join("")),d._valueGet()==v().join("")&&e.trigger("cleared"),g.showTooltip&&e.prop("title",i().mask)}).bind("complete.inputmask",g.oncomplete).bind("incomplete.inputmask",g.onincomplete).bind("cleared.inputmask",g.oncleared),ea.bind("keydown.inputmask",U).bind("keypress.inputmask",V),l||ea.bind("compositionstart.inputmask",Y).bind("compositionupdate.inputmask",Z).bind("compositionend.inputmask",$),"paste"===m&&ea.bind("input.inputmask",X)),ea.bind("setvalue.inputmask",function(){var b=this,c=b._valueGet();b._valueSet(a.isFunction(g.onBeforeMask)?g.onBeforeMask.call(b,c,g)||c:c),I(b,!0,!1),aa=w().join(""),(g.clearMaskOnLostFocus||g.clearIncomplete)&&b._valueGet()==v().join("")&&b._valueSet("")}),S(c);var e=a.isFunction(g.onBeforeMask)?g.onBeforeMask.call(c,c._valueGet(),g)||c._valueGet():c._valueGet();I(c,!0,!1,e.split(""));var f=w().slice();aa=f.join("");var h;try{h=document.activeElement}catch(j){}P(f)===!1&&g.clearIncomplete&&n(),g.clearMaskOnLostFocus&&(f.join("")==v().join("")?f=[]:O(f)),G(c,f),h===c&&M(c,D(o())),R(c)}var aa,ba,ca,da,ea,fa,ga=!1,ha=!1,ia=!1,ja=!1,ka=!0,la=!0;if(void 0!=e)switch(e.action){case"isComplete":return da=e.el,ea=a(da),f=da.inputmask.maskset,g=da.inputmask.opts,P(e.buffer);case"unmaskedvalue":return da=e.el,ea=a(da),f=da.inputmask.maskset,g=da.inputmask.opts,ga=da.inputmask.isRTL,K(ea);case"mask":aa=w().join(""),_(e.el);break;case"format":ea=a({}),ea[0].inputmask=new b,ea[0].inputmask.opts=g,ea[0].inputmask.el=ea[0],ea[0].inputmask.maskset=f,ea[0].inputmask.isRTL=g.numericInput,g.numericInput&&(ga=!0);var ma=(a.isFunction(g.onBeforeMask)?g.onBeforeMask.call(ea,e.value,g)||e.value:e.value).split("");return I(ea,!1,!1,ga?ma.reverse():ma),a.isFunction(g.onBeforeWrite)&&g.onBeforeWrite.call(this,void 0,w(),0,g),e.metadata?{value:ga?w().slice().reverse().join(""):w().join(""),metadata:ea.inputmask("getmetadata")}:ga?w().slice().reverse().join(""):w().join("");case"isValid":ea=a({}),ea[0].inputmask=new b,ea[0].inputmask.opts=g,ea[0].inputmask.el=ea[0],ea[0].inputmask.maskset=f,ea[0].inputmask.isRTL=g.numericInput,g.numericInput&&(ga=!0);var ma=e.value.split("");I(ea,!1,!0,ga?ma.reverse():ma);for(var na=w(),oa=N(),pa=na.length-1;pa>oa&&!B(pa);pa--);return na.splice(oa,pa+1-oa),P(na)&&e.value==na.join("");case"getemptymask":return da=e.el,ea=a(da),f=da.inputmask.maskset,g=da.inputmask.opts,v();case"remove":da=e.el,ea=a(da),f=da.inputmask.maskset,g=da.inputmask.opts,da._valueSet(K(ea)),ea.unbind(".inputmask"),da.inputmask=void 0;var qa;Object.getOwnPropertyDescriptor&&(qa=Object.getOwnPropertyDescriptor(da,"value")),qa&&qa.get?da._valueGet&&Object.defineProperty(da,"value",{get:da._valueGet,set:da._valueSet}):document.__lookupGetter__&&da.__lookupGetter__("value")&&da._valueGet&&(da.__defineGetter__("value",da._valueGet),da.__defineSetter__("value",da._valueSet));try{delete da._valueGet,delete da._valueSet}catch(ra){da._valueGet=void 0,da._valueSet=void 0}break;case"getmetadata":if(da=e.el,ea=a(da),f=da.inputmask.maskset,g=da.inputmask.opts,a.isArray(f.metadata)){for(var sa,ta=o(),ua=ta;ua>=0;ua--)if(i().validPositions[ua]&&void 0!=i().validPositions[ua].alternation){sa=i().validPositions[ua].alternation;break}return void 0!=sa?f.metadata[i().validPositions[ta].locator[sa]]:f.metadata[0]}return f.metadata}}b.prototype={defaults:{placeholder:"_",optionalmarker:{start:"[",end:"]"},quantifiermarker:{start:"{",end:"}"},groupmarker:{start:"(",end:")"},alternatormarker:"|",escapeChar:"\\",mask:null,oncomplete:a.noop,onincomplete:a.noop,oncleared:a.noop,repeat:0,greedy:!0,autoUnmask:!1,removeMaskOnSubmit:!1,clearMaskOnLostFocus:!0,insertMode:!0,clearIncomplete:!1,aliases:{},alias:null,onKeyDown:a.noop,onBeforeMask:void 0,onBeforePaste:void 0,onBeforeWrite:void 0,onUnMask:void 0,showMaskOnFocus:!0,showMaskOnHover:!0,onKeyValidation:a.noop,skipOptionalPartCharacter:" ",showTooltip:!1,numericInput:!1,rightAlign:!1,undoOnEscape:!0,radixPoint:"",groupSeparator:"",radixFocus:!1,nojumps:!1,nojumpsThreshold:0,keepStatic:void 0,definitions:{9:{validator:"[0-9]",cardinality:1,definitionSymbol:"*"},a:{validator:"[A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]",cardinality:1,definitionSymbol:"*"},"*":{validator:"[0-9A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]",cardinality:1}},ignorables:[8,9,13,19,27,33,34,35,36,37,38,39,40,45,46,93,112,113,114,115,116,117,118,119,120,121,122,123],isComplete:void 0,canClearPosition:a.noop,postValidation:void 0},keyCode:{ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91},masksCache:{},mask:function(c){var d=c.jquery&&c.length>0?c[0]:c,e=a.extend(!0,{},this.opts);f(c,e,a.extend(!0,{},this.userOptions));var i=g(e,this.noMasksCache);return void 0!=i&&(d.inputmask=new b,d.inputmask.opts=e,d.inputmask.noMasksCache=this.noMasksCache,d.inputmask.el=d,d.inputmask.maskset=i,d.inputmask.isRTL=!1,h({action:"mask",el:d},i,d.inputmask.opts)),c},unmaskedvalue:function(){return this.el?h({action:"unmaskedvalue",el:this.el}):void 0},remove:function(){return this.el?(h({action:"remove",el:this.el}),this.el.inputmask=void 0,this.el):void 0},getemptymask:function(){return this.el?h({action:"getemptymask",el:this.el}):void 0},hasMaskedValue:function(){return!this.opts.autoUnmask},isComplete:function(){return this.el?h({action:"isComplete",buffer:this.el._valueGet().split(""),el:this.el}):void 0},getmetadata:function(){return this.el?h({action:"getmetadata",el:this.el}):void 0}},b.extendDefaults=function(c){a.extend(b.prototype.defaults,c)},b.extendDefinitions=function(c){a.extend(b.prototype.defaults.definitions,c)},b.extendAliases=function(c){a.extend(b.prototype.defaults.aliases,c)},b.format=function(c,d,f){var i=a.extend(!0,{},b.prototype.defaults,d);return e(i.alias,d,i),h({action:"format",value:c,metadata:f},g(i,d&&void 0!==d.definitions),i)},b.isValid=function(c,d){var f=a.extend(!0,{},b.prototype.defaults,d);return e(f.alias,d,f),h({action:"isValid",value:c},g(f,d&&void 0!==d.definitions),f)},b.escapeRegex=function(a){var b=["/",".","*","+","?","|","(",")","[","]","{","}","\\","$","^"];return a.replace(new RegExp("(\\"+b.join("|\\")+")","gim"),"\\$1")},b.keyCode={ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91};var i=navigator.userAgent,j=null!==i.match(new RegExp("iphone","i")),k=(null!==i.match(new RegExp("android.*safari.*","i")),null!==i.match(new RegExp("android.*chrome.*","i"))),l=null!==i.match(new RegExp("android.*firefox.*","i")),m=(/Kindle/i.test(i)||/Silk/i.test(i)||/KFTT/i.test(i)||/KFOT/i.test(i)||/KFJWA/i.test(i)||/KFJWI/i.test(i)||/KFSOWI/i.test(i)||/KFTHWA/i.test(i)||/KFTHWI/i.test(i)||/KFAPWA/i.test(i)||/KFAPWI/i.test(i),c("paste")?"paste":c("input")?"input":"propertychange");return window.inputmask=b,b}(jQuery),function(a){return void 0===a.fn.inputmask&&(a.fn.inputmask=function(b,c){var d;if(c=c||{},"string"==typeof b)switch(b){case"mask":return d=new inputmask(c),this.each(function(){d.mask(this)});case"unmaskedvalue":var e=this.jquery&&this.length>0?this[0]:this;return e.inputmask?e.inputmask.unmaskedvalue():a(e).val();case"remove":return this.each(function(){this.inputmask&&this.inputmask.remove()});case"getemptymask":var e=this.jquery&&this.length>0?this[0]:this;return e.inputmask?e.inputmask.getemptymask():"";case"hasMaskedValue":var e=this.jquery&&this.length>0?this[0]:this;return e.inputmask?e.inputmask.hasMaskedValue():!1;case"isComplete":var e=this.jquery&&this.length>0?this[0]:this;return e.inputmask?e.inputmask.isComplete():!0;case"getmetadata":var e=this.jquery&&this.length>0?this[0]:this;return e.inputmask?e.inputmask.getmetadata():void 0;default:return c.alias=b,d=new inputmask(c),this.each(function(){d.mask(this)})}else{if("object"==typeof b)return d=new inputmask(b),this.each(function(){d.mask(this)});if(void 0==b)return this.each(function(){d=new inputmask(c),d.mask(this)})}}),a.fn.inputmask}(jQuery),function(a){return inputmask.extendDefinitions({h:{validator:"[01][0-9]|2[0-3]",cardinality:2,prevalidator:[{validator:"[0-2]",cardinality:1}]},s:{validator:"[0-5][0-9]",cardinality:2,prevalidator:[{validator:"[0-5]",cardinality:1}]},d:{validator:"0[1-9]|[12][0-9]|3[01]",cardinality:2,prevalidator:[{validator:"[0-3]",cardinality:1}]},m:{validator:"0[1-9]|1[012]",cardinality:2,prevalidator:[{validator:"[01]",cardinality:1}]},y:{validator:"(19|20)\\d{2}",cardinality:4,prevalidator:[{validator:"[12]",cardinality:1},{validator:"(19|20)",cardinality:2},{validator:"(19|20)\\d",cardinality:3}]}}),inputmask.extendAliases({"dd/mm/yyyy":{mask:"1/2/y",placeholder:"dd/mm/yyyy",regex:{val1pre:new RegExp("[0-3]"),val1:new RegExp("0[1-9]|[12][0-9]|3[01]"),val2pre:function(a){var b=inputmask.escapeRegex.call(this,a);return new RegExp("((0[1-9]|[12][0-9]|3[01])"+b+"[01])")},val2:function(a){var b=inputmask.escapeRegex.call(this,a);return new RegExp("((0[1-9]|[12][0-9])"+b+"(0[1-9]|1[012]))|(30"+b+"(0[13-9]|1[012]))|(31"+b+"(0[13578]|1[02]))")}},leapday:"29/02/",separator:"/",yearrange:{minyear:1900,maxyear:2099},isInYearRange:function(a,b,c){if(isNaN(a))return!1;var d=parseInt(a.concat(b.toString().slice(a.length))),e=parseInt(a.concat(c.toString().slice(a.length)));return(isNaN(d)?!1:d>=b&&c>=d)||(isNaN(e)?!1:e>=b&&c>=e)},determinebaseyear:function(a,b,c){var d=(new Date).getFullYear();if(a>d)return a;if(d>b){for(var e=b.toString().slice(0,2),f=b.toString().slice(2,4);e+c>b;)e--;var g=e+f;return a>g?a:g}return d},onKeyDown:function(b,c,d,e){var f=a(this);if(b.ctrlKey&&b.keyCode==inputmask.keyCode.RIGHT){var g=new Date;f.val(g.getDate().toString()+(g.getMonth()+1).toString()+g.getFullYear().toString()),f.triggerHandler("setvalue.inputmask")}},getFrontValue:function(a,b,c){for(var d=0,e=0,f=0;f<a.length&&"2"!=a.charAt(f);f++){var g=c.definitions[a.charAt(f)];g?(d+=e,e=g.cardinality):e++}return b.join("").substr(d,e)},definitions:{1:{validator:function(a,b,c,d,e){var f=e.regex.val1.test(a);return d||f||a.charAt(1)!=e.separator&&-1=="-./".indexOf(a.charAt(1))||!(f=e.regex.val1.test("0"+a.charAt(0)))?f:(b.buffer[c-1]="0",{refreshFromBuffer:{start:c-1,end:c},pos:c,c:a.charAt(0)})},cardinality:2,prevalidator:[{validator:function(a,b,c,d,e){var f=a;isNaN(b.buffer[c+1])||(f+=b.buffer[c+1]);var g=1==f.length?e.regex.val1pre.test(f):e.regex.val1.test(f);if(!d&&!g){if(g=e.regex.val1.test(a+"0"))return b.buffer[c]=a,b.buffer[++c]="0",{pos:c,c:"0"};if(g=e.regex.val1.test("0"+a))return b.buffer[c]="0",c++,{pos:c}}return g},cardinality:1}]},2:{validator:function(a,b,c,d,e){var f=e.getFrontValue(b.mask,b.buffer,e);-1!=f.indexOf(e.placeholder[0])&&(f="01"+e.separator);var g=e.regex.val2(e.separator).test(f+a);if(!d&&!g&&(a.charAt(1)==e.separator||-1!="-./".indexOf(a.charAt(1)))&&(g=e.regex.val2(e.separator).test(f+"0"+a.charAt(0))))return b.buffer[c-1]="0",{refreshFromBuffer:{start:c-1,end:c},pos:c,c:a.charAt(0)};if(e.mask.indexOf("2")==e.mask.length-1&&g){var h=b.buffer.join("").substr(4,4)+a;if(h!=e.leapday)return!0;var i=parseInt(b.buffer.join("").substr(0,4),10);return i%4===0?i%100===0?i%400===0?!0:!1:!0:!1}return g},cardinality:2,prevalidator:[{validator:function(a,b,c,d,e){isNaN(b.buffer[c+1])||(a+=b.buffer[c+1]);var f=e.getFrontValue(b.mask,b.buffer,e);-1!=f.indexOf(e.placeholder[0])&&(f="01"+e.separator);var g=1==a.length?e.regex.val2pre(e.separator).test(f+a):e.regex.val2(e.separator).test(f+a);return d||g||!(g=e.regex.val2(e.separator).test(f+"0"+a))?g:(b.buffer[c]="0",c++,{pos:c})},cardinality:1}]},y:{validator:function(a,b,c,d,e){if(e.isInYearRange(a,e.yearrange.minyear,e.yearrange.maxyear)){var f=b.buffer.join("").substr(0,6);if(f!=e.leapday)return!0;var g=parseInt(a,10);return g%4===0?g%100===0?g%400===0?!0:!1:!0:!1}return!1},cardinality:4,prevalidator:[{validator:function(a,b,c,d,e){var f=e.isInYearRange(a,e.yearrange.minyear,e.yearrange.maxyear);if(!d&&!f){var g=e.determinebaseyear(e.yearrange.minyear,e.yearrange.maxyear,a+"0").toString().slice(0,1);if(f=e.isInYearRange(g+a,e.yearrange.minyear,e.yearrange.maxyear))return b.buffer[c++]=g.charAt(0),{pos:c};if(g=e.determinebaseyear(e.yearrange.minyear,e.yearrange.maxyear,a+"0").toString().slice(0,2),f=e.isInYearRange(g+a,e.yearrange.minyear,e.yearrange.maxyear))return b.buffer[c++]=g.charAt(0),b.buffer[c++]=g.charAt(1),{pos:c}}return f},cardinality:1},{validator:function(a,b,c,d,e){var f=e.isInYearRange(a,e.yearrange.minyear,e.yearrange.maxyear);if(!d&&!f){var g=e.determinebaseyear(e.yearrange.minyear,e.yearrange.maxyear,a).toString().slice(0,2);if(f=e.isInYearRange(a[0]+g[1]+a[1],e.yearrange.minyear,e.yearrange.maxyear))return b.buffer[c++]=g.charAt(1),{pos:c};if(g=e.determinebaseyear(e.yearrange.minyear,e.yearrange.maxyear,a).toString().slice(0,2),e.isInYearRange(g+a,e.yearrange.minyear,e.yearrange.maxyear)){var h=b.buffer.join("").substr(0,6);if(h!=e.leapday)f=!0;else{var i=parseInt(a,10);f=i%4===0?i%100===0?i%400===0?!0:!1:!0:!1}}else f=!1;if(f)return b.buffer[c-1]=g.charAt(0),b.buffer[c++]=g.charAt(1),b.buffer[c++]=a.charAt(0),{refreshFromBuffer:{start:c-3,end:c},pos:c}}return f},cardinality:2},{validator:function(a,b,c,d,e){return e.isInYearRange(a,e.yearrange.minyear,e.yearrange.maxyear)},cardinality:3}]}},insertMode:!1,autoUnmask:!1},"mm/dd/yyyy":{placeholder:"mm/dd/yyyy",alias:"dd/mm/yyyy",regex:{val2pre:function(a){var b=inputmask.escapeRegex.call(this,a);return new RegExp("((0[13-9]|1[012])"+b+"[0-3])|(02"+b+"[0-2])")},val2:function(a){var b=inputmask.escapeRegex.call(this,a);return new RegExp("((0[1-9]|1[012])"+b+"(0[1-9]|[12][0-9]))|((0[13-9]|1[012])"+b+"30)|((0[13578]|1[02])"+b+"31)")},val1pre:new RegExp("[01]"),val1:new RegExp("0[1-9]|1[012]")},leapday:"02/29/",onKeyDown:function(b,c,d,e){var f=a(this);if(b.ctrlKey&&b.keyCode==inputmask.keyCode.RIGHT){var g=new Date;f.val((g.getMonth()+1).toString()+g.getDate().toString()+g.getFullYear().toString()),f.triggerHandler("setvalue.inputmask")}}},"yyyy/mm/dd":{mask:"y/1/2",placeholder:"yyyy/mm/dd",alias:"mm/dd/yyyy",leapday:"/02/29",onKeyDown:function(b,c,d,e){var f=a(this);if(b.ctrlKey&&b.keyCode==inputmask.keyCode.RIGHT){var g=new Date;f.val(g.getFullYear().toString()+(g.getMonth()+1).toString()+g.getDate().toString()),f.triggerHandler("setvalue.inputmask")}}},"dd.mm.yyyy":{mask:"1.2.y",placeholder:"dd.mm.yyyy",leapday:"29.02.",separator:".",alias:"dd/mm/yyyy"},"dd-mm-yyyy":{mask:"1-2-y",placeholder:"dd-mm-yyyy",leapday:"29-02-",separator:"-",alias:"dd/mm/yyyy"},"mm.dd.yyyy":{mask:"1.2.y",placeholder:"mm.dd.yyyy",leapday:"02.29.",separator:".",alias:"mm/dd/yyyy"},"mm-dd-yyyy":{mask:"1-2-y",placeholder:"mm-dd-yyyy",leapday:"02-29-",separator:"-",alias:"mm/dd/yyyy"},"yyyy.mm.dd":{mask:"y.1.2",placeholder:"yyyy.mm.dd",leapday:".02.29",separator:".",alias:"yyyy/mm/dd"},"yyyy-mm-dd":{mask:"y-1-2",placeholder:"yyyy-mm-dd",leapday:"-02-29",separator:"-",alias:"yyyy/mm/dd"},datetime:{mask:"1/2/y h:s",placeholder:"dd/mm/yyyy hh:mm",alias:"dd/mm/yyyy",regex:{hrspre:new RegExp("[012]"),hrs24:new RegExp("2[0-4]|1[3-9]"),hrs:new RegExp("[01][0-9]|2[0-4]"),ampm:new RegExp("^[a|p|A|P][m|M]"),mspre:new RegExp("[0-5]"),ms:new RegExp("[0-5][0-9]")},timeseparator:":",hourFormat:"24",definitions:{h:{validator:function(a,b,c,d,e){if("24"==e.hourFormat&&24==parseInt(a,10))return b.buffer[c-1]="0",b.buffer[c]="0",{refreshFromBuffer:{start:c-1,end:c},c:"0"};var f=e.regex.hrs.test(a);if(!d&&!f&&(a.charAt(1)==e.timeseparator||-1!="-.:".indexOf(a.charAt(1)))&&(f=e.regex.hrs.test("0"+a.charAt(0))))return b.buffer[c-1]="0",b.buffer[c]=a.charAt(0),c++,{refreshFromBuffer:{start:c-2,end:c},pos:c,c:e.timeseparator};if(f&&"24"!==e.hourFormat&&e.regex.hrs24.test(a)){var g=parseInt(a,10);return 24==g?(b.buffer[c+5]="a",b.buffer[c+6]="m"):(b.buffer[c+5]="p",b.buffer[c+6]="m"),g-=12,10>g?(b.buffer[c]=g.toString(),b.buffer[c-1]="0"):(b.buffer[c]=g.toString().charAt(1),b.buffer[c-1]=g.toString().charAt(0)),{refreshFromBuffer:{start:c-1,end:c+6},c:b.buffer[c]}}return f},cardinality:2,prevalidator:[{validator:function(a,b,c,d,e){var f=e.regex.hrspre.test(a);return d||f||!(f=e.regex.hrs.test("0"+a))?f:(b.buffer[c]="0",c++,{pos:c})},cardinality:1}]},s:{validator:"[0-5][0-9]",cardinality:2,prevalidator:[{validator:function(a,b,c,d,e){var f=e.regex.mspre.test(a);return d||f||!(f=e.regex.ms.test("0"+a))?f:(b.buffer[c]="0",c++,{pos:c})},cardinality:1}]},t:{validator:function(a,b,c,d,e){return e.regex.ampm.test(a+"m")},casing:"lower",cardinality:1}},insertMode:!1,autoUnmask:!1},datetime12:{mask:"1/2/y h:s t\\m",placeholder:"dd/mm/yyyy hh:mm xm",alias:"datetime",hourFormat:"12"},"hh:mm t":{mask:"h:s t\\m",placeholder:"hh:mm xm",alias:"datetime",hourFormat:"12"},"h:s t":{mask:"h:s t\\m",placeholder:"hh:mm xm",alias:"datetime",hourFormat:"12"},"hh:mm:ss":{mask:"h:s:s",placeholder:"hh:mm:ss",alias:"datetime",autoUnmask:!1},"hh:mm":{mask:"h:s",placeholder:"hh:mm",alias:"datetime",autoUnmask:!1},date:{alias:"dd/mm/yyyy"},"mm/yyyy":{mask:"1/y",placeholder:"mm/yyyy",leapday:"donotuse",separator:"/",alias:"mm/dd/yyyy"}}),inputmask}(jQuery),function(a){return inputmask.extendDefinitions({A:{validator:"[A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]",cardinality:1,casing:"upper"},"#":{validator:"[0-9A-Za-z\u0410-\u044f\u0401\u0451\xc0-\xff\xb5]",cardinality:1,casing:"upper"}}),inputmask.extendAliases({url:{mask:"ir",placeholder:"",separator:"",defaultPrefix:"http://",regex:{urlpre1:new RegExp("[fh]"),urlpre2:new RegExp("(ft|ht)"),urlpre3:new RegExp("(ftp|htt)"),urlpre4:new RegExp("(ftp:|http|ftps)"),urlpre5:new RegExp("(ftp:/|ftps:|http:|https)"),urlpre6:new RegExp("(ftp://|ftps:/|http:/|https:)"),urlpre7:new RegExp("(ftp://|ftps://|http://|https:/)"),urlpre8:new RegExp("(ftp://|ftps://|http://|https://)")},definitions:{i:{validator:function(a,b,c,d,e){return!0},cardinality:8,prevalidator:function(){for(var a=[],b=8,c=0;b>c;c++)a[c]=function(){var a=c;return{validator:function(b,c,d,e,f){if(f.regex["urlpre"+(a+1)]){var g,h=b;a+1-b.length>0&&(h=c.buffer.join("").substring(0,a+1-b.length)+""+h);var i=f.regex["urlpre"+(a+1)].test(h);if(!e&&!i){for(d-=a,g=0;g<f.defaultPrefix.length;g++)c.buffer[d]=f.defaultPrefix[g],d++;for(g=0;g<h.length-1;g++)c.buffer[d]=h[g],d++;return{pos:d}}return i}return!1},cardinality:a}}();return a}()},r:{validator:".",cardinality:50}},insertMode:!1,autoUnmask:!1},ip:{mask:"i[i[i]].i[i[i]].i[i[i]].i[i[i]]",definitions:{i:{validator:function(a,b,c,d,e){return c-1>-1&&"."!=b.buffer[c-1]?(a=b.buffer[c-1]+a,a=c-2>-1&&"."!=b.buffer[c-2]?b.buffer[c-2]+a:"0"+a):a="00"+a,new RegExp("25[0-5]|2[0-4][0-9]|[01][0-9][0-9]").test(a)},cardinality:1}}},email:{mask:"*{1,64}[.*{1,64}][.*{1,64}][.*{1,64}]@*{1,64}[.*{2,64}][.*{2,6}][.*{1,2}]",greedy:!1,onBeforePaste:function(a,b){return a=a.toLowerCase(),a.replace("mailto:","")},definitions:{"*":{validator:"[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]",cardinality:1,casing:"lower"}}}}),inputmask}(jQuery),function(a){return inputmask.extendAliases({numeric:{mask:function(a){function b(b){for(var c="",d=0;d<b.length;d++)c+=a.definitions[b[d]]?"\\"+b[d]:b[d];return c}if(0!==a.repeat&&isNaN(a.integerDigits)&&(a.integerDigits=a.repeat),a.repeat=0,a.groupSeparator==a.radixPoint&&("."==a.radixPoint?a.groupSeparator=",":","==a.radixPoint?a.groupSeparator=".":a.groupSeparator="")," "===a.groupSeparator&&(a.skipOptionalPartCharacter=void 0),a.autoGroup=a.autoGroup&&""!=a.groupSeparator,a.autoGroup&&("string"==typeof a.groupSize&&isFinite(a.groupSize)&&(a.groupSize=parseInt(a.groupSize)),isFinite(a.integerDigits))){var c=Math.floor(a.integerDigits/a.groupSize),d=a.integerDigits%a.groupSize;a.integerDigits=parseInt(a.integerDigits)+(0==d?c-1:c)}a.placeholder.length>1&&(a.placeholder=a.placeholder.charAt(0)),a.radixFocus=a.radixFocus&&"0"==a.placeholder,a.definitions[";"]=a.definitions["~"],a.definitions[";"].definitionSymbol="~";var e=b(a.prefix);return e+="[+]",e+="~{1,"+a.integerDigits+"}",void 0!=a.digits&&(isNaN(a.digits)||parseInt(a.digits)>0)&&(e+=a.digitsOptional?"["+(a.decimalProtect?":":a.radixPoint)+";{"+a.digits+"}]":(a.decimalProtect?":":a.radixPoint)+";{"+a.digits+"}"),""!=a.negationSymbol.back&&(e+="[-]"),e+=b(a.suffix),a.greedy=!1,e},placeholder:"",greedy:!1,digits:"*",digitsOptional:!0,radixPoint:".",radixFocus:!0,groupSize:3,autoGroup:!1,allowPlus:!0,allowMinus:!0,negationSymbol:{front:"-",back:""},integerDigits:"+",prefix:"",suffix:"",rightAlign:!0,decimalProtect:!0,min:void 0,max:void 0,step:1,insertMode:!0,autoUnmask:!1,unmaskAsNumber:!1,postFormat:function(b,c,d,e){var f=!1;b.length>=e.suffix.length&&b.join("").indexOf(e.suffix)==b.length-e.suffix.length&&(b.length=b.length-e.suffix.length,f=!0),c=c>=b.length?b.length-1:c<e.prefix.length?e.prefix.length:c;var g=!1,h=b[c];if(""==e.groupSeparator||-1!=a.inArray(e.radixPoint,b)&&c>a.inArray(e.radixPoint,b)||new RegExp("["+inputmask.escapeRegex(e.negationSymbol.front)+"+]").test(h)){if(f)for(var i=0,j=e.suffix.length;j>i;i++)b.push(e.suffix.charAt(i));return{pos:c}}var k=b.slice();h==e.groupSeparator&&(k.splice(c--,1),h=k[c]),d?h!=e.radixPoint&&(k[c]="?"):k.splice(c,0,"?");var l=k.join(""),m=l;if(l.length>0&&e.autoGroup||d&&-1!=l.indexOf(e.groupSeparator)){var n=inputmask.escapeRegex(e.groupSeparator);g=0==l.indexOf(e.groupSeparator),l=l.replace(new RegExp(n,"g"),"");var o=l.split(e.radixPoint);if(l=""==e.radixPoint?l:o[0],l!=e.prefix+"?0"&&l.length>=e.groupSize+e.prefix.length)for(var p=new RegExp("([-+]?[\\d?]+)([\\d?]{"+e.groupSize+"})");p.test(l);)l=l.replace(p,"$1"+e.groupSeparator+"$2"),l=l.replace(e.groupSeparator+e.groupSeparator,e.groupSeparator);""!=e.radixPoint&&o.length>1&&(l+=e.radixPoint+o[1])}g=m!=l,b.length=l.length;for(var i=0,j=l.length;j>i;i++)b[i]=l.charAt(i);var q=a.inArray("?",b);if(-1==q&&h==e.radixPoint&&(q=a.inArray(e.radixPoint,b)),d?b[q]=h:b.splice(q,1),!g&&f)for(var i=0,j=e.suffix.length;j>i;i++)b.push(e.suffix.charAt(i));return{pos:q,refreshFromBuffer:g,buffer:b}},onBeforeWrite:function(b,c,d,e){if(b&&"blur"==b.type){var f=c.join(""),g=f.replace(e.prefix,"");if(g=g.replace(e.suffix,""),g=g.replace(new RegExp(inputmask.escapeRegex(e.groupSeparator),"g"),""),","===e.radixPoint&&(g=g.replace(inputmask.escapeRegex(e.radixPoint),".")),isFinite(g)&&isFinite(e.min)&&parseFloat(g)<parseFloat(e.min))return a.extend(!0,{refreshFromBuffer:!0,buffer:(e.prefix+e.min).split("")},e.postFormat((e.prefix+e.min).split(""),0,!0,e));var h=""!=e.radixPoint?c.join("").split(e.radixPoint):[c.join("")],i=h[0].match(e.regex.integerPart(e)),j=2==h.length?h[1].match(e.regex.integerNPart(e)):void 0;!i||i[0]!=e.negationSymbol.front+"0"&&i[0]!=e.negationSymbol.front&&"+"!=i[0]||void 0!=j&&!j[0].match(/^0+$/)||c.splice(i.index,1);var k=a.inArray(e.radixPoint,c);if(-1!=k&&isFinite(e.digits)&&!e.digitsOptional){for(var l=1;l<=e.digits;l++)(void 0==c[k+l]||c[k+l]==e.placeholder.charAt(0))&&(c[k+l]="0");return{refreshFromBuffer:!0,buffer:c}}}if(e.autoGroup){var m=e.postFormat(c,d-1,!0,e);return m.caret=d<=e.prefix.length?m.pos:m.pos+1,m}},regex:{integerPart:function(a){return new RegExp("["+inputmask.escapeRegex(a.negationSymbol.front)+"+]?\\d+")},integerNPart:function(a){return new RegExp("[\\d"+inputmask.escapeRegex(a.groupSeparator)+"]+")}},signHandler:function(a,b,c,d,e){if(!d&&e.allowMinus&&"-"===a||e.allowPlus&&"+"===a){var f=b.buffer.join("").match(e.regex.integerPart(e));if(f&&f[0].length>0)return b.buffer[f.index]==("-"===a?"+":e.negationSymbol.front)?"-"==a?""!=e.negationSymbol.back?{pos:f.index,c:e.negationSymbol.front,remove:f.index,caret:c,insert:{pos:b.buffer.length-e.suffix.length-1,c:e.negationSymbol.back}}:{pos:f.index,c:e.negationSymbol.front,remove:f.index,caret:c}:""!=e.negationSymbol.back?{pos:f.index,c:"+",remove:[f.index,b.buffer.length-e.suffix.length-1],
caret:c}:{pos:f.index,c:"+",remove:f.index,caret:c}:b.buffer[f.index]==("-"===a?e.negationSymbol.front:"+")?"-"==a&&""!=e.negationSymbol.back?{remove:[f.index,b.buffer.length-e.suffix.length-1],caret:c-1}:{remove:f.index,caret:c-1}:"-"==a?""!=e.negationSymbol.back?{pos:f.index,c:e.negationSymbol.front,caret:c+1,insert:{pos:b.buffer.length-e.suffix.length,c:e.negationSymbol.back}}:{pos:f.index,c:e.negationSymbol.front,caret:c+1}:{pos:f.index,c:a,caret:c+1}}return!1},radixHandler:function(b,c,d,e,f){if(!e&&b===f.radixPoint&&f.digits>0){var g=a.inArray(f.radixPoint,c.buffer),h=c.buffer.join("").match(f.regex.integerPart(f));if(-1!=g&&c.validPositions[g])return c.validPositions[g-1]?{caret:g+1}:{pos:h.index,c:h[0],caret:g+1};if(!h||"0"==h[0]&&h.index+1!=d)return c.buffer[h?h.index:d]="0",{pos:(h?h.index:d)+1}}return!1},leadingZeroHandler:function(b,c,d,e,f){var g=c.buffer.join("").match(f.regex.integerNPart(f)),h=a.inArray(f.radixPoint,c.buffer);if(g&&!e&&(-1==h||h>=d))if(0==g[0].indexOf("0")){d<f.prefix.length&&(d=g.index);var i=a.inArray(f.radixPoint,c._buffer),j=c._buffer&&c.buffer.slice(h).join("")==c._buffer.slice(i).join("")||0==parseInt(c.buffer.slice(h+1).join("")),k=c._buffer&&c.buffer.slice(g.index,h).join("")==c._buffer.slice(f.prefix.length,i).join("")||"0"==c.buffer.slice(g.index,h).join("");if(-1==h||j&&k)return c.buffer.splice(g.index,1),d=d>g.index?d-1:g.index,{pos:d,remove:g.index};if(g.index+1==d||"0"==b)return c.buffer.splice(g.index,1),d=g.index,{pos:d,remove:g.index}}else if("0"===b&&d<=g.index&&g[0]!=f.groupSeparator)return!1;return!0},postValidation:function(a,b){var c=!0,d=a.join(""),e=d.replace(b.prefix,"");return e=e.replace(b.suffix,""),e=e.replace(new RegExp(inputmask.escapeRegex(b.groupSeparator),"g"),""),","===b.radixPoint&&(e=e.replace(inputmask.escapeRegex(b.radixPoint),".")),e=e.replace(new RegExp("^"+inputmask.escapeRegex(b.negationSymbol.front)),"-"),e=e.replace(new RegExp(inputmask.escapeRegex(b.negationSymbol.back)+"$"),""),isFinite(e)&&isFinite(b.max)&&(c=parseFloat(e)<=parseFloat(b.max)),c},definitions:{"~":{validator:function(b,c,d,e,f){var g=f.signHandler(b,c,d,e,f);if(!g&&(g=f.radixHandler(b,c,d,e,f),!g&&(g=e?new RegExp("[0-9"+inputmask.escapeRegex(f.groupSeparator)+"]").test(b):new RegExp("[0-9]").test(b),g===!0&&(g=f.leadingZeroHandler(b,c,d,e,f),g===!0)))){var h=a.inArray(f.radixPoint,c.buffer);g=-1!=h&&f.digitsOptional===!1&&d>h&&!e?{pos:d,remove:d}:{pos:d}}return g},cardinality:1,prevalidator:null},"+":{validator:function(a,b,c,d,e){var f=e.signHandler(a,b,c,d,e);return!f&&(d&&e.allowMinus&&a===e.negationSymbol.front||e.allowMinus&&"-"==a||e.allowPlus&&"+"==a)&&(f="-"==a?""!=e.negationSymbol.back?{pos:c,c:"-"===a?e.negationSymbol.front:"+",caret:c+1,insert:{pos:b.buffer.length,c:e.negationSymbol.back}}:{pos:c,c:"-"===a?e.negationSymbol.front:"+",caret:c+1}:!0),f},cardinality:1,prevalidator:null,placeholder:""},"-":{validator:function(a,b,c,d,e){var f=e.signHandler(a,b,c,d,e);return!f&&d&&e.allowMinus&&a===e.negationSymbol.back&&(f=!0),f},cardinality:1,prevalidator:null,placeholder:""},":":{validator:function(a,b,c,d,e){var f=e.signHandler(a,b,c,d,e);if(!f){var g="["+inputmask.escapeRegex(e.radixPoint)+"]";f=new RegExp(g).test(a),f&&b.validPositions[c]&&b.validPositions[c].match.placeholder==e.radixPoint&&(f={caret:c+1})}return f},cardinality:1,prevalidator:null,placeholder:function(a){return a.radixPoint}}},onUnMask:function(a,b,c){var d=a.replace(c.prefix,"");return d=d.replace(c.suffix,""),d=d.replace(new RegExp(inputmask.escapeRegex(c.groupSeparator),"g"),""),c.unmaskAsNumber?(d=d.replace(inputmask.escapeRegex.call(this,c.radixPoint),"."),Number(d)):d},isComplete:function(a,b){var c=a.join(""),d=a.slice();if(b.postFormat(d,0,!0,b),d.join("")!=c)return!1;var e=c.replace(b.prefix,"");return e=e.replace(b.suffix,""),e=e.replace(new RegExp(inputmask.escapeRegex(b.groupSeparator),"g"),""),","===b.radixPoint&&(e=e.replace(inputmask.escapeRegex(b.radixPoint),".")),isFinite(e)},onBeforeMask:function(a,b){if(""!=b.radixPoint&&isFinite(a))a=a.toString().replace(".",b.radixPoint);else{var c=a.match(/,/g),d=a.match(/\./g);d&&c?d.length>c.length?(a=a.replace(/\./g,""),a=a.replace(",",b.radixPoint)):c.length>d.length?(a=a.replace(/,/g,""),a=a.replace(".",b.radixPoint)):a=a.indexOf(".")<a.indexOf(",")?a.replace(/\./g,""):a=a.replace(/,/g,""):a=a.replace(new RegExp(inputmask.escapeRegex(b.groupSeparator),"g"),"")}if(0==b.digits&&(-1!=a.indexOf(".")?a=a.substring(0,a.indexOf(".")):-1!=a.indexOf(",")&&(a=a.substring(0,a.indexOf(",")))),""!=b.radixPoint&&isFinite(b.digits)&&-1!=a.indexOf(b.radixPoint)){var e=a.split(b.radixPoint),f=e[1].match(new RegExp("\\d*"))[0];if(parseInt(b.digits)<f.toString().length){var g=Math.pow(10,parseInt(b.digits));a=Math.round(parseFloat(a)*g)/g}}return a.toString()},canClearPosition:function(b,c,d,e,f){var g=b.validPositions[c].input,h=g!=f.radixPoint&&isFinite(g)||c==d||g==f.groupSeparator||g==f.negationSymbol.front||g==f.negationSymbol.back;if(h&&isFinite(g)){var i;if(!e&&b.buffer){i=b.buffer.join("").substr(0,c).match(f.regex.integerNPart(f));var j=c+1,k=null==i||0==parseInt(i[0].replace(new RegExp(inputmask.escapeRegex(f.groupSeparator),"g"),""));if(k)for(;b.validPositions[j]&&(b.validPositions[j].input==f.groupSeparator||"0"==b.validPositions[j].input);)delete b.validPositions[j],j++}var l=[];for(var m in b.validPositions)l.push(b.validPositions[m].input);i=l.join("").match(f.regex.integerNPart(f));var n=a.inArray(f.radixPoint,b.buffer);if(i&&(-1==n||n>=c))if(0==i[0].indexOf("0"))h=i.index!=c||-1==n;else{var o=parseInt(i[0].replace(new RegExp(inputmask.escapeRegex(f.groupSeparator),"g"),""));-1!=n&&10>o&&(b.validPositions[c].input="0",b.p=f.prefix.length+1,h=!1)}}return h},onKeyDown:function(b,c,d,e){var f=a(this);if(b.ctrlKey)switch(b.keyCode){case inputmask.keyCode.UP:f.val(parseFloat(this.inputmask.unmaskedvalue())+parseInt(e.step)),f.triggerHandler("setvalue.inputmask");break;case inputmask.keyCode.DOWN:f.val(parseFloat(this.inputmask.unmaskedvalue())-parseInt(e.step)),f.triggerHandler("setvalue.inputmask")}}},currency:{prefix:"$ ",groupSeparator:",",alias:"numeric",placeholder:"0",autoGroup:!0,digits:2,digitsOptional:!1,clearMaskOnLostFocus:!1},decimal:{alias:"numeric"},integer:{alias:"numeric",digits:0,radixPoint:""},percentage:{alias:"numeric",digits:2,radixPoint:".",placeholder:"0",autoGroup:!1,min:0,max:100,suffix:" %",allowPlus:!1,allowMinus:!1}}),inputmask}(jQuery),function(a){return inputmask.extendAliases({phone:{url:"phone-codes/phone-codes.js",countrycode:"",mask:function(b){b.definitions["#"]=b.definitions[9];var c=[];return a.ajax({url:b.url,async:!1,dataType:"json",success:function(a){c=a},error:function(a,c,d){alert(d+" - "+b.url)}}),c=c.sort(function(a,b){return(a.mask||a)<(b.mask||b)?-1:1})},keepStatic:!1,nojumps:!0,nojumpsThreshold:1,onBeforeMask:function(a,b){var c=a.replace(/^0/g,"");return(c.indexOf(b.countrycode)>1||-1==c.indexOf(b.countrycode))&&(c="+"+b.countrycode+c),c}},phonebe:{alias:"phone",url:"phone-codes/phone-be.js",countrycode:"32",nojumpsThreshold:4}}),inputmask}(jQuery),function(a){return inputmask.extendAliases({Regex:{mask:"r",greedy:!1,repeat:"*",regex:null,regexTokens:null,tokenizer:/\[\^?]?(?:[^\\\]]+|\\[\S\s]?)*]?|\\(?:0(?:[0-3][0-7]{0,2}|[4-7][0-7]?)?|[1-9][0-9]*|x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4}|c[A-Za-z]|[\S\s]?)|\((?:\?[:=!]?)?|(?:[?*+]|\{[0-9]+(?:,[0-9]*)?\})\??|[^.?*+^${[()|\\]+|./g,quantifierFilter:/[0-9]+[^,]/,isComplete:function(a,b){return new RegExp(b.regex).test(a.join(""))},definitions:{r:{validator:function(b,c,d,e,f){function g(a,b){this.matches=[],this.isGroup=a||!1,this.isQuantifier=b||!1,this.quantifier={min:1,max:1},this.repeaterPart=void 0}function h(){var a,b,c=new g,d=[];for(f.regexTokens=[];a=f.tokenizer.exec(f.regex);)switch(b=a[0],b.charAt(0)){case"(":d.push(new g(!0));break;case")":var e=d.pop();d.length>0?d[d.length-1].matches.push(e):c.matches.push(e);break;case"{":case"+":case"*":var h=new g(!1,!0);b=b.replace(/[{}]/g,"");var i=b.split(","),j=isNaN(i[0])?i[0]:parseInt(i[0]),k=1==i.length?j:isNaN(i[1])?i[1]:parseInt(i[1]);if(h.quantifier={min:j,max:k},d.length>0){var l=d[d.length-1].matches;if(a=l.pop(),!a.isGroup){var e=new g(!0);e.matches.push(a),a=e}l.push(a),l.push(h)}else{if(a=c.matches.pop(),!a.isGroup){var e=new g(!0);e.matches.push(a),a=e}c.matches.push(a),c.matches.push(h)}break;default:d.length>0?d[d.length-1].matches.push(b):c.matches.push(b)}c.matches.length>0&&f.regexTokens.push(c)}function i(b,c){var d=!1;c&&(k+="(",m++);for(var e=0;e<b.matches.length;e++){var f=b.matches[e];if(1==f.isGroup)d=i(f,!0);else if(1==f.isQuantifier){var g=a.inArray(f,b.matches),h=b.matches[g-1],j=k;if(isNaN(f.quantifier.max)){for(;f.repeaterPart&&f.repeaterPart!=k&&f.repeaterPart.length>k.length&&!(d=i(h,!0)););d=d||i(h,!0),d&&(f.repeaterPart=k),k=j+f.quantifier.max}else{for(var l=0,o=f.quantifier.max-1;o>l&&!(d=i(h,!0));l++);k=j+"{"+f.quantifier.min+","+f.quantifier.max+"}"}}else if(void 0!=f.matches)for(var p=0;p<f.length&&!(d=i(f[p],c));p++);else{var q;if("["==f.charAt(0)){q=k,q+=f;for(var r=0;m>r;r++)q+=")";var s=new RegExp("^("+q+")$");d=s.test(n)}else for(var t=0,u=f.length;u>t;t++)if("\\"!=f.charAt(t)){q=k,q+=f.substr(0,t+1),q=q.replace(/\|$/,"");for(var r=0;m>r;r++)q+=")";var s=new RegExp("^("+q+")$");if(d=s.test(n))break}k+=f}if(d)break}return c&&(k+=")",m--),d}null==f.regexTokens&&h();var j=c.buffer.slice(),k="",l=!1,m=0;j.splice(d,0,b);for(var n=j.join(""),o=0;o<f.regexTokens.length;o++){var g=f.regexTokens[o];if(l=i(g,g.isGroup))break}return l},cardinality:1}}}}),inputmask}(jQuery);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:74:"/bitrix/templates/aspro_optimus/js/jquery.easing.1.3.min.js?14990989753338";s:6:"source";s:55:"/bitrix/templates/aspro_optimus/js/jquery.easing.1.3.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
jQuery.easing.jswing=jQuery.easing.swing,jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(n,e,t,u,a){return jQuery.easing[jQuery.easing.def](n,e,t,u,a)},easeInQuad:function(n,e,t,u,a){return u*(e/=a)*e+t},easeOutQuad:function(n,e,t,u,a){return-u*(e/=a)*(e-2)+t},easeInOutQuad:function(n,e,t,u,a){return(e/=a/2)<1?u/2*e*e+t:-u/2*(--e*(e-2)-1)+t},easeInCubic:function(n,e,t,u,a){return u*(e/=a)*e*e+t},easeOutCubic:function(n,e,t,u,a){return u*((e=e/a-1)*e*e+1)+t},easeInOutCubic:function(n,e,t,u,a){return(e/=a/2)<1?u/2*e*e*e+t:u/2*((e-=2)*e*e+2)+t},easeInQuart:function(n,e,t,u,a){return u*(e/=a)*e*e*e+t},easeOutQuart:function(n,e,t,u,a){return-u*((e=e/a-1)*e*e*e-1)+t},easeInOutQuart:function(n,e,t,u,a){return(e/=a/2)<1?u/2*e*e*e*e+t:-u/2*((e-=2)*e*e*e-2)+t},easeInQuint:function(n,e,t,u,a){return u*(e/=a)*e*e*e*e+t},easeOutQuint:function(n,e,t,u,a){return u*((e=e/a-1)*e*e*e*e+1)+t},easeInOutQuint:function(n,e,t,u,a){return(e/=a/2)<1?u/2*e*e*e*e*e+t:u/2*((e-=2)*e*e*e*e+2)+t},easeInSine:function(n,e,t,u,a){return-u*Math.cos(e/a*(Math.PI/2))+u+t},easeOutSine:function(n,e,t,u,a){return u*Math.sin(e/a*(Math.PI/2))+t},easeInOutSine:function(n,e,t,u,a){return-u/2*(Math.cos(Math.PI*e/a)-1)+t},easeInExpo:function(n,e,t,u,a){return 0==e?t:u*Math.pow(2,10*(e/a-1))+t},easeOutExpo:function(n,e,t,u,a){return e==a?t+u:u*(-Math.pow(2,-10*e/a)+1)+t},easeInOutExpo:function(n,e,t,u,a){return 0==e?t:e==a?t+u:(e/=a/2)<1?u/2*Math.pow(2,10*(e-1))+t:u/2*(-Math.pow(2,-10*--e)+2)+t},easeInCirc:function(n,e,t,u,a){return-u*(Math.sqrt(1-(e/=a)*e)-1)+t},easeOutCirc:function(n,e,t,u,a){return u*Math.sqrt(1-(e=e/a-1)*e)+t},easeInOutCirc:function(n,e,t,u,a){return(e/=a/2)<1?-u/2*(Math.sqrt(1-e*e)-1)+t:u/2*(Math.sqrt(1-(e-=2)*e)+1)+t},easeInElastic:function(n,e,t,u,a){var r=1.70158,i=0,s=u;if(0==e)return t;if(1==(e/=a))return t+u;if(i||(i=.3*a),s<Math.abs(u)){s=u;var r=i/4}else var r=i/(2*Math.PI)*Math.asin(u/s);return-(s*Math.pow(2,10*(e-=1))*Math.sin((e*a-r)*(2*Math.PI)/i))+t},easeOutElastic:function(n,e,t,u,a){var r=1.70158,i=0,s=u;if(0==e)return t;if(1==(e/=a))return t+u;if(i||(i=.3*a),s<Math.abs(u)){s=u;var r=i/4}else var r=i/(2*Math.PI)*Math.asin(u/s);return s*Math.pow(2,-10*e)*Math.sin((e*a-r)*(2*Math.PI)/i)+u+t},easeInOutElastic:function(n,e,t,u,a){var r=1.70158,i=0,s=u;if(0==e)return t;if(2==(e/=a/2))return t+u;if(i||(i=a*(.3*1.5)),s<Math.abs(u)){s=u;var r=i/4}else var r=i/(2*Math.PI)*Math.asin(u/s);return 1>e?-.5*(s*Math.pow(2,10*(e-=1))*Math.sin((e*a-r)*(2*Math.PI)/i))+t:s*Math.pow(2,-10*(e-=1))*Math.sin((e*a-r)*(2*Math.PI)/i)*.5+u+t},easeInBack:function(n,e,t,u,a,r){return void 0==r&&(r=1.70158),u*(e/=a)*e*((r+1)*e-r)+t},easeOutBack:function(n,e,t,u,a,r){return void 0==r&&(r=1.70158),u*((e=e/a-1)*e*((r+1)*e+r)+1)+t},easeInOutBack:function(n,e,t,u,a,r){return void 0==r&&(r=1.70158),(e/=a/2)<1?u/2*(e*e*(((r*=1.525)+1)*e-r))+t:u/2*((e-=2)*e*(((r*=1.525)+1)*e+r)+2)+t},easeInBounce:function(n,e,t,u,a){return u-jQuery.easing.easeOutBounce(n,a-e,0,u,a)+t},easeOutBounce:function(n,e,t,u,a){return(e/=a)<1/2.75?u*(7.5625*e*e)+t:2/2.75>e?u*(7.5625*(e-=1.5/2.75)*e+.75)+t:2.5/2.75>e?u*(7.5625*(e-=2.25/2.75)*e+.9375)+t:u*(7.5625*(e-=2.625/2.75)*e+.984375)+t},easeInOutBounce:function(n,e,t,u,a){return a/2>e?.5*jQuery.easing.easeInBounce(n,2*e,0,u,a)+t:.5*jQuery.easing.easeOutBounce(n,2*e-a,0,u,a)+.5*u+t}});
/* End */
;
; /* Start:"a:4:{s:4:"full";s:64:"/bitrix/templates/aspro_optimus/js/equalize.min.js?1499098975588";s:6:"source";s:50:"/bitrix/templates/aspro_optimus/js/equalize.min.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
/**
 * equalize.js
 * Author & copyright (c) 2012: Tim Svensen
 * Dual MIT & GPL license
 *
 * Page: http://tsvensen.github.com/equalize.js
 * Repo: https://github.com/tsvensen/equalize.js/
 */
(function(b){b.fn.equalize=function(a){var d=!1,g=!1,c,e;b.isPlainObject(a)?(c=a.equalize||"height",d=a.children||!1,g=a.reset||!1):c=a||"height";if(!b.isFunction(b.fn[c]))return!1;e=0<c.indexOf("eight")?"height":"width";return this.each(function(){var a=d?b(this).find(d):b(this).children(),f=0;a.each(function(){var a=b(this);g&&a.css(e,"");a=a[c]();a>f&&(f=a)});a.css(e,f+"px")})}})(jQuery);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:75:"/bitrix/templates/aspro_optimus/js/jquery.alphanumeric.min.js?1499098975942";s:6:"source";s:57:"/bitrix/templates/aspro_optimus/js/jquery.alphanumeric.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
!function(n){n.fn.alphanumeric=function(r){var a,e,c=n(this),t="abcdefghijklmnopqrstuvwxyz",i=n.extend({ichars:"!@#$%^&*()+=[]\\';,/{}|\":<>?~`.- _",nchars:"",allow:""},r),h=i.allow.split(""),l=0;for(l;l<h.length;l++)-1!=i.ichars.indexOf(h[l])&&(h[l]="\\"+h[l]);return i.nocaps&&(i.nchars+=t.toUpperCase()),i.allcaps&&(i.nchars+=t),i.allow=h.join("|"),e=new RegExp(i.allow,"gi"),a=(i.ichars+i.nchars).replace(e,""),c.keypress(function(n){var r=String.fromCharCode(n.charCode?n.charCode:n.which);-1==a.indexOf(r)||n.ctrlKey||n.preventDefault()}),c.blur(function(){var n=c.val(),r=0;for(r;r<n.length;r++)if(-1!=a.indexOf(n[r]))return c.val(""),!1;return!1}),c},n.fn.numeric=function(r){var a="abcdefghijklmnopqrstuvwxyz",e=a.toUpperCase();return this.each(function(){n(this).alphanumeric(n.extend({nchars:a+e},r))})},n.fn.alpha=function(r){var a="1234567890";return this.each(function(){n(this).alphanumeric(n.extend({nchars:a},r))})}}(jQuery);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:70:"/bitrix/templates/aspro_optimus/js/jquery.cookie.min.js?14990989753066";s:6:"source";s:51:"/bitrix/templates/aspro_optimus/js/jquery.cookie.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
/*!
 * jQuery Cookie Plugin v1.4.0
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as anonymous module.
		define(['jquery'], factory);
	} else {
		// Browser globals.
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (value !== undefined && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setTime(+t + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {};

		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
		var cookies = document.cookie ? document.cookie.split('; ') : [];

		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');

			if (key && key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) === undefined) {
			return false;
		}

		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));

/* End */
;
; /* Start:"a:4:{s:4:"full";s:70:"/bitrix/templates/aspro_optimus/js/jquery.plugin.min.js?14990989753181";s:6:"source";s:55:"/bitrix/templates/aspro_optimus/js/jquery.plugin.min.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
/** Abstract base class for collection plugins v1.0.1.
	Written by Keith Wood (kbwood{at}iinet.com.au) December 2013.
	Licensed under the MIT (http://keith-wood.name/licence.html) license. */
(function(){var j=false;window.JQClass=function(){};JQClass.classes={};JQClass.extend=function extender(f){var g=this.prototype;j=true;var h=new this();j=false;for(var i in f){h[i]=typeof f[i]=='function'&&typeof g[i]=='function'?(function(d,e){return function(){var b=this._super;this._super=function(a){return g[d].apply(this,a||[])};var c=e.apply(this,arguments);this._super=b;return c}})(i,f[i]):f[i]}function JQClass(){if(!j&&this._init){this._init.apply(this,arguments)}}JQClass.prototype=h;JQClass.prototype.constructor=JQClass;JQClass.extend=extender;return JQClass}})();(function($){JQClass.classes.JQPlugin=JQClass.extend({name:'plugin',defaultOptions:{},regionalOptions:{},_getters:[],_getMarker:function(){return'is-'+this.name},_init:function(){$.extend(this.defaultOptions,(this.regionalOptions&&this.regionalOptions[''])||{});var c=camelCase(this.name);$[c]=this;$.fn[c]=function(a){var b=Array.prototype.slice.call(arguments,1);if($[c]._isNotChained(a,b)){return $[c][a].apply($[c],[this[0]].concat(b))}return this.each(function(){if(typeof a==='string'){if(a[0]==='_'||!$[c][a]){throw'Unknown method: '+a;}$[c][a].apply($[c],[this].concat(b))}else{$[c]._attach(this,a)}})}},setDefaults:function(a){$.extend(this.defaultOptions,a||{})},_isNotChained:function(a,b){if(a==='option'&&(b.length===0||(b.length===1&&typeof b[0]==='string'))){return true}return $.inArray(a,this._getters)>-1},_attach:function(a,b){a=$(a);if(a.hasClass(this._getMarker())){return}a.addClass(this._getMarker());b=$.extend({},this.defaultOptions,this._getMetadata(a),b||{});var c=$.extend({name:this.name,elem:a,options:b},this._instSettings(a,b));a.data(this.name,c);this._postAttach(a,c);this.option(a,b)},_instSettings:function(a,b){return{}},_postAttach:function(a,b){},_getMetadata:function(d){try{var f=d.data(this.name.toLowerCase())||'';f=f.replace(/'/g,'"');f=f.replace(/([a-zA-Z0-9]+):/g,function(a,b,i){var c=f.substring(0,i).match(/"/g);return(!c||c.length%2===0?'"'+b+'":':b+':')});f=$.parseJSON('{'+f+'}');for(var g in f){var h=f[g];if(typeof h==='string'&&h.match(/^new Date\((.*)\)$/)){f[g]=eval(h)}}return f}catch(e){return{}}},_getInst:function(a){return $(a).data(this.name)||{}},option:function(a,b,c){a=$(a);var d=a.data(this.name);if(!b||(typeof b==='string'&&c==null)){var e=(d||{}).options;return(e&&b?e[b]:e)}if(!a.hasClass(this._getMarker())){return}var e=b||{};if(typeof b==='string'){e={};e[b]=c}this._optionsChanged(a,d,e);$.extend(d.options,e)},_optionsChanged:function(a,b,c){},destroy:function(a){a=$(a);if(!a.hasClass(this._getMarker())){return}this._preDestroy(a,this._getInst(a));a.removeData(this.name).removeClass(this._getMarker())},_preDestroy:function(a,b){}});function camelCase(c){return c.replace(/-([a-z])/g,function(a,b){return b.toUpperCase()})}$.JQPlugin={createPlugin:function(a,b){if(typeof a==='object'){b=a;a='JQPlugin'}a=camelCase(a);var c=camelCase(b.name);JQClass.classes[c]=JQClass.classes[a].extend(b);new JQClass.classes[c]()}}})(jQuery);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:74:"/bitrix/templates/aspro_optimus/js/jquery.countdown.min.js?149909897513137";s:6:"source";s:58:"/bitrix/templates/aspro_optimus/js/jquery.countdown.min.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
!function(e){var t="countdown",i=0,s=1,n=2,o=3,r=4,a=5,l=6;e.JQPlugin.createPlugin({name:t,defaultOptions:{until:null,since:null,timezone:null,serverSync:null,format:"dHMS",layout:"",compact:!1,padZeroes:!1,significant:0,description:"",expiryUrl:"",expiryText:"",alwaysExpire:!1,onExpiry:null,onTick:null,tickInterval:1},regionalOptions:{"":{labels:["Years","Months","Weeks","Days","Hours","Minutes","Seconds"],labels1:["Year","Month","Week","Day","Hour","Minute","Second"],compactLabels:["y","m","w","d"],whichLabels:null,digits:["0","1","2","3","4","5","6","7","8","9"],timeSeparator:":",isRTL:!1}},_getters:["getTimes"],_rtlClass:t+"-rtl",_sectionClass:t+"-section",_amountClass:t+"-amount",_periodClass:t+"-period",_rowClass:t+"-row",_holdingClass:t+"-holding",_showClass:t+"-show",_descrClass:t+"-descr",_timerElems:[],_init:function(){function t(e){var a=1e12>e?n?performance.now()+performance.timing.navigationStart:s():e||s();a-r>=1e3&&(i._updateElems(),r=a),o(t)}var i=this;this._super(),this._serverSyncs=[];var s="function"==typeof Date.now?Date.now:function(){return(new Date).getTime()},n=window.performance&&"function"==typeof window.performance.now,o=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||null,r=0;!o||e.noRequestAnimationFrame?(e.noRequestAnimationFrame=null,setInterval(function(){i._updateElems()},980)):(r=window.animationStartTime||window.webkitAnimationStartTime||window.mozAnimationStartTime||window.oAnimationStartTime||window.msAnimationStartTime||s(),o(t))},UTCDate:function(e,t,i,s,n,o,r,a){"object"==typeof t&&t.constructor==Date&&(a=t.getMilliseconds(),r=t.getSeconds(),o=t.getMinutes(),n=t.getHours(),s=t.getDate(),i=t.getMonth(),t=t.getFullYear());var l=new Date;return l.setUTCFullYear(t),l.setUTCDate(1),l.setUTCMonth(i||0),l.setUTCDate(s||1),l.setUTCHours(n||0),l.setUTCMinutes((o||0)-(Math.abs(e)<30?60*e:e)),l.setUTCSeconds(r||0),l.setUTCMilliseconds(a||0),l},periodsToSeconds:function(e){return 31557600*e[0]+2629800*e[1]+604800*e[2]+86400*e[3]+3600*e[4]+60*e[5]+e[6]},resync:function(){var t=this;e("."+this._getMarker()).each(function(){var i=e.data(this,t.name);if(i.options.serverSync){for(var s=null,n=0;n<t._serverSyncs.length;n++)if(t._serverSyncs[n][0]==i.options.serverSync){s=t._serverSyncs[n];break}if(null==s[2]){var o=e.isFunction(i.options.serverSync)?i.options.serverSync.apply(this,[]):null;s[2]=(o?(new Date).getTime()-o.getTime():0)-s[1]}i._since&&i._since.setMilliseconds(i._since.getMilliseconds()+s[2]),i._until.setMilliseconds(i._until.getMilliseconds()+s[2])}});for(var i=0;i<t._serverSyncs.length;i++)null!=t._serverSyncs[i][2]&&(t._serverSyncs[i][1]+=t._serverSyncs[i][2],delete t._serverSyncs[i][2])},_instSettings:function(e,t){return{_periods:[0,0,0,0,0,0,0]}},_addElem:function(e){this._hasElem(e)||this._timerElems.push(e)},_hasElem:function(t){return e.inArray(t,this._timerElems)>-1},_removeElem:function(t){this._timerElems=e.map(this._timerElems,function(e){return e==t?null:e})},_updateElems:function(){for(var e=this._timerElems.length-1;e>=0;e--)this._updateCountdown(this._timerElems[e])},_optionsChanged:function(t,i,s){s.layout&&(s.layout=s.layout.replace(/&lt;/g,"<").replace(/&gt;/g,">")),this._resetExtraLabels(i.options,s);var n=i.options.timezone!=s.timezone;e.extend(i.options,s),this._adjustSettings(t,i,null!=s.until||null!=s.since||n);var o=new Date;(i._since&&i._since<o||i._until&&i._until>o)&&this._addElem(t[0]),this._updateCountdown(t,i)},_updateCountdown:function(t,i){if(t=t.jquery?t:e(t),i=i||this._getInst(t),i&&i.options){if(t.html(this._generateHTML(i)).toggleClass(this._rtlClass,i.options.isRTL),e.isFunction(i.options.onTick)){var s="lap"!=i._hold?i._periods:this._calculatePeriods(i,i._show,i.options.significant,new Date);(1==i.options.tickInterval||this.periodsToSeconds(s)%i.options.tickInterval==0)&&i.options.onTick.apply(t[0],[s])}var n="pause"!=i._hold&&(i._since?i._now.getTime()<i._since.getTime():i._now.getTime()>=i._until.getTime());if(n&&!i._expiring){if(i._expiring=!0,this._hasElem(t[0])||i.options.alwaysExpire){if(this._removeElem(t[0]),e.isFunction(i.options.onExpiry)&&i.options.onExpiry.apply(t[0],[]),i.options.expiryText){var o=i.options.layout;i.options.layout=i.options.expiryText,this._updateCountdown(t[0],i),i.options.layout=o}i.options.expiryUrl&&(window.location=i.options.expiryUrl)}i._expiring=!1}else"pause"==i._hold&&this._removeElem(t[0])}},_resetExtraLabels:function(e,t){for(var i in t)i.match(/[Ll]abels[02-9]|compactLabels1/)&&(e[i]=t[i]);for(var i in e)i.match(/[Ll]abels[02-9]|compactLabels1/)&&"undefined"==typeof t[i]&&(e[i]=null)},_adjustSettings:function(t,i,s){for(var n=null,o=0;o<this._serverSyncs.length;o++)if(this._serverSyncs[o][0]==i.options.serverSync){n=this._serverSyncs[o][1];break}if(null!=n)var r=i.options.serverSync?n:0,a=new Date;else{var l=e.isFunction(i.options.serverSync)?i.options.serverSync.apply(t[0],[]):null,a=new Date,r=l?a.getTime()-l.getTime():0;this._serverSyncs.push([i.options.serverSync,r])}var _=i.options.timezone;_=null==_?-a.getTimezoneOffset():_,(s||!s&&null==i._until&&null==i._since)&&(i._since=i.options.since,null!=i._since&&(i._since=this.UTCDate(_,this._determineTime(i._since,null)),i._since&&r&&i._since.setMilliseconds(i._since.getMilliseconds()+r)),i._until=this.UTCDate(_,this._determineTime(i.options.until,a)),r&&i._until.setMilliseconds(i._until.getMilliseconds()+r)),i._show=this._determineShow(i)},_preDestroy:function(e,t){this._removeElem(e[0]),e.empty()},pause:function(e){this._hold(e,"pause")},lap:function(e){this._hold(e,"lap")},resume:function(e){this._hold(e,null)},toggle:function(t){var i=e.data(t,this.name)||{};this[i._hold?"resume":"pause"](t)},toggleLap:function(t){var i=e.data(t,this.name)||{};this[i._hold?"resume":"lap"](t)},_hold:function(t,i){var s=e.data(t,this.name);if(s){if("pause"==s._hold&&!i){s._periods=s._savePeriods;var n=s._since?"-":"+";s[s._since?"_since":"_until"]=this._determineTime(n+s._periods[0]+"y"+n+s._periods[1]+"o"+n+s._periods[2]+"w"+n+s._periods[3]+"d"+n+s._periods[4]+"h"+n+s._periods[5]+"m"+n+s._periods[6]+"s"),this._addElem(t)}s._hold=i,s._savePeriods="pause"==i?s._periods:null,e.data(t,this.name,s),this._updateCountdown(t,s)}},getTimes:function(t){var i=e.data(t,this.name);return i?"pause"==i._hold?i._savePeriods:i._hold?this._calculatePeriods(i,i._show,i.options.significant,new Date):i._periods:null},_determineTime:function(e,t){var i=this,s=function(e){var t=new Date;return t.setTime(t.getTime()+1e3*e),t},n=function(e){e=e.toLowerCase();for(var t=new Date,s=t.getFullYear(),n=t.getMonth(),o=t.getDate(),r=t.getHours(),a=t.getMinutes(),l=t.getSeconds(),_=/([+-]?[0-9]+)\s*(s|m|h|d|w|o|y)?/g,p=_.exec(e);p;){switch(p[2]||"s"){case"s":l+=parseInt(p[1],10);break;case"m":a+=parseInt(p[1],10);break;case"h":r+=parseInt(p[1],10);break;case"d":o+=parseInt(p[1],10);break;case"w":o+=7*parseInt(p[1],10);break;case"o":n+=parseInt(p[1],10),o=Math.min(o,i._getDaysInMonth(s,n));break;case"y":s+=parseInt(p[1],10),o=Math.min(o,i._getDaysInMonth(s,n))}p=_.exec(e)}return new Date(s,n,o,r,a,l,0)},o=null==e?t:"string"==typeof e?n(e):"number"==typeof e?s(e):e;return o&&o.setMilliseconds(0),o},_getDaysInMonth:function(e,t){return 32-new Date(e,t,32).getDate()},_normalLabels:function(e){return e},_generateHTML:function(t){var _=this;t._periods=t._hold?t._periods:this._calculatePeriods(t,t._show,t.options.significant,new Date);for(var p=!1,c=0,u=t.options.significant,d=e.extend({},t._show),h=i;l>=h;h++)p|="?"==t._show[h]&&t._periods[h]>0,d[h]="?"!=t._show[h]||p?t._show[h]:null,c+=d[h]?1:0,u-=t._periods[h]>0?1:0;for(var m=[!1,!1,!1,!1,!1,!1,!1],h=l;h>=i;h--)t._show[h]&&(t._periods[h]?m[h]=!0:(m[h]=u>0,u--));var g=t.options.compact?t.options.compactLabels:t.options.labels,f=t.options.whichLabels||this._normalLabels,w=function(e){var i=t.options["compactLabels"+f(t._periods[e])];return d[e]?_._translateDigits(t,t._periods[e])+(i?i[e]:g[e])+" ":""},y=t.options.padZeroes?2:1,v=function(e){var i=t.options["labels"+f(t._periods[e])];return!t.options.significant&&d[e]||t.options.significant&&m[e]?'<span class="'+_._sectionClass+'"><span class="'+_._amountClass+'">'+_._minDigits(t,t._periods[e],y)+'</span><span class="'+_._periodClass+'">'+(i?i[e]:g[e])+"</span></span>":""};return t.options.layout?this._buildLayout(t,d,t.options.layout,t.options.compact,t.options.significant,m):(t.options.compact?'<span class="'+this._rowClass+" "+this._amountClass+(t._hold?" "+this._holdingClass:"")+'">'+w(i)+w(s)+w(n)+w(o)+(d[r]?this._minDigits(t,t._periods[r],2):"")+(d[a]?(d[r]?t.options.timeSeparator:"")+this._minDigits(t,t._periods[a],2):"")+(d[l]?(d[r]||d[a]?t.options.timeSeparator:"")+this._minDigits(t,t._periods[l],2):""):'<span class="'+this._rowClass+" "+this._showClass+(t.options.significant||c)+(t._hold?" "+this._holdingClass:"")+'">'+v(i)+v(s)+v(n)+v(o)+v(r)+v(a)+v(l))+"</span>"+(t.options.description?'<span class="'+this._rowClass+" "+this._descrClass+'">'+t.options.description+"</span>":"")},_buildLayout:function(t,_,p,c,u,d){for(var h=t.options[c?"compactLabels":"labels"],m=t.options.whichLabels||this._normalLabels,g=function(e){return(t.options[(c?"compactLabels":"labels")+m(t._periods[e])]||h)[e]},f=function(e,i){return t.options.digits[Math.floor(e/i)%10]},w={desc:t.options.description,sep:t.options.timeSeparator,yl:g(i),yn:this._minDigits(t,t._periods[i],1),ynn:this._minDigits(t,t._periods[i],2),ynnn:this._minDigits(t,t._periods[i],3),y1:f(t._periods[i],1),y10:f(t._periods[i],10),y100:f(t._periods[i],100),y1000:f(t._periods[i],1e3),ol:g(s),on:this._minDigits(t,t._periods[s],1),onn:this._minDigits(t,t._periods[s],2),onnn:this._minDigits(t,t._periods[s],3),o1:f(t._periods[s],1),o10:f(t._periods[s],10),o100:f(t._periods[s],100),o1000:f(t._periods[s],1e3),wl:g(n),wn:this._minDigits(t,t._periods[n],1),wnn:this._minDigits(t,t._periods[n],2),wnnn:this._minDigits(t,t._periods[n],3),w1:f(t._periods[n],1),w10:f(t._periods[n],10),w100:f(t._periods[n],100),w1000:f(t._periods[n],1e3),dl:g(o),dn:this._minDigits(t,t._periods[o],1),dnn:this._minDigits(t,t._periods[o],2),dnnn:this._minDigits(t,t._periods[o],3),d1:f(t._periods[o],1),d10:f(t._periods[o],10),d100:f(t._periods[o],100),d1000:f(t._periods[o],1e3),hl:g(r),hn:this._minDigits(t,t._periods[r],1),hnn:this._minDigits(t,t._periods[r],2),hnnn:this._minDigits(t,t._periods[r],3),h1:f(t._periods[r],1),h10:f(t._periods[r],10),h100:f(t._periods[r],100),h1000:f(t._periods[r],1e3),ml:g(a),mn:this._minDigits(t,t._periods[a],1),mnn:this._minDigits(t,t._periods[a],2),mnnn:this._minDigits(t,t._periods[a],3),m1:f(t._periods[a],1),m10:f(t._periods[a],10),m100:f(t._periods[a],100),m1000:f(t._periods[a],1e3),sl:g(l),sn:this._minDigits(t,t._periods[l],1),snn:this._minDigits(t,t._periods[l],2),snnn:this._minDigits(t,t._periods[l],3),s1:f(t._periods[l],1),s10:f(t._periods[l],10),s100:f(t._periods[l],100),s1000:f(t._periods[l],1e3)},y=p,v=i;l>=v;v++){var D="yowdhms".charAt(v),T=new RegExp("\\{"+D+"<\\}([\\s\\S]*)\\{"+D+">\\}","g");y=y.replace(T,!u&&_[v]||u&&d[v]?"$1":"")}return e.each(w,function(e,t){var i=new RegExp("\\{"+e+"\\}","g");y=y.replace(i,t)}),y},_minDigits:function(e,t,i){return t=""+t,t.length>=i?this._translateDigits(e,t):(t="0000000000"+t,this._translateDigits(e,t.substr(t.length-i)))},_translateDigits:function(e,t){return(""+t).replace(/[0-9]/g,function(t){return e.options.digits[t]})},_determineShow:function(e){var t=e.options.format,_=[];return _[i]=t.match("y")?"?":t.match("Y")?"!":null,_[s]=t.match("o")?"?":t.match("O")?"!":null,_[n]=t.match("w")?"?":t.match("W")?"!":null,_[o]=t.match("d")?"?":t.match("D")?"!":null,_[r]=t.match("h")?"?":t.match("H")?"!":null,_[a]=t.match("m")?"?":t.match("M")?"!":null,_[l]=t.match("s")?"?":t.match("S")?"!":null,_},_calculatePeriods:function(e,t,_,p){e._now=p,e._now.setMilliseconds(0);var c=new Date(e._now.getTime());e._since?p.getTime()<e._since.getTime()?e._now=p=c:p=e._since:(c.setTime(e._until.getTime()),p.getTime()>e._until.getTime()&&(e._now=p=c));var u=[0,0,0,0,0,0,0];if(t[i]||t[s]){var d=this._getDaysInMonth(p.getFullYear(),p.getMonth()),h=this._getDaysInMonth(c.getFullYear(),c.getMonth()),m=c.getDate()==p.getDate()||c.getDate()>=Math.min(d,h)&&p.getDate()>=Math.min(d,h),g=function(e){return 60*(60*e.getHours()+e.getMinutes())+e.getSeconds()},f=Math.max(0,12*(c.getFullYear()-p.getFullYear())+c.getMonth()-p.getMonth()+(c.getDate()<p.getDate()&&!m||m&&g(c)<g(p)?-1:0));u[i]=t[i]?Math.floor(f/12):0,u[s]=t[s]?f-12*u[i]:0,p=new Date(p.getTime());var w=p.getDate()==d,y=this._getDaysInMonth(p.getFullYear()+u[i],p.getMonth()+u[s]);p.getDate()>y&&p.setDate(y),p.setFullYear(p.getFullYear()+u[i]),p.setMonth(p.getMonth()+u[s]),w&&p.setDate(y)}var v=Math.floor((c.getTime()-p.getTime())/1e3),D=function(e,i){u[e]=t[e]?Math.floor(v/i):0,v-=u[e]*i};if(D(n,604800),D(o,86400),D(r,3600),D(a,60),D(l,1),v>0&&!e._since)for(var T=[1,12,4.3482,7,24,60,60],M=l,S=1,b=l;b>=i;b--)t[b]&&(u[M]>=S&&(u[M]=0,v=1),v>0&&(u[b]++,v=0,M=b,S=1)),S*=T[b];if(_)for(var b=i;l>=b;b++)_&&u[b]?_--:_||(u[b]=0);return u}})}(jQuery);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:76:"/bitrix/templates/aspro_optimus/js/jquery.countdown-ru.min.js?14990989751011";s:6:"source";s:57:"/bitrix/templates/aspro_optimus/js/jquery.countdown-ru.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
!function(e){e.countdown.regionalOptions.ru={labels:[BX.message("COUNTDOWN_YEAR0"),BX.message("COUNTDOWN_MONTH0"),BX.message("COUNTDOWN_WEAK0"),BX.message("COUNTDOWN_DAY0"),BX.message("COUNTDOWN_HOUR"),BX.message("COUNTDOWN_MIN"),BX.message("COUNTDOWN_SEC")],labels1:[BX.message("COUNTDOWN_YEAR1"),BX.message("COUNTDOWN_MONTH1"),BX.message("COUNTDOWN_WEAK1"),BX.message("COUNTDOWN_DAY1"),BX.message("COUNTDOWN_HOUR"),BX.message("COUNTDOWN_MIN"),BX.message("COUNTDOWN_SEC")],labels2:[BX.message("COUNTDOWN_YEAR2"),BX.message("COUNTDOWN_MONTH2"),BX.message("COUNTDOWN_WEAK2"),BX.message("COUNTDOWN_DAY2"),BX.message("COUNTDOWN_HOUR"),BX.message("COUNTDOWN_MIN"),BX.message("COUNTDOWN_SEC")],compactLabels:["л","м","н","д"],compactLabels1:["г","м","н","д"],whichLabels:function(e){var s=e%10,O=Math.floor(e%100/10);return 1==e?1:s>=2&&4>=s&&1!=O?2:1==s&&1!=O?1:0},digits:["0","1","2","3","4","5","6","7","8","9"],timeSeparator:":",isRTL:!1},e.countdown.setDefaults(e.countdown.regionalOptions.ru)}(jQuery);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:73:"/bitrix/templates/aspro_optimus/js/jquery.ikSelect.min.js?149909897517826";s:6:"source";s:53:"/bitrix/templates/aspro_optimus/js/jquery.ikSelect.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
!function(t,i,e,s){function o(i,e){var s={};this.el=i,this.$el=t(i);for(var o in h)s[o]=this.$el.data(o.toLowerCase());this.options=t.extend({},h,e,s),t.browser.mobile&&(this.options.filter=!1),this.init()}var n,r=t(i),h={syntax:'<div class="ik_select_link"><div class="ik_select_link_text"></div></div><div class="ik_select_dropdown"><div class="ik_select_list"></div></div>',autoWidth:!0,ddFullWidth:!0,equalWidths:!0,dynamicWidth:!1,extractLink:!1,customClass:"",linkCustomClass:"",ddCustomClass:"",ddMaxHeight:200,filter:!1,nothingFoundText:"Nothing found",isDisabled:!1,onInit:function(){},onShow:function(){},onHide:function(){},onKeyUp:function(){},onKeyDown:function(){},onHoverMove:function(){}},l=function(t){t=t.toLowerCase();var i=/(chrome)[ \/]([\w.]+)/.exec(t)||/(webkit)[ \/]([\w.]+)/.exec(t)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(t)||/(msie) ([\w.]+)/.exec(t)||t.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(t)||[];return{browser:i[1]||"",version:i[2]||"0"}};if(!t.browser){var p=l(navigator.userAgent),a={};p.browser&&(a[p.browser]=!0,a.version=p.version),a.chrome?a.webkit=!0:a.webkit&&(a.safari=!0),t.browser=a}t.browser.mobile=/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile/i.test(navigator.userAgent),t.browser.operamini="[object OperaMini]"===Object.prototype.toString.call(i.operamini),t.extend(o.prototype,{init:function(){this.$wrapper=t('<div class="ik_select">'+this.options.syntax+"</div>"),this.$link=t(".ik_select_link",this.$wrapper),this.$linkText=t(".ik_select_link_text",this.$wrapper),this.$dropdown=t(".ik_select_dropdown",this.$wrapper),this.$list=t(".ik_select_list",this.$wrapper),this.$listInner=t('<div class="ik_select_list_inner"/>'),this.$active=t([]),this.$hover=t([]),this.hoverIndex=0,this.$optionSet=t([]),this.$optgroupSet=t([]),this.$list.append(this.$listInner),this.options.filter&&(this.$filter=t([]),this.$optionSetOriginal=t([]),this.$nothingFoundText=t('<div class="ik_select_nothing_found"/>').html(this.options.nothingFoundText),this.$filterWrap=t(".ik_select_filter_wrap",this.$wrapper),this.$filterWrap.length||(this.$filterWrap=t('<div class="ik_select_filter_wrap"/>')),this.$filter=t('<input type="text" class="ik_select_filter">'),this.$filterWrap.append(this.$filter),this.$list.prepend(this.$filterWrap),this.$filter.on({"keydown.ikSelect keyup.ikSelect":t.proxy(this,"_elKeyUpDown"),"keyup.ikSelect":t.proxy(this,"_filterKeyup")})),this.$wrapper.addClass(this.options.customClass),this.$link.addClass(this.options.linkCustomClass||this.options.customClass&&this.options.customClass+"-link"),this.$dropdown.addClass(this.options.ddCustomClass||this.options.customClass&&this.options.customClass+"-dd"),this.reset(),this.toggle(!(this.options.isDisabled||this.$el.prop("disabled")||this.$el.hasClass("hidden"))),this.$link.on("click.ikSelect",t.proxy(this,"_linkClick")),this.$el.on({"focus.ikSelect":t.proxy(this,"_elFocus"),"blur.ikSelect":t.proxy(this,"_elBlur"),"change.ikSelect":t.proxy(this,"_syncOriginalOption"),"keydown.ikSelect keyup.ikSelect":t.proxy(this,"_elKeyUpDown")}),this.$list.on({"click.ikSelect":t.proxy(this,"_optionClick"),"mouseover.ikSelect":t.proxy(this,"_optionMouseover")},".ik_select_option"),this.$wrapper.on("click",function(){return!1}),this.$el.after(this.$wrapper),this.redraw(),this.$el.appendTo(this.$wrapper),this.options.onInit(this),this.$el.trigger("ikinit",this)},_linkClick:function(){this.isDisabled||(this===n?this.hideDropdown():this.showDropdown())},_optionClick:function(){this._makeOptionActive(this.searchIndexes?this.$optionSetOriginal.index(this.$hover):this.hoverIndex,!0),this.hideDropdown(),this.$el.change().focus()},_optionMouseover:function(i){var e=t(i.currentTarget);e.hasClass("ik_select_option_disabled")||(this.$hover.removeClass("ik_select_hover"),this.$hover=e.addClass("ik_select_hover"),this.hoverIndex=this.$optionSet.index(this.$hover))},_makeOptionActive:function(i,e){var s=t(this.el.options[i]),o="";s.data("img_src")&&(o+='<span class="icon_color" style="background: url('+s.data("img_src")+')"></span>',this.$link.addClass("img")),s.attr("title")&&this.$link.attr("title",s.attr("title")),o+=s.text(),this.$linkText.html(o),this.$link.toggleClass("ik_select_link_novalue",!s.attr("value")),this.$hover.removeClass("ik_select_hover"),this.$active.removeClass("ik_select_active"),this.$hover=this.$active=this.$optionSet.eq(i).addClass("ik_select_hover ik_select_active"),this.hoverIndex=i,e&&this._syncFakeOption()},_elKeyUpDown:function(i){var e,s=t(i.currentTarget),o=i.type,n=i.which;switch(n){case 38:"keydown"===o&&(i.preventDefault(),this._moveToPrevActive());break;case 40:"keydown"===o&&(i.preventDefault(),this._moveToNextActive());break;case 33:"keydown"===o&&(i.preventDefault(),e=this.$hover.position().top-this.$listInner.height(),this._moveToPrevActive(function(t){return e>=t}));break;case 34:"keydown"===o&&(i.preventDefault(),e=this.$hover.position().top+this.$listInner.height(),this._moveToNextActive(function(t){return t>=e}));break;case 36:"keydown"===o&&s.is(this.$el)&&(i.preventDefault(),this._moveToFirstActive());break;case 35:"keydown"===o&&s.is(this.$el)&&(i.preventDefault(),this._moveToLastActive());break;case 32:"keydown"===o&&s.is(this.$el)&&(i.preventDefault(),this.$dropdown.is(":visible")?this.$hover.click():this._linkClick());break;case 13:"keydown"===o&&this.$dropdown.is(":visible")&&(i.preventDefault(),this.$hover.click());break;case 27:"keydown"===o&&this.$dropdown.is(":visible")&&(i.preventDefault(),this.hideDropdown());break;case 9:"keydown"===o&&(t.browser.webkit&&this.$dropdown.is(":visible")?i.preventDefault():this.hideDropdown());break;default:"keyup"===o&&s.is(this.$el)&&this._syncOriginalOption()}"keyup"===o&&t.browser.mozilla&&this._syncFakeOption(),"keydown"===o&&(this.options.onKeyDown(this,n),this.$el.trigger("ikkeydown",[this,n])),"keyup"===o&&(this.options.onKeyUp(this,n),this.$el.trigger("ikkeyup",[this,n]))},_moveTo:function(i){var e,s,o;return!this.$dropdown.is(":visible")&&t.browser.webkit?(this.showDropdown(),this):(!this.$dropdown.is(":visible")||t.browser.mozilla?this._makeOptionActive(i,!0):(this.$hover.removeClass("ik_select_hover"),this.$hover=this.$optionSet.eq(i).addClass("ik_select_hover"),this.hoverIndex=i),e=this.$hover.position().top,s=e+this.$active.outerHeight(),this.$hover.index()||(o=this.$hover.closest(".ik_select_optgroup"),o.length&&(e=o.position().top)),s>this.$listInner.height()?this.$listInner.scrollTop(this.$listInner.scrollTop()+s-this.$listInner.height()):0>e&&this.$listInner.scrollTop(this.$listInner.scrollTop()+e),this.options.onHoverMove(this),void this.$el.trigger("ikhovermove",this))},_moveToFirstActive:function(){for(var t=0;t<this.$optionSet.length;t++)if(!this.$optionSet.eq(t).hasClass("ik_select_option_disabled")){this._moveTo(t);break}},_moveToLastActive:function(){for(var t=this.$optionSet.length-1;t>=0;t++)if(!this.$optionSet.eq(t).hasClass("ik_select_option_disabled")){this._moveTo(t);break}},_moveToPrevActive:function(t){for(var i,e=this.hoverIndex-1;e>=0;e--)if(i=this.$optionSet.eq(e),!i.hasClass("ik_select_option_disabled")&&("undefined"==typeof t||t(i.position().top))){this._moveTo(e);break}},_moveToNextActive:function(t){for(var i,e=this.hoverIndex+1;e<this.$optionSet.length;e++)if(i=this.$optionSet.eq(e),!i.hasClass("ik_select_option_disabled")&&("undefined"==typeof t||t(i.position().top))){this._moveTo(e);break}},_elFocus:function(){var t,i,e,s;return this.isDisabled?this:(this.$link.addClass("ik_select_link_focus"),t=this.$wrapper.offset().top,i=this.$wrapper.height(),e=r.scrollTop(),s=r.height(),void((t+i>e+s||e>t)&&r.scrollTop(t-s/2)))},_elBlur:function(){this.$link.removeClass("ik_select_link_focus")},_filterKeyup:function(){var i,e=t.trim(this.$filter.val());this.$listInner.show(),"undefined"==typeof this.searchIndexes&&(this.$optionSetOriginal=this.$optionSet,this.searchIndexes=t.makeArray(this.$optionSet.map(function(i,e){return t(e).text().toLowerCase()}))),e!==i&&(""===e?(this.$optionSet=this.$optionSetOriginal.show(),this.$optgroupSet.show(),this.$nothingFoundText.remove()):(this.$optionSet=t([]),this.$optgroupSet.show(),this.$optionSetOriginal.each(t.proxy(function(i,s){var o=t(s);this.searchIndexes[i].indexOf(e.toLowerCase())>=0?(this.$optionSet=this.$optionSet.add(o),o.show()):o.hide()},this)),this.$optionSet.length?(this.$nothingFoundText.remove(),this.$optgroupSet.each(function(i,e){var s=t(e);t(".ik_select_option:visible",s).length||s.hide()}),this.$hover.is(":visible")||this._moveToFirstActive()):(this.$listInner.hide(),this.$list.append(this.$nothingFoundText))),i=e)},_syncFakeOption:function(){this.el.selectedIndex=this.hoverIndex},_syncOriginalOption:function(){this._makeOptionActive(this.el.selectedIndex)},_fixHeight:function(){this.$dropdown.show(),this.$listInner.css("height","auto"),this.$listInner.height()>this.options.ddMaxHeight&&this.$listInner.css({overflow:"auto",height:this.options.ddMaxHeight,position:"relative"}),this.$dropdown.hide()},redraw:function(){var i,e,s;this.options.filter&&this.$filter.hide(),this.$wrapper.css({position:"relative"}),this.$dropdown.css({position:"absolute",zIndex:9998,width:"100%"}),this.$list.css({position:"relative"}),this._fixHeight(),(this.options.dynamicWidth||this.options.autoWidth||this.options.ddFullWidth)&&(this.$wrapper.width(""),this.$dropdown.show().width(9999),this.$listInner.css("float","left"),this.$list.css("float","left"),i=this.$list.outerWidth(!0),e=this.$listInner.width()-this.$listInnerUl.width(),this.$list.css("float",""),this.$listInner.css("float",""),this.$dropdown.css("width","100%"),this.options.ddFullWidth&&this.$dropdown.width(i+e),this.options.dynamicWidth?this.$wrapper.css({display:"inline-block",width:"auto",verticalAlign:"top"}):this.options.autoWidth&&this.$wrapper.width(i+(this.options.equalWidths?e:0)).addClass("ik_select_autowidth"),s=this.$wrapper.parent().width(),this.$wrapper.width()>s&&this.$wrapper.width(s)),this.options.filter&&this.$filter.show().outerWidth(this.$filterWrap.width()),this.$dropdown.hide(),this.$el.css({position:"absolute",margin:0,padding:0,top:0,left:-9999}),t.browser.mobile&&this.$el.css({opacity:0,left:0,height:this.$wrapper.height(),width:this.$wrapper.width()})},reset:function(){var i="";this.$linkText.html(this.$el.val()),this.$listInner.empty(),i="<ul>",this.$el.children().each(t.proxy(function(e,s){var o,n=t(s),r=s.tagName.toLowerCase();"optgroup"===r?(o=n.children().map(t.proxy(function(i,e){return this._generateOptionObject(t(e))},this)),o=t.makeArray(o),i+=this._renderListOptgroup({label:n.attr("label")||"&nbsp;",isDisabled:n.is(":disabled"),options:o})):"option"===r&&(i+=this._renderListOption(this._generateOptionObject(n)))},this)),i+="</ul>",this.$listInner.append(i),this._syncOriginalOption(),this.$listInnerUl=t("> ul",this.$listInner),this.$optgroupSet=t(".ik_select_optgroup",this.$listInner),this.$optionSet=t(".ik_select_option",this.$listInner)},hideDropdown:function(){this.options.filter&&(this.$filter.val(""),this._filterKeyup()),this.$dropdown.hide().appendTo(this.$wrapper).css({left:"",top:""}),this.options.extractLink&&(this.$wrapper.outerWidth(this.$wrapper.data("outerWidth")),this.$wrapper.height(""),this.$link.removeClass("ik_select_link_extracted").css({position:"",top:"",left:"",zIndex:""}).prependTo(this.$wrapper)),n=null,this.$el.focus(),this.options.onHide(this),this.$el.trigger("ikhide",this)},showDropdown:function(){var i,e,s,o,h,l,p,a,d;n!==this&&this.$optionSet.length&&(n&&n.hideDropdown(),this._syncOriginalOption(),t(".show_un_props").length?this.$dropdown.addClass("show_un_props"):t(".unshow_un_props").length&&this.$dropdown.addClass("unshow_un_props"),this.$dropdown.show(),i=this.$dropdown.offset(),e=this.$dropdown.outerWidth(!0),s=this.$dropdown.outerHeight(!0),o=this.$wrapper.offset(),l=r.width(),p=r.height(),a=r.scrollTop(),this.options.ddFullWidth&&o.left+e>l&&(i.left=l-e),i.top+s>a+p&&(i.top=a+p-s),this.$dropdown.css({left:i.left,top:i.top,width:this.$dropdown.width()}).appendTo("body"),this.options.extractLink&&(d=this.$link.offset(),h=this.$wrapper.outerWidth(),this.$wrapper.data("outerWidth",h),this.$wrapper.outerWidth(h),this.$wrapper.outerHeight(this.$wrapper.outerHeight()),this.$link.outerWidth(this.$link.outerWidth()),this.$link.addClass("ik_select_link_extracted").css({position:"absolute",top:d.top,left:d.left,zIndex:9999}).appendTo("body")),this.$listInner.scrollTop(this.$active.position().top-this.$list.height()/2),this.options.filter?this.$filter.focus():this.$el.focus(),n=this,this.options.onShow(this),this.$el.trigger("ikshow",this))},_generateOptionObject:function(t){return{value:t.val(),label:t.html()||"&nbsp;",isDisabled:t.is(":disabled"),dataParam:t.data(),className:t.attr("class"),title:t.attr("title")}},_renderListOption:function(t){var i,e=t.isDisabled?" ik_select_option_disabled":"",s=t.dataParam.img_src?" img":"",o="hidden"==t.className?" missing":"",n=t.title;return i='<li class="ik_select_option'+e+s+o+'" title="'+n+'" data-value="'+t.value+'">',i+='<span class="ik_select_option_label" title="'+n+'">',t.dataParam.img_src&&(i+='<span class="icon_color" style="background: url('+t.dataParam.img_src+')"></span>'),i+=t.label,i+="</span>",i+="</li>"},_renderListOptgroup:function(i){var e,s=i.isDisabled?" ik_select_optgroup_disabled":"";return e='<li class="ik_select_optgroup'+s+'">',e+='<div class="ik_select_optgroup_label">'+i.label+"</div>",e+="<ul>",t.isArray(i.options)&&t.each(i.options,t.proxy(function(t,i){e+=this._renderListOption({value:i.value,label:i.label||"&nbsp;",isDisabled:i.isDisabled})},this)),e+="</ul>",e+="</li>"},_renderOption:function(t){return'<option value="'+t.value+'">'+t.label+"</option>"},_renderOptgroup:function(i){var e;return e='<optgroup label="'+i.label+'">',t.isArray(i.options)&&t.each(i.options,t.proxy(function(t,i){e+=this._renderOption(i)},this)),e+="</option>"},addOptions:function(i,e,s){var o,n,r="",h="",l=this.$listInnerUl,p=this.$el;i=t.isArray(i)?i:[i],t.each(i,t.proxy(function(t,i){r+=this._renderListOption(i),h+=this._renderOption(i)},this)),t.isNumeric(s)&&s<this.$optgroupSet.length&&(l=this.$optgroupSet.eq(s),p=t("optgroup",this.$el).eq(s)),t.isNumeric(e)&&(o=t(".ik_select_option",l),e<o.length&&(o.eq(e).before(r),n=t("option",p),n.eq(e).before(h))),n||(l.append(r),p.append(h)),this.$optionSet=t(".ik_select_option",this.$listInner),this._fixHeight()},addOptgroups:function(i,e){var s="",o="";i&&(i=t.isArray(i)?i:[i],t.each(i,t.proxy(function(t,i){s+=this._renderListOptgroup(i),o+=this._renderOptgroup(i)},this)),t.isNumeric(e)&&e<this.$optgroupSet.length?(this.$optgroupSet.eq(e).before(s),t("optgroup",this.$el).eq(e).before(o)):(this.$listInnerUl.append(s),this.$el.append(o)),this.$optgroupSet=t(".ik_select_optgroup",this.$listInner),this.$optionSet=t(".ik_select_option",this.$listInner),this._fixHeight())},removeOptions:function(i,e){var s,o,n=t([]);t.isNumeric(e)&&(0>e?(s=t("> .ik_select_option",this.$listInnerUl),o=t("> option",this.$el)):e<this.$optgroupSet.length&&(s=t(".ik_select_option",this.$optgroupSet.eq(e)),o=t("optgroup",this.$el).eq(e).find("option"))),s||(s=this.$optionSet,o=t(this.el.options)),t.isArray(i)||(i=[i]),t.each(i,t.proxy(function(t,i){i<s.length&&(n=n.add(s.eq(i)).add(o.eq(i)))},this)),n.remove(),this.$optionSet=t(".ik_select_option",this.$listInner),this._syncOriginalOption(),this._fixHeight()},removeOptgroups:function(i){var e=t([]),s=t("optgroup",this.el);t.isArray(i)||(i=[i]),t.each(i,t.proxy(function(t,i){i<this.$optgroupSet.length&&(e=e.add(this.$optgroupSet.eq(i)).add(s.eq(i)))},this)),e.remove(),this.$optionSet=t(".ik_select_option",this.$listInner),this.$optgroupSet=t(".ik_select_optgroup",this.$listInner),this._syncOriginalOption(),this._fixHeight()},disable:function(){this.toggle(!1)},enable:function(){this.toggle(!0)},toggle:function(t){this.isDisabled="undefined"!=typeof t?!t:!this.isDisabled,this.$el.prop("disabled",this.isDisabled),this.$link.toggleClass("ik_select_link_disabled",this.isDisabled)},select:function(t,i){i?this.el.selectedIndex=t:this.$el.val(t),this._syncOriginalOption()},disableOptgroups:function(t){this.toggleOptgroups(t,!1)},enableOptgroups:function(t){this.toggleOptgroups(t,!0)},toggleOptgroups:function(i,e){t.isArray(i)||(i=[i]),t.each(i,t.proxy(function(i,s){var o,n,r,h=[],l=t("optgroup",this.$el).eq(s);o="undefined"!=typeof e?e:l.prop("disabled"),l.prop("disabled",!o),this.$optgroupSet.eq(s).toggleClass("ik_select_optgroup_disabled",!o),n=t("option",l),r=t(this.el.options).index(n.eq(0));for(var p=r;p<r+n.length;p++)h.push(p);this.toggleOptions(h,!0,o)},this)),this._syncOriginalOption()},disableOptions:function(t,i){this.toggleOptions(t,i,!1)},enableOptions:function(t,i){this.toggleOptions(t,i,!0)},toggleOptions:function(i,e,s){var o=t("option",this.el);t.isArray(i)||(i=[i]);var n=t.proxy(function(t,i){var e="undefined"!=typeof s?s:t.prop("disabled");t.prop("disabled",!e),this.$optionSet.eq(i).toggleClass("ik_select_option_disabled",!e)},this);t.each(i,function(i,s){e?n(o.eq(s),s):o.each(function(i,e){var o=t(e);return o.val()===s?(n(o,i),this):void 0})}),this._syncOriginalOption()},detach:function(){this.$el.off(".ikSelect").css({width:"",height:"",left:"",top:"",position:"",margin:"",padding:""}),this.$wrapper.before(this.$el),this.$wrapper.remove(),this.$el.removeData("plugin_ikSelect")}}),t.fn.ikSelect=function(i){var e;return t.browser.operamini?this:(e=Array.prototype.slice.call(arguments,1),this.each(function(){var s;t.data(this,"plugin_ikSelect")?"string"==typeof i&&(s=t.data(this,"plugin_ikSelect"),"function"==typeof s[i]&&s[i].apply(s,e)):t.data(this,"plugin_ikSelect",new o(this,i))}))},t.ikSelect={extendDefaults:function(i){t.extend(h,i)}},t(e).bind("click.ikSelect",function(){n&&n.hideDropdown()})}(jQuery,window,document);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:61:"/bitrix/templates/aspro_optimus/js/sly.min.js?149909897517577";s:6:"source";s:41:"/bitrix/templates/aspro_optimus/js/sly.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
!function(e,t,a){"use strict";function n(t,p,h){function D(a){var n=0,i=ke.length;if(we.old=e.extend({},we),me=pe?0:he[fe.horizontal?"width":"height"](),Ie=be[fe.horizontal?"width":"height"](),ye=pe?t:ge[fe.horizontal?"outerWidth":"outerHeight"](),ke.length=0,we.start=0,we.end=q(ye-me,0),Ye){n=xe.length,Te=ge.children(fe.itemSelector),xe.length=0;var r,s=c(ge,fe.horizontal?"paddingLeft":"paddingTop"),o=c(ge,fe.horizontal?"paddingRight":"paddingBottom"),l="border-box"===e(Te).css("boxSizing"),u="none"!==Te.css("float"),f=0,v=Te.length-1;ye=0,Te.each(function(t,a){var n=e(a),i=a.getBoundingClientRect(),l=A(fe.horizontal?i.width||i.right-i.left:i.height||i.bottom-i.top),d=c(n,fe.horizontal?"marginLeft":"marginTop"),p=c(n,fe.horizontal?"marginRight":"marginBottom");n.parents(".frame.props").length&&n.find("td").eq(0).length&&(l-=n.find("td").eq(0).outerWidth());var h=l+d+p,g=!d||!p,m={};m.el=a,m.size=g?l:h,m.half=m.size/2,m.start=ye+(g?d:0),m.center=m.start-A(me/2-m.size/2),m.end=m.start-me+m.size,t||(ye+=s),ye+=h,fe.horizontal||u||p&&d&&t>0&&(ye-=O(d,p)),t===v&&(m.end+=o,ye+=o,f=g?p:0),xe.push(m),r=m}),ge[0].style[fe.horizontal?"width":"height"]=(l?ye:ye-s-o)+"px",ye-=f,xe.length?(we.start=xe[0][Xe?"center":"start"],we.end=Xe?r.center:ye>me?r.end:we.start):we.start=we.end=0}if(we.center=A(we.end/2+we.start/2),W(),Pe.length&&Ie>0&&(fe.dynamicHandle?(Ce=we.start===we.end?Ie:A(Ie*me/ye),Ce=d(Ce,fe.minHandleSize,Ie),Pe[0].style[fe.horizontal?"width":"height"]=Ce+"px",Ce!=Ie?(e(be).parent().animate({opacity:1},50),e(be).fadeIn(),e(be).closest(".wrapp_scrollbar").find(".slider_navigation").fadeIn(),e(he).hasClass("props")&&e(he).addClass("border")):(e(be).parent().animate({opacity:0},50),e(be).fadeOut(),e(be).closest(".wrapp_scrollbar").find(".slider_navigation").fadeOut(),e(he).hasClass("props")&&e(he).removeClass("border"))):Ce=Pe[fe.horizontal?"outerWidth":"outerHeight"](),ze.end=Ie-Ce,Ke||X()),!pe&&me>0){var p=we.start,h="";if(Ye)e.each(xe,function(e,t){Xe?ke.push(t.center):t.start+t.size>p&&p<=we.end&&(p=t.start,ke.push(p),p+=me,p>we.end&&p<we.end+me&&ke.push(we.end))});else for(;p-me<we.end;)ke.push(p),p+=me;if(Se[0]&&i!==ke.length){for(var g=0;g<ke.length;g++)h+=fe.pageBuilder.call(ve,g);Be=Se.html(h).children(),Be.eq(Ae.activePage).addClass(fe.activeClass)}}if(Ae.slideeSize=ye,Ae.frameSize=me,Ae.sbSize=Ie,Ae.handleSize=Ce,Ye){a&&null!=fe.startAt&&(L(fe.startAt),ve[He?"toCenter":"toStart"](fe.startAt));var m=xe[Ae.activeItem];E(He&&m?m.center:d(we.dest,we.start,we.end))}else a?null!=fe.startAt&&E(fe.startAt,1):E(d(we.dest,we.start,we.end));ce("load")}function E(e,t,a){Je.init&&Je.slidee&&fe.elasticBounds?e>we.end?e=we.end+(e-we.end)/6:e<we.start&&(e=we.start+(e-we.start)/6):e=d(e,we.start,we.end),Ze.start=+new Date,Ze.time=0,Ze.from=we.cur,Ze.to=e,Ze.delta=e-we.cur,Ze.tweesing=Je.tweese||Je.init&&!Je.slidee,Ze.immediate=!Ze.tweesing&&(t||Je.init&&Je.slidee||!fe.speed),Je.tweese=0,e!==we.dest&&(we.dest=e,ce("change"),Ke||N()),Q(),W(),_(),H()}function N(){if(ve.initialized){if(!Ke)return Ke=y(N),void(Je.released&&ce("moveStart"));Ze.immediate?we.cur=Ze.to:Ze.tweesing?(Ze.tweeseDelta=Ze.to-we.cur,we.cur+=Ze.tweeseDelta*(Je.released?fe.swingSpeed:fe.syncSpeed)):(Ze.time=O(+new Date-Ze.start,fe.speed),we.cur=Ze.from+Ze.delta*e.easing[fe.easing](Ze.time/fe.speed,Ze.time,0,1,fe.speed)),Ze.to===we.cur?(we.cur=Ze.to,Je.tweese=Ke=0):Ke=y(N),ce("move"),pe||(f?ge[0].style[f]=v+(fe.horizontal?"translateX":"translateY")+"("+-we.cur+"px)":ge[0].style[fe.horizontal?"left":"top"]=-A(we.cur)+"px"),!Ke&&Je.released&&ce("moveEnd"),X()}}function X(){Pe.length&&(ze.cur=we.start===we.end?0:((Je.init&&!Je.slidee?we.dest:we.cur)-we.start)/(we.end-we.start)*ze.end,ze.cur=d(A(ze.cur),ze.start,ze.end),Qe.hPos!==ze.cur&&(Qe.hPos=ze.cur,f?Pe[0].style[f]=v+(fe.horizontal?"translateX":"translateY")+"("+ze.cur+"px)":Pe[0].style[fe.horizontal?"left":"top"]=ze.cur+"px"))}function H(){Be[0]&&Qe.page!==Ae.activePage&&(Qe.page=Ae.activePage,Be.removeClass(fe.activeClass).eq(Ae.activePage).addClass(fe.activeClass),ce("activePage",Qe.page))}function Y(){tt=Je.init?y(Y):0,Ge.now=+new Date,Ge.pos=we.cur+(Ge.now-Ge.lastTime)/1e3*Ge.speed,E(Je.init?Ge.pos:A(Ge.pos)),Je.init||we.cur!==we.dest||ce("moveEnd"),Ge.lastTime=Ge.now}function F(e,t,n){if("boolean"===i(t)&&(n=t,t=a),t===a)E(we[e],n);else{if(He&&"center"!==e)return;var r=ve.getPos(t);r&&E(r[e],n,!He)}}function j(e){return null!=e?l(e)?e>=0&&e<xe.length?e:-1:Te.index(e):-1}function M(e){return j(l(e)&&0>e?e+xe.length:e)}function L(e,t){var a=j(e);return!Ye||0>a?!1:((Qe.active!==a||t)&&(Te.eq(Ae.activeItem).removeClass(fe.activeClass),Te.eq(a).addClass(fe.activeClass),Qe.active=Ae.activeItem=a,_(),ce("active",a)),a)}function R(e){e=d(l(e)?e:we.dest,we.start,we.end);var t={},a=Xe?0:me/2;if(!pe)for(var n=0,i=ke.length;i>n;n++){if(e>=we.end||n===ke.length-1){t.activePage=ke.length-1;break}if(e<=ke[n]+a){t.activePage=n;break}}if(Ye){for(var r=!1,s=!1,o=!1,c=0,u=xe.length;u>c;c++)if(r===!1&&e<=xe[c].start+xe[c].half&&(r=c),o===!1&&e<=xe[c].center+xe[c].half&&(o=c),c===u-1||e<=xe[c].end+xe[c].half){s=c;break}t.firstItem=l(r)?r:0,t.centerItem=l(o)?o:t.firstItem,t.lastItem=l(s)?s:t.centerItem}return t}function W(t){e.extend(Ae,R(t))}function _(){var e=we.dest<=we.start,t=we.dest>=we.end,a=(e?1:0)|(t?2:0);if(Qe.slideePosState!==a&&(Qe.slideePosState=a,_e.is("button,input")&&_e.prop("disabled",e),Ue.is("button,input")&&Ue.prop("disabled",t),_e.add(Le)[e?"addClass":"removeClass"](fe.disabledClass),Ue.add(Me)[t?"addClass":"removeClass"](fe.disabledClass)),Qe.fwdbwdState!==a&&Je.released&&(Qe.fwdbwdState=a,Le.is("button,input")&&Le.prop("disabled",e),Me.is("button,input")&&Me.prop("disabled",t)),Ye&&null!=Ae.activeItem){var n=0===Ae.activeItem,i=Ae.activeItem>=xe.length-1,r=(n?1:0)|(i?2:0);Qe.itemsButtonState!==r&&(Qe.itemsButtonState=r,Re.is("button,input")&&Re.prop("disabled",n),We.is("button,input")&&We.prop("disabled",i),Re[n?"addClass":"removeClass"](fe.disabledClass),We[i?"addClass":"removeClass"](fe.disabledClass))}}function U(e,t,a){if(e=M(e),t=M(t),e>-1&&t>-1&&e!==t&&(!a||t!==e-1)&&(a||t!==e+1)){Te.eq(e)[a?"insertAfter":"insertBefore"](xe[t].el);var n=t>e?e:a?t:t-1,i=e>t?e:a?t+1:t,r=e>t;null!=Ae.activeItem&&(e===Ae.activeItem?Qe.active=Ae.activeItem=a?r?t+1:t:r?t:t-1:Ae.activeItem>n&&Ae.activeItem<i&&(Qe.active=Ae.activeItem+=r?1:-1)),D()}}function $(e,t){for(var a=0,n=$e[e].length;n>a;a++)if($e[e][a]===t)return a;return-1}function Q(){Je.released&&!ve.isPaused&&ve.resume()}function Z(e){return A(d(e,ze.start,ze.end)/ze.end*(we.end-we.start))+we.start}function G(){Je.history[0]=Je.history[1],Je.history[1]=Je.history[2],Je.history[2]=Je.history[3],Je.history[3]=Je.delta}function J(e){Je.released=0,Je.source=e,Je.slidee="slidee"===e}function K(t){var a="touchstart"===t.type,n=t.data.source,i="slidee"===n;Je.init||!a&&te(t.target)||("handle"!==n||fe.dragHandle&&ze.start!==ze.end)&&(!i||(a?fe.touchDragging:fe.mouseDragging&&t.which<2))&&(a||r(t),J(n),Je.init=0,Je.$source=e(t.target),Je.touch=a,Je.pointer=a?t.originalEvent.touches[0]:t,Je.initX=Je.pointer.pageX,Je.initY=Je.pointer.pageY,Je.initPos=i?we.cur:ze.cur,Je.start=+new Date,Je.time=0,Je.path=0,Je.delta=0,Je.locked=0,Je.history=[0,0,0,0],Je.pathToLock=i?a?30:10:0,w.on(a?I:P,V),ve.pause(1),(i?ge:Pe).addClass(fe.draggedClass),ce("moveStart"),i&&(Ve=setInterval(G,10)))}function V(e){if(Je.released="mouseup"===e.type||"touchend"===e.type,Je.pointer=Je.touch?e.originalEvent[Je.released?"changedTouches":"touches"][0]:e,Je.pathX=Je.pointer.pageX-Je.initX,Je.pathY=Je.pointer.pageY-Je.initY,Je.path=T(x(Je.pathX,2)+x(Je.pathY,2)),Je.delta=fe.horizontal?Je.pathX:Je.pathY,Je.released||!(Je.path<1)){if(!Je.init){if(!(fe.horizontal?k(Je.pathX)>k(Je.pathY):k(Je.pathX)<k(Je.pathY)))return ee();Je.init=1}r(e),!Je.locked&&Je.path>Je.pathToLock&&Je.slidee&&(Je.locked=1,Je.$source.on(C,s)),Je.released&&(ee(),fe.releaseSwing&&Je.slidee&&(Je.swing=(Je.delta-Je.history[0])/40*300,Je.delta+=Je.swing,Je.tweese=k(Je.swing)>10)),E(Je.slidee?A(Je.initPos-Je.delta):Z(Je.initPos+Je.delta))}}function ee(){clearInterval(Ve),Je.released=!0,w.off(Je.touch?I:P,V),(Je.slidee?ge:Pe).removeClass(fe.draggedClass),setTimeout(function(){Je.$source.off(C,s)}),we.cur===we.dest&&Je.init&&ce("moveEnd"),ve.resume(1),Je.init=0}function te(t){return~e.inArray(t.nodeName,S)||e(t).is(fe.interactive)}function ae(){ve.stop()}function ne(e){switch(r(e),this){case Me[0]:case Le[0]:ve.moveBy(Me.is(this)?fe.moveBy:-fe.moveBy),w.on("mouseup",ae);break;case Re[0]:ve.prev();break;case We[0]:ve.next();break;case _e[0]:ve.prevPage();break;case Ue[0]:ve.nextPage()}}function ie(e){fe.clickBar&&e.target===be[0]&&(r(e),E(Z((fe.horizontal?e.pageX-be.offset().left:e.pageY-be.offset().top)-Ce/2)))}function re(e){if(fe.keyboardNavBy)switch(e.which){case fe.horizontal?37:38:r(e),ve["pages"===fe.keyboardNavBy?"prevPage":"prev"]();break;case fe.horizontal?39:40:r(e),ve["pages"===fe.keyboardNavBy?"nextPage":"next"]()}}function se(e){return te(this)?void(e.originalEvent[g+"ignore"]=!0):void(this.parentNode!==ge[0]||e.originalEvent[g+"ignore"]||ve.activate(this))}function oe(){this.parentNode===Se[0]&&ve.activatePage(Be.index(this))}function le(e){fe.pauseOnHover&&ve["mouseenter"===e.type?"pause":"resume"](2)}function ce(e,t){if($e[e]){for(ue=$e[e].length,B.length=0,de=0;ue>de;de++)B.push($e[e][de]);for(de=0;ue>de;de++)B[de].call(ve,e,t)}}var de,ue,fe=e.extend({},n.defaults,p),ve=this,pe=l(t),he=e(t),ge=fe.slidee?e(fe.slidee).eq(0):he.children().eq(0),me=0,ye=0,we={start:0,center:0,end:0,cur:0,dest:0},be=e(fe.scrollBar).eq(0),Pe=be.children().eq(0),Ie=0,Ce=0,ze={start:0,end:0,cur:0},Se=e(fe.pagesBar),Be=0,ke=[],Te=0,xe=[],Ae={firstItem:0,lastItem:0,centerItem:0,activeItem:null,activePage:0},qe=new u(he[0]),Oe=new u(ge[0]),De=new u(be[0]),Ee=new u(Pe[0]),Ne="basic"===fe.itemNav,Xe="forceCentered"===fe.itemNav,He="centered"===fe.itemNav||Xe,Ye=!pe&&(Ne||He||Xe),Fe=fe.scrollSource?e(fe.scrollSource):he,je=fe.dragSource?e(fe.dragSource):he,Me=e(fe.forward),Le=e(fe.backward),Re=e(fe.prev),We=e(fe.next),_e=e(fe.prevPage),Ue=e(fe.nextPage),$e={},Qe={},Ze={},Ge={},Je={released:1},Ke=0,Ve=0,et=0,tt=0;pe||(t=he[0]),ve.initialized=0,ve.frame=t,ve.slidee=ge[0],ve.pos=we,ve.rel=Ae,ve.items=xe,ve.pages=ke,ve.isPaused=0,ve.options=fe,ve.dragging=Je,ve.reload=function(){D()},ve.getPos=function(e){if(Ye){var t=j(e);return-1!==t?xe[t]:!1}var a=ge.find(e).eq(0);if(a[0]){var n=fe.horizontal?a.offset().left-ge.offset().left:a.offset().top-ge.offset().top,i=a[fe.horizontal?"outerWidth":"outerHeight"]();return{start:n,center:n-me/2+i/2,end:n-me+i,size:i}}return!1},ve.moveBy=function(e){Ge.speed=e,Ge.lastTime=+new Date,Ge.startPos=we.cur,J("button"),Je.init=1,ce("moveStart"),m(tt),Y()},ve.stop=function(){"button"===Je.source&&(Je.init=0,Je.released=1)},ve.prev=function(){ve.activate(null==Ae.activeItem?0:Ae.activeItem-1)},ve.next=function(){ve.activate(null==Ae.activeItem?0:Ae.activeItem+1)},ve.prevPage=function(){ve.activatePage(Ae.activePage-1)},ve.nextPage=function(){ve.activatePage(Ae.activePage+1)},ve.slideBy=function(e,t){e&&(Ye?ve[He?"toCenter":"toStart"](d((He?Ae.centerItem:Ae.firstItem)+fe.scrollBy*e,0,xe.length)):E(we.dest+e,t))},ve.slideTo=function(e,t){E(e,t)},ve.toStart=function(e,t){F("start",e,t)},ve.toEnd=function(e,t){F("end",e,t)},ve.toCenter=function(e,t){F("center",e,t)},ve.getIndex=j,ve.activate=function(e,t){var a=L(e);fe.smart&&a!==!1&&(He?ve.toCenter(a,t):a>=Ae.lastItem?ve.toStart(a,t):a<=Ae.firstItem?ve.toEnd(a,t):Q())},ve.activatePage=function(e,t){l(e)&&E(ke[d(e,0,ke.length-1)],t)},ve.resume=function(e){fe.cycleBy&&fe.cycleInterval&&("items"!==fe.cycleBy||xe[0]&&null!=Ae.activeItem)&&!(e<ve.isPaused)&&(ve.isPaused=0,et?et=clearTimeout(et):ce("resume"),et=setTimeout(function(){switch(ce("cycle"),fe.cycleBy){case"items":ve.activate(Ae.activeItem>=xe.length-1?0:Ae.activeItem+1);break;case"pages":ve.activatePage(Ae.activePage>=ke.length-1?0:Ae.activePage+1)}},fe.cycleInterval))},ve.pause=function(e){e<ve.isPaused||(ve.isPaused=e||100,et&&(et=clearTimeout(et),ce("pause")))},ve.toggle=function(){ve[et?"pause":"resume"]()},ve.set=function(t,a){e.isPlainObject(t)?e.extend(fe,t):fe.hasOwnProperty(t)&&(fe[t]=a)},ve.add=function(t,a){var n=e(t);Ye?(null==a||!xe[0]||a>=xe.length?n.appendTo(ge):xe.length&&n.insertBefore(xe[a].el),null!=Ae.activeItem&&a<=Ae.activeItem&&(Qe.active=Ae.activeItem+=n.length)):ge.append(n),D()},ve.remove=function(t){if(Ye){var a=M(t);if(a>-1){Te.eq(a).remove();var n=a===Ae.activeItem;null!=Ae.activeItem&&a<Ae.activeItem&&(Qe.active=--Ae.activeItem),D(),n&&(Qe.active=null,ve.activate(Ae.activeItem))}}else e(t).remove(),D()},ve.moveAfter=function(e,t){U(e,t,1)},ve.moveBefore=function(e,t){U(e,t)},ve.on=function(e,t){if("object"===i(e))for(var a in e)e.hasOwnProperty(a)&&ve.on(a,e[a]);else if("function"===i(t))for(var n=e.split(" "),r=0,s=n.length;s>r;r++)$e[n[r]]=$e[n[r]]||[],-1===$(n[r],t)&&$e[n[r]].push(t);else if("array"===i(t))for(var o=0,l=t.length;l>o;o++)ve.on(e,t[o])},ve.one=function(e,t){function a(){t.apply(ve,arguments),ve.off(e,a)}ve.on(e,a)},ve.off=function(e,t){if(t instanceof Array)for(var a=0,n=t.length;n>a;a++)ve.off(e,t[a]);else for(var i=e.split(" "),r=0,s=i.length;s>r;r++)if($e[i[r]]=$e[i[r]]||[],null==t)$e[i[r]].length=0;else{var o=$(i[r],t);-1!==o&&$e[i[r]].splice(o,1)}},ve.destroy=function(){return Fe.add(Pe).add(be).add(Se).add(Me).add(Le).add(Re).add(We).add(_e).add(Ue).off("."+g),w.off("keydown",re),Re.add(We).add(_e).add(Ue).removeClass(fe.disabledClass),Te&&null!=Ae.activeItem&&Te.eq(Ae.activeItem).removeClass(fe.activeClass),Se.empty(),pe||(he.off("."+g),qe.restore(),Oe.restore(),De.restore(),Ee.restore(),e.removeData(t,g)),xe.length=ke.length=0,Qe={},ve.initialized=0,ve},ve.init=function(){if(!ve.initialized){ve.on(h);var e=["overflow","position"],t=["position","webkitTransform","msTransform","transform","left","top","width","height"];qe.save.apply(qe,e),De.save.apply(De,e),Oe.save.apply(Oe,t),Ee.save.apply(Ee,t);var a=Pe;return pe||(a=a.add(ge),he.css("overflow","hidden"),f||"static"!==he.css("position")||he.css("position","relative")),f?v&&a.css(f,v):("static"===be.css("position")&&be.css("position","relative"),a.css({position:"absolute"})),fe.forward&&Me.on(z,ne),fe.backward&&Le.on(z,ne),fe.prev&&Re.on(C,ne),fe.next&&We.on(C,ne),fe.prevPage&&_e.on(C,ne),fe.nextPage&&Ue.on(C,ne),be[0]&&be.on(C,ie),Ye&&fe.activateOn&&he.on(fe.activateOn+"."+g,"*",se),Se[0]&&fe.activatePageOn&&Se.on(fe.activatePageOn+"."+g,"*",oe),je.on(b,{source:"slidee"},K),Pe&&Pe.on(b,{source:"handle"},K),w.on("keydown",re),pe||(he.on("mouseenter."+g+" mouseleave."+g,le),he.on("scroll."+g,o)),ve.initialized=1,D(!0),fe.cycleBy&&!pe&&ve[fe.startPaused?"pause":"resume"](),ve}}}function i(e){return null==e?String(e):"object"==typeof e||"function"==typeof e?Object.prototype.toString.call(e).match(/\s([a-z]+)/i)[1].toLowerCase()||"object":typeof e}function r(e,t){e.preventDefault(),t&&e.stopPropagation()}function s(t){r(t,1),e(this).off(t.type,s)}function o(){this.scrollLeft=0,this.scrollTop=0}function l(e){return!isNaN(parseFloat(e))&&isFinite(e)}function c(e,t){return 0|A(String(e.css(t)).replace(/[^\-0-9.]/g,""))}function d(e,t,a){return t>e?t:e>a?a:e}function u(e){var t={};return t.style={},t.save=function(){if(e&&e.nodeType){for(var a=0;a<arguments.length;a++)t.style[arguments[a]]=e.style[arguments[a]];return t}},t.restore=function(){if(e&&e.nodeType){for(var a in t.style)t.style.hasOwnProperty(a)&&(e.style[a]=t.style[a]);return t}},t}var f,v,p="sly",h="Sly",g=p,m=t.cancelAnimationFrame||t.cancelRequestAnimationFrame,y=t.requestAnimationFrame,w=e(document),b="touchstart."+g+" mousedown."+g,P="mousemove."+g+" mouseup."+g,I="touchmove."+g+" touchend."+g,C=((document.implementation.hasFeature("Event.wheel","3.0")?"wheel.":"mousewheel.")+g,"click."+g),z="mousedown."+g,S=["INPUT","SELECT","BUTTON","TEXTAREA"],B=[],k=Math.abs,T=Math.sqrt,x=Math.pow,A=Math.round,q=Math.max,O=Math.min;!function(e){function t(e){var t=(new Date).getTime(),n=Math.max(0,16-(t-a)),i=setTimeout(e,n);return a=t,i}y=e.requestAnimationFrame||e.webkitRequestAnimationFrame||t;var a=(new Date).getTime(),n=e.cancelAnimationFrame||e.webkitCancelAnimationFrame||e.clearTimeout;m=function(t){n.call(e,t)}}(window),function(){function e(e){for(var n=0,i=t.length;i>n;n++){var r=t[n]?t[n]+e.charAt(0).toUpperCase()+e.slice(1):e;if(null!=a.style[r])return r}}var t=["","webkit","moz","ms","o"],a=document.createElement("div");f=e("transform"),v=e("perspective")?"translateZ(0) ":""}(),t[h]=n,e.fn[p]=function(t,a){var r,s;return e.isPlainObject(t)||(("string"===i(t)||t===!1)&&(r=t===!1?"destroy":t,s=Array.prototype.slice.call(arguments,1)),t={}),this.each(function(i,o){var l=e.data(o,g);l||r?l&&r&&l[r]&&l[r].apply(l,s):l=e.data(o,g,new n(o,t,a).init())})},n.defaults={slidee:null,horizontal:!1,itemNav:null,itemSelector:null,smart:!1,activateOn:null,activateMiddle:!1,scrollSource:null,scrollBy:0,scrollHijack:300,scrollTrap:!1,dragSource:null,mouseDragging:!1,touchDragging:!1,releaseSwing:!1,swingSpeed:.2,elasticBounds:!1,interactive:null,scrollBar:null,dragHandle:!1,dynamicHandle:!1,minHandleSize:50,clickBar:!1,syncSpeed:.5,pagesBar:null,activatePageOn:null,pageBuilder:function(e){return"<li>"+(e+1)+"</li>"},forward:null,backward:null,prev:null,next:null,prevPage:null,nextPage:null,cycleBy:null,cycleInterval:5e3,pauseOnHover:!1,startPaused:!1,moveBy:300,speed:0,easing:"swing",startAt:null,keyboardNavBy:null,draggedClass:"dragged",activeClass:"active",disabledClass:"disabled"}}(jQuery,window);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:69:"/bitrix/templates/aspro_optimus/js/equalize_ext.min.js?14990989751531";s:6:"source";s:50:"/bitrix/templates/aspro_optimus/js/equalize_ext.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
$.fn.equalizeHeightsExt=function(e,t,i){for(var s=this.map(function(i,s){var n=0,r=0;return t!==!1&&(n=parseInt($(s).find(t).actual("outerHeight"))),n&&(n+=12),$(s).css("height",""),r=1==e?$(s).actual("outerHeight")-n:$(s).actual("height")-n}).get(),n=0,r=s.length;r>n;++n)s[n]%2&&--s[n];return this.height(Math.max.apply(this,s))},$.fn.sliceHeightExt=function(e){function t(t){if(t.each(function(){$(this).css("line-height",""),$(this).css("height","")}),"undefined"==typeof e.autoslicecount||e.autoslicecount!==!1){var i=t.first().hasClass("item")?t.first().outerWidth():t.first().parents(".item").outerWidth(),s=t.first().parents(".top_wrapper").outerWidth();i||(i=e.parent?t.closest(e.parent).outerWidth()-5:t.first().outerWidth()-5),s||(s=t.first().parents(".row").outerWidth()),s&&i&&(e.slice=Math.floor(s/i))}if(e.slice)for(var n=0;n<t.length;n+=e.slice)$(t.slice(n,n+e.slice)).equalizeHeightsExt(e.outer,e.classNull,e.minHeight);if(e.lineheight){var r=parseInt(e.lineheight);isNaN(r)&&(r=0),t.each(function(){$(this).css("line-height",$(this).actual("height")+r+"px")})}}var e=$.extend({slice:null,outer:!1,lineheight:!1,autoslicecount:!0,classNull:!1,minHeight:!1,options:!1,parent:!1},e),i=$(this);t(i),BX.addCustomEvent("onWindowResize",function(e){ignoreResize.push(!0),t(i),ignoreResize.pop()})};var timerResize=!1,ignoreResize=[];$(window).resize(function(){ignoreResize.length||(timerResize&&(clearTimeout(timerResize),timerResize=!1),timerResize=setTimeout(function(){BX.onCustomEvent("onWindowResize",!1)},100))});
/* End */
;
; /* Start:"a:4:{s:4:"full";s:73:"/bitrix/templates/aspro_optimus/js/jquery.dotdotdot.min.js?14990989755908";s:6:"source";s:54:"/bitrix/templates/aspro_optimus/js/jquery.dotdotdot.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
!function(t,e){function n(t,e,n){var r=t.children(),o=!1;t.empty();for(var i=0,d=r.length;d>i;i++){var l=r.eq(i);if(t.append(l),n&&t.append(n),a(t,e)){l.remove(),o=!0;break}n&&n.detach()}return o}function r(e,n,i,d,l){var s=!1,c="a table, thead, tbody, tfoot, tr, col, colgroup, object, embed, param, ol, ul, dl, blockquote, select, optgroup, option, textarea, script, style",u="script, .dotdotdot-keep";return e.contents().detach().each(function(){var f=this,h=t(f);if("undefined"==typeof f||3==f.nodeType&&0==t.trim(f.data).length)return!0;if(h.is(u))e.append(h);else{if(s)return!0;e.append(h),!l||h.is(d.after)||h.find(d.after).length||e[e.is(c)?"after":"append"](l),a(i,d)&&(s=3==f.nodeType?o(h,n,i,d,l):r(h,n,i,d,l),s||(h.detach(),s=!0)),s||l&&l.detach()}}),s}function o(e,n,r,o,d){var c=e[0];if(!c)return!1;var f=s(c),h=-1!==f.indexOf(" ")?" ":"　",p="letter"==o.wrap?"":h,g=f.split(p),v=-1,w=-1,y=0,b=g.length-1;for(o.fallbackToLetter&&0==y&&0==b&&(p="",g=f.split(p),b=g.length-1);b>=y&&(0!=y||0!=b);){var m=Math.floor((y+b)/2);if(m==w)break;w=m,l(c,g.slice(0,w+1).join(p)+o.ellipsis),a(r,o)?(b=w,o.fallbackToLetter&&0==y&&0==b&&(p="",g=g[0].split(p),v=-1,w=-1,y=0,b=g.length-1)):(v=w,y=w)}if(-1==v||1==g.length&&0==g[0].length){var T=e.parent();e.detach();var x=d&&d.closest(T).length?d.length:0;T.contents().length>x?c=u(T.contents().eq(-1-x),n):(c=u(T,n,!0),x||T.detach()),c&&(f=i(s(c),o),l(c,f),x&&d&&t(c).parent().append(d))}else f=i(g.slice(0,v+1).join(p),o),l(c,f);return!0}function a(t,e){return t.innerHeight()>e.maxHeight}function i(e,n){for(;t.inArray(e.slice(-1),n.lastCharacter.remove)>-1;)e=e.slice(0,-1);return t.inArray(e.slice(-1),n.lastCharacter.noEllipsis)<0&&(e+=n.ellipsis),e}function d(t){return{width:t.innerWidth(),height:t.innerHeight()}}function l(t,e){t.innerText?t.innerText=e:t.nodeValue?t.nodeValue=e:t.textContent&&(t.textContent=e)}function s(t){return t.innerText?t.innerText:t.nodeValue?t.nodeValue:t.textContent?t.textContent:""}function c(t){do t=t.previousSibling;while(t&&1!==t.nodeType&&3!==t.nodeType);return t}function u(e,n,r){var o,a=e&&e[0];if(a){if(!r){if(3===a.nodeType)return a;if(t.trim(e.text()))return u(e.contents().last(),n)}for(o=c(a);!o;){if(e=e.parent(),e.is(n)||!e.length)return!1;o=c(e[0])}if(o)return u(t(o),n)}return!1}function f(e,n){return e?"string"==typeof e?(e=t(e,n),e.length?e:!1):e.jquery?e:!1:!1}function h(t){for(var e=t.innerHeight(),n=["paddingTop","paddingBottom"],r=0,o=n.length;o>r;r++){var a=parseInt(t.css(n[r]),10);isNaN(a)&&(a=0),e-=a}return e}if(!t.fn.dotdotdot){t.fn.dotdotdot=function(e){if(0==this.length)return t.fn.dotdotdot.debug('No element found for "'+this.selector+'".'),this;if(this.length>1)return this.each(function(){t(this).dotdotdot(e)});var o=this;o.data("dotdotdot")&&o.trigger("destroy.dot"),o.data("dotdotdot-style",o.attr("style")||""),o.css("word-wrap","break-word"),"nowrap"===o.css("white-space")&&o.css("white-space","normal"),o.bind_events=function(){return o.bind("update.dot",function(e,d){e.preventDefault(),e.stopPropagation(),l.maxHeight="number"==typeof l.height?l.height:h(o),l.maxHeight+=l.tolerance,"undefined"!=typeof d&&(("string"==typeof d||"nodeType"in d&&1===d.nodeType)&&(d=t("<div />").append(d).contents()),d instanceof t&&(i=d)),g=o.wrapInner('<div class="dotdotdot" />').children(),g.contents().detach().end().append(i.clone(!0)).find("br").replaceWith("  <br />  ").end().css({height:"auto",width:"auto",border:"none",padding:0,margin:0});var c=!1,u=!1;return s.afterElement&&(c=s.afterElement.clone(!0),c.show(),s.afterElement.detach()),a(g,l)&&(u="children"==l.wrap?n(g,l,c):r(g,o,g,l,c)),g.replaceWith(g.contents()),g=null,t.isFunction(l.callback)&&l.callback.call(o[0],u,i),s.isTruncated=u,u}).bind("isTruncated.dot",function(t,e){return t.preventDefault(),t.stopPropagation(),"function"==typeof e&&e.call(o[0],s.isTruncated),s.isTruncated}).bind("originalContent.dot",function(t,e){return t.preventDefault(),t.stopPropagation(),"function"==typeof e&&e.call(o[0],i),i}).bind("destroy.dot",function(t){t.preventDefault(),t.stopPropagation(),o.unwatch().unbind_events().contents().detach().end().append(i).attr("style",o.data("dotdotdot-style")||"").data("dotdotdot",!1)}),o},o.unbind_events=function(){return o.unbind(".dot"),o},o.watch=function(){if(o.unwatch(),"window"==l.watch){var e=t(window),n=e.width(),r=e.height();e.bind("resize.dot"+s.dotId,function(){n==e.width()&&r==e.height()&&l.windowResizeFix||(n=e.width(),r=e.height(),u&&clearInterval(u),u=setTimeout(function(){o.trigger("update.dot")},100))})}else c=d(o),u=setInterval(function(){if(o.is(":visible")){var t=d(o);(c.width!=t.width||c.height!=t.height)&&(o.trigger("update.dot"),c=t)}},500);return o},o.unwatch=function(){return t(window).unbind("resize.dot"+s.dotId),u&&clearInterval(u),o};var i=o.contents(),l=t.extend(!0,{},t.fn.dotdotdot.defaults,e),s={},c={},u=null,g=null;return l.lastCharacter.remove instanceof Array||(l.lastCharacter.remove=t.fn.dotdotdot.defaultArrays.lastCharacter.remove),l.lastCharacter.noEllipsis instanceof Array||(l.lastCharacter.noEllipsis=t.fn.dotdotdot.defaultArrays.lastCharacter.noEllipsis),s.afterElement=f(l.after,o),s.isTruncated=!1,s.dotId=p++,o.data("dotdotdot",!0).bind_events().trigger("update.dot"),l.watch&&o.watch(),o},t.fn.dotdotdot.defaults={ellipsis:"... ",wrap:"word",fallbackToLetter:!0,lastCharacter:{},tolerance:0,callback:null,after:null,height:null,watch:!1,windowResizeFix:!0},t.fn.dotdotdot.defaultArrays={lastCharacter:{remove:[" ","　",",",";",".","!","?"],noEllipsis:[]}},t.fn.dotdotdot.debug=function(t){};var p=1,g=t.fn.html;t.fn.html=function(n){return n!=e&&!t.isFunction(n)&&this.data("dotdotdot")?this.trigger("update",[n]):g.apply(this,arguments)};var v=t.fn.text;t.fn.text=function(n){return n!=e&&!t.isFunction(n)&&this.data("dotdotdot")?(n=t("<div />").text(n).html(),this.trigger("update",[n])):v.apply(this,arguments)}}}(jQuery);
/* End */
;
; /* Start:"a:4:{s:4:"full";s:62:"/bitrix/templates/aspro_optimus/js/main.min.js?149909897585164";s:6:"source";s:42:"/bitrix/templates/aspro_optimus/js/main.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
function setLocationSKU(e){var t=parseUrlQuery(),a=0,i="",o="",n="";if("oid"in t&&parseInt(t.oid)>0){t.oid=e;for(var s in t)parseInt(a)>0&&(i="&"),o=o+i+s+"="+t[s],a++;o&&(n=location.pathname+"?"+o);try{return void history.pushState(null,null,n)}catch(r){}location.hash="#"+n.substr(1)}}function clearViewedProduct(){try{var e=arOptimusOptions.SITE_ID,t="OPTIMUS_VIEWED_ITEMS_"+e,a={path:"/",expires:30};"undefined"!=typeof BX.localStorage&&BX.localStorage.set(t,{},0),$.removeCookie(t,a)}catch(i){console.error(i)}}function setViewedProduct(e,t){try{var a=$.cookie.json;$.cookie.json=!0;var i=arOptimusOptions.SITE_ID,o="OPTIMUS_VIEWED_ITEMS_"+i,n={path:"/",expires:30};if("undefined"!=typeof BX.localStorage&&"undefined"!=typeof e&&"undefined"!=typeof t){var s="undefined"!=typeof t.PRODUCT_ID?t.PRODUCT_ID:e,r=BX.localStorage.get(o)?BX.localStorage.get(o):{},l=$.cookie(o)?$.cookie(o):{},d=0;for(var c in r)r[c].IS_LAST=!1,"undefined"==typeof l[c]&&delete r[c];for(var c in l)"undefined"==typeof r[c]&&delete l[c];for(var c in l)d++;"undefined"!=typeof r[s]&&r[s].ID!=e&&(delete r[s],delete l[s]),delete r[2243],delete l[2243];var u=(new Date).getTime();t.ID=e,t.ACTIVE_FROM=u,t.IS_LAST=!0,r[s]=t,l[s]=[u.toString(),t.PICTURE_ID],$.cookie(o,l,n),BX.localStorage.set(o,r,2592e3)}}catch(p){console.error(p)}finally{$.cookie.json=a}}function initSelects(e){var t=navigator.userAgent.match(/(iPad|iPhone|iPod)/g)?!0:!1;if(!t&&!$("#bx-soa-order").length){$(e).find(".wrapper select:visible").ikSelect({syntax:'<div class="ik_select_link"> 						<span class="ik_select_link_text"></span> 						<div class="trigger"></div> 					</div> 					<div class="ik_select_dropdown"> 						<div class="ik_select_list"> 						</div> 					</div>',dynamicWidth:!0,ddMaxHeight:112,customClass:"common_select",onShow:function(e){e.$dropdown.css("top",parseFloat(e.$dropdown.css("top"))-5+"px"),e.$dropdown.outerWidth()<e.$link.outerWidth()&&e.$dropdown.css("width",e.$link.outerWidth()),e.$dropdown.outerWidth()>e.$link.outerWidth()&&e.$dropdown.css("width",e.$link.outerWidth());var t=0,a=0;e.$dropdown.css("left",e.$link.offset().left),$(e.$listInnerUl).find("li").each(function(){$(this).hasClass("ik_select_option_disabled")||(++t,a+=$(this).outerHeight())}),112>a?e.$listInner.css("height","auto"):e.$listInner.css("height","112px"),e.$link.addClass("opened"),e.$listInner.addClass("scroller")},onHide:function(e){e.$link.removeClass("opened")}});var a;$(window).on("resize",function(){clearTimeout(a),a=setTimeout(function(){var e="";(e=$(".common_select-link.opened + select").ikSelect().data("plugin_ikSelect"))&&e.$dropdown.css("left",e.$link.offset().left+"px")},20)})}}function initHoverBlock(e){$(e).find(".catalog_item.item_wrap").on("mouseenter",function(){$(this).addClass("hover")}),$(e).find(".catalog_item.item_wrap").on("mouseleave",function(){$(this).removeClass("hover")})}function setStatusButton(){$.ajax({url:arOptimusOptions.SITE_DIR+"ajax/getAjaxBasket.php",type:"POST",success:function(e){if(e.BASKET)for(var t in e.BASKET)$(".to-cart[data-item="+e.BASKET[t]+"]").hide(),$(".counter_block[data-item="+e.BASKET[t]+"]").hide(),$(".in-cart[data-item="+e.BASKET[t]+"]").show(),$(".in-cart[data-item="+e.BASKET[t]+"]").closest(".button_block").addClass("wide");if(e.DELAY)for(var t in e.DELAY)$(".wish_item.to[data-item="+e.DELAY[t]+"]").hide(),$(".wish_item.in[data-item="+e.DELAY[t]+"]").show(),$(".wish_item[data-item="+e.DELAY[t]+"]").find(".value.added").length&&($(".wish_item[data-item="+e.DELAY[t]+"]").addClass("added"),$(".wish_item[data-item="+e.DELAY[t]+"]").find(".value").hide(),$(".wish_item[data-item="+e.DELAY[t]+"]").find(".value.added").show());if(e.SUBSCRIBE)for(var t in e.SUBSCRIBE)$(".to-subscribe[data-item="+e.SUBSCRIBE[t]+"]").hide(),$(".in-subscribe[data-item="+e.SUBSCRIBE[t]+"]").show();if(e.COMPARE)for(var t in e.COMPARE)$(".compare_item.to[data-item="+e.COMPARE[t]+"]").hide(),$(".compare_item.in[data-item="+e.COMPARE[t]+"]").show(),$(".compare_item[data-item="+e.COMPARE[t]+"]").find(".value.added").length&&($(".compare_item[data-item="+e.COMPARE[t]+"]").find(".value").hide(),$(".compare_item[data-item="+e.COMPARE[t]+"]").find(".value.added").show())}})}function scroll_block(e){var t=e.offset().top;$("header").outerHeight(!0,!0);if($(".stores_tab").length)$(".stores_tab").addClass("current").siblings().removeClass("current");else if($(".prices_tab").addClass("current").siblings().removeClass("current"),$(".prices_tab .opener").length&&!$(".prices_tab .opener .opened").length){var a=$(".prices_tab .opener").first();a.find(".opener_icon").addClass("opened"),a.parents("tr").addClass("nb"),a.parents("tr").next(".offer_stores").find(".stores_block_wrap").slideDown(200)}$("html,body").animate({scrollTop:t-30},150)}function replaceBasketPopup(e){"undefined"!=typeof e&&(e.w.hide(),e.o.hide())}function waitLayer(e,t){"undefined"!=typeof dataLayer?"function"==typeof t&&t():setTimeout(function(){waitLayer(e,t)},e)}function checkCounters(e){return"undefined"!=typeof e?"google"==e&&"Y"==arOptimusOptions.COUNTERS.GOOGLE_ECOMERCE&&arOptimusOptions.COUNTERS.GOOGLE_COUNTER>0?!0:"yandex"==e&&"Y"==arOptimusOptions.COUNTERS.USE_YA_COUNTER&&"Y"==arOptimusOptions.COUNTERS.YANDEX_ECOMERCE&&arOptimusOptions.COUNTERS.YANDEX_COUNTER>0?!0:!1:"Y"==arOptimusOptions.COUNTERS.USE_YA_COUNTER&&"Y"==arOptimusOptions.COUNTERS.YANDEX_ECOMERCE&&arOptimusOptions.COUNTERS.YANDEX_COUNTER>0||"Y"==arOptimusOptions.COUNTERS.GOOGLE_ECOMERCE&&arOptimusOptions.COUNTERS.GOOGLE_COUNTER>0?!0:!1}function addBasketCounter(e){if("N"!==arOptimusOptions.COUNTERS.USE_BASKET_GOALS){var t={goal:"goal_basket_add",params:{id:e}};BX.onCustomEvent("onCounterGoals",[t])}checkCounters()&&$.ajax({url:arOptimusOptions.SITE_DIR+"ajax/goals.php",dataType:"json",type:"POST",data:{ID:e},success:function(e){e&&e.ID&&waitLayer(100,function(){dataLayer.push({event:arOptimusOptions.COUNTERS.GOOGLE_EVENTS.ADD2BASKET,ecommerce:{currencyCode:e.CURRENCY,add:{products:[{id:e.ID,name:e.NAME,price:e.PRICE,brand:e.BRAND,category:e.CATEGORY,quantity:e.QUANTITY}]}}})})}})}function purchaseCounter(e,t,a){checkCounters()?$.ajax({url:arOptimusOptions.SITE_DIR+"ajax/goals.php",dataType:"json",type:"POST",data:{ORDER_ID:e,TYPE:t},success:function(e){var i=[];if(e.ITEMS)for(var o in e.ITEMS)i.push({id:e.ITEMS[o].ID,sku:e.ITEMS[o].ID,name:e.ITEMS[o].NAME,price:e.ITEMS[o].PRICE,brand:e.ITEMS[o].BRAND,category:e.ITEMS[o].CATEGORY,quantity:e.ITEMS[o].QUANTITY});e.ID?waitLayer(100,function(){dataLayer.push({ecommerce:d={purchase:{actionField:{id:e.ACCOUNT_NUMBER,shipping:e.PRICE_DELIVERY,tax:e.TAX_VALUE,list:t,revenue:e.PRICE},products:i}}}),"undefined"!=typeof a&&a(d)}):"undefined"!=typeof a&&a(!1)},error:function(){"undefined"!=typeof a&&a(!1)}}):"undefined"!=typeof a&&a(!1)}function viewItemCounter(e,t){checkCounters()&&$.ajax({url:arOptimusOptions.SITE_DIR+"ajax/goals.php",dataType:"json",type:"POST",data:{PRODUCT_ID:e,PRICE_ID:t},success:function(e){e.ID&&waitLayer(100,function(){dataLayer.push({ecommerce:{detail:{products:[{id:e.ID,name:e.NAME,price:e.PRICE,brand:e.BRAND,category:e.CATEGORY}]}}})})}})}function checkoutCounter(e,t,a){checkCounters("google")&&$.ajax({url:arOptimusOptions.SITE_DIR+"ajax/goals.php",dataType:"json",type:"POST",data:{BASKET:"Y"},success:function(a){var i=[];if(a.ITEMS)for(var o in a.ITEMS)i.push({id:a.ITEMS[o].ID,name:a.ITEMS[o].NAME,price:a.ITEMS[o].PRICE,brand:a.ITEMS[o].BRAND,category:a.ITEMS[o].CATEGORY,quantity:a.ITEMS[o].QUANTITY});i&&waitLayer(100,function(){dataLayer.push({event:arOptimusOptions.COUNTERS.GOOGLE_EVENTS.CHECKOUT_ORDER,ecommerce:{actionField:{step:e,option:t},products:i}})})}})}function delFromBasketCounter(e,t){checkCounters()&&$.ajax({url:arOptimusOptions.SITE_DIR+"ajax/goals.php",dataType:"json",type:"POST",data:{ID:e},success:function(e){e.ID&&waitLayer(100,function(){dataLayer.push({event:arOptimusOptions.COUNTERS.GOOGLE_EVENTS.REMOVE_BASKET,ecommerce:{remove:{products:[{id:e.ID,name:e.NAME,category:e.CATEGORY}]}}}),"function"==typeof t&&t()})}})}function setHeightCompany(){$(".md-50.img").height($(".md-50.big").outerHeight()-35)}function initSly(){var e=$(document).find(".frame"),t=(e.children("ul").eq(0),e.parent());arOptimusOptions.PAGES.CATALOG_PAGE&&(e.sly({horizontal:1,itemNav:"basic",smart:1,mouseDragging:0,touchDragging:0,releaseSwing:0,startAt:0,scrollBar:t.find(".scrollbar"),scrollBy:1,speed:300,elasticBounds:0,easing:"swing",dragHandle:1,dynamicHandle:1,clickBar:1,forward:t.find(".forward"),backward:t.find(".backward")}),e.sly("reload"))}function createTableCompare(e,t,a){try{var i=e.clone().removeAttr("id").addClass("clone");a.length?(a.remove(),t.html(""),t.html(i)):t.append(i)}catch(o){}finally{}}function isRealValue(e){return e&&"null"!==e&&"undefined"!==e}function rightScroll(e,t){var a=BX("prop_"+e+"_"+t);if(a){var i=parseInt(a.style.marginLeft);i>=0&&(a.style.marginLeft=i-20+"%")}}function leftScroll(e,t){var a=BX("prop_"+e+"_"+t);if(a){var i=parseInt(a.style.marginLeft);0>i&&(a.style.marginLeft=i+20+"%")}}function touchMenu(e){$(window).outerWidth()>600?$(e).each(function(){var e=$(this);e.on("touchend",function(t){e.find(".child").length&&!e.hasClass("hover")&&(t.preventDefault(),t.stopPropagation(),e.siblings().removeClass("hover"),e.addClass("hover"),$(".menu .child").css({display:"none"}),e.find(".child").css({display:"block"}))})}):$(e).off()}function touchItemBlock(e){$(e).each(function(){var e=$(this),t=e.closest(".catalog_item");e.on("touchend",function(a){if(!t.hasClass("hover")){a.preventDefault(),a.stopPropagation(),t.siblings().removeClass("hover"),t.siblings().blur(),t.closest(".catalog_block").find(".catalog_item").removeClass("hover"),t.addClass("hover"),t.addClass("touch");var i=1*e.closest(".tab").attr("data-hover"),o=1*e.closest(".tab").attr("data-unhover");e.closest(".tab").stop().animate({height:o},100),e.closest(".catalog_item").siblings().find(".buttons_block").stop().fadeOut(233),e.closest(".tab").fadeTo(100,1),e.closest(".tab").stop().css({height:i}),e.closest(".catalog_item").find(".buttons_block").fadeIn(450,"easeOutCirc")}})})}function touchBasket(e){"N"!==arOptimusOptions.THEME.SHOW_BASKET_ONADDTOCART&&($(window).outerWidth()>600?$(document).find(e).on("touchend",function(e){$(this).parent().find(".basket_popup_wrapp").length&&!$(this).hasClass("hover")&&(e.preventDefault(),e.stopPropagation(),$(this).addClass("hover"),$(this).parent().find(".basket_popup_wrapp").slideDown())}):$(e).off())}function initFull(){initSelects(document),initHoverBlock(document),touchItemBlock(".catalog_item a"),InitOrderCustom(),$("html.print").length?window.print():checkStickyFooter(),basketActions(),orderActions()}var basketTimeoutSlide,resizeEventTimer,funcDefined=function(e){try{return"function"==typeof e?!0:"function"==typeof window[e]}catch(t){return!1}};if(!funcDefined("setLocationSKU"),!funcDefined("trimPrice"))var trimPrice=function(e){return e=e.split(" ").join(""),e=e.split("&nbsp;").join("")};if(!funcDefined("markProductRemoveBasket"))var markProductRemoveBasket=function(e){$(".in-cart[data-item="+e+"]").hide(),$(".to-cart[data-item="+e+"]").show(),$(".to-cart[data-item="+e+"]").closest(".button_block").removeClass("wide"),$(".to-cart[data-item="+e+"]").closest(".counter_wrapp").find(".counter_block").show(),$(".counter_block[data-item="+e+"]").show(),$(".in-subscribe[data-item="+e+"]").hide(),$(".to-subscribe[data-item="+e+"]").show(),$(".wish_item[data-item="+e+"]").removeClass("added"),$(".wish_item[data-item="+e+"] .value:not(.added)").show(),$(".wish_item[data-item="+e+"] .value.added").hide()};if(!funcDefined("markProductAddBasket"))var markProductAddBasket=function(e){$(".to-cart[data-item="+e+"]").hide(),$(".to-cart[data-item="+e+"]").closest(".counter_wrapp").find(".counter_block").hide(),$(".to-cart[data-item="+e+"]").closest(".button_block").addClass("wide"),$(".in-cart[data-item="+e+"]").show(),$(".wish_item[data-item="+e+"]").removeClass("added"),$(".wish_item[data-item="+e+"] .value:not(.added)").show(),$(".wish_item[data-item="+e+"] .value.added").hide()};if(!funcDefined("markProductDelay"))var markProductDelay=function(e){$(".in-cart[data-item="+e+"]").hide(),$(".to-cart[data-item="+e+"]").show(),$(".to-cart[data-item="+e+"]").closest(".counter_wrapp").find(".counter_block").show(),$(".to-cart[data-item="+e+"]").closest(".button_block").removeClass("wide"),$(".wish_item[data-item="+e+"]").addClass("added"),$(".wish_item[data-item="+e+"] .value:not(.added)").hide(),$(".wish_item[data-item="+e+"] .value.added").css("display","block")};if(!funcDefined("markProductSubscribe"))var markProductSubscribe=function(e){$(".to-subscribe[data-item="+e+"]").hide(),$(".in-subscribe[data-item="+e+"]").css("display","block")};if(!funcDefined("basketFly"))var basketFly=function(e){$.post(arOptimusOptions.SITE_DIR+"ajax/basket_fly.php","PARAMS="+$("#basket_form").find("input#fly_basket_params").val(),$.proxy(function(t){var a=$(".opener .basket_count").hasClass("small");$(t).find(".basket_count").find(".items div").text();$("#basket_line .basket_fly").addClass("loaded").html(t),"refresh"==e&&$("li[data-type=AnDelCanBuy]").trigger("click"),window.matchMedia("(min-width: 769px)").matches&&("open"==e?a?"N"!==arOptimusOptions.THEME.SHOW_BASKET_ONADDTOCART&&$(".opener .basket_count").click():($(".opener .basket_count").removeClass("small"),$('.tabs_content.basket li[item-section="AnDelCanBuy"]').addClass("cur"),$('#basket_line ul.tabs li[item-section="AnDelCanBuy"]').addClass("cur")):"wish"==e?a?"N"!==arOptimusOptions.THEME.SHOW_BASKET_ONADDTOCART&&$(".opener .wish_count").click():($(".opener .wish_count").removeClass("small"),$('.tabs_content.basket li[item-section="DelDelCanBuy"]').addClass("cur"),$('#basket_line ul.tabs li[item-section="DelDelCanBuy"]').addClass("cur")):"N"!==arOptimusOptions.THEME.SHOW_BASKET_ONADDTOCART&&$(".opener .basket_count").click())}))};if(!funcDefined("clearViewedProduct"),!funcDefined("setViewedProduct"),!funcDefined("initSelects"),funcDefined("CheckTopMenuFullCatalogSubmenu")||(CheckTopMenuFullCatalogSubmenu=function(){var e=$(".menu_top_block"),t=e.parents(".wrap_menu"),a=t.actual("outerWidth"),i=t.offset().left,o=i+a,n=e.hasClass("catalogfirst"),s=$(".menu_top_block:visible li.full"),r=e.actual("outerWidth");$(".catalog_block.menu_top_block").length&&$(".catalog_block.menu_top_block").is(":visible")&&(s=$(".menu_top_block.catalog_block li.full")),s.each(function(){var e=$(this),t=e.find(">.dropdown");t.length&&(n?t.css({left:r+"px",width:a-r+"px","padding-left":"0px","padding-right":"0px"}):t.css({left:-1*e.offset().left+"px",width:$(window).width()-1+"px","padding-left":i+"px","padding-right":$(window).width()-o+"px"}),!isOnceInited&&n&&"top"==arOptimusOptions.THEME.MENU_POSITION&&e.on("mouseenter",function(){t.css("min-height",e.closest(".dropdown").actual("outerHeight")+"px")}))})}),$.fn.getMaxHeights=function(e,t,a){for(var i=this.map(function(t,a){var i=0;return $(a).css("height",""),i=1==e?$(a).actual("outerHeight"):$(a).actual("height")}).get(),o=0,n=i.length;n>o;++o)i[o]%2&&--i[o];return Math.max.apply(this,i)},$.fn.equalizeHeights=function(e,t,a){for(var i=this.map(function(i,o){var n=0,s=0;return t!==!1&&(n=parseInt($(o).find(t).actual("outerHeight"))),n&&(n+=12),$(o).css("height",""),s=1==e?$(o).actual("outerHeight")-n:$(o).actual("height")-n,a!==!1&&(a>s&&(s+=a-s),window.matchMedia("(max-width: 520px)").matches&&(s=300),window.matchMedia("(max-width: 400px)").matches&&(s=200)),s}).get(),o=0,n=i.length;n>o;++o)i[o]%2&&--i[o];return this.height(Math.max.apply(this,i))},$.fn.getFloatWidth=function(){var e=0;if($(this).length){var t=$(this)[0].getBoundingClientRect();(e=t.width)||(e=t.right-t.left)}return e},$.fn.sliceHeight=function(e){function t(t){if(t.each(function(){$(this).css("line-height",""),$(this).css("height","")}),"undefined"==typeof e.autoslicecount||e.autoslicecount!==!1){var a="undefined"!=typeof e.row&&e.row.length?t.first().parents(e.row).getFloatWidth():t.first().parents(".items").getFloatWidth(),i="undefined"!=typeof e.item&&e.item.length?$(e.item).first().getFloatWidth():t.first().hasClass("item")?t.first().getFloatWidth():t.first().parents(".item").getFloatWidth();a||(a=t.first().parents(".row").getFloatWidth()),a&&i&&(e.slice=Math.floor(a/i))}if(e.slice)for(var o=0;o<t.length;o+=e.slice)$(t.slice(o,o+e.slice)).equalizeHeights(e.outer,e.classNull,e.minHeight);if(e.lineheight){var n=parseInt(e.lineheight);isNaN(n)&&(n=0),t.each(function(){$(this).css("line-height",$(this).actual("height")+n+"px")})}}var e=$.extend({slice:null,outer:!1,lineheight:!1,autoslicecount:!0,classNull:!1,minHeight:!1,options:!1,resize:!0,row:!1,item:!1},e),a=$(this);t(a),e.resize&&BX.addCustomEvent("onWindowResize",function(e){ignoreResize.push(!0),t(a),ignoreResize.pop()})},$.fn.sliceHeightNoResize=function(e){function t(t){if(t.each(function(){$(this).css("line-height",""),$(this).css("height","")}),"undefined"==typeof e.autoslicecount||e.autoslicecount!==!1){var a=t.first().hasClass("item")?t.first().getFloatWidth():t.first().parents(".item").getFloatWidth(),i=t.first().parents(".items").getFloatWidth();i||(i=t.first().parents(".row").getFloatWidth()),i&&a&&(e.slice=Math.floor(i/a))}if(e.slice)for(var o=0;o<t.length;o+=e.slice)$(t.slice(o,o+e.slice)).equalizeHeights(e.outer,e.classNull,e.minHeight);if(e.lineheight){var n=parseInt(e.lineheight);isNaN(n)&&(n=0),t.each(function(){$(this).css("line-height",$(this).actual("height")+n+"px")})}}var e=$.extend({slice:null,outer:!1,lineheight:!1,autoslicecount:!0,classNull:!1,minHeight:!1,options:!1,resize:!0},e),a=$(this);t(a)},!funcDefined("initHoverBlock"),!funcDefined("setStatusButton"),!funcDefined("onLoadjqm"))var onLoadjqm=function(e,t,a,i,o,n,s){t.w.addClass("show").css({"margin-left":$(window).width()>t.w.outerWidth()?"-"+t.w.outerWidth()/2+"px":"-"+$(window).width()/2+"px",top:$(document).scrollTop()+($(window).height()>t.w.outerHeight()?($(window).height()-t.w.outerHeight())/2:10)+"px",opacity:1}),"undefined"==typeof a&&(a=""),"undefined"==typeof i&&(i=!1);var r=$("."+e+"_frame").width();$("."+e+"_frame").css("margin-left","-"+r/2+"px"),"order-popup-call"==e||("order-button"==e?$(".order-button_frame").find("div[product_name]").find("input").val(t.t.title).attr("readonly","readonly").css({overflow:"hidden","text-overflow":"ellipsis"}):"to-order"==e&&i?($(".to-order_frame").find('[data-sid="PRODUCT_NAME"]').val($(i).data("name")).attr("readonly","readonly").css({overflow:"hidden","text-overflow":"ellipsis"}),$(".to-order_frame").find('[data-sid="PRODUCT_ID"]').val($(i).attr("data-item"))):"services"==e&&i?$(".services_frame").find('[data-sid="SERVICE"]').val($(i).attr("data-title")):"resume"==e&&i?$(i).attr("data-jobs")&&$(".resume_frame").find('[data-sid="POST"]').attr("readonly","readonly").val($(i).attr("data-jobs")):"basket_error"==e?($(".basket_error_frame .pop-up-title").text(o),$(".basket_error_frame .ajax_text").html(a),initSelects(document),"Y"==n&&s&&$("<div class='popup_button_basket_wr'><span class='popup_button_basket big_btn button' data-item="+s.data("item")+"><span>"+BX.message("ERROR_BASKET_BUTTON")+"</span></span></div>").insertAfter($(".basket_error_frame .ajax_text"))):"one_click_buy"==e?($("#one_click_buy_form_button").on("click",function(){console.log("click"),$(this).hasClass("clicked")||$("#one_click_buy_form").valid()&&$(this).addClass("clicked")}),$("#one_click_buy_form").submit(function(){if($("#one_click_buy_form").valid()){if($("."+e+"_frame form input.error").length||$("."+e+"_frame form textarea.error").length)return!1;$.ajax({url:$(this).attr("action"),data:$(this).serialize(),type:"POST",dataType:"json",error:function(e){alert("Error connecting server")},success:function(e){if("Y"==e.result){if("N"!==arOptimusOptions.COUNTERS.USE_1CLICK_GOALS){var t={goal:"goal_1click_success"};BX.onCustomEvent("onCounterGoals",[t])}$(".one_click_buy_result").show(),$(".one_click_buy_result_success").show(),purchaseCounter(e.message,arOptimusOptions.COUNTERS.TYPE.ONE_CLICK)}else $(".one_click_buy_result").show(),$(".one_click_buy_result_fail").show(),"err"in e&&e.err&&(e.message=e.message+" \n"+e.err),$(".one_click_buy_result_text").html(e.message);$(".one_click_buy_modules_button",self).removeClass("disabled"),$("#one_click_buy_form").hide(),$("#one_click_buy_form_result").show()}})}return!1})):"one_click_buy_basket"==e&&($("#one_click_buy_form_button").on("click",function(){$(this).hasClass("clicked")||$("#one_click_buy_form").valid()&&$(this).addClass("clicked")}),$("#one_click_buy_form").on("submit",function(){if($("#one_click_buy_form").valid()){if($("."+e+"_frame form input.error").length||$("."+e+"_frame form textarea.error").length)return!1;$.ajax({url:$(this).attr("action"),data:$(this).serialize(),type:"POST",dataType:"json",error:function(e){window.console&&console.log(e)},success:function(e){if("Y"==e.result){if("N"!==arOptimusOptions.COUNTERS.USE_FASTORDER_GOALS){var t={goal:"goal_fastorder_success"};BX.onCustomEvent("onCounterGoals",[t])}$(".one_click_buy_result").show(),$(".one_click_buy_result_success").show(),purchaseCounter(e.message,arOptimusOptions.COUNTERS.TYPE.QUICK_ORDER)}else $(".one_click_buy_result").show(),$(".one_click_buy_result_fail").show(),"err"in e&&e.err&&(e.message=e.message+" \n"+e.err),$(".one_click_buy_result_text").text(e.message);$(".one_click_buy_modules_button",self).removeClass("disabled"),$("#one_click_buy_form").hide(),$("#one_click_buy_form_result").show()}})}return!1}))),$("."+e+"_frame").show()};if(!funcDefined("onHidejqm"))var onHidejqm=function(e,t){t.w.find(".one_click_buy_result_success").is(":visible")&&"one_click_buy_basket"==e&&(window.location.href=window.location.href),t.w.css("opacity",0).hide(),t.w.empty(),t.o.remove(),t.w.removeClass("show")};if(!funcDefined("oneClickBuy"))var oneClickBuy=function(e,t,a){var i="one_click_buy",o=1,n=!1,s=$(a).closest(".buy_block").find(".to-cart"),r=$(a).closest("tr").find(".to-cart");"undefined"!=typeof a&&(o=$(a).attr("data-quantity"),n=$(a).attr("data-props")),0>o&&(o=1);var l=s.data("props"),d=r.data("props"),c="",u="",p="N",f={},m=s.data("iblockid"),_=s.attr("data-item");l?c=l.split(";"):d&&(c=d.split(";")),s.data("part_props")&&(u=s.data("part_props")),s.data("add_props")&&(p=s.data("add_props")),f=fillBasketPropsExt(s,"prop",s.data("bakset_div")),f.iblockID=m,f.part_props=u,f.add_props=p,f.props=JSON.stringify(c),f.item=_,f.ocb_item="Y",$("body").find("."+i+"_frame").remove(),$("body").find("."+i+"_trigger").remove(),$("body").append('<div class="'+i+'_frame popup"></div>'),$("body").append('<div class="'+i+'_trigger"></div>'),$("."+i+"_frame").jqm({trigger:"."+i+"_trigger",onHide:function(e){onHidejqm(i,e)},toTop:!1,onLoad:function(e){onLoadjqm(i,e)},ajax:arOptimusOptions.SITE_DIR+"ajax/one_click_buy.php?ELEMENT_ID="+e+"&IBLOCK_ID="+t+"&ELEMENT_QUANTITY="+o+"&OFFER_PROPS="+f.props}),$("."+i+"_trigger").click()};if(!funcDefined("oneClickBuyBasket"))var oneClickBuyBasket=function(){name="one_click_buy_basket",$("body").find("."+name+"_frame").remove(),$("body").find("."+name+"_trigger").remove(),$("body").append('<div class="'+name+'_frame popup"></div>'),$("body").append('<div class="'+name+'_trigger"></div>'),$("."+name+"_frame").jqm({trigger:"."+name+"_trigger",onHide:function(e){onHidejqm(name,e)},onLoad:function(e){onLoadjqm(name,e)},ajax:arOptimusOptions.SITE_DIR+"ajax/one_click_buy_basket.php"}),$("."+name+"_trigger").click()};if(!funcDefined("scroll_block"),!funcDefined("jqmEd"))var jqmEd=function(e,t,a,i,o,n,s,r){return"undefined"==typeof i&&(i=""),"undefined"==typeof o&&(o=!1),$("body").find("."+e+"_frame").remove(),$("body").append('<div class="'+e+'_frame popup"></div>'),"undefined"==typeof a?$("."+e+"_frame").jqm({trigger:"."+e+"_frame.popup",onHide:function(t){onHidejqm(e,t)},onLoad:function(t){onLoadjqm(e,t,i,o)},ajax:arOptimusOptions.SITE_DIR+"ajax/form.php?form_id="+t+(i.length?"&"+i:"")}):("enter"==e?$("."+e+"_frame").jqm({trigger:a,onHide:function(t){onHidejqm(e,t)},onLoad:function(t){onLoadjqm(e,t,i,o)},ajax:arOptimusOptions.SITE_DIR+"ajax/auth.php"}):"basket_error"==e?$("."+e+"_frame").jqm({trigger:a,onHide:function(t){onHidejqm(e,t)},onLoad:function(t){onLoadjqm(e,t,i,o,n,s,r)},ajax:arOptimusOptions.SITE_DIR+"ajax/basket_error.php"}):$("."+e+"_frame").jqm({trigger:a,onHide:function(t){onHidejqm(e,t)},onLoad:function(t){onLoadjqm(e,t,i,o)},ajax:arOptimusOptions.SITE_DIR+"ajax/form.php?form_id="+t+(i.length?"&"+i:"")}),$(a).dblclick(function(){return!1})),!0};if(!funcDefined("replaceBasketPopup"),!funcDefined("waitLayer"),funcDefined("waitCounter")||(waitCounter=function(e,t,a){var i=window["yaCounter"+e];"object"==typeof i?"function"==typeof a&&a():setTimeout(function(){waitCounter(e,t,a)},t)}),funcDefined("InitTopestMenuGummi")||(InitTopestMenuGummi=function(){function e(){var e=a.find(">li:not(.more)"),t=e.length;if(t)for(var i=0,o=0;t>o;++o){var r=e.eq(o),l=r.actual("outerWidth",{includeMargin:!0});s[o]=(i+=l)+(o!==t-1?n:0)}}function t(){var e=a.actual("innerWidth"),t=a.find(">li:not(.more),li.more>.dropdown>li"),n=t.length;if(n){for(var r=!1,l=n-1;l>=0;--l){var d=t.eq(l),c=d.parents(".more").length>0;if(!c&&s[l]>e){r||(r=!0,i.removeClass("hidden"));var u=d.clone();u.find(">a").addClass("dark_font"),u.prependTo(o),d.addClass("cloned")}}for(var l=0;n>l;++l){var d=t.eq(l),c=d.parents(".more").length>0;if(c&&s[l]<=e){l===n-1&&(r=!1,i.addClass("hidden"));var u=d.clone();u.find(">a").removeClass("dark_font"),u.insertBefore(i),d.addClass("cloned")}}a.find("li.cloned").remove()}}if(!isOnceInited){var a=$(".menu.topest"),i=a.find(">.more"),o=i.find(">.dropdown"),n=i.actual("outerWidth",{includeMargin:!0}),s=[];ignoreResize.push(!0),e(),t(),ignoreResize.pop(),$(window).resize(function(){ignoreResize.push(!0),t(),ignoreResize.pop()})}}),funcDefined("InitTopMenuGummi")||(InitTopMenuGummi=function(){function e(){var e=a.closest(".wrap_menu").find(".inc_menu .menu_top_block >li:not(.more)"),t=e.length;if(t)for(var i=0,o=0;t>o;++o){var n=e.eq(o),l=n.actual("outerWidth");r[o]=(i+=l)+(o!==t-1?s:0)}}function t(){var e=i.actual("innerWidth")-o.actual("innerWidth"),t=a.find(">li:not(.more):not(.catalog),li.more>.dropdown>li"),s=t.length;if(s){for(var d=!1,u=s-1;u>=0;--u){var p=t.eq(u),f=p.parents(".more").length>0;if(!f&&r[u]>e){d||(d=!0,n.removeClass("hidden"));var m=p.clone();m.find(">.dropdown").removeAttr("style").removeClass("toleft"),m.find(">a").addClass("dark_font").removeAttr("style"),m.prependTo(l),p.addClass("cloned")}}for(var u=0;s>u;++u){var p=t.eq(u),f=p.parents(".more").length>0;if(f&&r[u]<=e){u===s-1&&(d=!1,n.addClass("hidden"));var m=p.clone();m.find(">a").removeClass("dark_font"),m.insertBefore(n),p.addClass("cloned")}}a.find("li.cloned").remove();var _=a.find(">li:not(.more):not(.catalog)").length,h=e-r[_-1],b=Math.floor(h/(_+(n.hasClass("hidden")?0:1))),k=c+b,v=Math.floor(k/2),g=k-v;a.find(">li:not(.catalog):visible>a").each(function(){$(this).css({"padding-left":v+"px"}),$(this).css({"padding-right":g+"px"})});var w=k+h-(_+(n.is(":visible")?1:0))*b,C=Math.floor(w/2),E=w-C;a.find(">li:visible").last().find(">a").css({"padding-left":C+"px"}),a.find(">li:visible").last().find(">a").css({"padding-right":E+"px"}),CheckTopMenuFullCatalogSubmenu()}}var a=$(".menu_top_block"),i=a.parents(".wrap_menu"),o=i.find(".catalog_menu_ext"),n=a.find(">.more"),s=n.actual("outerWidth",{includeMargin:!0});n.addClass("hidden");var r=[],l=n.find(">.dropdown"),d=2*parseInt(n.find(">a").css("padding-left")),c=d;ignoreResize.push(!0),e(),t(),ignoreResize.pop(),$(window).resize(function(){ignoreResize.push(!0),t(),ignoreResize.pop()})}),!funcDefined("checkCounters"),!funcDefined("addBasketCounter"),!funcDefined("purchaseCounter"),!funcDefined("viewItemCounter"),!funcDefined("checkoutCounter"),!funcDefined("delFromBasketCounter"),!funcDefined("setHeightCompany"),!funcDefined("initSly"),!funcDefined("createTableCompare"),funcDefined("fillBasketPropsExt")||(fillBasketPropsExt=function(e,t,a){var i=0,o=null,n=!1,s={},r=null;if(r=BX(a)){if(o=r.getElementsByTagName("select"),o&&o.length)for(i=0;i<o.length;i++)if(!o[i].disabled)switch(o[i].type.toLowerCase()){case"select-one":s[o[i].name]=o[i].value,n=!0}if(o=r.getElementsByTagName("input"),o&&o.length)for(i=0;i<o.length;i++)if(!o[i].disabled)switch(o[i].type.toLowerCase()){case"hidden":s[o[i].name]=o[i].value,n=!0;break;case"radio":o[i].checked&&(s[o[i].name]=o[i].value,n=!0)}}return n||(s[t]=[],s[t][0]=0),s}),funcDefined("showBasketError")||(showBasketError=function(e,t,a,i){var o=t?t:BX.message("ERROR_BASKET_TITLE"),n="N",s="";void 0!==typeof a&&(n="Y"),void 0!==typeof i&&(s=i),$("body").append("<span class='add-error-bakset' style='display:none;'></span>"),jqmEd("basket_error","error-bakset",".add-error-bakset",e,this,o,n,s),$("body .add-error-bakset").click(),$("body .add-error-bakset").remove()}),!funcDefined("isRealValue"),!funcDefined("rightScroll"),!funcDefined("leftScroll"),funcDefined("InitOrderCustom")||(InitOrderCustom=function(){$(".ps_logo img").wrap('<div class="image"></div>'),$("#bx-soa-order .radio-inline").each(function(){"checked"==$(this).find("input").attr("checked")&&$(this).addClass("checked")}),$("#bx-soa-order .checkbox input[type=checkbox]").each(function(){"checked"==$(this).attr("checked")&&$(this).parent().addClass("checked")}),$("#bx-soa-order .bx-authform-starrequired").each(function(){var e=$(this).html();$(this).closest("label").append('<span class="bx-authform-starrequired"> '+e+"</span>"),$(this).detach()}),$(".bx_ordercart_coupon").each(function(){$(this).find(".bad").length?$(this).addClass("bad"):$(this).find(".good").length&&$(this).addClass("good")})}),funcDefined("InitLabelAnimation")||(InitLabelAnimation=function(e){$(e).length&&($(e).find(".form-group").each(function(){$(this).find("input[type=text], textarea").length&&!$(this).find(".dropdown-block").length&&""!=$(this).find("input[type=text], textarea").val()&&$(this).addClass("value_y")}),$(document).on("click",e+" .form-group:not(.bx-soa-pp-field) label",function(){$(this).parent().find("input, textarea").focus()}),$(document).on("focusout",e+" .form-group:not(.bx-soa-pp-field) input, "+e+" .form-group:not(.bx-soa-pp-field) textarea",function(){var e=$(this).val();""==e||$(this).closest(".form-group").find(".dropdown-block").length||$(this).closest(".form-group").find("#profile_change").length?$(this).closest(".form-group").removeClass("value_y"):$(this).closest(".form-group").addClass("value_y")}),$(document).on("focus",e+" .form-group:not(.bx-soa-pp-field) input, "+e+" .form-group:not(.bx-soa-pp-field) textarea",function(){$(this).closest(".form-group").find(".dropdown-block").length||$(this).closest(".form-group").find("#profile_change").length||$(this).closest(".form-group").find("[name=PERSON_TYPE_OLD]").length||$(this).closest(".form-group").addClass("value_y")}))}),checkPopupWidth=function(){$(".popup.show").each(function(){var e=$(this).actual("width");$(this).css({"margin-left":$(window).width()>e?"-"+e/2+"px":"-"+$(window).width()/2+"px"})})},checkCaptchaWidth=function(){$(".captcha-row").each(function(){var e=$(this).actual("width");$(this).hasClass("b")?e>320&&$(this).removeClass("b"):320>=e&&$(this).addClass("b")})},checkFormWidth=function(){$(".form .form_left").each(function(){var e=$(this).parents(".form"),t=e.actual("width");e.hasClass("b")?t>417&&e.removeClass("b"):417>=t&&e.addClass("b")})},checkFormControlWidth=function(){$(".form-control").each(function(){var e=$(this).actual("width"),t=$(this).find("label:not(.error) > span").actual("width"),a=$(this).find("label.error").actual("width");a>0?$(this).hasClass("h")?e>t+a+5&&$(this).removeClass("h"):t+a+5>=e&&$(this).addClass("h"):$(this).removeClass("h")})},scrollToTop=function(){if("NONE"!==arOptimusOptions.THEME.SCROLLTOTOP_TYPE){var e=!1;$("body").append($("<a />").addClass("scroll-to-top "+arOptimusOptions.THEME.SCROLLTOTOP_TYPE+" "+arOptimusOptions.THEME.SCROLLTOTOP_POSITION).attr({href:"#",id:"scrollToTop"
})),$("#scrollToTop").click(function(e){return e.preventDefault(),$("body, html").animate({scrollTop:0},500),!1}),$(window).scroll(function(){e||(e=!0,$(window).scrollTop()>150?($("#scrollToTop").stop(!0,!0).addClass("visible"),e=!1):($("#scrollToTop").stop(!0,!0).removeClass("visible"),e=!1),checkScrollToTop())})}},checkScrollToTop=function(){var e=55,t=$(window).scrollTop(),a=$(window).height(),i=$("footer").offset().top+70;"CONTENT"==arOptimusOptions.THEME.SCROLLTOTOP_POSITION&&(warpperWidth=$("body > .wrapper > .wrapper_inner").width(),$("#scrollToTop").css("margin-left",Math.ceil(warpperWidth/2)+23)),t+a>i?$("#scrollToTop").css("bottom",e+t+a-i-0):parseInt($("#scrollToTop").css("bottom"))>e&&$("#scrollToTop").css("bottom",e)},CheckObjectsSizes=function(){$(".container iframe,.container object,.container video").each(function(){var e=$(this).attr("height"),t=$(this).attr("width");e&&t&&$(this).css("height",$(this).outerWidth()*e/t)})},!funcDefined("reloadTopBasket"))var reloadTopBasket=function(e,t,a,i,o,n){var s={PARAMS:$("#top_basket_params").val(),ACTION:e};"undefined"!=typeof n&&(s.delete_top_item="Y",s.delete_top_item_id=n.data("id")),$.post(arOptimusOptions.SITE_DIR+"ajax/show_basket_popup.php",s,$.proxy(function(e){$(t).html(e),getActualBasket(),"N"!==arOptimusOptions.THEME.SHOW_BASKET_ONADDTOCART&&$(window).outerWidth()>520&&("Y"==o&&$(t).find(".basket_popup_wrapp").stop(!0,!0).slideDown(a),clearTimeout(basketTimeoutSlide),basketTimeoutSlide=setTimeout(function(){var e=$("#basket_line").find(".basket_popup_wrapp");e.is(":hover")?e.show():$("#basket_line").find(".basket_popup_wrapp").slideUp(a)},i))}))};if(!funcDefined("initCountdown"))var initCountdown=function(){$(".view_sale_block").size()&&$(".view_sale_block").each(function(){var e=$(this).find(".active_to").text(),t=new Date(e.replace(/(\d+)\.(\d+)\.(\d+)/,"$3/$2/$1"));$(this).find(".countdown").countdown({until:t,format:"dHMS",padZeroes:!0,layout:'{d<}<span class="days item">{dnn}<div class="text">{dl}</div></span>{d>} <span class="hours item">{hnn}<div class="text">{hl}</div></span> <span class="minutes item">{mnn}<div class="text">{ml}</div></span> <span class="sec item">{snn}<div class="text">{sl}</div></span>'},$.countdown.regionalOptions.ru)})};var isOnceInited=insertFilter=!1,animationTime=200,delayTime=200,topMenuEnterTimer=!1,isMobile=jQuery.browser.mobile;if(isMobile&&(document.documentElement.className+=" mobile"),!funcDefined("checkVerticalMobileFilter"))var checkVerticalMobileFilter=function(){if($(".right_block1.catalog.vertical").length){if("undefined"!=typeof window.trackBarOptions){window.trackBarValues={};for(key in window.trackBarOptions)window.trackBarValues[key]={leftPercent:window["trackBar"+key].leftPercent,leftValue:window["trackBar"+key].minInput.value,rightPercent:window["trackBar"+key].rightPercent,rightValue:window["trackBar"+key].maxInput.value}}if(window.matchMedia("(max-width: 950px)").matches?insertFilter||($(".js_filter .bx_filter.bx_filter_vertical").html($(".left_block .bx_filter.bx_filter_vertical").html()),$(".left_block .bx_filter.bx_filter_vertical .bx_filter_section").remove(),insertFilter=!0):insertFilter&&($(".left_block .bx_filter.bx_filter_vertical").html($(".js_filter .bx_filter.bx_filter_vertical").html()),$(".js_filter .bx_filter.bx_filter_vertical .bx_filter_section").remove(),insertFilter=!1),"undefined"!=typeof window.trackBarOptions)for(key in window.trackBarOptions)window.trackBarOptions[key].leftPercent=window.trackBarValues[key].leftPercent,window.trackBarOptions[key].rightPercent=window.trackBarValues[key].rightPercent,window.trackBarOptions[key].curMinPrice=window.trackBarValues[key].leftValue,window.trackBarOptions[key].curMaxPrice=window.trackBarValues[key].rightValue,window["trackBar"+key]=new BX.Iblock.SmartFilter(window.trackBarOptions[key]),window["trackBar"+key].minInput.value=window.trackBarValues[key].leftValue,window["trackBar"+key].maxInput.value=window.trackBarValues[key].rightValue}};$(document).on("click",".menu_top_block>li .more a",function(){$this=$(this),$this.parents(".dropdown").first().find(">.hidden").removeClass("hidden"),$this.parent().addClass("hidden"),setTimeout(function(){$this.parent().remove()},500)}),$(document).on("mouseenter",".menu_top_block.catalogfirst>li>.dropdown>li.full",function(){var e=$(this).find(">.dropdown");e.length&&topMenuEnterTimer&&(clearTimeout(topMenuEnterTimer),topMenuEnterTimer=!1)}),$(document).on("mouseenter",".menu_top_block>li:not(.full)",function(){var e=$(this).find(">.dropdown");if(e.length&&!e.hasClass("visible")){var t=$(this).parents(".menu"),a=t.parents(".wrap_menu"),i=a.actual("outerWidth"),o=a.offset().left,n=o+i,s=n-($(this).offset().left+e.actual("outerWidth"));if(window.matchMedia("(min-width: 951px)").matches&&$(this).hasClass("catalog")&&($(".banner_auto").hasClass("catalog_page")||$(".banner_auto").hasClass("front_page")))return;0>s&&e.css({left:s+"px"}),e.stop().slideDown(animationTime,function(){e.css({height:"",overflow:"visible"})}),$(this).on("mouseleave",function(){var t=setTimeout(function(){e.stop().slideUp(animationTime,function(){e.css({left:""})})},delayTime);$(this).on("mouseenter",function(){t&&(clearTimeout(t),t=!1)})})}}),$(document).on("mouseenter",".menu_top_block>li .dropdown>li",function(){var e=$(this),t=e.find(">.dropdown");if(t.length&&(!e.parents(".full").length&&!e.hasClass("full")||e.parents(".more").length)){var a=e.parents(".menu"),o=a.parents(".wrap_menu"),n=[];topMenuEnterTimer=setTimeout(function(){var a=o.actual("outerWidth"),i=o.offset().left,s=i+a,r=e.parent(),l=r.hasClass("toleft")?!0:!1;l=l?e.offset().left+e.actual("outerWidth")-t.actual("outerWidth")<i:e.offset().left+e.actual("outerWidth")+t.actual("outerWidth")>s,l?e.find(">.dropdown").addClass("toleft").show():e.find(">.dropdown").removeClass("toleft").show();var d=t.offset().left,c=d+t.actual("outerWidth");e.parents(".dropdown").each(function(){var e=$(this),t=e.offset().left,a=t+e.actual("outerWidth");(t>=d&&c-1>t||a>d+1&&c>=a)&&(n.push(e),e.find(">li>a").css({opacity:"0.1"}))})},delayTime),e.unbind("mouseleave"),e.on("mouseleave",function(){var t=setTimeout(function(){if(e.find(".dropdown").removeClass("toleft").hide(),n.length)for(i in n)n[i].find(">li>a").css({opacity:""})},delayTime);e.unbind("mouseenter"),e.on("mouseenter",function(){t&&(clearTimeout(t),t=!1)})})}}),getGridSize=function(e){var t=1;return window.matchMedia("(min-width: 1200px)").matches&&(t=e[0]),window.matchMedia("(max-width: 1200px)").matches&&(t=e[1]),window.matchMedia("(max-width: 992px)").matches&&(t=e[2]),e[3]&&window.matchMedia("(max-width: 600px)").matches&&(t=e[3]),e[4]&&window.matchMedia("(max-width: 400px)").matches&&(t=e[4]),t},CheckFlexSlider=function(){$(".flexslider:not(.thmb)").each(function(){var e=$(this);e.resize();var t=e.data("flexslider").vars.counts;if("undefined"!=typeof t){var a=getGridSize(t),i=a!=e.data("flexslider").vars.minItems||a!=e.data("flexslider").vars.maxItems||a!=e.data("flexslider").vars.move;i&&(e.data("flexslider").vars.minItems=a,e.data("flexslider").vars.maxItems=a,e.data("flexslider").vars.move=a,e.flexslider(0),e.resize(),e.resize())}})},InitFlexSlider=function(){$(".flexslider:not(.thmb):not(.flexslider-init)").each(function(){var e,t=$(this),a={animationLoop:!1,controlNav:!1,directionNav:!0,animation:"slide"},i=$.extend({},a,e,t.data("plugin-options"));t.parent().hasClass("top_slider_wrapp")||("undefined"!=typeof i.counts&&"vertical"!==i.direction&&(i.maxItems=getGridSize(i.counts),i.minItems=getGridSize(i.counts),i.move=getGridSize(i.counts),i.itemWidth=200),i.after=i.start=function(e){var t={slider:e};BX.onCustomEvent("onSlide",[t])},i.end=function(e){var t={slider:e};BX.onCustomEvent("onSlideEnd",[t])},t.flexslider(i).addClass("flexslider-init"),i.controlNav&&t.addClass("flexslider-control-nav"),i.directionNav&&t.addClass("flexslider-direction-nav"))})},InitZoomPict=function(){if($(".zoom_picture").length){var e,t=($(".zoom_picture").closest(".slides"),$(".zoom_picture")),a={zoomWidth:200,zoomHeight:200,adaptive:!1,title:!0,Xoffset:15},i=$.extend({},a,e,t.data("plugin-options"));t.xzoom(i)}};var arBasketAsproCounters={};SetActualBasketFlyCounters=function(){1==arBasketAsproCounters.DEFAULT?$.ajax({url:arOptimusOptions.SITE_DIR+"ajax/basket_fly.php",type:"post",success:function(e){$("#basket_line .basket_fly").addClass("loaded").html(e)}}):($("#header .basket_fly .opener .basket_count .count").attr("class","count"+(arBasketAsproCounters.READY.COUNT>0?"":" empty_items")).find(".items span").text(arBasketAsproCounters.READY.COUNT),$("#header .basket_fly .opener .basket_count + a").attr("href",arBasketAsproCounters.READY.HREF),$("#header .basket_fly .opener .basket_count").attr("title",arBasketAsproCounters.READY.TITLE).attr("class","basket_count small clicked"+(arBasketAsproCounters.READY.COUNT>0?"":" empty")),$("#header .basket_fly .opener .wish_count .count").attr("class","count"+(arBasketAsproCounters.DELAY.COUNT>0?"":" empty_items")).find(".items span").text(arBasketAsproCounters.DELAY.COUNT),$("#header .basket_fly .opener .wish_count + a").attr("href",arBasketAsproCounters.DELAY.HREF),$("#header .basket_fly .opener .wish_count").attr("title",arBasketAsproCounters.DELAY.TITLE).attr("class","wish_count small clicked"+(arBasketAsproCounters.DELAY.COUNT>0?"":" empty")),$("#header .basket_fly .opener .compare_count .count").attr("class","count"+(arBasketAsproCounters.COMPARE.COUNT>0?"":" empty_items")).find(".items span").text(arBasketAsproCounters.COMPARE.COUNT),$("#header .basket_fly .opener .compare_count + a").attr("href",arBasketAsproCounters.COMPARE.HREF),$("#header .basket_fly .opener .user_block").attr("title",arBasketAsproCounters.PERSONAL.TITLE).find("+ a").attr("href",arBasketAsproCounters.PERSONAL.HREF),$("#header .basket_fly .opener .user_block .wraps_icon_block").attr("class","wraps_icon_block"+(arBasketAsproCounters.PERSONAL.ID>0?" user_auth":" user_reg")+(arBasketAsproCounters.PERSONAL.SRC?" w_img":" no_img")).attr("style",arBasketAsproCounters.PERSONAL.SRC?'background:url("'+arBasketAsproCounters.PERSONAL.SRC+'") center center no-repeat;':""))},$(document).ready(function(){if(arOptimusOptions.PAGES.ORDER_PAGE){var e=parseUrlQuery();if("ORDER_ID"in e){var t=e.ORDER_ID;if("N"!==arOptimusOptions.COUNTERS.USE_FULLORDER_GOALS){var a={goal:"goal_order_success",result:t};BX.onCustomEvent("onCounterGoals",[a])}if(checkCounters()&&"undefined"!=typeof BX.localStorage){var i=BX.localStorage.get("gtm_e_"+t);"object"==typeof i&&dataLayer.push({ecommerce:i}),"undefined"!=typeof localStorage&&localStorage.removeItem("gtm_e_"+t)}}}"Y"===arOptimusOptions.COUNTERS.USE_DEBUG_GOALS?$.cookie("_ym_debug",1,{path:"/"}):$.cookie("_ym_debug",null,{path:"/"}),scrollToTop(),checkVerticalMobileFilter(),checkFormWidth(),setTimeout(function(){InitTopestMenuGummi(),InitTopMenuGummi(),isOnceInited=!0,InitFlexSlider();try{$("header .wrap_menu").css({overflow:"visible"}),$(".visible_on_ready").removeClass("visible_on_ready")}catch(e){console.error(e)}},100),InitZoomPict(),$("body").on("click",".captcha_reload",function(e){var t=$(this).parents(".captcha-row");e.preventDefault(),$.ajax({url:arOptimusOptions.SITE_DIR+"ajax/captcha.php"}).done(function(e){t.find("input[name=captcha_sid]").val(e),t.find("img").attr("src","/bitrix/tools/captcha.php?captcha_sid="+e),t.find("input[name=captcha_word]").val("").removeClass("error"),t.find(".captcha_input").removeClass("error").find(".error").remove()})}),setTimeout(function(){$(".bg_image_site").css({opacity:1})},200),window.matchMedia("(min-width: 768px)").matches&&$(".wrapper_middle_menu.wrap_menu").removeClass("mobile"),window.matchMedia("(max-width: 767px)").matches&&$(".wrapper_middle_menu.wrap_menu").addClass("mobile"),setTimeout(function(){$(window).scroll()},400),$(".show_props").live("click",function(){$(this).prev(".props_list_wrapp").stop().slideToggle(333),$(this).find(".char_title").toggleClass("opened")}),$(".see_more").live("click",function(e){e.preventDefault();var t=$(this).is(".see_more")?$(this):$(this).parents(".see_more").first(),a=t.find("> a").length?t.find("> a"):t,i=t.parent().find("> .d");return t.hasClass("open")?(a.text(BX.message("CATALOG_VIEW_MORE")),t.removeClass("open"),i.hide()):(a.text(BX.message("CATALOG_VIEW_LESS")),t.addClass("open"),i.show()),!1}),$(".button.faq_button").click(function(e){e.preventDefault(),$(this).toggleClass("opened"),$(".faq_ask .form").slideToggle()}),$(".staff.list .staff_section .staff_section_title a").click(function(e){e.preventDefault(),$(this).parents(".staff_section").toggleClass("opened"),$(this).parents(".staff_section").find(".staff_section_items").stop().slideToggle(600),$(this).parents(".staff_section_title").find(".opener_icon").toggleClass("opened")}),$(".jobs_wrapp .item .name").click(function(e){$(this).closest(".item").toggleClass("opened"),$(this).closest(".item").find(".description_wrapp").stop().slideToggle(600),$(this).closest(".item").find(".opener_icon").toggleClass("opened")}),$(".faq.list .item .q a").live("click",function(e){e.preventDefault(),$(this).parents(".item").toggleClass("opened"),$(this).parents(".item").find(".a").stop().slideToggle(),$(this).parents(".item").find(".q .opener_icon").toggleClass("opened")}),$(".opener_icon").click(function(e){e.preventDefault(),$(this).parent().find("a").trigger("click")}),$(".to-order").live("click",function(e){e.preventDefault(),$("body").append("<span class='evb-toorder' style='display:none;'></span>"),jqmEd("to-order",arOptimusOptions.FORM.TOORDER_FORM_ID,".evb-toorder","",this),$("body .evb-toorder").click(),$("body .evb-toorder").remove()}),$(".dotdot").dotdotdot(),$(".more_block span").live("click",function(){var e=$(".catalog_detail .tabs_section").offset();$("html, body").animate({scrollTop:e.top-23},400)}),$(".counter_block:not(.basket) .plus").live("click",function(){if(!$(this).parents(".basket_wrapp").length&&"Y"!=$(this).parent().data("offers")){var e=$(this).parents(".counter_block_wr").length;input=$(this).parents(".counter_block").find("input[type=text]"),tmp_ratio=e?$(this).parents("tr").first().find("td.buy .to-cart").data("ratio"):$(this).parents(".counter_wrapp").find(".to-cart").data("ratio"),isDblQuantity=e?$(this).parents("tr").first().find("td.buy .to-cart").data("float_ratio"):$(this).parents(".counter_wrapp").find(".to-cart").data("float_ratio"),ratio=isDblQuantity?parseFloat(tmp_ratio):parseInt(tmp_ratio,10),max_value="",currentValue=input.val(),isDblQuantity&&(ratio=Math.round(ratio*arOptimusOptions.JS_ITEM_CLICK.precisionFactor)/arOptimusOptions.JS_ITEM_CLICK.precisionFactor),curValue=isDblQuantity?parseFloat(currentValue):parseInt(currentValue,10),curValue+=ratio,isDblQuantity&&(curValue=Math.round(curValue*arOptimusOptions.JS_ITEM_CLICK.precisionFactor)/arOptimusOptions.JS_ITEM_CLICK.precisionFactor),parseFloat($(this).data("max"))>0?input.val()<$(this).data("max")&&(curValue>$(this).data("max")?input.val($(this).data("max")):input.val(curValue),input.change()):(input.val(curValue),input.change())}}),$(".counter_block:not(.basket) .minus").live("click",function(){if(!$(this).parents(".basket_wrapp").length&&"Y"!=$(this).parent().data("offers")){var e=$(this).parents(".counter_block_wr").length;input=$(this).parents(".counter_block").find("input[type=text]"),tmp_ratio=e?$(this).parents("tr").first().find("td.buy .to-cart").data("ratio"):$(this).parents(".counter_wrapp").find(".to-cart").data("ratio"),isDblQuantity=e?$(this).parents("tr").first().find("td.buy .to-cart").data("float_ratio"):$(this).parents(".counter_wrapp").find(".to-cart").data("float_ratio"),ratio=isDblQuantity?parseFloat(tmp_ratio):parseInt(tmp_ratio,10),max_value="",currentValue=input.val(),isDblQuantity&&(ratio=Math.round(ratio*arOptimusOptions.JS_ITEM_CLICK.precisionFactor)/arOptimusOptions.JS_ITEM_CLICK.precisionFactor),curValue=isDblQuantity?parseFloat(currentValue):parseInt(currentValue,10),curValue-=ratio,isDblQuantity&&(curValue=Math.round(curValue*arOptimusOptions.JS_ITEM_CLICK.precisionFactor)/arOptimusOptions.JS_ITEM_CLICK.precisionFactor),parseFloat($(this).parents(".counter_block").find(".plus").data("max"))>0?currentValue>ratio&&(curValue<ratio?input.val(ratio):input.val(curValue),input.change()):(curValue>ratio?input.val(curValue):ratio?input.val(ratio):currentValue>1&&input.val(curValue),input.change())}}),$(".counter_block input[type=text]").numeric({allow:"."}),$(".counter_block input[type=text]").live("focus",function(){$(this).addClass("focus")}),$(".counter_block input[type=text]").live("blur",function(){$(this).removeClass("focus")}),$(".counter_block input[type=text]").live("change",function(e){if(!$(this).parents(".basket_wrapp").length){var t=$(this).val(),a=$(this).parents(".counter_wrapp").find(".to-cart").data("ratio")?$(this).parents(".counter_wrapp").find(".to-cart").data("ratio"):$(this).parents("tr").first().find("td.buy .to-cart").data("ratio"),i=$(this).parents(".counter_wrapp").find(".to-cart").data("float_ratio")?$(this).parents(".counter_wrapp").find(".to-cart").data("float_ratio"):$(this).parents("tr").first().find("td.buy .to-cart").data("float_ratio"),o=i?parseFloat(a):parseInt(a,10);i&&(o=Math.round(o*arOptimusOptions.JS_ITEM_CLICK.precisionFactor)/arOptimusOptions.JS_ITEM_CLICK.precisionFactor),$(this).hasClass("focus")&&(t-=t%o),parseFloat($(this).parents(".counter_block").find(".plus").data("max"))>0&&t>parseFloat($(this).parents(".counter_block").find(".plus").data("max"))&&(t=parseFloat($(this).parents(".counter_block").find(".plus").data("max")),t-=t%o),o>t?t=o:parseFloat(t)||(t=1),$(this).parents(".counter_block").parent().parent().find(".to-cart").attr("data-quantity",t),$(this).parents(".counter_block").parent().parent().find(".one_click").attr("data-quantity",t),$(this).val(t)}}),$(document).on("mouseenter","#basket_line .basket_normal:not(.empty_cart):not(.bcart) .basket_block ",function(){$(this).closest(".basket_normal").find(".popup").addClass("block"),$(this).closest(".basket_normal").find(".basket_popup_wrapp").stop(!0,!0).slideDown(150)}),$(document).on("mouseleave","#basket_line .basket_normal .basket_block ",function(){var e=$(this);$(this).closest(".basket_normal").find(".basket_popup_wrapp").stop(!0,!0).slideUp(150,function(){e.closest(".basket_normal").find(".popup").removeClass("block")})}),$(document).on("click",".popup_button_basket",function(){var e=$(".to-cart[data-item="+$(this).data("item")+"]"),t=e.attr("data-quantity");t||($val=1);var a=e.data("props"),i="",o="",n="N",s={},r=e.data("iblockid"),l=e.data("offers"),d="",c="",u=e.attr("data-item");"Y"!=l?l="N":c=e.closest(".prices_tab").find(".bx_sku_props input").val(),a&&(i=a.split(";")),e.data("part_props")&&(o=e.data("part_props")),e.data("add_props")&&(n=e.data("add_props")),$(".rid_item").length?d=$(".rid_item").data("rid"):e.data("rid")&&(d=e.data("rid")),s=fillBasketPropsExt(e,"prop","bx_ajax_text"),s.quantity=t,s.add_item="Y",s.rid=d,s.offers=l,s.iblockID=r,s.part_props=o,s.add_props=n,s.props=JSON.stringify(i),s.item=u,s.basket_props=c,$.ajax({type:"POST",url:arOptimusOptions.SITE_DIR+"ajax/item.php",data:s,dataType:"json",success:function(t){$(".basket_error_frame").jqmHide(),"STATUS"in t?(getActualBasket(s.iblockID),"OK"===t.STATUS?(e.hide(),e.closest(".counter_wrapp").find(".counter_block").hide(),e.parents("tr").find(".counter_block_wr .counter_block").hide(),e.closest(".button_block").addClass("wide"),e.parent().find(".in-cart").show(),addBasketCounter(u),$(".wish_item[data-item="+u+"]").removeClass("added"),$(".wish_item[data-item="+u+"]").find(".value").show(),$(".wish_item[data-item="+u+"]").find(".value.added").hide(),$("#basket_line .cart").length?($("#basket_line .cart").is(".empty_cart")&&($("#basket_line .cart").removeClass("empty_cart").find(".cart_wrapp a.basket_link").removeAttr("href").addClass("cart-call"),$("#basket_line .cart").removeClass("ecart"),touchBasket(".cart:not(.empty_cart) .basket_block .link")),reloadTopBasket("add",$("#basket_line"),200,5e3,"Y")):$("#basket_line .basket_fly").length&&basketFly($(window).outerWidth()>768?"open":"refresh")):showBasketError(BX.message(t.MESSAGE))):showBasketError(BX.message("CATALOG_PARTIAL_BASKET_PROPERTIES_ERROR"))}})}),$(document).on("click",".to-cart:not(.read_more)",function(e){e.preventDefault();var t=$(this),a=$(this).attr("data-quantity");a||($val=1);var i=$(this).data("props"),o="",n="",s="N",r={},l=$(this).data("iblockid"),d=$(this).data("offers"),c="",u="",p=$(this).attr("data-item");"Y"!=d?d="N":u=$(this).closest(".prices_tab").find(".bx_sku_props input").val(),i&&(o=i.split(";")),$(this).data("part_props")&&(n=$(this).data("part_props")),$(this).data("add_props")&&(s=$(this).data("add_props")),$(".rid_item").length?c=$(".rid_item").data("rid"):$(this).data("rid")&&(c=$(this).data("rid")),r=fillBasketPropsExt(t,"prop",t.data("bakset_div")),r.quantity=a,r.add_item="Y",r.rid=c,r.offers=d,r.iblockID=l,r.part_props=n,r.add_props=s,r.props=JSON.stringify(o),r.item=p,r.basket_props=u,"N"==t.data("empty_props")?showBasketError($("#"+t.data("bakset_div")).html(),BX.message("ERROR_BASKET_PROP_TITLE"),"Y",t):$.ajax({type:"POST",url:arOptimusOptions.SITE_DIR+"ajax/item.php",data:r,dataType:"json",success:function(e){getActualBasket(r.iblockID),null!==e?"STATUS"in e?(null===e.MESSAGE_EXT&&(e.MESSAGE_EXT=""),"OK"===e.STATUS?(t.hide(),t.closest(".counter_wrapp").find(".counter_block").hide(),t.parents("tr").find(".counter_block_wr .counter_block").hide(),t.closest(".button_block").addClass("wide"),t.parent().find(".in-cart").show(),addBasketCounter(p),$(".wish_item[data-item="+p+"]").removeClass("added"),$(".wish_item[data-item="+p+"]").find(".value").show(),$(".wish_item[data-item="+p+"]").find(".value.added").hide(),$("#basket_line .cart").length?($("#basket_line .cart").is(".empty_cart")&&($("#basket_line .cart").removeClass("empty_cart").find(".cart_wrapp a.basket_link").removeAttr("href").addClass("cart-call"),$("#basket_line .cart").removeClass("ecart"),touchBasket(".cart:not(.empty_cart) .basket_block .link")),reloadTopBasket("add",$("#basket_line"),200,5e3,"Y")):$("#basket_line .basket_fly").length&&basketFly($(window).outerWidth()>768?"open":"refresh")):showBasketError(BX.message(e.MESSAGE)+" <br/>"+e.MESSAGE_EXT)):showBasketError(BX.message("CATALOG_PARTIAL_BASKET_PROPERTIES_ERROR")):(t.hide(),t.closest(".counter_wrapp").find(".counter_block").hide(),t.parents("tr").find(".counter_block_wr .counter_block").hide(),t.closest(".button_block").addClass("wide"),t.parent().find(".in-cart").show(),addBasketCounter(p),$(".wish_item[data-item="+p+"]").removeClass("added"),$(".wish_item[data-item="+p+"]").find(".value").show(),$(".wish_item[data-item="+p+"]").find(".value.added").hide(),$("#basket_line .cart").length?($("#basket_line .cart").is(".empty_cart")&&($("#basket_line .cart").removeClass("empty_cart").find(".cart_wrapp a.basket_link").removeAttr("href").addClass("cart-call"),$("#basket_line .cart").removeClass("ecart"),touchBasket(".cart:not(.empty_cart) .basket_block .link")),reloadTopBasket("add",$("#basket_line"),200,5e3,"Y")):$("#basket_line .basket_fly").length&&$(window).outerWidth()>768&&basketFly("open"))}})}),$(document).on("click",".to-subscribe",function(e){if(e.preventDefault(),$(this).is(".auth"))location.href=arOptimusOptions.SITE_DIR+"auth/?backurl="+location.pathname;else{var t=$(this).attr("data-item"),a=$(this).attr("data-iblockid");$(this).hide(),$(this).parent().find(".in-subscribe").show(),$.get(arOptimusOptions.SITE_DIR+"ajax/item.php?item="+t+"&subscribe_item=Y",$.proxy(function(e){$(".wish_item[data-item="+t+"]").removeClass("added"),getActualBasket(a)}))}}),$(document).on("click",".in-subscribe",function(e){e.preventDefault();var t=$(this).attr("data-item"),a=$(this).attr("data-iblockid");$(this).hide(),$(this).parent().find(".to-subscribe").show(),$.get(arOptimusOptions.SITE_DIR+"ajax/item.php?item="+t+"&subscribe_item=Y",$.proxy(function(e){getActualBasket(a)}))}),$(document).on("click",".wish_item",function(e){e.preventDefault();var t=$(this).attr("data-quantity"),a=$(this).data("offers"),i=$(this).data("iblock"),o=$(this).data("props"),n="",s=$(this).data("item");item2=$(this).attr("data-item"),t||($val=1),"Y"!=a&&(a="N"),o&&(n=o.split(";")),$(this).hasClass("text")?$(this).hasClass("added")?($(".wish_item[data-item="+s+"]").removeClass("added"),$(".wish_item[data-item="+s+"]").find(".value").show(),$(".wish_item[data-item="+s+"]").find(".value.added").hide(),$(".like_icons").each(function(){$(this).find(".wish_item_button").length&&($(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').removeClass("added"),$(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').find(".value").show(),$(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').find(".value.added").hide())})):($(".wish_item[data-item="+s+"]").addClass("added"),$(".wish_item[data-item="+s+"]").find(".value").hide(),$(".wish_item[data-item="+s+"]").find(".value.added").css("display","block"),$(".like_icons").each(function(){$(this).find(".wish_item_button").length&&($(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').addClass("added"),$(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').find(".value").hide(),$(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').find(".value.added").show())})):$(this).hasClass("added")?($(this).hide(),$(this).closest(".wish_item_button").find(".to").show(),$(".like_icons").each(function(){$(this).find('.wish_item.text[data-item="'+s+'"]').length&&($(this).find('.wish_item.text[data-item="'+s+'"]').removeClass("added"),$(this).find('.wish_item.text[data-item="'+s+'"]').find(".value").show(),$(this).find('.wish_item.text[data-item="'+s+'"]').find(".value.added").hide()),$(this).find(".wish_item_button").length&&($(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').removeClass("added"),$(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').find(".value").show(),$(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').find(".value.added").hide())})):($(this).hide(),$(this).closest(".wish_item_button").find(".in").addClass("added").show(),$(".like_icons").each(function(){$(this).find('.wish_item.text[data-item="'+s+'"]').length&&($(this).find('.wish_item.text[data-item="'+s+'"]').addClass("added"),$(this).find('.wish_item.text[data-item="'+s+'"]').find(".value").hide(),$(this).find('.wish_item.text[data-item="'+s+'"]').find(".value.added").css({display:"block"})),$(this).find(".wish_item_button").length&&($(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').addClass("added"),$(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').find(".value").hide(),$(this).find(".wish_item_button").find('.wish_item[data-item="'+s+'"]').find(".value.added").show())})),$(".in-cart[data-item="+s+"]").hide(),$(".to-cart[data-item="+s+"]").parent().removeClass("wide"),$(".to-cart[data-item="+s+"]").show(),$(".counter_block[data-item="+s+"]").show(),$(this).closest(".module-cart").size()||$.ajax({type:"GET",url:arOptimusOptions.SITE_DIR+"ajax/item.php",data:"item="+item2+"&quantity="+t+"&wish_item=Y&offers="+a+"&iblockID="+i+"&props="+JSON.stringify(n),dataType:"json",success:function(e){if(getActualBasket(i),null!==e)if(null===e.MESSAGE_EXT&&(e.MESSAGE_EXT=""),"STATUS"in e)if("OK"===e.STATUS){if("N"!==arOptimusOptions.COUNTERS.USE_BASKET_GOALS){var t={goal:"goal_wish_add",params:{id:item2}};BX.onCustomEvent("onCounterGoals",[t])}$("#basket_line .cart").size()?reloadTopBasket("wish",$("#basket_line"),200,5e3,"N"):basketFly("wish")}else showBasketError(BX.message(e.MESSAGE)+" <br/>"+e.MESSAGE_EXT,BX.message("ERROR_ADD_DELAY_ITEM"));else showBasketError(BX.message(e.MESSAGE)+" <br/>"+e.MESSAGE_EXT,BX.message("ERROR_ADD_DELAY_ITEM"));else $("#basket_line .cart").size()?reloadTopBasket("wish",$("#basket_line"),200,5e3,"N"):basketFly("wish")}})}),$(document).on("click",".compare_item",function(e){e.preventDefault();var t=$(this).attr("data-item"),a=$(this).attr("data-iblock");$(this).hasClass("text")?$(this).hasClass("added")?($(".compare_item[data-item="+t+"]").removeClass("added"),$(".compare_item[data-item="+t+"]").find(".value").show(),$(".compare_item[data-item="+t+"]").find(".value.added").hide(),$(".like_icons").each(function(){$(this).find(".compare_item_button").length&&($(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').removeClass("added"),$(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').find(".value").show(),$(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').find(".value.added").hide())})):($(".compare_item[data-item="+t+"]").addClass("added"),$(".compare_item[data-item="+t+"]").find(".value").hide(),$(".compare_item[data-item="+t+"]").find(".value.added").css("display","block"),$(".like_icons").each(function(){$(this).find(".compare_item_button").length&&($(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').addClass("added"),$(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').find(".value.added").show(),$(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').find(".value").hide())})):$(this).hasClass("added")?($(this).hide(),$(this).closest(".compare_item_button").find(".to").show(),$(".like_icons").each(function(){$(this).find('.compare_item.text[data-item="'+t+'"]').length&&($(this).find('.compare_item.text[data-item="'+t+'"]').removeClass("added"),$(this).find('.compare_item.text[data-item="'+t+'"]').find(".value").show(),$(this).find('.compare_item.text[data-item="'+t+'"]').find(".value.added").hide()),$(this).find(".compare_item_button").length&&($(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').removeClass("added"),$(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').find(".value").show(),$(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').find(".value.added").hide())})):($(this).hide(),$(this).closest(".compare_item_button").find(".in").show(),$(".like_icons").each(function(){$(this).find('.compare_item.text[data-item="'+t+'"]').length&&($(this).find('.compare_item.text[data-item="'+t+'"]').addClass("added"),$(this).find('.compare_item.text[data-item="'+t+'"]').find(".value").hide(),$(this).find('.compare_item.text[data-item="'+t+'"]').find(".value.added").css({display:"block"})),$(this).find(".compare_item_button").length&&($(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').addClass("added"),$(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').find(".value.added").show(),$(this).find(".compare_item_button").find('.compare_item[data-item="'+t+'"]').find(".value").hide())})),$.get(arOptimusOptions.SITE_DIR+"ajax/item.php?item="+t+"&compare_item=Y&iblock_id="+a,$.proxy(function(e){getActualBasket(a),jsAjaxUtil.InsertDataToNode(arOptimusOptions.SITE_DIR+"ajax/show_compare_preview_top.php","compare_line",!1),$("#compare_fly").length&&jsAjaxUtil.InsertDataToNode(arOptimusOptions.SITE_DIR+"ajax/show_compare_preview_fly.php","compare_fly",!1)}))}),$(".fancy").fancybox({openEffect:"fade",closeEffect:"fade",nextEffect:"fade",prevEffect:"fade",tpl:{closeBtn:'<a title="'+BX.message("FANCY_CLOSE")+'" class="fancybox-item fancybox-close" href="javascript:;"></a>',next:'<a title="'+BX.message("FANCY_NEXT")+'" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',prev:'<a title="'+BX.message("FANCY_PREV")+'" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'}}),$(".tabs>li").on("click",function(){if(!$(this).hasClass("active")){var e=$(this).index(),t=$(this).closest(".top_blocks").find(".slider_navigation").find(">li:eq("+e+")"),a=$(this).closest(".top_blocks").siblings(".tabs_content").find(">li:eq("+e+")");$(this).addClass("active").addClass("cur").siblings().removeClass("active").removeClass("cur"),
a.addClass("cur").siblings().removeClass("cur"),t.addClass("cur").siblings().removeClass("cur"),a.find(".catalog_block .catalog_item_wrapp .catalog_item .item_info:visible .item-title").sliceHeight({item:".catalog_item:visible"}),a.find(".catalog_block .catalog_item_wrapp .catalog_item .item_info:visible").sliceHeight({classNull:".footer_button",item:".catalog_item:visible"}),a.find(".catalog_block .catalog_item_wrapp .catalog_item:visible").sliceHeight({classNull:".footer_button",item:".catalog_item:visible"})}}),$(".search_block .icon").on("click",function(){var e=$(this);$(this).hasClass("open")?($(this).closest(".center_block").find(".search_middle_block").removeClass("active"),$(this).removeClass("open"),$(this).closest(".center_block").find(".search_middle_block").find(".noborder").hide()):(setTimeout(function(){e.closest(".center_block").find(".search_middle_block").find(".noborder").show()},100),$(this).closest(".center_block").find(".search_middle_block").addClass("active"),$(this).addClass("open"))}),$(document).on("mouseenter",".display_list .item_wrap",function(){$(this).prev().addClass("prev")}),$(document).on("mouseleave",".display_list .item_wrap",function(){$(this).prev().removeClass("prev")}),$(document).on("mouseenter",".catalog_block .item_wrap",function(){$(this).addClass("shadow_delay")}),$(document).on("mouseleave",".catalog_block .item_wrap",function(){$(this).removeClass("shadow_delay")}),$(document).on("click",".no_goods .button",function(){$(".bx_filter .smartfilter .bx_filter_search_reset").trigger("click")}),$(document).on("click",".ajax_load_btn",function(){var e=$(this).closest(".right_block").find(".module-pagination .flex-direction-nav .flex-next").attr("href"),t=$(this).find(".more_text_ajax");t.addClass("loading"),$.ajax({url:e,data:{ajax_get:"Y"},success:function(e){$.parseHTML(e);t.removeClass("loading"),$(".display_list").length?$(".display_list").append(e):$(".block_list").length?($(".block_list").append(e),touchItemBlock(".catalog_item a")):$(".module_products_list").length&&$(".module_products_list > tbody").append(e),setStatusButton(),initCountdown(),BX.onCustomEvent("onAjaxSuccess"),$(".bottom_nav").html($(e).find(".bottom_nav").html())}})}),$(document).on("click",".bx_compare .tabs-head li",function(){var e=$(this).find(".sortbutton").data("href");BX.showWait(BX("bx_catalog_compare_block")),$.ajax({url:e,data:{ajax_action:"Y"},success:function(t){history.pushState(null,null,e),$("#bx_catalog_compare_block").html(t),BX.closeWait()}})});var o;$(document).on({mouseover:function(e){var t=$(this),a=t.closest("tbody").index()+1,i=t.index()+1;o=$(e.delegateTarget).find(".data_table_props").children(":nth-child("+a+")").children(":nth-child("+i+")").addClass("hovered")},mouseleave:function(e){o&&o.removeClass("hovered")}},".bx_compare .data_table_props tbody>tr"),$(document).on("click",".fancy_offer",function(e){e.preventDefault();var t=[];$(".sliders .slides_block li").each(function(){var e={};e={title:$(this).find("img").attr("alt"),href:$(this).data("big")},$(this).hasClass("current")?t.unshift(e):t.push(e)}),console.log(t),$.fancybox(t,{openEffect:"fade",closeEffect:"fade",nextEffect:"fade",prevEffect:"fade",type:"image",tpl:{closeBtn:'<a title="'+BX.message("FANCY_CLOSE")+'" class="fancybox-item fancybox-close" href="javascript:;"></a>',next:'<a title="'+BX.message("FANCY_NEXT")+'" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',prev:'<a title="'+BX.message("FANCY_PREV")+'" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'}})}),$(".tabs_section .tabs-head li").live("click",function(){$(this).is(".current")||($(".tabs_section .tabs-head li").removeClass("current"),$(this).addClass("current"),$(".tabs_section ul.tabs_content li").removeClass("current"),"product_reviews_tab"==$(this).attr("id")?($(".shadow.common").hide(),$("#reviews_content").show()):($(".shadow.common").show(),$("#reviews_content").hide(),$(".tabs_section ul.tabs_content > li:eq("+$(this).index()+")").addClass("current")))}),setTimeout(function(){$(".jobs_wrapp .item:first .name tr").trigger("click")},300),$(".buy_block .slide_offer").on("click",function(){scroll_block($(".tabs_section"))}),$(".share_wrapp .text").on("click",function(){$(this).parent().find(".shares").fadeToggle()}),$("html, body").live("mousedown",function(e){e.stopPropagation(),$(".shares").fadeOut(),$(".search_middle_block").removeClass("active_wide")}),$(".share_wrapp").find("*").live("mousedown",function(e){e.stopPropagation()}),$(document).on("click",".reviews-collapse-link",function(){$(".reviews-reply-form").slideToggle()}),initCountdown(),$(".menu.adaptive").on("click",function(){$(this).toggleClass("opened"),$(this).hasClass("opened")?$(".mobile_menu").toggleClass("opened").slideDown():$(".mobile_menu").toggleClass("opened").slideUp()}),$(".mobile_menu .has-child >a").on("click",function(e){var t=$(this).parent();e.preventDefault(),t.toggleClass("opened"),t.find(".dropdown").slideToggle()}),$(".mobile_menu .search-input-div input").live("keyup",function(e){var t=$(this).val();$(".center_block .stitle_form input").val(t),13==e.keyCode&&$(".center_block .stitle_form form").submit()}),$(".center_block .stitle_form input").live("keyup",function(e){var t=$(this).val();$(".mobile_menu .search-input-div input").val(t),13==e.keyCode&&$(".center_block .stitle_form form").submit()}),$(".mobile_menu .search-button-div button").live("click",function(e){e.preventDefault();var t=$(this).parents().find("input").val();$(".center_block .stitle_form input").val(t),$(".center_block .stitle_form form").submit()}),$(".btn.btn-add").on("click",function(){$.ajax({type:"GET",url:arOptimusOptions.SITE_DIR+"ajax/clearBasket.php",success:function(e){}})}),$(".sale-order-detail-payment-options-methods-info-change-link").on("click",function(){$(this).closest(".sale-order-detail-payment-options-methods-info").addClass("opened").siblings().addClass("opened")}),$(document).on("click",".expand_block",function(){togglePropBlock($(this))}),document.addEventListener("touchend",function(e){if($(e.target).closest(".menu_item_l1").length||($(".menu .menu_item_l1 .child").css({display:"none"}),$(".menu_item_l1").removeClass("hover")),$(e.target).closest(".basket_block").length||($(".basket_block .link").removeClass("hover"),$(".basket_block .basket_popup_wrapp").slideUp()),!$(e.target).closest(".catalog_item").length){var t=1*$(".tab:visible").attr("data-unhover");$(".tab:visible").stop().animate({height:t},100),$(".tab:visible").find(".catalog_item").removeClass("hover"),$(".tab:visible").find(".catalog_item .buttons_block").stop().fadeOut(233),$(".catalog_block").length&&$(".catalog_block").find(".catalog_item").removeClass("hover")}},!1),$(document).on("keyup",".coupon .input_coupon input",function(){$(this).val().length?($(this).removeClass("error"),$(this).closest(".input_coupon").find(".error").remove()):($(this).addClass("error"),$("<label class='error'>"+BX.message("INPUT_COUPON")+"</label>").insertBefore($(this)))}),BX.addCustomEvent(window,"onAjaxSuccess",function(){initSelects(document),InitOrderCustom(),$(".catalog_detail").length&&($(".bx_filter").remove(),InitFlexSlider()),arOptimusOptions.PAGES.ORDER_PAGE&&orderActions()}),BX.addCustomEvent(window,"onFrameDataRequestFail",function(e){console.log(e)})}),funcDefined("togglePropBlock")||(togglePropBlock=function(e){var t=e.closest(".bx_filter_parameters_box_container").find(".hidden_values");t.length&&(e.hasClass("inner_text")||e.hasClass("expand_block"))&&(t.is(":visible")?(e.text(BX.message("FILTER_EXPAND_VALUES")),t.hide()):(e.text(BX.message("FILTER_HIDE_VALUES")),t.show()))}),funcDefined("showPhoneMask")||(showPhoneMask=function(e){$(e).inputmask("mask",{mask:arOptimusOptions.THEME.PHONE_MASK,showMaskOnHover:!1})}),funcDefined("parseUrlQuery")||(parseUrlQuery=function(){var e={};if(location.search)for(var t=location.search.substr(1).split("&"),a=0;a<t.length;a++){var i=t[a].split("=");e[i[0]]=i[1]}return e}),funcDefined("getActualBasket")||(getActualBasket=function(e){var t="";"undefined"!=typeof e&&(t={iblockID:e}),$.ajax({type:"GET",url:arOptimusOptions.SITE_DIR+"ajax/actualBasket.php",data:t,success:function(e){$(".js_ajax").length||$("body").append('<div class="js_ajax"></div>'),$(".js_ajax").html(e)}})}),funcDefined("orderActions")||(orderActions=function(){if(arOptimusOptions.PAGES.ORDER_PAGE&&$(".bx-soa-cart-total").length){if($(".change_basket").length||$(".bx-soa-cart-total").prepend('<div class="change_basket">'+BX.message("BASKET_CHANGE_TITLE")+'<a href="'+arOptimusOptions.SITE_DIR+'basket/" class="change_link">'+BX.message("BASKET_CHANGE_LINK")+"</a></div>"),"object"==typeof BX.Sale.OrderAjaxComponent){if("N"!==arOptimusOptions.COUNTERS.USE_FULLORDER_GOALS&&"undefined"==typeof BX.Sale.OrderAjaxComponent.reachgoalbegin){BX.Sale.OrderAjaxComponent.reachgoalbegin=!0;var e={goal:"goal_order_begin"};BX.onCustomEvent("onCounterGoals",[e])}$(".bx-soa-cart-total-line-total").length&&!$(".bx-soa-cart-total .licence_block").length&&"Y"==arOptimusOptions.THEME.SHOW_LICENCE&&($('<div class="form"><div class="licence_block filter label_block"><label data-for="licenses_order" class="hidden error">'+BX.message("JS_REQUIRED_LICENSES")+'</label><input type="checkbox" name="licenses_order" required value="Y"><label data-for="licenses_order" class="license">'+BX.message("LICENSES_TEXT")+"</label></div></div>").insertAfter($("#bx-soa-total .bx-soa-cart-total-line-total")),$("#bx-soa-orderSave, .bx-soa-cart-total-button-container").addClass("lic_condition"),"undefined"==typeof BX.Sale.OrderAjaxComponent.oldClickOrderSaveAction&&"undefined"!=typeof BX.Sale.OrderAjaxComponent.clickOrderSaveAction&&(BX.Sale.OrderAjaxComponent.oldClickOrderSaveAction=BX.Sale.OrderAjaxComponent.clickOrderSaveAction,BX.Sale.OrderAjaxComponent.clickOrderSaveAction=function(e){$('input[name="licenses_order"]').prop("checked")?($(".bx-soa-cart-total .licence_block label.error").addClass("hidden"),BX.Sale.OrderAjaxComponent.oldClickOrderSaveAction(e)):$(".bx-soa-cart-total .licence_block label.error").removeClass("hidden")},BX.unbindAll(BX.Sale.OrderAjaxComponent.totalInfoBlockNode.querySelector("a.btn-order-save")),BX.unbindAll(BX.Sale.OrderAjaxComponent.mobileTotalBlockNode.querySelector("a.btn-order-save")),BX.unbindAll(BX.Sale.OrderAjaxComponent.orderSaveBlockNode.querySelector("a")),BX.bind(BX.Sale.OrderAjaxComponent.totalInfoBlockNode.querySelector("a.btn-order-save"),"click",BX.proxy(BX.Sale.OrderAjaxComponent.clickOrderSaveAction,BX.Sale.OrderAjaxComponent)),BX.bind(BX.Sale.OrderAjaxComponent.mobileTotalBlockNode.querySelector("a.btn-order-save"),"click",BX.proxy(BX.Sale.OrderAjaxComponent.clickOrderSaveAction,BX.Sale.OrderAjaxComponent)),BX.bind(BX.Sale.OrderAjaxComponent.orderSaveBlockNode.querySelector("a"),"click",BX.proxy(BX.Sale.OrderAjaxComponent.clickOrderSaveAction,BX.Sale.OrderAjaxComponent))),$(".bx-soa-cart-total .licence_block label.license").on("click",function(){var e=$(this).data("for");$(".bx-soa-cart-total .licence_block label.error").addClass("hidden"),$("input[name="+e+"]").prop("checked")?$("input[name="+e+"]").prop("checked",""):$("input[name="+e+"]").prop("checked","checked")})),BX.Sale.OrderAjaxComponent.hasOwnProperty("params")&&($(".bx-soa-cart-total .change_link").attr("href",BX.Sale.OrderAjaxComponent.params.PATH_TO_BASKET),arOptimusOptions.PRICES.MIN_PRICE&&arOptimusOptions.PRICES.MIN_PRICE>Number(BX.Sale.OrderAjaxComponent.result.TOTAL.ORDER_PRICE)&&($('<div class="fademask_ext"></div>').appendTo($("body")),location.href=BX.Sale.OrderAjaxComponent.params.PATH_TO_BASKET)),checkCounters()&&"undefined"==typeof BX.Sale.OrderAjaxComponent.oldSaveOrder&&"undefined"!=typeof BX.Sale.OrderAjaxComponent.saveOrder&&(BX.Sale.OrderAjaxComponent.oldSaveOrder=BX.Sale.OrderAjaxComponent.saveOrder,BX.Sale.OrderAjaxComponent.saveOrder=function(e){var t=BX.parseJSON(e);t&&t.order?t.order.SHOW_AUTH?BX.Sale.OrderAjaxComponent.oldSaveOrder(e):t.order.REDIRECT_URL&&t.order.REDIRECT_URL.length&&(!t.order.ERROR||BX.util.object_keys(t.order.ERROR).length<1)&&(arMatch=t.order.REDIRECT_URL.match(/ORDER_ID\=[^&=]*/g))&&arMatch.length&&(_id=arMatch[0].replace(/ORDER_ID\=/g,"",arMatch[0]))?$.ajax({url:arOptimusOptions.SITE_DIR+"ajax/check_order.php",dataType:"json",type:"POST",data:{ID:_id},success:function(t){parseInt(t)?purchaseCounter(parseInt(t),BX.message("FULL_ORDER"),function(t){"object"==typeof t&&"undefined"!=typeof BX.localStorage&&BX.localStorage.set("gtm_e_"+_id,t,60),BX.Sale.OrderAjaxComponent.oldSaveOrder(e)}):BX.Sale.OrderAjaxComponent.oldSaveOrder(e)},error:function(){BX.Sale.OrderAjaxComponent.oldSaveOrder(e)}}):BX.Sale.OrderAjaxComponent.oldSaveOrder(e):BX.Sale.OrderAjaxComponent.oldSaveOrder(e)})}$(".bx-ui-sls-quick-locations.quick-locations").on("click",function(){$(this).siblings().removeClass("active"),$(this).addClass("active")})}}),funcDefined("basketActions")||(basketActions=function(){if(arOptimusOptions.PAGES.BASKET_PAGE){if(checkMinPrice(),location.hash){var e=location.hash.substring(1);$("#basket_toolbar_button_"+e).length&&$("#basket_toolbar_button_"+e).trigger("click")}$(".bx_sort_container").append('<div class="top_control basket_sort"><span class="delete_all button grey_br transparent remove_all_basket">'+BX.message("BASKET_CLEAR_ALL_BUTTON")+"</span></div>"),"Y"==arOptimusOptions.THEME.SHOW_BASKET_PRINT&&$(".bx_sort_container .top_control").prepend('<span class="basket_print button grey_br transparent">'+BX.message("BASKET_PRINT_BUTTON")+"</span>"),$(".bx_sort_container .top_control .delete_all").data("type",$(".bx_sort_container a.current").index()),$(".bx_ordercart .bx_ordercart_coupon #coupon").wrap('<div class="input"></div>'),$(".bx_sort_container > a").on("click",function(){$(".bx_sort_container .top_control .delete_all").data("type",$(this).index())}),$(".basket_print").on("click",function(){window.print()}),$(".delete_all").on("click",function(){if("N"!==arOptimusOptions.COUNTERS.USE_BASKET_GOALS){var e={goal:"goal_basket_clear",params:{type:$(this).data("type")}};BX.onCustomEvent("onCounterGoals",[e])}$.post(arOptimusOptions.SITE_DIR+"ajax/action_basket.php","TYPE="+$(this).data("type")+"&CLEAR_ALL=Y",$.proxy(function(e){location.reload()}))}),$(".bx_item_list_section .bx_catalog_item").sliceHeight({row:".bx_item_list_slide",item:".bx_catalog_item"}),BX.addCustomEvent("onAjaxSuccess",function(){checkMinPrice();var e=$.trim($("#warning_message").text());$("#basket_items_list .error_text").detach(),""!=e&&($("#warning_message").hide().text(""),$("#basket_items_list").prepend('<div class="error_text">'+e+"</div>"))})}}),funcDefined("checkMinPrice")||(checkMinPrice=function(){if(arOptimusOptions.PAGES.BASKET_PAGE){var e=$("#allSum_FORMATED").text().replace(/[^0-9\.]/g,""),t=parseFloat(e);$(".catalog_back").length||$(".bx_ordercart_order_pay_center").prepend('<a href="/catalog/" class="catalog_back button transparent big_btn grey_br">'+BX.message("BASKET_CONTINUE_BUTTON")+"</a>"),arOptimusOptions.PRICES.MIN_PRICE?arOptimusOptions.PRICES.MIN_PRICE>t?($(".icon_error_wrapper").length||$(".bx_ordercart_order_pay_center").prepend('<div class="icon_error_wrapper"><div class="icon_error_block">'+BX.message("MIN_ORDER_PRICE_TEXT").replace("#PRICE#",jsPriceFormat(arOptimusOptions.PRICES.MIN_PRICE))+"</div></div>"),$(".oneclickbuy.fast_order").length&&$(".oneclickbuy.fast_order").remove(),$(".bx_ordercart_order_pay .checkout").length&&$(".bx_ordercart_order_pay .checkout").remove()):($(".icon_error_wrapper").length&&$(".icon_error_wrapper").remove(),$(".bx_ordercart_order_pay .checkout").length?$(".bx_ordercart .bx_ordercart_order_pay .checkout").css("opacity","1"):$(".bx_ordercart_order_pay_center").append('<a href="javascript:void(0)" onclick="checkOut();" class="checkout" style="opacity: 1;">'+BX.message("BASKET_ORDER_BUTTON")+"</a>"),$(".oneclickbuy.fast_order").length||"Y"!=arOptimusOptions.THEME.SHOW_ONECLICKBUY_ON_BASKET_PAGE||$(".bx_ordercart_order_pay_center").append('<span class="oneclickbuy button big_btn fast_order" onclick="oneClickBuyBasket()">'+BX.message("BASKET_QUICK_ORDER_BUTTON")+"</span>")):($(".bx_ordercart .bx_ordercart_order_pay .checkout").css("opacity","1"),$(".oneclickbuy.fast_order").length||"Y"!=arOptimusOptions.THEME.SHOW_ONECLICKBUY_ON_BASKET_PAGE||$(".bx_ordercart_order_pay_center").append('<span class="oneclickbuy button big_btn fast_order" onclick="oneClickBuyBasket()">'+BX.message("BASKET_QUICK_ORDER_BUTTON")+"</span>"))}});var isFrameDataReceived=!1;"undefined"!=typeof window.frameCacheVars?BX.addCustomEvent("onFrameDataReceived",function(e){initFull(),isFrameDataReceived=!0}):$(document).ready(initFull),funcDefined("setHeightBlockSlider")||(setHeightBlockSlider=function(){$(document).find(".specials.tab_slider_wrapp").outerWidth();$(document).find(".specials.tab_slider_wrapp .tabs_content > li.cur").css("height",""),$(document).find(".specials.tab_slider_wrapp .tabs_content .tab.cur .tabs_slider .buttons_block").hide(),$(document).find(".specials.tab_slider_wrapp .tabs_content > li.cur").equalize({children:".item-title"}),$(document).find(".specials.tab_slider_wrapp .tabs_content > li.cur").equalize({children:".item_info"}),$(document).find(".specials.tab_slider_wrapp .tabs_content > li.cur").equalize({children:".catalog_item"});var e=$(document).find(".specials.tab_slider_wrapp .tabs_content .tab.cur .tabs_slider li .buttons_block").height(),t=1*$(document).find(".specials.tab_slider_wrapp .tabs_content .tab.cur").height(),a=t+e+50;$(document).find(".specials.tab_slider_wrapp .tabs_content .tab.cur").attr("data-unhover",t),$(document).find(".specials.tab_slider_wrapp .tabs_content .tab.cur").attr("data-hover",a),$(document).find(".specials.tab_slider_wrapp .tabs_content").height(t)}),funcDefined("checkStickyFooter")||(checkStickyFooter=function(){try{ignoreResize.push(!0),$("#content").css("min-height","");var e=$("#content").offset().top,t=e+$("#content").outerHeight(),a=$("footer").offset().top;$("#content").css("min-height",$(window).height()-e-(a-t)-$("footer").outerHeight()+"px"),ignoreResize.pop()}catch(i){console.error(i)}});var timerResize=!1,ignoreResize=[];$(window).resize(function(){$("html.print").length||checkStickyFooter(),ignoreResize.length||(timerResize&&(clearTimeout(timerResize),timerResize=!1),timerResize=setTimeout(function(){BX.onCustomEvent("onWindowResize",!1)},50))});var timerScroll=!1,ignoreScroll=[],documentScrollTopLast=$(document).scrollTop();$(window).scroll(function(){documentScrollTopLast=$(document).scrollTop(),ignoreScroll.length||(timerScroll&&(clearTimeout(timerScroll),timerScroll=!1),timerScroll=setTimeout(function(){BX.onCustomEvent("onWindowScroll",!1)},50))}),BX.addCustomEvent("onWindowResize",function(e){try{if(ignoreResize.push(!0),checkScrollToTop(),checkPopupWidth(),checkCaptchaWidth(),checkFormWidth(),checkFormControlWidth(),touchMenu("ul.menu:not(.opened) > li.menu_item_l1"),touchBasket(".cart:not(.empty_cart) .basket_block .link"),CheckObjectsSizes(),CheckFlexSlider(),initSly(),checkVerticalMobileFilter(),window.matchMedia("(max-width: 768px)").matches&&$(".group_description_block.top").length){var t=$(".adaptive_filter").position().top;$(".bx_filter.bx_filter_vertical").css({top:t+20})}window.matchMedia("(min-width: 767px)").matches&&$(".wrapper_middle_menu.wrap_menu").removeClass("mobile"),window.matchMedia("(max-width: 767px)").matches&&$(".wrapper_middle_menu.wrap_menu").addClass("mobile"),$(window).outerWidth()>600?($("#header ul.menu").removeClass("opened").css("display",""),$(".authorization-cols").length&&($(".authorization-cols").equalize({children:".col .auth-title",reset:!0}),$(".authorization-cols").equalize({children:".col .form-block",reset:!0}))):($(".authorization-cols .auth-title").css("height",""),$(".authorization-cols .form-block").css("height","")),$("#basket_form").length&&$(window).outerWidth()<=600&&$("#basket_form .tabs_content.basket > li.cur td").each(function(){$(this).css("width","")}),$(".front_slider_wrapp").length&&$(".extended_pagination li i").each(function(){$(this).css({borderBottomWidth:$(this).parent("li").outerHeight()/2,borderTopWidth:$(this).parent("li").outerHeight()/2})}),setHeightCompany(),$(".bx_filter_section .bx_filter_select_container").each(function(){var e=$(this).closest(".bx_filter_parameters_box").attr("property_id");$("#smartFilterDropDown"+e).length&&$("#smartFilterDropDown"+e).css("max-width",$(this).width())})}catch(a){}finally{ignoreResize.pop()}}),BX.addCustomEvent("onWindowScroll",function(e){try{ignoreScroll.push(!0)}catch(t){}finally{ignoreScroll.pop()}}),BX.addCustomEvent("onCounterGoals",function(e){if("Y"===arOptimusOptions.COUNTERS.USE_YA_COUNTER){var t=arOptimusOptions.COUNTERS.YA_COUNTER_ID;if(t=parseInt(t),"object"!=typeof e&&(e={goal:"undefined"}),"string"!=typeof e.goal&&(e.goal="undefined"),t)try{waitCounter(t,50,function(){var a=window["yaCounter"+t];"object"==typeof a&&a.reachGoal(e.goal)})}catch(a){console.error(a)}else console.info("Bad counter id!",t)}});
/* End */
;
; /* Start:"a:4:{s:4:"full";s:67:"/bitrix/components/bitrix/search.title/script.min.js?14990878986110";s:6:"source";s:48:"/bitrix/components/bitrix/search.title/script.js";s:3:"min";s:52:"/bitrix/components/bitrix/search.title/script.min.js";s:3:"map";s:52:"/bitrix/components/bitrix/search.title/script.map.js";}"*/
function JCTitleSearch(t){var e=this;this.arParams={AJAX_PAGE:t.AJAX_PAGE,CONTAINER_ID:t.CONTAINER_ID,INPUT_ID:t.INPUT_ID,MIN_QUERY_LEN:parseInt(t.MIN_QUERY_LEN)};if(t.WAIT_IMAGE)this.arParams.WAIT_IMAGE=t.WAIT_IMAGE;if(t.MIN_QUERY_LEN<=0)t.MIN_QUERY_LEN=1;this.cache=[];this.cache_key=null;this.startText="";this.running=false;this.currentRow=-1;this.RESULT=null;this.CONTAINER=null;this.INPUT=null;this.WAIT=null;this.ShowResult=function(t){if(BX.type.isString(t)){e.RESULT.innerHTML=t}e.RESULT.style.display=e.RESULT.innerHTML!==""?"block":"none";var s=e.adjustResultNode();var i;var r;var n=BX.findChild(e.RESULT,{tag:"table","class":"title-search-result"},true);if(n){r=BX.findChild(n,{tag:"th"},true)}if(r){var a=BX.pos(n);a.width=a.right-a.left;var l=BX.pos(r);l.width=l.right-l.left;r.style.width=l.width+"px";e.RESULT.style.width=s.width+l.width+"px";e.RESULT.style.left=s.left-l.width-1+"px";if(a.width-l.width>s.width)e.RESULT.style.width=s.width+l.width-1+"px";a=BX.pos(n);i=BX.pos(e.RESULT);if(i.right>a.right){e.RESULT.style.width=a.right-a.left+"px"}}var o;if(n)o=BX.findChild(e.RESULT,{"class":"title-search-fader"},true);if(o&&r){i=BX.pos(e.RESULT);o.style.left=i.right-i.left-18+"px";o.style.width=18+"px";o.style.top=0+"px";o.style.height=i.bottom-i.top+"px";o.style.display="block"}};this.onKeyPress=function(t){var s=BX.findChild(e.RESULT,{tag:"table","class":"title-search-result"},true);if(!s)return false;var i;var r=s.rows.length;switch(t){case 27:e.RESULT.style.display="none";e.currentRow=-1;e.UnSelectAll();return true;case 40:if(e.RESULT.style.display=="none")e.RESULT.style.display="block";var n=-1;for(i=0;i<r;i++){if(!BX.findChild(s.rows[i],{"class":"title-search-separator"},true)){if(n==-1)n=i;if(e.currentRow<i){e.currentRow=i;break}else if(s.rows[i].className=="title-search-selected"){s.rows[i].className=""}}}if(i==r&&e.currentRow!=i)e.currentRow=n;s.rows[e.currentRow].className="title-search-selected";return true;case 38:if(e.RESULT.style.display=="none")e.RESULT.style.display="block";var a=-1;for(i=r-1;i>=0;i--){if(!BX.findChild(s.rows[i],{"class":"title-search-separator"},true)){if(a==-1)a=i;if(e.currentRow>i){e.currentRow=i;break}else if(s.rows[i].className=="title-search-selected"){s.rows[i].className=""}}}if(i<0&&e.currentRow!=i)e.currentRow=a;s.rows[e.currentRow].className="title-search-selected";return true;case 13:if(e.RESULT.style.display=="block"){for(i=0;i<r;i++){if(e.currentRow==i){if(!BX.findChild(s.rows[i],{"class":"title-search-separator"},true)){var l=BX.findChild(s.rows[i],{tag:"a"},true);if(l){window.location=l.href;return true}}}}}return false}return false};this.onTimeout=function(){e.onChange(function(){setTimeout(e.onTimeout,500)})};this.onChange=function(t){if(e.running)return;e.running=true;if(e.INPUT.value!=e.oldValue&&e.INPUT.value!=e.startText){e.oldValue=e.INPUT.value;if(e.INPUT.value.length>=e.arParams.MIN_QUERY_LEN){e.cache_key=e.arParams.INPUT_ID+"|"+e.INPUT.value;if(e.cache[e.cache_key]==null){if(e.WAIT){var s=BX.pos(e.INPUT);var i=s.bottom-s.top-2;e.WAIT.style.top=s.top+1+"px";e.WAIT.style.height=i+"px";e.WAIT.style.width=i+"px";e.WAIT.style.left=s.right-i+2+"px";e.WAIT.style.display="block"}BX.ajax.post(e.arParams.AJAX_PAGE,{ajax_call:"y",INPUT_ID:e.arParams.INPUT_ID,q:e.INPUT.value,l:e.arParams.MIN_QUERY_LEN},function(s){e.cache[e.cache_key]=s;e.ShowResult(s);e.currentRow=-1;e.EnableMouseEvents();if(e.WAIT)e.WAIT.style.display="none";if(!!t)t();e.running=false});return}else{e.ShowResult(e.cache[e.cache_key]);e.currentRow=-1;e.EnableMouseEvents()}}else{e.RESULT.style.display="none";e.currentRow=-1;e.UnSelectAll()}}if(!!t)t();e.running=false};this.UnSelectAll=function(){var t=BX.findChild(e.RESULT,{tag:"table","class":"title-search-result"},true);if(t){var s=t.rows.length;for(var i=0;i<s;i++)t.rows[i].className=""}};this.EnableMouseEvents=function(){var t=BX.findChild(e.RESULT,{tag:"table","class":"title-search-result"},true);if(t){var s=t.rows.length;for(var i=0;i<s;i++)if(!BX.findChild(t.rows[i],{"class":"title-search-separator"},true)){t.rows[i].id="row_"+i;t.rows[i].onmouseover=function(t){if(e.currentRow!=this.id.substr(4)){e.UnSelectAll();this.className="title-search-selected";e.currentRow=this.id.substr(4)}};t.rows[i].onmouseout=function(t){this.className="";e.currentRow=-1}}}};this.onFocusLost=function(t){setTimeout(function(){e.RESULT.style.display="none"},250)};this.onFocusGain=function(){if(e.RESULT.innerHTML.length)e.ShowResult()};this.onKeyDown=function(t){if(!t)t=window.event;if(e.RESULT.style.display=="block"){if(e.onKeyPress(t.keyCode))return BX.PreventDefault(t)}};this.adjustResultNode=function(){var t;var s=BX.findParent(e.CONTAINER,BX.is_fixed);if(!!s){e.RESULT.style.position="fixed";e.RESULT.style.zIndex=BX.style(s,"z-index")+2;t=BX.pos(e.CONTAINER,true)}else{e.RESULT.style.position="absolute";t=BX.pos(e.CONTAINER)}t.width=t.right-t.left;e.RESULT.style.top=t.bottom+2+"px";e.RESULT.style.left=t.left+"px";e.RESULT.style.width=t.width+"px";return t};this._onContainerLayoutChange=function(){if(e.RESULT.style.display!=="none"&&e.RESULT.innerHTML!==""){e.adjustResultNode()}};this.Init=function(){this.CONTAINER=document.getElementById(this.arParams.CONTAINER_ID);BX.addCustomEvent(this.CONTAINER,"OnNodeLayoutChange",this._onContainerLayoutChange);this.RESULT=document.body.appendChild(document.createElement("DIV"));this.RESULT.className="title-search-result";this.INPUT=document.getElementById(this.arParams.INPUT_ID);this.startText=this.oldValue=this.INPUT.value;BX.bind(this.INPUT,"focus",function(){e.onFocusGain()});BX.bind(this.INPUT,"blur",function(){e.onFocusLost()});this.INPUT.onkeydown=this.onKeyDown;if(this.arParams.WAIT_IMAGE){this.WAIT=document.body.appendChild(document.createElement("DIV"));this.WAIT.style.backgroundImage="url('"+this.arParams.WAIT_IMAGE+"')";if(!BX.browser.IsIE())this.WAIT.style.backgroundRepeat="none";this.WAIT.style.display="none";this.WAIT.style.position="absolute";this.WAIT.style.zIndex="1100"}BX.bind(this.INPUT,"bxchange",function(){e.onChange()})};BX.ready(function(){e.Init(t)})}
/* End */
;
; /* Start:"a:4:{s:4:"full";s:58:"/bitrix/templates/aspro_optimus/js/custom.js?1499098975100";s:6:"source";s:44:"/bitrix/templates/aspro_optimus/js/custom.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
/*
You can use this file with your scripts.
It will not be overwritten when you upgrade solution.
*/
/* End */
;
; /* Start:"a:4:{s:4:"full";s:108:"/bitrix/templates/aspro_optimus/components/bitrix/map.google.view/map/markerclustererplus.js?149909897451430";s:6:"source";s:92:"/bitrix/templates/aspro_optimus/components/bitrix/map.google.view/map/markerclustererplus.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
/**
 * @name MarkerClustererPlus for Google Maps V3
 * @version 2.1.2 [May 28, 2014]
 * @author Gary Little
 * @fileoverview
 * The library creates and manages per-zoom-level clusters for large amounts of markers.
 * <p>
 * This is an enhanced V3 implementation of the
 * <a href="http://gmaps-utility-library-dev.googlecode.com/svn/tags/markerclusterer/"
 * >V2 MarkerClusterer</a> by Xiaoxi Wu. It is based on the
 * <a href="http://google-maps-utility-library-v3.googlecode.com/svn/tags/markerclusterer/"
 * >V3 MarkerClusterer</a> port by Luke Mahe. MarkerClustererPlus was created by Gary Little.
 * <p>
 * v2.0 release: MarkerClustererPlus v2.0 is backward compatible with MarkerClusterer v1.0. It
 *  adds support for the <code>ignoreHidden</code>, <code>title</code>, <code>batchSizeIE</code>,
 *  and <code>calculator</code> properties as well as support for four more events. It also allows
 *  greater control over the styling of the text that appears on the cluster marker. The
 *  documentation has been significantly improved and the overall code has been simplified and
 *  polished. Very large numbers of markers can now be managed without causing Javascript timeout
 *  errors on Internet Explorer. Note that the name of the <code>clusterclick</code> event has been
 *  deprecated. The new name is <code>click</code>, so please change your application code now.
 */

/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * @name ClusterIconStyle
 * @class This class represents the object for values in the <code>styles</code> array passed
 *  to the {@link MarkerClusterer} constructor. The element in this array that is used to
 *  style the cluster icon is determined by calling the <code>calculator</code> function.
 *
 * @property {string} url The URL of the cluster icon image file. Required.
 * @property {number} height The display height (in pixels) of the cluster icon. Required.
 * @property {number} width The display width (in pixels) of the cluster icon. Required.
 * @property {Array} [anchorText] The position (in pixels) from the center of the cluster icon to
 *  where the text label is to be centered and drawn. The format is <code>[yoffset, xoffset]</code>
 *  where <code>yoffset</code> increases as you go down from center and <code>xoffset</code>
 *  increases to the right of center. The default is <code>[0, 0]</code>.
 * @property {Array} [anchorIcon] The anchor position (in pixels) of the cluster icon. This is the
 *  spot on the cluster icon that is to be aligned with the cluster position. The format is
 *  <code>[yoffset, xoffset]</code> where <code>yoffset</code> increases as you go down and
 *  <code>xoffset</code> increases to the right of the top-left corner of the icon. The default
 *  anchor position is the center of the cluster icon.
 * @property {string} [textColor="black"] The color of the label text shown on the
 *  cluster icon.
 * @property {number} [textSize=11] The size (in pixels) of the label text shown on the
 *  cluster icon.
 * @property {string} [textDecoration="none"] The value of the CSS <code>text-decoration</code>
 *  property for the label text shown on the cluster icon.
 * @property {string} [fontWeight="bold"] The value of the CSS <code>font-weight</code>
 *  property for the label text shown on the cluster icon.
 * @property {string} [fontStyle="normal"] The value of the CSS <code>font-style</code>
 *  property for the label text shown on the cluster icon.
 * @property {string} [fontFamily="Arial,sans-serif"] The value of the CSS <code>font-family</code>
 *  property for the label text shown on the cluster icon.
 * @property {string} [backgroundPosition="0 0"] The position of the cluster icon image
 *  within the image defined by <code>url</code>. The format is <code>"xpos ypos"</code>
 *  (the same format as for the CSS <code>background-position</code> property). You must set
 *  this property appropriately when the image defined by <code>url</code> represents a sprite
 *  containing multiple images. Note that the position <i>must</i> be specified in px units.
 */
/**
 * @name ClusterIconInfo
 * @class This class is an object containing general information about a cluster icon. This is
 *  the object that a <code>calculator</code> function returns.
 *
 * @property {string} text The text of the label to be shown on the cluster icon.
 * @property {number} index The index plus 1 of the element in the <code>styles</code>
 *  array to be used to style the cluster icon.
 * @property {string} title The tooltip to display when the mouse moves over the cluster icon.
 *  If this value is <code>undefined</code> or <code>""</code>, <code>title</code> is set to the
 *  value of the <code>title</code> property passed to the MarkerClusterer.
 */
/**
 * A cluster icon.
 *
 * @constructor
 * @extends google.maps.OverlayView
 * @param {Cluster} cluster The cluster with which the icon is to be associated.
 * @param {Array} [styles] An array of {@link ClusterIconStyle} defining the cluster icons
 *  to use for various cluster sizes.
 * @private
 */
function ClusterIcon(cluster, styles) {
  cluster.getMarkerClusterer().extend(ClusterIcon, google.maps.OverlayView);

  this.cluster_ = cluster;
  this.className_ = cluster.getMarkerClusterer().getClusterClass();
  this.styles_ = styles;
  this.center_ = null;
  this.div_ = null;
  this.sums_ = null;
  this.visible_ = false;

  this.setMap(cluster.getMap()); // Note: this causes onAdd to be called
}


/**
 * Adds the icon to the DOM.
 */
ClusterIcon.prototype.onAdd = function () {
  var cClusterIcon = this;
  var cMouseDownInCluster;
  var cDraggingMapByCluster;

  this.div_ = document.createElement("div");
  this.div_.className = this.className_;
  if (this.visible_) {
    this.show();
  }

  this.getPanes().overlayMouseTarget.appendChild(this.div_);

  // Fix for Issue 157
  this.boundsChangedListener_ = google.maps.event.addListener(this.getMap(), "bounds_changed", function () {
    cDraggingMapByCluster = cMouseDownInCluster;
  });

  google.maps.event.addDomListener(this.div_, "mousedown", function () {
    cMouseDownInCluster = true;
    cDraggingMapByCluster = false;
  });

  google.maps.event.addDomListener(this.div_, "click", function (e) {
    cMouseDownInCluster = false;
    if (!cDraggingMapByCluster) {
      var theBounds;
      var mz;
      var mc = cClusterIcon.cluster_.getMarkerClusterer();
      /**
       * This event is fired when a cluster marker is clicked.
       * @name MarkerClusterer#click
       * @param {Cluster} c The cluster that was clicked.
       * @event
       */
      google.maps.event.trigger(mc, "click", cClusterIcon.cluster_);
      google.maps.event.trigger(mc, "clusterclick", cClusterIcon.cluster_); // deprecated name

      // The default click handler follows. Disable it by setting
      // the zoomOnClick property to false.
      if (mc.getZoomOnClick()) {
        // Zoom into the cluster.
        mz = mc.getMaxZoom();
        theBounds = cClusterIcon.cluster_.getBounds();
        mc.getMap().fitBounds(theBounds);
        // There is a fix for Issue 170 here:
        setTimeout(function () {
          mc.getMap().fitBounds(theBounds);
          // Don't zoom beyond the max zoom level
          if (mz !== null && (mc.getMap().getZoom() > mz)) {
            mc.getMap().setZoom(mz + 1);
          }
        }, 100);
      }

      // Prevent event propagation to the map:
      e.cancelBubble = true;
      if (e.stopPropagation) {
        e.stopPropagation();
      }
    }
  });

  google.maps.event.addDomListener(this.div_, "mouseover", function () {
    var mc = cClusterIcon.cluster_.getMarkerClusterer();
    /**
     * This event is fired when the mouse moves over a cluster marker.
     * @name MarkerClusterer#mouseover
     * @param {Cluster} c The cluster that the mouse moved over.
     * @event
     */
    google.maps.event.trigger(mc, "mouseover", cClusterIcon.cluster_);
  });

  google.maps.event.addDomListener(this.div_, "mouseout", function () {
    var mc = cClusterIcon.cluster_.getMarkerClusterer();
    /**
     * This event is fired when the mouse moves out of a cluster marker.
     * @name MarkerClusterer#mouseout
     * @param {Cluster} c The cluster that the mouse moved out of.
     * @event
     */
    google.maps.event.trigger(mc, "mouseout", cClusterIcon.cluster_);
  });
};


/**
 * Removes the icon from the DOM.
 */
ClusterIcon.prototype.onRemove = function () {
  if (this.div_ && this.div_.parentNode) {
    this.hide();
    google.maps.event.removeListener(this.boundsChangedListener_);
    google.maps.event.clearInstanceListeners(this.div_);
    this.div_.parentNode.removeChild(this.div_);
    this.div_ = null;
  }
};


/**
 * Draws the icon.
 */
ClusterIcon.prototype.draw = function () {
  if (this.visible_) {
    var pos = this.getPosFromLatLng_(this.center_);
    this.div_.style.top = pos.y + "px";
    this.div_.style.left = pos.x + "px";
  }
};


/**
 * Hides the icon.
 */
ClusterIcon.prototype.hide = function () {
  if (this.div_) {
    this.div_.style.display = "none";
  }
  this.visible_ = false;
};


/**
 * Positions and shows the icon.
 */
ClusterIcon.prototype.show = function () {
  if (this.div_) {
    var img = "";
    // NOTE: values must be specified in px units
    var bp = this.backgroundPosition_.split(" ");
    var spriteH = parseInt(bp[0].replace(/^\s+|\s+$/g, ""), 10);
    var spriteV = parseInt(bp[1].replace(/^\s+|\s+$/g, ""), 10);
    var pos = this.getPosFromLatLng_(this.center_);
    this.div_.style.cssText = this.createCss(pos);
    img = "<img src='" + this.url_ + "' style='position: absolute; top: " + spriteV + "px; left: " + spriteH + "px; ";
    if (!this.cluster_.getMarkerClusterer().enableRetinaIcons_) {
      img += "clip: rect(" + (-1 * spriteV) + "px, " + ((-1 * spriteH) + this.width_) + "px, " +
          ((-1 * spriteV) + this.height_) + "px, " + (-1 * spriteH) + "px);";
    }
    img += "'>";
    this.div_.innerHTML = img + "<div style='" +
        "position: absolute;" +
        "top: " + this.anchorText_[0] + "px;" +
        "left: " + this.anchorText_[1] + "px;" +
        "color: " + this.textColor_ + ";" +
        "font-size: " + this.textSize_ + "px;" +
        "font-family: " + this.fontFamily_ + ";" +
        "font-weight: " + this.fontWeight_ + ";" +
        "font-style: " + this.fontStyle_ + ";" +
        "text-decoration: " + this.textDecoration_ + ";" +
        "text-align: center;" +
        "width: " + this.width_ + "px;" +
        "line-height:" + this.height_ + "px;" +
        "'>" + this.sums_.text + "</div>";
    if (typeof this.sums_.title === "undefined" || this.sums_.title === "") {
      this.div_.title = this.cluster_.getMarkerClusterer().getTitle();
    } else {
      this.div_.title = this.sums_.title;
    }
    this.div_.style.display = "";
  }
  this.visible_ = true;
};


/**
 * Sets the icon styles to the appropriate element in the styles array.
 *
 * @param {ClusterIconInfo} sums The icon label text and styles index.
 */
ClusterIcon.prototype.useStyle = function (sums) {
  this.sums_ = sums;
  var index = Math.max(0, sums.index - 1);
  index = Math.min(this.styles_.length - 1, index);
  var style = this.styles_[index];
  this.url_ = style.url;
  this.height_ = style.height;
  this.width_ = style.width;
  this.anchorText_ = style.anchorText || [0, 0];
  this.anchorIcon_ = style.anchorIcon || [parseInt(this.height_ / 2, 10), parseInt(this.width_ / 2, 10)];
  this.textColor_ = style.textColor || "black";
  this.textSize_ = style.textSize || 11;
  this.textDecoration_ = style.textDecoration || "none";
  this.fontWeight_ = style.fontWeight || "bold";
  this.fontStyle_ = style.fontStyle || "normal";
  this.fontFamily_ = style.fontFamily || "Arial,sans-serif";
  this.backgroundPosition_ = style.backgroundPosition || "0 0";
};


/**
 * Sets the position at which to center the icon.
 *
 * @param {google.maps.LatLng} center The latlng to set as the center.
 */
ClusterIcon.prototype.setCenter = function (center) {
  this.center_ = center;
};


/**
 * Creates the cssText style parameter based on the position of the icon.
 *
 * @param {google.maps.Point} pos The position of the icon.
 * @return {string} The CSS style text.
 */
ClusterIcon.prototype.createCss = function (pos) {
  var style = [];
  style.push("cursor: pointer;");
  style.push("position: absolute; top: " + pos.y + "px; left: " + pos.x + "px;");
  style.push("width: " + this.width_ + "px; height: " + this.height_ + "px;");
  return style.join("");
};


/**
 * Returns the position at which to place the DIV depending on the latlng.
 *
 * @param {google.maps.LatLng} latlng The position in latlng.
 * @return {google.maps.Point} The position in pixels.
 */
ClusterIcon.prototype.getPosFromLatLng_ = function (latlng) {
  var pos = this.getProjection().fromLatLngToDivPixel(latlng);
  pos.x -= this.anchorIcon_[1];
  pos.y -= this.anchorIcon_[0];
  pos.x = parseInt(pos.x, 10);
  pos.y = parseInt(pos.y, 10);
  return pos;
};


/**
 * Creates a single cluster that manages a group of proximate markers.
 *  Used internally, do not call this constructor directly.
 * @constructor
 * @param {MarkerClusterer} mc The <code>MarkerClusterer</code> object with which this
 *  cluster is associated.
 */
function Cluster(mc) {
  this.markerClusterer_ = mc;
  this.map_ = mc.getMap();
  this.gridSize_ = mc.getGridSize();
  this.minClusterSize_ = mc.getMinimumClusterSize();
  this.averageCenter_ = mc.getAverageCenter();
  this.markers_ = [];
  this.center_ = null;
  this.bounds_ = null;
  this.clusterIcon_ = new ClusterIcon(this, mc.getStyles());
}


/**
 * Returns the number of markers managed by the cluster. You can call this from
 * a <code>click</code>, <code>mouseover</code>, or <code>mouseout</code> event handler
 * for the <code>MarkerClusterer</code> object.
 *
 * @return {number} The number of markers in the cluster.
 */
Cluster.prototype.getSize = function () {
  return this.markers_.length;
};


/**
 * Returns the array of markers managed by the cluster. You can call this from
 * a <code>click</code>, <code>mouseover</code>, or <code>mouseout</code> event handler
 * for the <code>MarkerClusterer</code> object.
 *
 * @return {Array} The array of markers in the cluster.
 */
Cluster.prototype.getMarkers = function () {
  return this.markers_;
};


/**
 * Returns the center of the cluster. You can call this from
 * a <code>click</code>, <code>mouseover</code>, or <code>mouseout</code> event handler
 * for the <code>MarkerClusterer</code> object.
 *
 * @return {google.maps.LatLng} The center of the cluster.
 */
Cluster.prototype.getCenter = function () {
  return this.center_;
};


/**
 * Returns the map with which the cluster is associated.
 *
 * @return {google.maps.Map} The map.
 * @ignore
 */
Cluster.prototype.getMap = function () {
  return this.map_;
};


/**
 * Returns the <code>MarkerClusterer</code> object with which the cluster is associated.
 *
 * @return {MarkerClusterer} The associated marker clusterer.
 * @ignore
 */
Cluster.prototype.getMarkerClusterer = function () {
  return this.markerClusterer_;
};


/**
 * Returns the bounds of the cluster.
 *
 * @return {google.maps.LatLngBounds} the cluster bounds.
 * @ignore
 */
Cluster.prototype.getBounds = function () {
  var i;
  var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
  var markers = this.getMarkers();
  for (i = 0; i < markers.length; i++) {
    bounds.extend(markers[i].getPosition());
  }
  return bounds;
};


/**
 * Removes the cluster from the map.
 *
 * @ignore
 */
Cluster.prototype.remove = function () {
  this.clusterIcon_.setMap(null);
  this.markers_ = [];
  delete this.markers_;
};


/**
 * Adds a marker to the cluster.
 *
 * @param {google.maps.Marker} marker The marker to be added.
 * @return {boolean} True if the marker was added.
 * @ignore
 */
Cluster.prototype.addMarker = function (marker) {
  var i;
  var mCount;
  var mz;

  if (this.isMarkerAlreadyAdded_(marker)) {
    return false;
  }

  if (!this.center_) {
    this.center_ = marker.getPosition();
    this.calculateBounds_();
  } else {
    if (this.averageCenter_) {
      var l = this.markers_.length + 1;
      var lat = (this.center_.lat() * (l - 1) + marker.getPosition().lat()) / l;
      var lng = (this.center_.lng() * (l - 1) + marker.getPosition().lng()) / l;
      this.center_ = new google.maps.LatLng(lat, lng);
      this.calculateBounds_();
    }
  }

  marker.isAdded = true;
  this.markers_.push(marker);

  mCount = this.markers_.length;
  mz = this.markerClusterer_.getMaxZoom();
  if (mz !== null && this.map_.getZoom() > mz) {
    // Zoomed in past max zoom, so show the marker.
    if (marker.getMap() !== this.map_) {
      marker.setMap(this.map_);
    }
  } else if (mCount < this.minClusterSize_) {
    // Min cluster size not reached so show the marker.
    if (marker.getMap() !== this.map_) {
      marker.setMap(this.map_);
    }
  } else if (mCount === this.minClusterSize_) {
    // Hide the markers that were showing.
    for (i = 0; i < mCount; i++) {
      this.markers_[i].setMap(null);
    }
  } else {
    marker.setMap(null);
  }

  this.updateIcon_();
  return true;
};


/**
 * Determines if a marker lies within the cluster's bounds.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @return {boolean} True if the marker lies in the bounds.
 * @ignore
 */
Cluster.prototype.isMarkerInClusterBounds = function (marker) {
  return this.bounds_.contains(marker.getPosition());
};


/**
 * Calculates the extended bounds of the cluster with the grid.
 */
Cluster.prototype.calculateBounds_ = function () {
  var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
  this.bounds_ = this.markerClusterer_.getExtendedBounds(bounds);
};


/**
 * Updates the cluster icon.
 */
Cluster.prototype.updateIcon_ = function () {
  var mCount = this.markers_.length;
  var mz = this.markerClusterer_.getMaxZoom();

  if (mz !== null && this.map_.getZoom() > mz) {
    this.clusterIcon_.hide();
    return;
  }

  if (mCount < this.minClusterSize_) {
    // Min cluster size not yet reached.
    this.clusterIcon_.hide();
    return;
  }

  var numStyles = this.markerClusterer_.getStyles().length;
  var sums = this.markerClusterer_.getCalculator()(this.markers_, numStyles);
  this.clusterIcon_.setCenter(this.center_);
  this.clusterIcon_.useStyle(sums);
  this.clusterIcon_.show();
};


/**
 * Determines if a marker has already been added to the cluster.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @return {boolean} True if the marker has already been added.
 */
Cluster.prototype.isMarkerAlreadyAdded_ = function (marker) {
  var i;
  if (this.markers_.indexOf) {
    return this.markers_.indexOf(marker) !== -1;
  } else {
    for (i = 0; i < this.markers_.length; i++) {
      if (marker === this.markers_[i]) {
        return true;
      }
    }
  }
  return false;
};


/**
 * @name MarkerClustererOptions
 * @class This class represents the optional parameter passed to
 *  the {@link MarkerClusterer} constructor.
 * @property {number} [gridSize=60] The grid size of a cluster in pixels. The grid is a square.
 * @property {number} [maxZoom=null] The maximum zoom level at which clustering is enabled or
 *  <code>null</code> if clustering is to be enabled at all zoom levels.
 * @property {boolean} [zoomOnClick=true] Whether to zoom the map when a cluster marker is
 *  clicked. You may want to set this to <code>false</code> if you have installed a handler
 *  for the <code>click</code> event and it deals with zooming on its own.
 * @property {boolean} [averageCenter=false] Whether the position of a cluster marker should be
 *  the average position of all markers in the cluster. If set to <code>false</code>, the
 *  cluster marker is positioned at the location of the first marker added to the cluster.
 * @property {number} [minimumClusterSize=2] The minimum number of markers needed in a cluster
 *  before the markers are hidden and a cluster marker appears.
 * @property {boolean} [ignoreHidden=false] Whether to ignore hidden markers in clusters. You
 *  may want to set this to <code>true</code> to ensure that hidden markers are not included
 *  in the marker count that appears on a cluster marker (this count is the value of the
 *  <code>text</code> property of the result returned by the default <code>calculator</code>).
 *  If set to <code>true</code> and you change the visibility of a marker being clustered, be
 *  sure to also call <code>MarkerClusterer.repaint()</code>.
 * @property {string} [title=""] The tooltip to display when the mouse moves over a cluster
 *  marker. (Alternatively, you can use a custom <code>calculator</code> function to specify a
 *  different tooltip for each cluster marker.)
 * @property {function} [calculator=MarkerClusterer.CALCULATOR] The function used to determine
 *  the text to be displayed on a cluster marker and the index indicating which style to use
 *  for the cluster marker. The input parameters for the function are (1) the array of markers
 *  represented by a cluster marker and (2) the number of cluster icon styles. It returns a
 *  {@link ClusterIconInfo} object. The default <code>calculator</code> returns a
 *  <code>text</code> property which is the number of markers in the cluster and an
 *  <code>index</code> property which is one higher than the lowest integer such that
 *  <code>10^i</code> exceeds the number of markers in the cluster, or the size of the styles
 *  array, whichever is less. The <code>styles</code> array element used has an index of
 *  <code>index</code> minus 1. For example, the default <code>calculator</code> returns a
 *  <code>text</code> value of <code>"125"</code> and an <code>index</code> of <code>3</code>
 *  for a cluster icon representing 125 markers so the element used in the <code>styles</code>
 *  array is <code>2</code>. A <code>calculator</code> may also return a <code>title</code>
 *  property that contains the text of the tooltip to be used for the cluster marker. If
 *   <code>title</code> is not defined, the tooltip is set to the value of the <code>title</code>
 *   property for the MarkerClusterer.
 * @property {string} [clusterClass="cluster"] The name of the CSS class defining general styles
 *  for the cluster markers. Use this class to define CSS styles that are not set up by the code
 *  that processes the <code>styles</code> array.
 * @property {Array} [styles] An array of {@link ClusterIconStyle} elements defining the styles
 *  of the cluster markers to be used. The element to be used to style a given cluster marker
 *  is determined by the function defined by the <code>calculator</code> property.
 *  The default is an array of {@link ClusterIconStyle} elements whose properties are derived
 *  from the values for <code>imagePath</code>, <code>imageExtension</code>, and
 *  <code>imageSizes</code>.
 * @property {boolean} [enableRetinaIcons=false] Whether to allow the use of cluster icons that
 * have sizes that are some multiple (typically double) of their actual display size. Icons such
 * as these look better when viewed on high-resolution monitors such as Apple's Retina displays.
 * Note: if this property is <code>true</code>, sprites cannot be used as cluster icons.
 * @property {number} [batchSize=MarkerClusterer.BATCH_SIZE] Set this property to the
 *  number of markers to be processed in a single batch when using a browser other than
 *  Internet Explorer (for Internet Explorer, use the batchSizeIE property instead).
 * @property {number} [batchSizeIE=MarkerClusterer.BATCH_SIZE_IE] When Internet Explorer is
 *  being used, markers are processed in several batches with a small delay inserted between
 *  each batch in an attempt to avoid Javascript timeout errors. Set this property to the
 *  number of markers to be processed in a single batch; select as high a number as you can
 *  without causing a timeout error in the browser. This number might need to be as low as 100
 *  if 15,000 markers are being managed, for example.
 * @property {string} [imagePath=MarkerClusterer.IMAGE_PATH]
 *  The full URL of the root name of the group of image files to use for cluster icons.
 *  The complete file name is of the form <code>imagePath</code>n.<code>imageExtension</code>
 *  where n is the image file number (1, 2, etc.).
 * @property {string} [imageExtension=MarkerClusterer.IMAGE_EXTENSION]
 *  The extension name for the cluster icon image files (e.g., <code>"png"</code> or
 *  <code>"jpg"</code>).
 * @property {Array} [imageSizes=MarkerClusterer.IMAGE_SIZES]
 *  An array of numbers containing the widths of the group of
 *  <code>imagePath</code>n.<code>imageExtension</code> image files.
 *  (The images are assumed to be square.)
 */
/**
 * Creates a MarkerClusterer object with the options specified in {@link MarkerClustererOptions}.
 * @constructor
 * @extends google.maps.OverlayView
 * @param {google.maps.Map} map The Google map to attach to.
 * @param {Array.<google.maps.Marker>} [opt_markers] The markers to be added to the cluster.
 * @param {MarkerClustererOptions} [opt_options] The optional parameters.
 */
function MarkerClusterer(map, opt_markers, opt_options) {
  // MarkerClusterer implements google.maps.OverlayView interface. We use the
  // extend function to extend MarkerClusterer with google.maps.OverlayView
  // because it might not always be available when the code is defined so we
  // look for it at the last possible moment. If it doesn't exist now then
  // there is no point going ahead :)
  this.extend(MarkerClusterer, google.maps.OverlayView);

  opt_markers = opt_markers || [];
  opt_options = opt_options || {};

  this.markers_ = [];
  this.clusters_ = [];
  this.listeners_ = [];
  this.activeMap_ = null;
  this.ready_ = false;

  this.gridSize_ = opt_options.gridSize || 60;
  this.minClusterSize_ = opt_options.minimumClusterSize || 2;
  this.maxZoom_ = opt_options.maxZoom || null;
  this.styles_ = opt_options.styles || [];
  this.title_ = opt_options.title || "";
  this.zoomOnClick_ = true;
  if (opt_options.zoomOnClick !== undefined) {
    this.zoomOnClick_ = opt_options.zoomOnClick;
  }
  this.averageCenter_ = false;
  if (opt_options.averageCenter !== undefined) {
    this.averageCenter_ = opt_options.averageCenter;
  }
  this.ignoreHidden_ = false;
  if (opt_options.ignoreHidden !== undefined) {
    this.ignoreHidden_ = opt_options.ignoreHidden;
  }
  this.enableRetinaIcons_ = false;
  if (opt_options.enableRetinaIcons !== undefined) {
    this.enableRetinaIcons_ = opt_options.enableRetinaIcons;
  }
  this.imagePath_ = opt_options.imagePath || MarkerClusterer.IMAGE_PATH;
  this.imageExtension_ = opt_options.imageExtension || MarkerClusterer.IMAGE_EXTENSION;
  this.imageSizes_ = opt_options.imageSizes || MarkerClusterer.IMAGE_SIZES;
  this.calculator_ = opt_options.calculator || MarkerClusterer.CALCULATOR;
  this.batchSize_ = opt_options.batchSize || MarkerClusterer.BATCH_SIZE;
  this.batchSizeIE_ = opt_options.batchSizeIE || MarkerClusterer.BATCH_SIZE_IE;
  this.clusterClass_ = opt_options.clusterClass || "cluster";

  if (navigator.userAgent.toLowerCase().indexOf("msie") !== -1) {
    // Try to avoid IE timeout when processing a huge number of markers:
    this.batchSize_ = this.batchSizeIE_;
  }

  this.setupStyles_();

  this.addMarkers(opt_markers, true);
  this.setMap(map); // Note: this causes onAdd to be called
}


/**
 * Implementation of the onAdd interface method.
 * @ignore
 */
MarkerClusterer.prototype.onAdd = function () {
  var cMarkerClusterer = this;

  this.activeMap_ = this.getMap();
  this.ready_ = true;

  this.repaint();

  // Add the map event listeners
  this.listeners_ = [
    google.maps.event.addListener(this.getMap(), "zoom_changed", function () {
      cMarkerClusterer.resetViewport_(false);
      // Workaround for this Google bug: when map is at level 0 and "-" of
      // zoom slider is clicked, a "zoom_changed" event is fired even though
      // the map doesn't zoom out any further. In this situation, no "idle"
      // event is triggered so the cluster markers that have been removed
      // do not get redrawn. Same goes for a zoom in at maxZoom.
      if (this.getZoom() === (this.get("minZoom") || 0) || this.getZoom() === this.get("maxZoom")) {
        google.maps.event.trigger(this, "idle");
      }
    }),
    google.maps.event.addListener(this.getMap(), "idle", function () {
      cMarkerClusterer.redraw_();
    })
  ];
};


/**
 * Implementation of the onRemove interface method.
 * Removes map event listeners and all cluster icons from the DOM.
 * All managed markers are also put back on the map.
 * @ignore
 */
MarkerClusterer.prototype.onRemove = function () {
  var i;

  // Put all the managed markers back on the map:
  for (i = 0; i < this.markers_.length; i++) {
    if (this.markers_[i].getMap() !== this.activeMap_) {
      this.markers_[i].setMap(this.activeMap_);
    }
  }

  // Remove all clusters:
  for (i = 0; i < this.clusters_.length; i++) {
    this.clusters_[i].remove();
  }
  this.clusters_ = [];

  // Remove map event listeners:
  for (i = 0; i < this.listeners_.length; i++) {
    google.maps.event.removeListener(this.listeners_[i]);
  }
  this.listeners_ = [];

  this.activeMap_ = null;
  this.ready_ = false;
};


/**
 * Implementation of the draw interface method.
 * @ignore
 */
MarkerClusterer.prototype.draw = function () {};


/**
 * Sets up the styles object.
 */
MarkerClusterer.prototype.setupStyles_ = function () {
  var i, size;
  if (this.styles_.length > 0) {
    return;
  }

  for (i = 0; i < this.imageSizes_.length; i++) {
    size = this.imageSizes_[i];
    this.styles_.push({
      url: this.imagePath_ + (i + 1) + "." + this.imageExtension_,
      height: size,
      width: size
    });
  }
};


/**
 *  Fits the map to the bounds of the markers managed by the clusterer.
 */
MarkerClusterer.prototype.fitMapToMarkers = function () {
  var i;
  var markers = this.getMarkers();
  var bounds = new google.maps.LatLngBounds();
  for (i = 0; i < markers.length; i++) {
    bounds.extend(markers[i].getPosition());
  }

  this.getMap().fitBounds(bounds);
};


/**
 * Returns the value of the <code>gridSize</code> property.
 *
 * @return {number} The grid size.
 */
MarkerClusterer.prototype.getGridSize = function () {
  return this.gridSize_;
};


/**
 * Sets the value of the <code>gridSize</code> property.
 *
 * @param {number} gridSize The grid size.
 */
MarkerClusterer.prototype.setGridSize = function (gridSize) {
  this.gridSize_ = gridSize;
};


/**
 * Returns the value of the <code>minimumClusterSize</code> property.
 *
 * @return {number} The minimum cluster size.
 */
MarkerClusterer.prototype.getMinimumClusterSize = function () {
  return this.minClusterSize_;
};

/**
 * Sets the value of the <code>minimumClusterSize</code> property.
 *
 * @param {number} minimumClusterSize The minimum cluster size.
 */
MarkerClusterer.prototype.setMinimumClusterSize = function (minimumClusterSize) {
  this.minClusterSize_ = minimumClusterSize;
};


/**
 *  Returns the value of the <code>maxZoom</code> property.
 *
 *  @return {number} The maximum zoom level.
 */
MarkerClusterer.prototype.getMaxZoom = function () {
  return this.maxZoom_;
};


/**
 *  Sets the value of the <code>maxZoom</code> property.
 *
 *  @param {number} maxZoom The maximum zoom level.
 */
MarkerClusterer.prototype.setMaxZoom = function (maxZoom) {
  this.maxZoom_ = maxZoom;
};


/**
 *  Returns the value of the <code>styles</code> property.
 *
 *  @return {Array} The array of styles defining the cluster markers to be used.
 */
MarkerClusterer.prototype.getStyles = function () {
  return this.styles_;
};


/**
 *  Sets the value of the <code>styles</code> property.
 *
 *  @param {Array.<ClusterIconStyle>} styles The array of styles to use.
 */
MarkerClusterer.prototype.setStyles = function (styles) {
  this.styles_ = styles;
};


/**
 * Returns the value of the <code>title</code> property.
 *
 * @return {string} The content of the title text.
 */
MarkerClusterer.prototype.getTitle = function () {
  return this.title_;
};


/**
 *  Sets the value of the <code>title</code> property.
 *
 *  @param {string} title The value of the title property.
 */
MarkerClusterer.prototype.setTitle = function (title) {
  this.title_ = title;
};


/**
 * Returns the value of the <code>zoomOnClick</code> property.
 *
 * @return {boolean} True if zoomOnClick property is set.
 */
MarkerClusterer.prototype.getZoomOnClick = function () {
  return this.zoomOnClick_;
};


/**
 *  Sets the value of the <code>zoomOnClick</code> property.
 *
 *  @param {boolean} zoomOnClick The value of the zoomOnClick property.
 */
MarkerClusterer.prototype.setZoomOnClick = function (zoomOnClick) {
  this.zoomOnClick_ = zoomOnClick;
};


/**
 * Returns the value of the <code>averageCenter</code> property.
 *
 * @return {boolean} True if averageCenter property is set.
 */
MarkerClusterer.prototype.getAverageCenter = function () {
  return this.averageCenter_;
};


/**
 *  Sets the value of the <code>averageCenter</code> property.
 *
 *  @param {boolean} averageCenter The value of the averageCenter property.
 */
MarkerClusterer.prototype.setAverageCenter = function (averageCenter) {
  this.averageCenter_ = averageCenter;
};


/**
 * Returns the value of the <code>ignoreHidden</code> property.
 *
 * @return {boolean} True if ignoreHidden property is set.
 */
MarkerClusterer.prototype.getIgnoreHidden = function () {
  return this.ignoreHidden_;
};


/**
 *  Sets the value of the <code>ignoreHidden</code> property.
 *
 *  @param {boolean} ignoreHidden The value of the ignoreHidden property.
 */
MarkerClusterer.prototype.setIgnoreHidden = function (ignoreHidden) {
  this.ignoreHidden_ = ignoreHidden;
};


/**
 * Returns the value of the <code>enableRetinaIcons</code> property.
 *
 * @return {boolean} True if enableRetinaIcons property is set.
 */
MarkerClusterer.prototype.getEnableRetinaIcons = function () {
  return this.enableRetinaIcons_;
};


/**
 *  Sets the value of the <code>enableRetinaIcons</code> property.
 *
 *  @param {boolean} enableRetinaIcons The value of the enableRetinaIcons property.
 */
MarkerClusterer.prototype.setEnableRetinaIcons = function (enableRetinaIcons) {
  this.enableRetinaIcons_ = enableRetinaIcons;
};


/**
 * Returns the value of the <code>imageExtension</code> property.
 *
 * @return {string} The value of the imageExtension property.
 */
MarkerClusterer.prototype.getImageExtension = function () {
  return this.imageExtension_;
};


/**
 *  Sets the value of the <code>imageExtension</code> property.
 *
 *  @param {string} imageExtension The value of the imageExtension property.
 */
MarkerClusterer.prototype.setImageExtension = function (imageExtension) {
  this.imageExtension_ = imageExtension;
};


/**
 * Returns the value of the <code>imagePath</code> property.
 *
 * @return {string} The value of the imagePath property.
 */
MarkerClusterer.prototype.getImagePath = function () {
  return this.imagePath_;
};


/**
 *  Sets the value of the <code>imagePath</code> property.
 *
 *  @param {string} imagePath The value of the imagePath property.
 */
MarkerClusterer.prototype.setImagePath = function (imagePath) {
  this.imagePath_ = imagePath;
};


/**
 * Returns the value of the <code>imageSizes</code> property.
 *
 * @return {Array} The value of the imageSizes property.
 */
MarkerClusterer.prototype.getImageSizes = function () {
  return this.imageSizes_;
};


/**
 *  Sets the value of the <code>imageSizes</code> property.
 *
 *  @param {Array} imageSizes The value of the imageSizes property.
 */
MarkerClusterer.prototype.setImageSizes = function (imageSizes) {
  this.imageSizes_ = imageSizes;
};


/**
 * Returns the value of the <code>calculator</code> property.
 *
 * @return {function} the value of the calculator property.
 */
MarkerClusterer.prototype.getCalculator = function () {
  return this.calculator_;
};


/**
 * Sets the value of the <code>calculator</code> property.
 *
 * @param {function(Array.<google.maps.Marker>, number)} calculator The value
 *  of the calculator property.
 */
MarkerClusterer.prototype.setCalculator = function (calculator) {
  this.calculator_ = calculator;
};


/**
 * Returns the value of the <code>batchSizeIE</code> property.
 *
 * @return {number} the value of the batchSizeIE property.
 */
MarkerClusterer.prototype.getBatchSizeIE = function () {
  return this.batchSizeIE_;
};


/**
 * Sets the value of the <code>batchSizeIE</code> property.
 *
 *  @param {number} batchSizeIE The value of the batchSizeIE property.
 */
MarkerClusterer.prototype.setBatchSizeIE = function (batchSizeIE) {
  this.batchSizeIE_ = batchSizeIE;
};


/**
 * Returns the value of the <code>clusterClass</code> property.
 *
 * @return {string} the value of the clusterClass property.
 */
MarkerClusterer.prototype.getClusterClass = function () {
  return this.clusterClass_;
};


/**
 * Sets the value of the <code>clusterClass</code> property.
 *
 *  @param {string} clusterClass The value of the clusterClass property.
 */
MarkerClusterer.prototype.setClusterClass = function (clusterClass) {
  this.clusterClass_ = clusterClass;
};


/**
 *  Returns the array of markers managed by the clusterer.
 *
 *  @return {Array} The array of markers managed by the clusterer.
 */
MarkerClusterer.prototype.getMarkers = function () {
  return this.markers_;
};


/**
 *  Returns the number of markers managed by the clusterer.
 *
 *  @return {number} The number of markers.
 */
MarkerClusterer.prototype.getTotalMarkers = function () {
  return this.markers_.length;
};


/**
 * Returns the current array of clusters formed by the clusterer.
 *
 * @return {Array} The array of clusters formed by the clusterer.
 */
MarkerClusterer.prototype.getClusters = function () {
  return this.clusters_;
};


/**
 * Returns the number of clusters formed by the clusterer.
 *
 * @return {number} The number of clusters formed by the clusterer.
 */
MarkerClusterer.prototype.getTotalClusters = function () {
  return this.clusters_.length;
};


/**
 * Adds a marker to the clusterer. The clusters are redrawn unless
 *  <code>opt_nodraw</code> is set to <code>true</code>.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @param {boolean} [opt_nodraw] Set to <code>true</code> to prevent redrawing.
 */
MarkerClusterer.prototype.addMarker = function (marker, opt_nodraw) {
  this.pushMarkerTo_(marker);
  if (!opt_nodraw) {
    this.redraw_();
  }
};


/**
 * Adds an array of markers to the clusterer. The clusters are redrawn unless
 *  <code>opt_nodraw</code> is set to <code>true</code>.
 *
 * @param {Array.<google.maps.Marker>} markers The markers to add.
 * @param {boolean} [opt_nodraw] Set to <code>true</code> to prevent redrawing.
 */
MarkerClusterer.prototype.addMarkers = function (markers, opt_nodraw) {
  var key;
  for (key in markers) {
    if (markers.hasOwnProperty(key)) {
      this.pushMarkerTo_(markers[key]);
    }
  }  
  if (!opt_nodraw) {
    this.redraw_();
  }
};


/**
 * Pushes a marker to the clusterer.
 *
 * @param {google.maps.Marker} marker The marker to add.
 */
MarkerClusterer.prototype.pushMarkerTo_ = function (marker) {
  // If the marker is draggable add a listener so we can update the clusters on the dragend:
  if (marker.getDraggable()) {
    var cMarkerClusterer = this;
    google.maps.event.addListener(marker, "dragend", function () {
      if (cMarkerClusterer.ready_) {
        this.isAdded = false;
        cMarkerClusterer.repaint();
      }
    });
  }
  marker.isAdded = false;
  this.markers_.push(marker);
};


/**
 * Removes a marker from the cluster.  The clusters are redrawn unless
 *  <code>opt_nodraw</code> is set to <code>true</code>. Returns <code>true</code> if the
 *  marker was removed from the clusterer.
 *
 * @param {google.maps.Marker} marker The marker to remove.
 * @param {boolean} [opt_nodraw] Set to <code>true</code> to prevent redrawing.
 * @return {boolean} True if the marker was removed from the clusterer.
 */
MarkerClusterer.prototype.removeMarker = function (marker, opt_nodraw) {
  var removed = this.removeMarker_(marker);

  if (!opt_nodraw && removed) {
    this.repaint();
  }

  return removed;
};


/**
 * Removes an array of markers from the cluster. The clusters are redrawn unless
 *  <code>opt_nodraw</code> is set to <code>true</code>. Returns <code>true</code> if markers
 *  were removed from the clusterer.
 *
 * @param {Array.<google.maps.Marker>} markers The markers to remove.
 * @param {boolean} [opt_nodraw] Set to <code>true</code> to prevent redrawing.
 * @return {boolean} True if markers were removed from the clusterer.
 */
MarkerClusterer.prototype.removeMarkers = function (markers, opt_nodraw) {
  var i, r;
  var removed = false;

  for (i = 0; i < markers.length; i++) {
    r = this.removeMarker_(markers[i]);
    removed = removed || r;
  }

  if (!opt_nodraw && removed) {
    this.repaint();
  }

  return removed;
};


/**
 * Removes a marker and returns true if removed, false if not.
 *
 * @param {google.maps.Marker} marker The marker to remove
 * @return {boolean} Whether the marker was removed or not
 */
MarkerClusterer.prototype.removeMarker_ = function (marker) {
  var i;
  var index = -1;
  if (this.markers_.indexOf) {
    index = this.markers_.indexOf(marker);
  } else {
    for (i = 0; i < this.markers_.length; i++) {
      if (marker === this.markers_[i]) {
        index = i;
        break;
      }
    }
  }

  if (index === -1) {
    // Marker is not in our list of markers, so do nothing:
    return false;
  }

  marker.setMap(null);
  this.markers_.splice(index, 1); // Remove the marker from the list of managed markers
  return true;
};


/**
 * Removes all clusters and markers from the map and also removes all markers
 *  managed by the clusterer.
 */
MarkerClusterer.prototype.clearMarkers = function () {
  this.resetViewport_(true);
  this.markers_ = [];
};


/**
 * Recalculates and redraws all the marker clusters from scratch.
 *  Call this after changing any properties.
 */
MarkerClusterer.prototype.repaint = function () {
  var oldClusters = this.clusters_.slice();
  this.clusters_ = [];
  this.resetViewport_(false);
  this.redraw_();

  // Remove the old clusters.
  // Do it in a timeout to prevent blinking effect.
  setTimeout(function () {
    var i;
    for (i = 0; i < oldClusters.length; i++) {
      oldClusters[i].remove();
    }
  }, 0);
};


/**
 * Returns the current bounds extended by the grid size.
 *
 * @param {google.maps.LatLngBounds} bounds The bounds to extend.
 * @return {google.maps.LatLngBounds} The extended bounds.
 * @ignore
 */
MarkerClusterer.prototype.getExtendedBounds = function (bounds) {
  var projection = this.getProjection();

  // Turn the bounds into latlng.
  var tr = new google.maps.LatLng(bounds.getNorthEast().lat(),
      bounds.getNorthEast().lng());
  var bl = new google.maps.LatLng(bounds.getSouthWest().lat(),
      bounds.getSouthWest().lng());

  // Convert the points to pixels and the extend out by the grid size.
  var trPix = projection.fromLatLngToDivPixel(tr);
  trPix.x += this.gridSize_;
  trPix.y -= this.gridSize_;

  var blPix = projection.fromLatLngToDivPixel(bl);
  blPix.x -= this.gridSize_;
  blPix.y += this.gridSize_;

  // Convert the pixel points back to LatLng
  var ne = projection.fromDivPixelToLatLng(trPix);
  var sw = projection.fromDivPixelToLatLng(blPix);

  // Extend the bounds to contain the new bounds.
  bounds.extend(ne);
  bounds.extend(sw);

  return bounds;
};


/**
 * Redraws all the clusters.
 */
MarkerClusterer.prototype.redraw_ = function () {
  this.createClusters_(0);
};


/**
 * Removes all clusters from the map. The markers are also removed from the map
 *  if <code>opt_hide</code> is set to <code>true</code>.
 *
 * @param {boolean} [opt_hide] Set to <code>true</code> to also remove the markers
 *  from the map.
 */
MarkerClusterer.prototype.resetViewport_ = function (opt_hide) {
  var i, marker;
  // Remove all the clusters
  for (i = 0; i < this.clusters_.length; i++) {
    this.clusters_[i].remove();
  }
  this.clusters_ = [];

  // Reset the markers to not be added and to be removed from the map.
  for (i = 0; i < this.markers_.length; i++) {
    marker = this.markers_[i];
    marker.isAdded = false;
    if (opt_hide) {
      marker.setMap(null);
    }
  }
};


/**
 * Calculates the distance between two latlng locations in km.
 *
 * @param {google.maps.LatLng} p1 The first lat lng point.
 * @param {google.maps.LatLng} p2 The second lat lng point.
 * @return {number} The distance between the two points in km.
 * @see http://www.movable-type.co.uk/scripts/latlong.html
*/
MarkerClusterer.prototype.distanceBetweenPoints_ = function (p1, p2) {
  var R = 6371; // Radius of the Earth in km
  var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
  var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d;
};


/**
 * Determines if a marker is contained in a bounds.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @param {google.maps.LatLngBounds} bounds The bounds to check against.
 * @return {boolean} True if the marker is in the bounds.
 */
MarkerClusterer.prototype.isMarkerInBounds_ = function (marker, bounds) {
  return bounds.contains(marker.getPosition());
};


/**
 * Adds a marker to a cluster, or creates a new cluster.
 *
 * @param {google.maps.Marker} marker The marker to add.
 */
MarkerClusterer.prototype.addToClosestCluster_ = function (marker) {
  var i, d, cluster, center;
  var distance = 40000; // Some large number
  var clusterToAddTo = null;
  for (i = 0; i < this.clusters_.length; i++) {
    cluster = this.clusters_[i];
    center = cluster.getCenter();
    if (center) {
      d = this.distanceBetweenPoints_(center, marker.getPosition());
      if (d < distance) {
        distance = d;
        clusterToAddTo = cluster;
      }
    }
  }

  if (clusterToAddTo && clusterToAddTo.isMarkerInClusterBounds(marker)) {
    clusterToAddTo.addMarker(marker);
  } else {
    cluster = new Cluster(this);
    cluster.addMarker(marker);
    this.clusters_.push(cluster);
  }
};


/**
 * Creates the clusters. This is done in batches to avoid timeout errors
 *  in some browsers when there is a huge number of markers.
 *
 * @param {number} iFirst The index of the first marker in the batch of
 *  markers to be added to clusters.
 */
MarkerClusterer.prototype.createClusters_ = function (iFirst) {
  var i, marker;
  var mapBounds;
  var cMarkerClusterer = this;
  if (!this.ready_) {
    return;
  }

  // Cancel previous batch processing if we're working on the first batch:
  if (iFirst === 0) {
    /**
     * This event is fired when the <code>MarkerClusterer</code> begins
     *  clustering markers.
     * @name MarkerClusterer#clusteringbegin
     * @param {MarkerClusterer} mc The MarkerClusterer whose markers are being clustered.
     * @event
     */
    google.maps.event.trigger(this, "clusteringbegin", this);

    if (typeof this.timerRefStatic !== "undefined") {
      clearTimeout(this.timerRefStatic);
      delete this.timerRefStatic;
    }
  }

  // Get our current map view bounds.
  // Create a new bounds object so we don't affect the map.
  //
  // See Comments 9 & 11 on Issue 3651 relating to this workaround for a Google Maps bug:
  if (this.getMap().getZoom() > 3) {
    mapBounds = new google.maps.LatLngBounds(this.getMap().getBounds().getSouthWest(),
      this.getMap().getBounds().getNorthEast());
  } else {
    mapBounds = new google.maps.LatLngBounds(new google.maps.LatLng(85.02070771743472, -178.48388434375), new google.maps.LatLng(-85.08136444384544, 178.00048865625));
  }
  var bounds = this.getExtendedBounds(mapBounds);

  var iLast = Math.min(iFirst + this.batchSize_, this.markers_.length);

  for (i = iFirst; i < iLast; i++) {
    marker = this.markers_[i];
    if (!marker.isAdded && this.isMarkerInBounds_(marker, bounds)) {
      if (!this.ignoreHidden_ || (this.ignoreHidden_ && marker.getVisible())) {
        this.addToClosestCluster_(marker);
      }
    }
  }

  if (iLast < this.markers_.length) {
    this.timerRefStatic = setTimeout(function () {
      cMarkerClusterer.createClusters_(iLast);
    }, 0);
  } else {
    delete this.timerRefStatic;

    /**
     * This event is fired when the <code>MarkerClusterer</code> stops
     *  clustering markers.
     * @name MarkerClusterer#clusteringend
     * @param {MarkerClusterer} mc The MarkerClusterer whose markers are being clustered.
     * @event
     */
    google.maps.event.trigger(this, "clusteringend", this);
  }
};


/**
 * Extends an object's prototype by another's.
 *
 * @param {Object} obj1 The object to be extended.
 * @param {Object} obj2 The object to extend with.
 * @return {Object} The new extended object.
 * @ignore
 */
MarkerClusterer.prototype.extend = function (obj1, obj2) {
  return (function (object) {
    var property;
    for (property in object.prototype) {
      this.prototype[property] = object.prototype[property];
    }
    return this;
  }).apply(obj1, [obj2]);
};


/**
 * The default function for determining the label text and style
 * for a cluster icon.
 *
 * @param {Array.<google.maps.Marker>} markers The array of markers represented by the cluster.
 * @param {number} numStyles The number of marker styles available.
 * @return {ClusterIconInfo} The information resource for the cluster.
 * @constant
 * @ignore
 */
MarkerClusterer.CALCULATOR = function (markers, numStyles) {
  var index = 0;
  var title = "";
  var count = markers.length.toString();

  var dv = count;
  while (dv !== 0) {
    dv = parseInt(dv / 10, 10);
    index++;
  }

  index = Math.min(index, numStyles);
  return {
    text: count,
    index: index,
    title: title
  };
};


/**
 * The number of markers to process in one batch.
 *
 * @type {number}
 * @constant
 */
MarkerClusterer.BATCH_SIZE = 2000;


/**
 * The number of markers to process in one batch (IE only).
 *
 * @type {number}
 * @constant
 */
MarkerClusterer.BATCH_SIZE_IE = 500;


/**
 * The default root name for the marker cluster images.
 *
 * @type {string}
 * @constant
 */
MarkerClusterer.IMAGE_PATH = "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/images/m";


/**
 * The default extension name for the marker cluster images.
 *
 * @type {string}
 * @constant
 */
MarkerClusterer.IMAGE_EXTENSION = "png";


/**
 * The default array of sizes for the marker cluster images.
 *
 * @type {Array.<number>}
 * @constant
 */
MarkerClusterer.IMAGE_SIZES = [53, 56, 66, 78, 90];

/* End */
;
; /* Start:"a:4:{s:4:"full";s:96:"/bitrix/templates/aspro_optimus/components/bitrix/map.google.view/map/infobox.js?149909897423431";s:6:"source";s:80:"/bitrix/templates/aspro_optimus/components/bitrix/map.google.view/map/infobox.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
/**
 * @name InfoBox
 * @version 1.1.13 [March 19, 2014]
 * @author Gary Little (inspired by proof-of-concept code from Pamela Fox of Google)
 * @copyright Copyright 2010 Gary Little [gary at luxcentral.com]
 * @fileoverview InfoBox extends the Google Maps JavaScript API V3 <tt>OverlayView</tt> class.
 *  <p>
 *  An InfoBox behaves like a <tt>google.maps.InfoWindow</tt>, but it supports several
 *  additional properties for advanced styling. An InfoBox can also be used as a map label.
 *  <p>
 *  An InfoBox also fires the same events as a <tt>google.maps.InfoWindow</tt>.
 */

/*!
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*jslint browser:true */
/*global google */

/**
 * @name InfoBoxOptions
 * @class This class represents the optional parameter passed to the {@link InfoBox} constructor.
 * @property {string|Node} content The content of the InfoBox (plain text or an HTML DOM node).
 * @property {boolean} [disableAutoPan=false] Disable auto-pan on <tt>open</tt>.
 * @property {number} maxWidth The maximum width (in pixels) of the InfoBox. Set to 0 if no maximum.
 * @property {Size} pixelOffset The offset (in pixels) from the top left corner of the InfoBox
 *  (or the bottom left corner if the <code>alignBottom</code> property is <code>true</code>)
 *  to the map pixel corresponding to <tt>position</tt>.
 * @property {LatLng} position The geographic location at which to display the InfoBox.
 * @property {number} zIndex The CSS z-index style value for the InfoBox.
 *  Note: This value overrides a zIndex setting specified in the <tt>boxStyle</tt> property.
 * @property {string} [boxClass="infoBox"] The name of the CSS class defining the styles for the InfoBox container.
 * @property {Object} [boxStyle] An object literal whose properties define specific CSS
 *  style values to be applied to the InfoBox. Style values defined here override those that may
 *  be defined in the <code>boxClass</code> style sheet. If this property is changed after the
 *  InfoBox has been created, all previously set styles (except those defined in the style sheet)
 *  are removed from the InfoBox before the new style values are applied.
 * @property {string} closeBoxMargin The CSS margin style value for the close box.
 *  The default is "2px" (a 2-pixel margin on all sides).
 * @property {string} closeBoxURL The URL of the image representing the close box.
 *  Note: The default is the URL for Google's standard close box.
 *  Set this property to "" if no close box is required.
 * @property {Size} infoBoxClearance Minimum offset (in pixels) from the InfoBox to the
 *  map edge after an auto-pan.
 * @property {boolean} [isHidden=false] Hide the InfoBox on <tt>open</tt>.
 *  [Deprecated in favor of the <tt>visible</tt> property.]
 * @property {boolean} [visible=true] Show the InfoBox on <tt>open</tt>.
 * @property {boolean} alignBottom Align the bottom left corner of the InfoBox to the <code>position</code>
 *  location (default is <tt>false</tt> which means that the top left corner of the InfoBox is aligned).
 * @property {string} pane The pane where the InfoBox is to appear (default is "floatPane").
 *  Set the pane to "mapPane" if the InfoBox is being used as a map label.
 *  Valid pane names are the property names for the <tt>google.maps.MapPanes</tt> object.
 * @property {boolean} enableEventPropagation Propagate mousedown, mousemove, mouseover, mouseout,
 *  mouseup, click, dblclick, touchstart, touchend, touchmove, and contextmenu events in the InfoBox
 *  (default is <tt>false</tt> to mimic the behavior of a <tt>google.maps.InfoWindow</tt>). Set
 *  this property to <tt>true</tt> if the InfoBox is being used as a map label.
 */

/**
 * Creates an InfoBox with the options specified in {@link InfoBoxOptions}.
 *  Call <tt>InfoBox.open</tt> to add the box to the map.
 * @constructor
 * @param {InfoBoxOptions} [opt_opts]
 */
function InfoBox(opt_opts) {

  opt_opts = opt_opts || {};

  google.maps.OverlayView.apply(this, arguments);

  // Standard options (in common with google.maps.InfoWindow):
  //
  this.content_ = opt_opts.content || "";
  this.disableAutoPan_ = opt_opts.disableAutoPan || false;
  this.maxWidth_ = opt_opts.maxWidth || 0;
  this.pixelOffset_ = opt_opts.pixelOffset || new google.maps.Size(0, 0);
  this.position_ = opt_opts.position || new google.maps.LatLng(0, 0);
  this.zIndex_ = opt_opts.zIndex || null;

  // Additional options (unique to InfoBox):
  //
  this.boxClass_ = opt_opts.boxClass || "infoBox";
  this.boxStyle_ = opt_opts.boxStyle || {};
  this.closeBoxMargin_ = opt_opts.closeBoxMargin || "2px";
  this.closeBoxURL_ = opt_opts.closeBoxURL || "http://www.google.com/intl/en_us/mapfiles/close.gif";
  if (opt_opts.closeBoxURL === "") {
    this.closeBoxURL_ = "";
  }
  this.infoBoxClearance_ = opt_opts.infoBoxClearance || new google.maps.Size(1, 1);

  if (typeof opt_opts.visible === "undefined") {
    if (typeof opt_opts.isHidden === "undefined") {
      opt_opts.visible = true;
    } else {
      opt_opts.visible = !opt_opts.isHidden;
    }
  }
  this.isHidden_ = !opt_opts.visible;

  this.alignBottom_ = opt_opts.alignBottom || false;
  this.pane_ = opt_opts.pane || "floatPane";
  this.enableEventPropagation_ = opt_opts.enableEventPropagation || false;

  this.div_ = null;
  this.closeListener_ = null;
  this.moveListener_ = null;
  this.contextListener_ = null;
  this.eventListeners_ = null;
  this.fixedWidthSet_ = null;
}

/* InfoBox extends OverlayView in the Google Maps API v3.
 */
InfoBox.prototype = new google.maps.OverlayView();

/**
 * Creates the DIV representing the InfoBox.
 * @private
 */
InfoBox.prototype.createInfoBoxDiv_ = function () {

  var i;
  var events;
  var bw;
  var me = this;

  // This handler prevents an event in the InfoBox from being passed on to the map.
  //
  var cancelHandler = function (e) {
    e.cancelBubble = true;
    if (e.stopPropagation) {
      e.stopPropagation();
    }
  };

  // This handler ignores the current event in the InfoBox and conditionally prevents
  // the event from being passed on to the map. It is used for the contextmenu event.
  //
  var ignoreHandler = function (e) {

    e.returnValue = false;

    if (e.preventDefault) {

      e.preventDefault();
    }

    if (!me.enableEventPropagation_) {

      cancelHandler(e);
    }
  };

  if (!this.div_) {

    this.div_ = document.createElement("div");

    this.setBoxStyle_();

    if (typeof this.content_.nodeType === "undefined") {
      this.div_.innerHTML = this.getCloseBoxImg_() + this.content_;
    } else {
      this.div_.innerHTML = this.getCloseBoxImg_();
      this.div_.appendChild(this.content_);
    }

    // Add the InfoBox DIV to the DOM
    this.getPanes()[this.pane_].appendChild(this.div_);

    this.addClickHandler_();

    if (this.div_.style.width) {

      this.fixedWidthSet_ = true;

    } else {

      if (this.maxWidth_ !== 0 && this.div_.offsetWidth > this.maxWidth_) {

        this.div_.style.width = this.maxWidth_;
        this.div_.style.overflow = "auto";
        this.fixedWidthSet_ = true;

      } else { // The following code is needed to overcome problems with MSIE

        bw = this.getBoxWidths_();

        this.div_.style.width = (this.div_.offsetWidth - bw.left - bw.right) + "px";
        this.fixedWidthSet_ = false;
      }
    }

    this.panBox_(this.disableAutoPan_);

    if (!this.enableEventPropagation_) {

      this.eventListeners_ = [];

      // Cancel event propagation.
      //
      // Note: mousemove not included (to resolve Issue 152)
      events = ["mousedown", "mouseover", "mouseout", "mouseup",
      "click", "dblclick", "touchstart", "touchend", "touchmove"];

      for (i = 0; i < events.length; i++) {

        this.eventListeners_.push(google.maps.event.addDomListener(this.div_, events[i], cancelHandler));
      }
      
      // Workaround for Google bug that causes the cursor to change to a pointer
      // when the mouse moves over a marker underneath InfoBox.
      this.eventListeners_.push(google.maps.event.addDomListener(this.div_, "mouseover", function (e) {
        this.style.cursor = "default";
      }));
    }

    this.contextListener_ = google.maps.event.addDomListener(this.div_, "contextmenu", ignoreHandler);

    /**
     * This event is fired when the DIV containing the InfoBox's content is attached to the DOM.
     * @name InfoBox#domready
     * @event
     */
    google.maps.event.trigger(this, "domready");
  }
};

/**
 * Returns the HTML <IMG> tag for the close box.
 * @private
 */
InfoBox.prototype.getCloseBoxImg_ = function () {

  var img = "";

  if (this.closeBoxURL_ !== "") {

    /*img  = "<img";
    img += " src='" + this.closeBoxURL_ + "'";
    img += " align=right"; // Do this because Opera chokes on style='float: right;'
    img += " style='";
    img += " position: relative;"; // Required by MSIE
    img += " cursor: pointer;";
    img += " margin: " + this.closeBoxMargin_ + ";";
    img += "'>";*/
	img = "<div class='close_info'>";
	img += 'x';
	img += "'</div>";
  }

  return img;
};

/**
 * Adds the click handler to the InfoBox close box.
 * @private
 */
InfoBox.prototype.addClickHandler_ = function () {

  var closeBox;

  if (this.closeBoxURL_ !== "") {

    closeBox = this.div_.firstChild;
    this.closeListener_ = google.maps.event.addDomListener(closeBox, "click", this.getCloseClickHandler_());

  } else {

    this.closeListener_ = null;
  }
};

/**
 * Returns the function to call when the user clicks the close box of an InfoBox.
 * @private
 */
InfoBox.prototype.getCloseClickHandler_ = function () {

  var me = this;

  return function (e) {

    // 1.0.3 fix: Always prevent propagation of a close box click to the map:
    e.cancelBubble = true;

    if (e.stopPropagation) {

      e.stopPropagation();
    }

    /**
     * This event is fired when the InfoBox's close box is clicked.
     * @name InfoBox#closeclick
     * @event
     */
    google.maps.event.trigger(me, "closeclick");

    me.close();
  };
};

/**
 * Pans the map so that the InfoBox appears entirely within the map's visible area.
 * @private
 */
InfoBox.prototype.panBox_ = function (disablePan) {

  var map;
  var bounds;
  var xOffset = 0, yOffset = 0;

  if (!disablePan) {

    map = this.getMap();

    if (map instanceof google.maps.Map) { // Only pan if attached to map, not panorama

      if (!map.getBounds().contains(this.position_)) {
      // Marker not in visible area of map, so set center
      // of map to the marker position first.
        map.setCenter(this.position_);
      }

      bounds = map.getBounds();

      var mapDiv = map.getDiv();
      var mapWidth = mapDiv.offsetWidth;
      var mapHeight = mapDiv.offsetHeight;
      var iwOffsetX = this.pixelOffset_.width;
      var iwOffsetY = this.pixelOffset_.height;
      var iwWidth = this.div_.offsetWidth;
      var iwHeight = this.div_.offsetHeight;
      var padX = this.infoBoxClearance_.width;
      var padY = this.infoBoxClearance_.height;
      var pixPosition = this.getProjection().fromLatLngToContainerPixel(this.position_);

      if (pixPosition.x < (-iwOffsetX + padX)) {
        xOffset = pixPosition.x + iwOffsetX - padX;
      } else if ((pixPosition.x + iwWidth + iwOffsetX + padX) > mapWidth) {
        xOffset = pixPosition.x + iwWidth + iwOffsetX + padX - mapWidth;
      }
      if (this.alignBottom_) {
        if (pixPosition.y < (-iwOffsetY + padY + iwHeight)) {
          yOffset = pixPosition.y + iwOffsetY - padY - iwHeight;
        } else if ((pixPosition.y + iwOffsetY + padY) > mapHeight) {
          yOffset = pixPosition.y + iwOffsetY + padY - mapHeight;
        }
      } else {
        if (pixPosition.y < (-iwOffsetY + padY)) {
          yOffset = pixPosition.y + iwOffsetY - padY;
        } else if ((pixPosition.y + iwHeight + iwOffsetY + padY) > mapHeight) {
          yOffset = pixPosition.y + iwHeight + iwOffsetY + padY - mapHeight;
        }
      }

      if (!(xOffset === 0 && yOffset === 0)) {

        // Move the map to the shifted center.
        //
        var c = map.getCenter();
        map.panBy(xOffset, yOffset);
      }
    }
  }
};

/**
 * Sets the style of the InfoBox by setting the style sheet and applying
 * other specific styles requested.
 * @private
 */
InfoBox.prototype.setBoxStyle_ = function () {

  var i, boxStyle;

  if (this.div_) {

    // Apply style values from the style sheet defined in the boxClass parameter:
    this.div_.className = this.boxClass_;

    // Clear existing inline style values:
    this.div_.style.cssText = "";

    // Apply style values defined in the boxStyle parameter:
    boxStyle = this.boxStyle_;
    for (i in boxStyle) {

      if (boxStyle.hasOwnProperty(i)) {

        this.div_.style[i] = boxStyle[i];
      }
    }

    // Fix for iOS disappearing InfoBox problem.
    // See http://stackoverflow.com/questions/9229535/google-maps-markers-disappear-at-certain-zoom-level-only-on-iphone-ipad
    this.div_.style.WebkitTransform = "translateZ(0)";

    // Fix up opacity style for benefit of MSIE:
    //
    if (typeof this.div_.style.opacity !== "undefined" && this.div_.style.opacity !== "") {
      // See http://www.quirksmode.org/css/opacity.html
      this.div_.style.MsFilter = "\"progid:DXImageTransform.Microsoft.Alpha(Opacity=" + (this.div_.style.opacity * 100) + ")\"";
      this.div_.style.filter = "alpha(opacity=" + (this.div_.style.opacity * 100) + ")";
    }

    // Apply required styles:
    //
    this.div_.style.position = "absolute";
    this.div_.style.visibility = 'hidden';
    if (this.zIndex_ !== null) {

      this.div_.style.zIndex = this.zIndex_;
    }
  }
};

/**
 * Get the widths of the borders of the InfoBox.
 * @private
 * @return {Object} widths object (top, bottom left, right)
 */
InfoBox.prototype.getBoxWidths_ = function () {

  var computedStyle;
  var bw = {top: 0, bottom: 0, left: 0, right: 0};
  var box = this.div_;

  if (document.defaultView && document.defaultView.getComputedStyle) {

    computedStyle = box.ownerDocument.defaultView.getComputedStyle(box, "");

    if (computedStyle) {

      // The computed styles are always in pixel units (good!)
      bw.top = parseInt(computedStyle.borderTopWidth, 10) || 0;
      bw.bottom = parseInt(computedStyle.borderBottomWidth, 10) || 0;
      bw.left = parseInt(computedStyle.borderLeftWidth, 10) || 0;
      bw.right = parseInt(computedStyle.borderRightWidth, 10) || 0;
    }

  } else if (document.documentElement.currentStyle) { // MSIE

    if (box.currentStyle) {

      // The current styles may not be in pixel units, but assume they are (bad!)
      bw.top = parseInt(box.currentStyle.borderTopWidth, 10) || 0;
      bw.bottom = parseInt(box.currentStyle.borderBottomWidth, 10) || 0;
      bw.left = parseInt(box.currentStyle.borderLeftWidth, 10) || 0;
      bw.right = parseInt(box.currentStyle.borderRightWidth, 10) || 0;
    }
  }

  return bw;
};

/**
 * Invoked when <tt>close</tt> is called. Do not call it directly.
 */
InfoBox.prototype.onRemove = function () {

  if (this.div_) {

    this.div_.parentNode.removeChild(this.div_);
    this.div_ = null;
  }
};

/**
 * Draws the InfoBox based on the current map projection and zoom level.
 */
InfoBox.prototype.draw = function () {

  this.createInfoBoxDiv_();

  var pixPosition = this.getProjection().fromLatLngToDivPixel(this.position_);

  this.div_.style.left = (pixPosition.x + this.pixelOffset_.width) + "px";
  
  if (this.alignBottom_) {
    this.div_.style.bottom = -(pixPosition.y + this.pixelOffset_.height) + "px";
  } else {
    this.div_.style.top = (pixPosition.y + this.pixelOffset_.height) + "px";
  }

  if (this.isHidden_) {

    this.div_.style.visibility = "hidden";

  } else {

    this.div_.style.visibility = "visible";
  }
};

/**
 * Sets the options for the InfoBox. Note that changes to the <tt>maxWidth</tt>,
 *  <tt>closeBoxMargin</tt>, <tt>closeBoxURL</tt>, and <tt>enableEventPropagation</tt>
 *  properties have no affect until the current InfoBox is <tt>close</tt>d and a new one
 *  is <tt>open</tt>ed.
 * @param {InfoBoxOptions} opt_opts
 */
InfoBox.prototype.setOptions = function (opt_opts) {
  if (typeof opt_opts.boxClass !== "undefined") { // Must be first

    this.boxClass_ = opt_opts.boxClass;
    this.setBoxStyle_();
  }
  if (typeof opt_opts.boxStyle !== "undefined") { // Must be second

    this.boxStyle_ = opt_opts.boxStyle;
    this.setBoxStyle_();
  }
  if (typeof opt_opts.content !== "undefined") {

    this.setContent(opt_opts.content);
  }
  if (typeof opt_opts.disableAutoPan !== "undefined") {

    this.disableAutoPan_ = opt_opts.disableAutoPan;
  }
  if (typeof opt_opts.maxWidth !== "undefined") {

    this.maxWidth_ = opt_opts.maxWidth;
  }
  if (typeof opt_opts.pixelOffset !== "undefined") {

    this.pixelOffset_ = opt_opts.pixelOffset;
  }
  if (typeof opt_opts.alignBottom !== "undefined") {

    this.alignBottom_ = opt_opts.alignBottom;
  }
  if (typeof opt_opts.position !== "undefined") {

    this.setPosition(opt_opts.position);
  }
  if (typeof opt_opts.zIndex !== "undefined") {

    this.setZIndex(opt_opts.zIndex);
  }
  if (typeof opt_opts.closeBoxMargin !== "undefined") {

    this.closeBoxMargin_ = opt_opts.closeBoxMargin;
  }
  if (typeof opt_opts.closeBoxURL !== "undefined") {

    this.closeBoxURL_ = opt_opts.closeBoxURL;
  }
  if (typeof opt_opts.infoBoxClearance !== "undefined") {

    this.infoBoxClearance_ = opt_opts.infoBoxClearance;
  }
  if (typeof opt_opts.isHidden !== "undefined") {

    this.isHidden_ = opt_opts.isHidden;
  }
  if (typeof opt_opts.visible !== "undefined") {

    this.isHidden_ = !opt_opts.visible;
  }
  if (typeof opt_opts.enableEventPropagation !== "undefined") {

    this.enableEventPropagation_ = opt_opts.enableEventPropagation;
  }

  if (this.div_) {

    this.draw();
  }
};

/**
 * Sets the content of the InfoBox.
 *  The content can be plain text or an HTML DOM node.
 * @param {string|Node} content
 */
InfoBox.prototype.setContent = function (content) {
  this.content_ = content;

  if (this.div_) {

    if (this.closeListener_) {

      google.maps.event.removeListener(this.closeListener_);
      this.closeListener_ = null;
    }

    // Odd code required to make things work with MSIE.
    //
    if (!this.fixedWidthSet_) {

      this.div_.style.width = "";
    }

    if (typeof content.nodeType === "undefined") {
      this.div_.innerHTML = this.getCloseBoxImg_() + content;
    } else {
      this.div_.innerHTML = this.getCloseBoxImg_();
      this.div_.appendChild(content);
    }

    // Perverse code required to make things work with MSIE.
    // (Ensures the close box does, in fact, float to the right.)
    //
    if (!this.fixedWidthSet_) {
      this.div_.style.width = this.div_.offsetWidth + "px";
      if (typeof content.nodeType === "undefined") {
        this.div_.innerHTML = this.getCloseBoxImg_() + content;
      } else {
        this.div_.innerHTML = this.getCloseBoxImg_();
        this.div_.appendChild(content);
      }
    }

    this.addClickHandler_();
  }

  /**
   * This event is fired when the content of the InfoBox changes.
   * @name InfoBox#content_changed
   * @event
   */
  google.maps.event.trigger(this, "content_changed");
};

/**
 * Sets the geographic location of the InfoBox.
 * @param {LatLng} latlng
 */
InfoBox.prototype.setPosition = function (latlng) {

  this.position_ = latlng;

  if (this.div_) {

    this.draw();
  }

  /**
   * This event is fired when the position of the InfoBox changes.
   * @name InfoBox#position_changed
   * @event
   */
  google.maps.event.trigger(this, "position_changed");
};

/**
 * Sets the zIndex style for the InfoBox.
 * @param {number} index
 */
InfoBox.prototype.setZIndex = function (index) {

  this.zIndex_ = index;

  if (this.div_) {

    this.div_.style.zIndex = index;
  }

  /**
   * This event is fired when the zIndex of the InfoBox changes.
   * @name InfoBox#zindex_changed
   * @event
   */
  google.maps.event.trigger(this, "zindex_changed");
};

/**
 * Sets the visibility of the InfoBox.
 * @param {boolean} isVisible
 */
InfoBox.prototype.setVisible = function (isVisible) {

  this.isHidden_ = !isVisible;
  if (this.div_) {
    this.div_.style.visibility = (this.isHidden_ ? "hidden" : "visible");
  }
};

/**
 * Returns the content of the InfoBox.
 * @returns {string}
 */
InfoBox.prototype.getContent = function () {

  return this.content_;
};

/**
 * Returns the geographic location of the InfoBox.
 * @returns {LatLng}
 */
InfoBox.prototype.getPosition = function () {

  return this.position_;
};

/**
 * Returns the zIndex for the InfoBox.
 * @returns {number}
 */
InfoBox.prototype.getZIndex = function () {

  return this.zIndex_;
};

/**
 * Returns a flag indicating whether the InfoBox is visible.
 * @returns {boolean}
 */
InfoBox.prototype.getVisible = function () {

  var isVisible;

  if ((typeof this.getMap() === "undefined") || (this.getMap() === null)) {
    isVisible = false;
  } else {
    isVisible = !this.isHidden_;
  }
  return isVisible;
};

/**
 * Shows the InfoBox. [Deprecated; use <tt>setVisible</tt> instead.]
 */
InfoBox.prototype.show = function () {

  this.isHidden_ = false;
  if (this.div_) {
    this.div_.style.visibility = "visible";
  }
};

/**
 * Hides the InfoBox. [Deprecated; use <tt>setVisible</tt> instead.]
 */
InfoBox.prototype.hide = function () {

  this.isHidden_ = true;
  if (this.div_) {
    this.div_.style.visibility = "hidden";
  }
};

/**
 * Adds the InfoBox to the specified map or Street View panorama. If <tt>anchor</tt>
 *  (usually a <tt>google.maps.Marker</tt>) is specified, the position
 *  of the InfoBox is set to the position of the <tt>anchor</tt>. If the
 *  anchor is dragged to a new location, the InfoBox moves as well.
 * @param {Map|StreetViewPanorama} map
 * @param {MVCObject} [anchor]
 */
InfoBox.prototype.open = function (map, anchor) {

  var me = this;

  if (anchor) {

    this.position_ = anchor.getPosition();
    this.moveListener_ = google.maps.event.addListener(anchor, "position_changed", function () {
      me.setPosition(this.getPosition());
    });
  }

  this.setMap(map);

  if (this.div_) {

    this.panBox_();
  }
};

/**
 * Removes the InfoBox from the map.
 */
InfoBox.prototype.close = function () {

  var i;

  if (this.closeListener_) {

    google.maps.event.removeListener(this.closeListener_);
    this.closeListener_ = null;
  }

  if (this.eventListeners_) {
    
    for (i = 0; i < this.eventListeners_.length; i++) {

      google.maps.event.removeListener(this.eventListeners_[i]);
    }
    this.eventListeners_ = null;
  }

  if (this.moveListener_) {

    google.maps.event.removeListener(this.moveListener_);
    this.moveListener_ = null;
  }

  if (this.contextListener_) {

    google.maps.event.removeListener(this.contextListener_);
    this.contextListener_ = null;
  }

  this.setMap(null);
};

/* End */
;; /* /bitrix/templates/aspro_optimus/js/jquery.actual.min.js?14990989751251*/
; /* /bitrix/templates/aspro_optimus/js/jqModal.min.js?14990989753355*/
; /* /bitrix/templates/aspro_optimus/js/jquery.fancybox.min.js?149909897521528*/
; /* /bitrix/templates/aspro_optimus/js/jquery.history.min.js?149909897521571*/
; /* /bitrix/templates/aspro_optimus/js/jquery.flexslider.min.js?149909897522345*/
; /* /bitrix/templates/aspro_optimus/js/jquery.validate.min.js?149909897522257*/
; /* /bitrix/templates/aspro_optimus/js/jquery.inputmask.bundle.min.js?149909897563845*/
; /* /bitrix/templates/aspro_optimus/js/jquery.easing.1.3.min.js?14990989753338*/
; /* /bitrix/templates/aspro_optimus/js/equalize.min.js?1499098975588*/
; /* /bitrix/templates/aspro_optimus/js/jquery.alphanumeric.min.js?1499098975942*/
; /* /bitrix/templates/aspro_optimus/js/jquery.cookie.min.js?14990989753066*/
; /* /bitrix/templates/aspro_optimus/js/jquery.plugin.min.js?14990989753181*/
; /* /bitrix/templates/aspro_optimus/js/jquery.countdown.min.js?149909897513137*/
; /* /bitrix/templates/aspro_optimus/js/jquery.countdown-ru.min.js?14990989751011*/
; /* /bitrix/templates/aspro_optimus/js/jquery.ikSelect.min.js?149909897517826*/
; /* /bitrix/templates/aspro_optimus/js/sly.min.js?149909897517577*/
; /* /bitrix/templates/aspro_optimus/js/equalize_ext.min.js?14990989751531*/
; /* /bitrix/templates/aspro_optimus/js/jquery.dotdotdot.min.js?14990989755908*/
; /* /bitrix/templates/aspro_optimus/js/main.min.js?149909897585164*/
; /* /bitrix/components/bitrix/search.title/script.min.js?14990878986110*/
; /* /bitrix/templates/aspro_optimus/js/custom.js?1499098975100*/
; /* /bitrix/templates/aspro_optimus/components/bitrix/map.google.view/map/markerclustererplus.js?149909897451430*/
; /* /bitrix/templates/aspro_optimus/components/bitrix/map.google.view/map/infobox.js?149909897423431*/

//# sourceMappingURL=template_d490255e0f28716f3f5a5f486747b1fd.map.js